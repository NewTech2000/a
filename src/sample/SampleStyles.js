import {
    PixelRatio,
    StyleSheet,
    Dimensions
  } from 'react-native';
  import Colors from '../../resources/Colors';
  var { height, width } = Dimensions.get('window');
  import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      // backgroundColor: '#0a1142',
    },

    togleButtonView:{
        width: wp('90%'),
        height:hp('10%'),
        backgroundColor:'white',
        borderRadius:20,
        elevation:8
    },
 
  title: {
      width: width,
      // backgroundColor: "#eee",
      color: "#232323",
      paddingLeft: 8,
      paddingVertical: 4,
      textAlign: "left"
  },
  wtf: {
      flex: 1,
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
      backgroundColor:'#126F85',
   
     
      
      


  },
  name: {
      flex: 1,
      textAlign: "left",
      fontSize:14,
      color:'#126F85',
      top:8
     
  },
  address: {
    bottom:8,
      flex: 1,
      textAlign: "right",
      fontSize:12,
      color:'#126F85',
     
  },
  ToucherbleIconStyle2: { height: 30, width: 30 },
  headerStyle: {
      backgroundColor: Colors.PRIMARY_COLOR,
      shadowOpacity: 0.75,
      shadowRadius: 5,
      shadowOffset: { height: 0, width: 0 },

  },
  header: {
      justifyContent: 'center',
      alignItems: 'center'
  },
  headerTextStyle: {
      justifyContent: 'center',
      alignItems: 'center',
      fontSize: 25,
      left: 50,
      // fontWeight:'bold',
      color: 'white',
  },
    content: {
      flex: 1,
      justifyContent: 'flex-end',
      padding: PixelRatio.getPixelSizeForLayoutSize(12),
    },
    logoBack: {
      justifyContent: 'center',
      alignItems: 'center',
      height: 150,
      width: 150,
      top: -30,
      borderRadius: 100,
      backgroundColor: '#ffffff90'
    },
  
    logoContainer: {
  
      justifyContent: 'center',
      alignItems: 'center',
      top: 50
    },
    logoText: {
      color: Colors.DARK_BLACK_TEXT_COLOR,
      fontSize: 24,
      fontWeight: '700',
      marginTop: 10
    },
    logoText1: {
      color: Colors.DARK_BLACK_TEXT_COLOR,
      fontSize: 15,
      fontWeight: '700',
    },
    backgroundImage: { width: "100%", height: "100%" },
  
    logo: {
      left: -3,
      height: hp('12%'),
      width: wp('90%'),
      resizeMode: 'contain',
    },
    form: {
      flex: 1,
      justifyContent: 'flex-end',
    },
    item: {
      marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
      backgroundColor: Colors.PRIMARY_COLOR,
      borderColor: Colors.PRIMARY_COLOR,
    },
    input: {
      color: 'white',
      left: 20
    },
    place: {
      width: 25,
      height: 25,
  
    },
    buttonContainer: {
      flex: 1,
      marginTop: PixelRatio.getPixelSizeForLayoutSize(10),
    },
    forgotPasswordContainer: {
      alignItems: 'center',
      marginTop: PixelRatio.getPixelSizeForLayoutSize(2),
    },
    forgotPasswordText: {
      color: 'white',
      fontSize: 16,
    },
    button: {
      marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
      backgroundColor: Colors.PRIMARY_COLOR,
    },
    loginText: {
      color: '#ffff',
      fontSize: 20,
      fontWeight: 'bold',
    },
    signupContainer: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      marginTop: PixelRatio.getPixelSizeForLayoutSize(3),
    },
    dontHaveAccountText: {
      color: '#bec0ce',
      fontSize: 16,
    },
    signupText: {
      color: '#000',
      fontSize: 16,
      textDecorationLine: 'underline'
    },
  });
  
  
  export default styles;