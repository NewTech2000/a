
import React, { Component } from 'react';
import {
    ActivityIndicator,
    Platform,
    StyleSheet,
    Text,
    Image,
    ImageBackground,
    View,
    ScrollView,
    DeviceEventEmitter,
    NativeEventEmitter,
    Switch,
    TouchableOpacity,
    StatusBar,
    Dimensions,
    AsyncStorage,
    PixelRatio,
    ToastAndroid
} from 'react-native';
import { BluetoothEscposPrinter, BluetoothManager, BluetoothTscPrinter } from "react-native-bluetooth-escpos-printer";
import {
    Header,
    Left,
    Right,
    Label,
    Card,
    Button
} from 'native-base';
import styles from './SampleStyles'
import Colors from '../../resources/Colors'

export default class BluetoothSettings extends Component {


    _listeners = [];

    constructor() {
        super();
        this.state = {
            switchValue: false,
            devices: null,
            pairedDs: [],
            foundDs: [],
            bleOpend: false,
            loading: true,
            boundAddress: '',
            debugMsg: '',
            name: ''
        }
    }



    //getting QuickSettings status code from AsyncStorage
    showData = async () => {
        let myArray = await AsyncStorage.getItem('quickSettings');
        let d = JSON.parse(myArray);

        this.setState({
            switchValue: d
        })

        console.log(JSON.stringify(d));

    }




    //store quickSettings status code in AsyncStorage 
    _storeData = async (value) => {
        // Alert.alert(value+"")

        this.setState({
            switchValue: value
        })

        try {
            AsyncStorage.setItem('quickSettings', JSON.stringify(value));

        } catch (error) {
            console.error(error);
        }
    }


    componentDidMount() {
        this.showData()
        //alert(BluetoothManager)
        BluetoothManager.isBluetoothEnabled().then((enabled) => {
            this.setState({
                bleOpend: Boolean(enabled),
                loading: false
            })
        }, (err) => {
            err
        });

        if (Platform.OS === 'ios') {
            let bluetoothManagerEmitter = new NativeEventEmitter(BluetoothManager);
            this._listeners.push(bluetoothManagerEmitter.addListener(BluetoothManager.EVENT_DEVICE_ALREADY_PAIRED,
                (rsp) => {
                    this._deviceAlreadPaired(rsp)
                }));
            this._listeners.push(bluetoothManagerEmitter.addListener(BluetoothManager.EVENT_DEVICE_FOUND, (rsp) => {
                this._deviceFoundEvent(rsp)
            }));
            this._listeners.push(bluetoothManagerEmitter.addListener(BluetoothManager.EVENT_CONNECTION_LOST, () => {
                this.setState({
                    name: '',
                    boundAddress: ''
                });
            }));
        } else if (Platform.OS === 'android') {
            this._listeners.push(DeviceEventEmitter.addListener(
                BluetoothManager.EVENT_DEVICE_ALREADY_PAIRED, (rsp) => {
                    this._deviceAlreadPaired(rsp)
                }));
            this._listeners.push(DeviceEventEmitter.addListener(
                BluetoothManager.EVENT_DEVICE_FOUND, (rsp) => {
                    this._deviceFoundEvent(rsp)
                }));
            this._listeners.push(DeviceEventEmitter.addListener(
                BluetoothManager.EVENT_CONNECTION_LOST, () => {
                    this.setState({
                        name: '',
                        boundAddress: ''
                    });
                }
            ));
            this._listeners.push(DeviceEventEmitter.addListener(
                BluetoothManager.EVENT_BLUETOOTH_NOT_SUPPORT, () => {
                    ToastAndroid.show("Device Not Support Bluetooth !", ToastAndroid.LONG);
                }
            ))
        }
    }

    componentWillUnmount() {
        //for (let ls in this._listeners) {
        //    this._listeners[ls].remove();
        //}
    }

    _deviceAlreadPaired(rsp) {
        var ds = null;
        if (typeof (rsp.devices) == 'object') {
            ds = rsp.devices;
        } else {
            try {
                ds = JSON.parse(rsp.devices);
            } catch (e) {
            }
        }
        if (ds && ds.length) {
            let pared = this.state.pairedDs;
            pared = pared.concat(ds || []);
            this.setState({
                pairedDs: pared
            });
        }
    }

    _deviceFoundEvent(rsp) {//alert(JSON.stringify(rsp))
        var r = null;
        try {
            if (typeof (rsp.device) == "object") {
                r = rsp.device;
            } else {
                r = JSON.parse(rsp.device);
            }
        } catch (e) {//alert(e.message);
            //ignore
        }
        //alert('f')
        if (r) {
            let found = this.state.foundDs || [];
            if (found.findIndex) {
                let duplicated = found.findIndex(function (x) {
                    return x.address == r.address
                });
                //CHECK DEPLICATED HERE...
                if (duplicated == -1) {
                    found.push(r);
                    this.setState({
                        foundDs: found
                    });
                }
            }
        }
    }

    _renderRow(rows) {
        let items = [];
        for (let i in rows) {
            let row = rows[i];
            if (row.address) {
                items.push(
                    <TouchableOpacity key={new Date().getTime() + i} stlye={styles.wtf} onPress={() => {
                        this.setState({
                            loading: true
                        });
                        BluetoothManager.connect(row.address)
                            .then((s) => {
                                this.setState({
                                    loading: false,
                                    boundAddress: row.address,
                                    name: row.name || "UNKNOWN"
                                })
                            }, (e) => {
                                this.setState({
                                    loading: false
                                })
                                alert(e);
                            })

                    }}><View style={{borderColor:'#126F85',marginTop:10,flex:1,padding:5,height:50,borderWidth:2,borderRadius:15,}}>
                   <Text style={styles.name}>{row.name || "UNKNOWN"}</Text><Text
                        style={styles.address}>{row.address}</Text>
                    </View>
                        </TouchableOpacity>
                );
            }
        }
        return items;
    }

    render() {
        return (
            <ImageBackground
            source={require("../../assets/newBack2.png")}
            style={{ width: "100%", height: "100%" }}>
           
                <View style={{ backgroundColor: '#126F85', width: '100%', height: 60, borderBottomLeftRadius: 50 }}>
                    <View style={{ backgroundColor: '#126F85', width: 50, top: 13, borderRadius: 60, left: 25, position: "absolute", top: 8 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('PaymentScreen')}>
                            <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                    <Image source={require("../../assets/newBack3.png")} style={{ width: 20, height: 20 }}></Image>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Text style={{ fontFamily: 'Ubuntu-Medium', color: '#ffff', justifyContent: "center", textAlign: "center", fontSize: 20, position: "absolute", top: 10, left: 100, fontWeight: "bold" }}>Printer Settings</Text>
                </View>

                <ScrollView style={styles.container}>
                <StatusBar barStyle="light-content" hidden={false} backgroundColor={'#126F85'} />
                {/* <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 20 }}>
                    <View style={styles.togleButtonView}>

                        <Text style={{ fontSize: 20, color: Colors.DARK_BLACK_TEXT_COLOR, fontWeight: 'bold', left: 10, top: 20 }}>Quick Billing</Text>
                        <Switch
                            style={{ left: -10 }}
                            value={this.state.switchValue}
                            onValueChange={(switchValue) => this._storeData(switchValue)} />
                    </View>
                </View> */}
                <Label></Label>
                <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 20 }}>
                    <View style={styles.togleButtonView}>

                        <Text style={{ fontSize: 20, color: Colors.DARK_BLACK_TEXT_COLOR, fontWeight: 'bold', left: 10, top: 20 }}>Bluetooth</Text>
                        <Switch value={this.state.bleOpend} onValueChange={(v) => {
                            this.setState({
                                loading: true
                            })
                            if (!v) {
                                BluetoothManager.disableBluetooth().then(() => {
                                    this.setState({
                                        bleOpend: false,
                                        loading: false,
                                        foundDs: [],
                                        pairedDs: []
                                    });
                                }, (err) => { alert(err) });

                            } else {
                                BluetoothManager.enableBluetooth().then((r) => {
                                    var paired = [];
                                    if (r && r.length > 0) {
                                        for (var i = 0; i < r.length; i++) {
                                            try {
                                                paired.push(JSON.parse(r[i]));
                                            } catch (e) {
                                                //ignore
                                            }
                                        }
                                    }
                                    this.setState({
                                        bleOpend: true,
                                        loading: false,
                                        pairedDs: paired
                                    })
                                }, (err) => {
                                    this.setState({
                                        loading: false
                                    })
                                    alert(err)
                                });
                            }
                        }} />
                    </View>
                </View>
                <View style={{
                    flex: 1,
                    justifyContent: 'flex-end',
                    padding: PixelRatio.getPixelSizeForLayoutSize(12),
                }}>
                    <View>
                        <Button
                            style={styles.button}
                            onPress={() => this._scan()}
                            hasText
                            block
                            large
                            dark
                            disabled={this.state.loading || !this.state.bleOpend}
                            rounded
                        >
                            <Text style={styles.loginText}>SCAN</Text>
                        </Button>

                       
                    </View>
                   

                    <Text style={styles.title}>Connected:<Text style={{ color: "blue" }}>{!this.state.name ? 'No Devices' : this.state.name}</Text></Text>
                    <Text style={styles.title}>Found(tap to connect):</Text>
                    {this.state.loading ? (<ActivityIndicator animating={true} />) : null}
                    <View style={{ flex: 1, flexDirection: "column" }}>
                        {
                            this._renderRow(this.state.foundDs)
                        }
                    </View>
                    <Text style={styles.title}>Paired:</Text>
                    </View>
                    {this.state.loading ? (<ActivityIndicator animating={true} />) : null}
                    <View style={{ flex: 1, flexDirection: "column",justifyContent:"center",marginLeft:20,marginRight:20 }}>
                        {
                            this._renderRow(this.state.pairedDs)
                        }
                    </View>

                    {/* <View style={{ flexDirection: "row", justifyContent: "space-around", paddingVertical: 30,elevation:5 }}> */}
                        {/* <Button disabled={this.state.loading || !(this.state.bleOpend && this.state.boundAddress.length > 0)}
                            title="Connected" onPress={() => {
                                alert(this.state.name + "" + this.state.boundAddress) */}
                            {/* // }} /> */}

                    {/* </View> */}
                   

            </ScrollView>
            </ImageBackground>

        );
    }

    _selfTest() {
        this.setState({
            loading: true
        }, () => {
            BluetoothEscposPrinter.selfTest(() => {
            });

            this.setState({
                loading: false
            })
        })
    }

    _scan() {
        this.setState({
            loading: true
        })
        BluetoothManager.scanDevices()
            .then((s) => {
                var ss = s;
                var found = ss.found;
                try {
                    found = JSON.parse(found);//@FIX_it: the parse action too weired..
                } catch (e) {
                    //ignore
                }
                var fds = this.state.foundDs;
                if (found && found.length) {
                    fds = found;
                }
                this.setState({
                    foundDs: fds,
                    loading: false
                });
            }, (er) => {
                this.setState({
                    loading: false
                })
                alert('error' + JSON.stringify(er));
            });
    }


}
