import React, { Component } from 'react';
import { View, TextInput, Image, TouchableOpacity, ImageBackground, StatusBar } from 'react-native';
import {
    Button,
    Container,
    Content,
    Form,
    Item,
    Input,
    Text,
} from 'native-base';
import styles from './FlashScreenStyle';
import { ScrollView } from "react-native-gesture-handler";
export default class Login extends Component {

    state = {
        email: 'shehan@gmail.com',
        password: '123456',
        name: 'shehan'
    };


    componentDidMount() {
        console.disableYellowBox = true
    }


    render() {
        return (
            <ScrollView >
                <Container >
                    <StatusBar barStyle="dark-content" hidden={false} backgroundColor={"#fff"} />
                    <Content contentContainerStyle={styles.content}>
                        <View style={styles.logoContainer}>
                            <View style={styles.logoBack}>
                                <Image
                                    style={styles.logo}
                                    source={require('../../assets/darklogo.png')}
                                />
                            </View>
                            <Text style={styles.logoText}>Welcome!</Text>
                            <Text style={styles.logoText1}>It's Manage your Business</Text>
                        </View>
                        <View style={styles.buttonContainer}>

                            <Button
                                style={styles.button}
                                onPress={() => this.props.navigation.navigate('LoginScreen')}
                                hasText
                                block
                                large
                                dark
                                rounded
                            >
                                <Text style={styles.loginText}>Start!</Text>
                            </Button>

                            <View style={styles.forgotPasswordContainer}>

                                <Text style={styles.signupText}>Powered by Commercial Technologies Plus(PVt).ltd</Text>

                            </View>
                            <View style={styles.signupContainer}>

                            </View>
                        </View>
                    </Content>
                </Container>
            </ScrollView>
        );
    }
}



