import {
    PixelRatio,
    StyleSheet,
} from 'react-native';
import Colors from '../../resources/Colors';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#0a1142',
    },
    content: {
        flex: 1,
        justifyContent: 'flex-end',
        padding: PixelRatio.getPixelSizeForLayoutSize(12),
    },
    logoBack: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 150,
        width: 150,
        top: -30,
        borderRadius: 100,
        backgroundColor: '#ffffff90'
    },

    logoContainer: {

        justifyContent: 'center',
        alignItems: 'center',
        marginTop: PixelRatio.getPixelSizeForLayoutSize(40),
        marginBottom: PixelRatio.getPixelSizeForLayoutSize(40),
    },
    logoText: {
        color: Colors.DARK_BLACK_TEXT_COLOR,
        fontSize: 24,
        fontWeight: '700',
        marginTop: 10
    },
    logoText1: {
        color: Colors.DARK_BLACK_TEXT_COLOR,
        fontSize: 15,
        fontWeight: '700',

    },
    backgroundImage: { width: "100%", height: "100%" },

    logo: {
        left: -3,
        height: PixelRatio.getPixelSizeForLayoutSize(100),
        width: PixelRatio.getPixelSizeForLayoutSize(100),
        resizeMode: 'contain',
    },
    place: {
        width: 50,
        height: 50,
        left: -10
    },
    buttonContainer: {
        flex: 1,
        marginTop: PixelRatio.getPixelSizeForLayoutSize(10),
    },
    forgotPasswordContainer: {
        alignItems: 'center',
        bottom: -150
    },
    forgotPasswordText: {
        color: 'white',
        fontSize: 16,
    },
    button: {
        marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
        backgroundColor: Colors.PRIMARY_COLOR,
       
    },
    loginText: {
        color: '#fff',
        fontSize: 20,
        fontWeight: 'bold',
    },
    signupContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: PixelRatio.getPixelSizeForLayoutSize(3),
    },
    dontHaveAccountText: {
        color: '#bec0ce',
        fontSize: 16,
    },
    signupText: {
        color: '#000',
        fontSize: 10,
    },
});


export default styles;