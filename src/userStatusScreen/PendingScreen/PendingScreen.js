import React, { Component } from 'react';
import { StatusBar, Alert, AsyncStorage, PixelRatio, View, TouchableOpacity, Linking, Image } from 'react-native';
import {
    Text,
    Button,
    Label,
    Content,
} from 'native-base';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Spinner from "react-native-spinkit";
import Colors from '../../../resources/Colors';
import Strings from '../../../resources/Strings';
import Messages from '../../../resources/Message';
import Spinner2 from 'react-native-loading-spinner-overlay';
import { NavigationActions, StackActions } from 'react-navigation'
import NetInfo from "@react-native-community/netinfo";






export default class PendingScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            getNoDomainEmail: "",
            spinner: true,
            userName: "",
            password: "",
            name: "",
            spinner2: false,
            companyDetails: [],
            // yourEmail:"",
        };
    }




    getNoDomainEmail = email => {
        //     var yourEmail = "jonth.nt49@4codev.com"
        // console.log(getNoDomainEmail(yourEmail));
        // // result : jonth.nt49

        //     let noDomainEmail = null;
        //       const pos = email.search('@'); // get position of domain
        //       if (pos > 0) {
        //         const emailDomain = email.slice(pos); // get domain name
        //         noDomainEmail = email.slice(0, -(emailDomain.length)); // use the slice method to remove domain name
        //       }
        //       return noDomainEmail;
    };


    componentWillMount() {
        this.showData()

    }

    //getting auth code from AsyncStorage
    showData = async () => {
        let myArray = await AsyncStorage.getItem('loginDetails');
        let d = JSON.parse(myArray);

        this.setState({
            userName: d.userName,
            password: d.password,

        })

        var email = this.state.userName;
        var name = email.substring(0, email.lastIndexOf("@"));
        this.setState({
            name: name
        })
        // var domain = email.substring(email.lastIndexOf("@") +1);

        console.log(name);
    }


    //store auth code in AsyncStorage & navigate to home screen
    _storeData = async () => {
        this.setState({
            spinner2: false,
        });

        try {
            await AsyncStorage.setItem('companyDetails', JSON.stringify(this.state.companyDetails));
            await AsyncStorage.removeItem('loginDetails');
            await AsyncStorage.setItem('myArray', JSON.stringify(this.state.token), () => {
                Messages.messageName(Strings.WARNING_SUCESS, Strings.LOGIN_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'HomeScreen' })],
                });
                this.props.navigation.dispatch(resetAction);
            });

        } catch (error) {
            console.error(error);
        }
    }

    //check availability user 
    gettingAuth() {
        this.setState({
            spinner2: true,
        });
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + '/api/auth?username=' + this.state.userName + '&password=' + this.state.password,
                    {
                        method: "POST",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",

                        },

                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        console.log("Login" + JSON.stringify(responseJson))
                        this.setState({
                            spinner2: false,
                        });
                        if (responseJson.errorCode == "USER_NOT_FOUND") {
                            Messages.messageName(Strings.WARNING, Strings.USER_NOT_FOUNd, Strings.ICON[0], Strings.TYPE[0], Colors.PRIMARY_COLOR);

                        } else if (responseJson.errorCode == "INVALID_CREDENTIALS") {
                            Messages.messageName(Strings.WARNING, Strings.USER_DID_NOT_MATCH, Strings.ICON[0], Strings.TYPE[0], Colors.PRIMARY_COLOR);
                        } else if (responseJson.errorCode == "COMPANY_IS_NOT_ACTIVATED") {
                            Messages.messageName(Strings.WARNING, Strings.USER_NOT_ACTIVE, Strings.ICON[0], Strings.TYPE[0], Colors.PRIMARY_COLOR);
                        }
                        this.state.companyDetails = responseJson.user
                        this.state.token = responseJson.accessToken
                        if (this.state.token == null) {
                            // Messages.messageName(Strings.WARNING_SUCESS, Strings.USER_NOT_FOUNd, Strings.ICON[2], Strings.TYPE[2]);
                        } else {
                            this._storeData()
                        }

                    })


                    .catch(error => {
                        console.log(error);
                        Messages.messageName(Strings.WARNING_CONNECTION, Strings.INTERNAL_ERROR, Strings.ICON[2], Strings.TYPE[2], Colors.DARK_BLACK_TEXT_COLOR);
                        this.setState({
                            spinner2: true,
                        });

                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner2: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your network connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.gettingAuth()

                            }
                        },
                    ],
                    { cancelable: true }
                );



            }
        });

    }




    render() {
        return (

            <View style={{ justifyContent: 'center', alignItems: 'center', }}>
                <View style={{ justifyContent: 'center', alignItems: 'center', }}>
                    <Text style={{ fontSize: 30, textAlign: "center", top: 30, color: Colors.FLAT_TEXT_COLOR, fontWeight: "bold" }}>Pending</Text>
                </View>
                <View style={{ width: 200, height: 200, borderWidth: 10, borderRadius: 100, borderColor: '#126F85', top: 80 }}>
                    <View style={{ justifyContent: "center", alignContent: "center", alignItems: "center", top: 30 }}>
                        <Spinner
                            isVisible={this.state.spinner}
                            type={'Bounce'}
                            textStyle={{ color: 'white' }}
                            size={120}
                            color={Colors.FLAT_TEXT_COLOR}

                        />
                    </View>
                    <Spinner2
                        visible={this.state.spinner2}
                        textContent={'Loading...'}
                        textStyle={{ color: 'white' }}
                        overlayColor={"#0f0e0e90"}
                        animation={"fade"}
                        color={"#00CEFD"}
                    />


                </View>
                <StatusBar barStyle="dark-content" hidden={false} backgroundColor={Colors.BACKGROUD_WHITE} />

                <View style={{ top: 100 }}>
                    <Text style={{ fontSize: 25, fontWeight: "bold", marginLeft: wp('18%'),alignContent:"center",alignItems:"center",justifyContent:"center" }}>Hey, {this.state.name}</Text>
                    <Text style={{ color: "#707070", fontSize: 18, fontWeight: "bold", textAlign: "center", }}>Company profile activation pending </Text>
                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
                        <View style={{ width: '80%', }}>
                            <Text style={{ fontSize: 14, textAlign: "center", top: 5, color: Colors.WARNING_ALERT }}>For immediate activation contact us</Text>
                        </View>
                    </View>
                </View>
                <Label></Label>
                <View style={{ width: '80%', borderRadius: 20, top: 120 }}>
                    <TouchableOpacity onPress={() => Linking.openURL("tel:0094385202000")}>
                        <Image
                            style={{
                                width: 30,
                                height: 30,
                            }}
                            source={require('../../../assets/phone.png')}
                        />
                        <Text style={{ position: "absolute", fontSize: 12, fontWeight: "bold", top: 10, left: 40, color: '#126F85' }}>Customer Service</Text>
                    </TouchableOpacity>

                </View>
                <View style={{ width: '80%', borderRadius: 20, top: '30%' }}>
                    <TouchableOpacity>
                        <Image
                            style={{
                                width: 30,
                                height: 30,
                            }}
                            source={require('../../../assets/message.png')}
                        />
                        <Text style={{ position: "absolute", fontSize: 12, fontWeight: "bold", top: 10, left: 40, color: '#126F85' }}>BillaInfo@commercialtp.com</Text>
                    </TouchableOpacity>

                </View>
                <View style={{ width: '80%', borderRadius: 20, top: '45%' }} >
                    <Button
                        style={{
                            marginVertical: PixelRatio.getPixelSizeForLayoutSize(1),
                            backgroundColor: Colors.FLAT_TEXT_COLOR
                        }}

                        //hasText
                        block
                        large
                        dark
                        rounded
                        onPress={() => this.gettingAuth()}
                    >
                        <Text style={{
                            color: '#ffff',
                            fontSize: hp('2.4%'),
                            fontWeight: 'bold',
                        }}>Continue</Text>
                    </Button>

                </View>



            </View>
        );
    }
}