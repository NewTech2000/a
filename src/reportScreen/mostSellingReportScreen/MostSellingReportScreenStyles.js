import {
    PixelRatio,
    StyleSheet,
} from 'react-native';
import Colors from '../../../resources/Colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: '#ffff',

    },
    headerStyle: {
        backgroundColor: Colors.PRIMARY_COLOR,
        shadowOpacity: 0.75,
        shadowRadius: 5,
        shadowOffset: { height: 0, width: 0 },

    },
    ToucherbleIconStyle2: {
        height: 30,
        width: 30
    },
    gridView: {
        flex: 1,
        top:20
    },
    itemName2: {
        fontSize: 16,
    position: 'absolute',
    left: 15,
    color:'#9e9e9e',
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center"
    },
    textqty:{
        fontSize: 16,
        color:'#9e9e9e',
        justifyContent: "center",
        alignItems: "center",
        textAlign: "center"
    },
textcount:{
    fontSize: 16,
    position: 'absolute',
    color:'#9e9e9e',
    right: 15,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center"
},
    itemContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 15,
        borderColor: Colors.PRIMARY_COLOR,
        borderWidth: 2,
        padding: 10,
        height: 80,
        shadowColor: Colors.DARK_BLACK_TEXT_COLOR,
        shadowOpacity: 0.8,
        elevation: 7,
        shadowRadius: 30,
        shadowOffset: { width: 1, height: 13 }

    },
    button: {
        width: wp('80%'),
        backgroundColor: Colors.PRIMARY_COLOR,
        top:20
    },
    buttonContainer: {
         bottom:30,
        
        alignItems: 'center',
        alignContent: 'center',

    },
    loginText: {
        color: '#ffff',
        fontSize: hp('2.5%'),
        fontWeight: 'bold',
    },
    ToucherbleIconStyle3: {
        height: 50,
        width: 50,
        top: 20,
        left: 20,
    },
    dailyText: {
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 25,
        left: 100,
        fontWeight: 'bold',
        top: -20,
        color: Colors.PRIMARY_COLOR,
    },
    header: {
        justifyContent: 'center',
        alignItems: 'center',
        left: 50
    },
    headerTextStyle: {
        fontSize: 25,
        // left:70,
        // fontWeight:'bold',
        color: 'white',
    },
    gridView2: {
        flex: 1,
        // marginTop: -100,
        marginBottom:30,
      top:30
    },
    itemContainer2: {
        // borderColor:'#eeeeee',
        // borderWidth:2,
        
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomEndRadius: 15,
        borderTopLeftRadius: 15,
        borderBottomLeftRadius: 15,
        borderTopEndRadius: 12,
        // padding: 5,
        height: 50,
        // top:30,
        // margin: 5,
        elevation: 3,
        marginTop: 2,
        
    },
    
});


export default styles;



