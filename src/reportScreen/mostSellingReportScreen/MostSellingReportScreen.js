import React, { Component } from 'react';
import { Image, TouchableOpacity, ImageBackground, StatusBar,BackHandler, View, Alert, AsyncStorage } from 'react-native';
import {
    Text,
    Header,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Button
} from 'native-base';
import NetInfo from "@react-native-community/netinfo";
import styles from './MostSellingReportScreenStyles';
import Colors from '../../../resources/Colors';
import Strings from '../../../resources/Strings';
import DatePicker from 'react-native-date-ranges';
import { FlatGrid } from 'react-native-super-grid';
import Spinner from "react-native-spinkit";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
 
export default class MostSelling extends Component {
 
    constructor(props) {
        super(props);
 
        this.state = {
            startDate: "",
            endDate: "",
            spinner: false,
            token: "",
            nodata: false,
            data: [],
            dateselect: false,
 
        };
 
    }

    onButtonPress = () => {

    }


    handleBackButton = () => {
        this.props.navigation.navigate('ReportsScreen');
        return true;
    };



    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
 
 
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        if(this.state.data.length == 0){
            this.setState({
                dateselect:true
            })
         
       
            this.showData()
        }
        
       
    }
 
    showData = async () => {
        let myArray = await AsyncStorage.getItem('myArray');
        let d = JSON.parse(myArray);
 
        this.setState({
            token: d
 
        })
        console.log(this.state.token);
        this.productDaySales()
 
    }
 
    mostSellingList() {
        this.setState({
            spinner: true,
        });
 
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + '/api/report/most-selling?fromDate=' + this.state.startDate + '&toDate=' + this.state.endDate + '&page=0&limit=500',
 
                    {
                        method: "GET",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },
 
                    }
                )
 
                    .then(resp => resp.json())
                    .then(responseJson => {
                        console.log("sales" + JSON.stringify(responseJson));
                        this.setState({
                            spinner: false,
                        });
                        this.setState({
                            data: responseJson.content
                        })
 
                        if (this.state.data.length == 0) {
                            this.setState({
                                nodata: true,
                                dateselect:false
                               
                            })
                        } else {
                            this.setState({
                                nodata: false,
                                dateselect:false
                            })
                        }
 
                    })
 
                    .catch(error => {
                        console.log(error);
                        this.setState({
                            spinner: false,
                        });
 
                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })
 
                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.mostSellingList()
 
                            }
                        },
                    ],
                    { cancelable: true }
                );
            }
        });
    }
 
    //no data iamge
    nodataImage() {
        if (this.state.nodata) {
            return (
                <View style={{ top: 80, justifyContent: 'center', alignItems: 'center', }}>
                    <Image style={{ width: 200, height: 200, borderRadius: 100 }}
                        source={require("../../../assets/no_data_found.png")}
                    />
 
                </View>
            )
        }
    }
    dateSelectimage() {
        if (this.state.dateselect) {
            return (
                <View style={{ top: 80, justifyContent: 'center', alignItems: 'center', }}>
                    <Image style={{ width: 200, height: 200, borderRadius: 100 }}
                        source={require("../../../assets/no_data_found.png")}
                    />
                    <Text style={{fontWeight:"bold",fontSize:15,color:Colors.DARK_BLACK_TEXT_COLOR}}>Please select the date range.</Text>

                </View>
            )
        }
    }
    pickerRange(value) {
        console.log(JSON.stringify(value))
        this.setState({
            startDate: value.startDate,
            endDate: value.endDate
        })
        this.mostSellingList()
 
    }
 
    customButton = (onConfirm) => (
 
        <View style={styles.buttonContainer}>
            <View style={{justifyContent: 'center', alignItems: 'center',position:"absolute",bottom:60}}>
                    <View style={{justifyContent: 'center', alignItems: 'center',flexDirection:'row' }}>
                    <Image style={{ width: 60, height: 60, }}
                        source={require("../../../assets/date1.png")}
                    />
                    <Image style={{ width: 60, height: 60,  }}
                        source={require("../../../assets/arrow.png")}
                    />
                    <Image style={{width: 60, height: 60,}}
                        source={require("../../../assets/date2.png")}
                    />
                    </View>
                    <Text style={{ fontWeight: "bold", fontSize: 15, color: Colors.DARK_BLACK_TEXT_COLOR ,top:10}}>Please select the date range.</Text>

                </View>
            <Button
                style={styles.button}
                onPressIn={onConfirm}
                // onPress={() => this.test()}
                hasText
                block
                large
                dark
                rounded
            >
                <Text style={styles.loginText}>SUBMIT</Text>
            </Button>
        </View>
    )


 
    render() {
        const {
            ...rest
        } = this.props;
 
        return (
            <View style={styles.container}>
                <ImageBackground
                    source={require("../../../assets/newBack2.png")}
                    style={{ width: "100%", height: "100%" }}>
 
                    <View style={{ backgroundColor: '#126F85', width: '100%', height: 60, borderBottomLeftRadius: 50 }}>
                        <View style={{ backgroundColor: '#126F85', width: 50, top: 13, borderRadius: 60, left: 25, position: "absolute", top: 8 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('ReportsScreen')}>
                                <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                        <Image source={require("../../../assets/newBack3.png")} style={{ width: 20, height: 20 }}></Image>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <Text style={{ fontFamily: 'Ubuntu-Medium', color: '#ffff', justifyContent: "center", textAlign: "center", fontSize: 20, position: "absolute", top: 10, left: 100, fontWeight: "bold" }}>Most Sellings</Text>
                    </View>
                    <StatusBar barStyle="light-content" hidden={false} backgroundColor={'#126F85'} />
 
                   
                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 10 }}>
                        <DatePicker
                            style={{ width: 330, height: 45 }}
                            customStyles={{
                                placeholderText: { fontSize: 15 }, // placeHolder style
                                headerStyle: { backgroundColor: Colors.PRIMARY_COLOR },         // title container style
                                headerMarkTitle: {}, // title mark style 
                                headerDateTitle: {}, // title Date style
                                contentInput: {}, //content text container style
                                contentText: {}, //after selected text Style
                            }} // optional 
                            centerAlign // optional text will align center or not
                            // allowFontScaling={false} // optional
                            placeholder={'From date → To date'}
                            returnFormat={'YYYY-MM-DD'}
                            markText={"Most Sellings"}
                            onConfirm={value => this.pickerRange(value)}
                            mode={'range'}
                            ref={(ref) => this.picker = ref}
                            {...rest}
                            customButton={this.customButton}
                        />
 
                    </View>
                    <View style={{ height: 50, top: 30 }}>
                        <View style={{
                            marginLeft: 7, marginRight: 7, backgroundColor: Colors.PRIMARY_COLOR,
                            height: 50, flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderTopEndRadius: 13, borderTopLeftRadius: 13
                        }}>
                            <Text style={{ position: 'absolute', left: 15, fontWeight: 'bold', fontSize: 17, color: '#FFFFFF' }}>Name</Text>
                            <Text style={{ position: 'absolute', right: 15, fontWeight: 'bold', fontSize: 17, color: '#FFFFFF' }}>Sold Qty.</Text>
                            <Text style={{ fontWeight: 'bold', fontSize: 17, color: '#FFFFFF' }}>Inhand Qty.</Text>
                        </View>
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'center',top:30 }}>
                        <Spinner
                            isVisible={this.state.spinner}
                            type={'ThreeBounce'}
                            textStyle={{ color: 'white' }}
                            size={70}
                            color={Colors.FLAT_TEXT_COLOR}
                        />
 
                    </View>
                    {this.nodataImage()}
                    {/* <FlatGrid
                        itemDimension={350}
                        items={this.state.data}
                        style={styles.gridView}
                        renderItem={({ item, index }) => (
                            // <TouchableHighlight >
                            <View style={[styles.itemContainer, { backgroundColor: Colors.BACKGROUD_WHITE }]}>
                                <Text numberOfLines={1} style={styles.itemName}>Name :{item.name}</Text>
                                <Text numberOfLines={1} style={styles.itemName}>totalQty :{item.totalQty}</Text>
                                <Text numberOfLines={1} style={styles.itemName}>totalCount :{item.totalCount}</Text>
 
                            </View>
                            // </TouchableHighlight>
                        )}
                    /> */}
                     {this.dateSelectimage()}
                    <FlatGrid
                        itemDimension={350}
                        items={this.state.data}
                        style={styles.gridView2}
                        spacing={8}
                        renderItem={({ item, index }) => (
 
                            <TouchableOpacity>
                                <View
                                    style={[
                                        styles.itemContainer2,
                                        { backgroundColor: '#ffff' }
                                    ]}
                                >
                                    <Text style={styles.itemName2}>{item.name}</Text>
                                    <Text>       </Text>
                                    <Text style={styles.textcount}>{item.totalCount}</Text>
                                    <Text>       </Text>
                                    <Text style={styles.textqty}>{item.totalQty}</Text>
                                  
 
                                </View>
                            </TouchableOpacity>
                        )}
                    />
 
                </ImageBackground>
            </View>
        );
    }
}