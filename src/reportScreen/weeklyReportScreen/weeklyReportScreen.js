import React, { Component } from 'react'
import {
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    Image,
    AsyncStorage,
    ImageBackground,BackHandler,
    Alert,
    PixelRatio
} from 'react-native'
import { Container, Label, Footer, FooterTab, Content, Card, CardItem, Body, Header, Left, Right, Button } from 'native-base';
import { FlatGrid } from 'react-native-super-grid';
import styles from './weeklyReportScreenStyle'
import Strings from '../../../resources/Strings';
import NetInfo from "@react-native-community/netinfo";
import Modal from 'react-native-modalbox';
import Colors from '../../../resources/Colors';
import Moment from 'moment';
import Spinner from "react-native-spinkit";
import DatePicker from 'react-native-date-ranges';
import { BluetoothEscposPrinter, BluetoothManager } from "react-native-bluetooth-escpos-printer";
export default class weeklyReportScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {
            token: "",
            data: [],
            total: 0,
            total2: 0,
            date: "",
            nodata: false,
            footer: true,
            companyAddress: "",
            companyContactNumber: "",
            companyName: "",
            spinner: false,
            startDate: "",
            endDate: "",
            dateselect: false,
            selectedDate: '',

        }

    }

    async componentWillMount() {
        if (this.state.data.length == 0) {
            this.setState({
                dateselect: true
            })
            this.dateSelectimage()
        }
        let user = await AsyncStorage.getItem('companyDetails');
        let parsed = JSON.parse(user);
        console.log(JSON.stringify(parsed))
        // console.log(JSON.stringify(parsed.name))
        this.setState({
            companyName: parsed.company.name,
            companyAddress: parsed.company.address,
            companyContactNumber: parsed.company.customSettings.contact,


        })

        console.log("dddddddddddddddddddddddd" + JSON.stringify(this.state.companyAddress))
        console.log("wwwwwwwwwwwwwwwwwwww" + this.state.companyName)

    }

    onButtonPress = () => {

    }


    handleBackButton = () => {
        this.props.navigation.navigate('ReportsScreen');
        return true;
     
    };

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        BluetoothManager.connect('DC:0D:30:81:15:77')
            .then((s) => {
                this.setState({
                    bluetoothConnected: 1
                })
            },
                (err) => {
                    this.setState({
                        bluetoothConnected: 0
                    })
                })
        this.showData()
        this.setState({
            date: Moment().format('YYYY-MM-D')
        })
        console.log(this.state.date);
    }

    showData = async () => {
        let myArray = await AsyncStorage.getItem('myArray');
        let d = JSON.parse(myArray);

        this.setState({
            token: d

        })
        console.log(this.state.token);
        this.productDaySales()

    }



    weekelyReport() {
        this.setState({
            spinner: true,
        });

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + '/api/report/product/day-sales/time-base?fromDate=' + this.state.startDate + '&toDate=' + this.state.endDate,

                    {
                        method: "GET",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },

                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        console.log("sales" + JSON.stringify(responseJson));
                        this.setState({
                            spinner: false,
                        });
                        this.setState({
                            data: responseJson
                        })
                        var a = 0;
                        for (let index = 0; index < responseJson.length; index++) {
                            a = a + responseJson[index].totalPrice

                        }
                        this.setState({
                            total: a
                        })

                        if (this.state.data.length == 0) {
                            this.setState({
                                nodata: true,
                                dateselect:false
                            })
                        } else {
                            this.setState({
                                nodata: false,
                                dateselect: false
                            })
                        }

                    })

                    .catch(error => {
                        console.log(error);
                        this.setState({
                            spinner: false,
                        });

                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.productDaySales()

                            }
                        },
                    ],
                    { cancelable: true }
                );
            }
        });

    }

    printReport() {
        this.refs.modal4.open()
        this.setState({
            footer: false
        })
    }

    printReceipt = async () => {
        try {
            await BluetoothEscposPrinter.printerInit();
            await BluetoothEscposPrinter.printerLeftSpace(0);
            await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.CENTER);
            await BluetoothEscposPrinter.setBlob(0);
            await BluetoothEscposPrinter.printText("Custom Report\r\n", {
                encoding: 'GBK',
                codepage: 0,
                widthtimes: 1,
                heigthtimes: 1,
                fonttype: 1
            });
            // await BluetoothEscposPrinter.printText("--------------------------------\r\n", {});
            // await BluetoothEscposPrinter.printText(this.state.companyName + "\r\n", { encoding: 'GBK', fonttype: 11 });
            // await BluetoothEscposPrinter.setBlob(0);
            // await BluetoothEscposPrinter.printText(this.state.companyAddress + "\r\n", {
            //     encoding: 'GBK',
            //     codepage: 0,
            //     widthtimes: 0,
            //     heigthtimes: 0,
            //     fonttype: 1
            // });
            await BluetoothEscposPrinter.printText("--------------------------------\r\n", {});
            await BluetoothEscposPrinter.printText(this.state.companyName + "\r\n", { encoding: 'GBK', fonttype: 11 });
            await BluetoothEscposPrinter.setBlob(0);
            await BluetoothEscposPrinter.printText(this.state.companyAddress + "\r\n", {
                encoding: 'GBK',
                codepage: 0,
                widthtimes: 0,
                heigthtimes: 0,
                fonttype: 1
            });
            await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.LEFT);
            await BluetoothEscposPrinter.printText("Date: " + this.state.startDate + " to " + this.state.endDate + "\r\n", { encoding: 'GBK', });
            await BluetoothEscposPrinter.printText("--------------------------------\r\n", {});
            let columnWidths = [12, 8, 12];
            await BluetoothEscposPrinter.printColumn(columnWidths,
                [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.CENTER, BluetoothEscposPrinter.ALIGN.RIGHT],
                ['Name', "Qty", 'Price'], {});
            for (let index = 0; index < this.state.data.length; index++) {
                await BluetoothEscposPrinter.printText("\r\n", {});
                await BluetoothEscposPrinter.printColumn(columnWidths,
                    [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.CENTER, BluetoothEscposPrinter.ALIGN.RIGHT],
                    [this.state.data[index].name + "", this.state.data[index].totalQty + "", this.state.data[index].totalPrice + ""]
                    , {});

            }
            await BluetoothEscposPrinter.printText("\r\n", {});
            await BluetoothEscposPrinter.printText("--------------------------------\r\n", {});
            await BluetoothEscposPrinter.printText("\r\n", {});
            await BluetoothEscposPrinter.setBlob(0);
            await BluetoothEscposPrinter.printText("Amount : Rs." +this.state.total+ "\r\n", {
                encoding: 'GBK',
                codepage: 0,
                widthtimes: 3,
                heigthtimes: 3,
                fonttype: 1
            });

            await BluetoothEscposPrinter.printText("--------------------------------\r\n", {});
            // await BluetoothEscposPrinter.printText("IT Partner - Billa cloud\r0774653543\r\n", {});
            await BluetoothEscposPrinter.printText("--------------------------------\r\n", {});
            // await BluetoothEscposPrinter.printText("IT Partner - Billa cloud\r0774653543\r\n", {});
            await BluetoothEscposPrinter.setBlob(0);
            await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.CENTER);
            await BluetoothEscposPrinter.printText("IT Partner - Billa cloud\r\n", {
                encoding: 'GBK',
                fonttype: 11
            });
            await BluetoothEscposPrinter.setBlob(0);
            await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.CENTER);
            await BluetoothEscposPrinter.printText("0774653543\r\n", {
                encoding: 'GBK',
                fonttype: 11
            });
            await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.LEFT);
            await BluetoothEscposPrinter.printText("\r\n\r\n\r\n", {});
        } catch (e) {
            // console.log(JSON.stringify(e));

            if (e.code == "EUNSPECIFIED") {

                Alert.alert(
                    'Connection Error!',
                    'there is something wrong with printer connection',
                    [
                        { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                        {
                            text: 'Navigate to Settings', onPress: () => {

                                this.props.navigation.navigate('DailyReportPrinterSettings')
                                this.refs.modal4.close()

                            }
                        },
                    ],
                    { cancelable: true }
                );

            }

        }
    }

    financial(x) {
        return Number.parseFloat(x).toFixed(2);
    }

    //when open model, plus button invisible 
    onClose = () => {
        this.setState({
            footer: true
        })
    }
    closeModel() {
        this.refs.modal4.close()
        this.setState({
            footer: true
        })
    }

    //no data iamge
    nodataImage() {
        if (this.state.nodata) {
            return (
                <View style={{ top: 80, justifyContent: 'center', alignItems: 'center', }}>
                    <Image style={{ width: 200, height: 200, borderRadius: 100 }}
                        source={require("../../../assets/no_data_found.png")}
                    />

                </View>
            )
        }
    }
    dateSelectimage() {
        if (this.state.dateselect) {
            return (
                <View style={{ top: 80, justifyContent: 'center', alignItems: 'center', }}>
                    <Image style={{ width: 200, height: 200, borderRadius: 100 }}
                        source={require("../../../assets/no_data_found.png")}
                    />
                    <Text style={{ fontWeight: "bold", fontSize: 15, color: Colors.DARK_BLACK_TEXT_COLOR }}>Please select the date range.</Text>

                </View>
            )
        }
    }
    closemodal4() {
        this.refs.modal4.close()
    }


    pickerRange(value) {
        console.log(JSON.stringify(value))
        this.setState({
            startDate: value.startDate,
            endDate: value.endDate
        })
        this.weekelyReport()

    }


    customButton = (onConfirm) => (


        <View style={styles.buttonContainer}>
             <View style={{bottom:60,justifyContent: 'center', alignItems: 'center',position:"absolute", }}>
                    <View style={{justifyContent: 'center', alignItems: 'center',flexDirection:'row' }}>
                    <Image style={{ width: 60, height: 60, }}
                        source={require("../../../assets/date1.png")}
                    />
                    <Image style={{ width: 60, height: 60,  }}
                        source={require("../../../assets/arrow.png")}
                    />
                    <Image style={{width: 60, height: 60,}}
                        source={require("../../../assets/date2.png")}
                    />
                    </View>
                    <Text style={{ fontWeight: "bold", fontSize: 15, color: Colors.DARK_BLACK_TEXT_COLOR ,top:10}}>Please select the date range.</Text>

                </View>
            <Button
                style={styles.button}
                onPressIn={onConfirm}
                // onPress={() => this.test()}
                hasText
                block
                large
                dark
                rounded
            >
                <Text style={styles.loginText}>SUBMIT</Text>
            </Button>
        </View>
    )


    render() {

        const {
            ...rest
        } = this.props;


        return (
            <Container>
                <ImageBackground
                    source={require("../../../assets/newBack2.png")}
                    style={{ width: "100%", height: "100%" }}>

                    <View style={{ backgroundColor: '#126F85', width: '100%', height: 60, borderBottomLeftRadius: 50 }}>
                        <View style={{ backgroundColor: '#126F85', width: 50, top: 13, borderRadius: 60, left: 25, position: "absolute", top: 8 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('ReportsScreen')}>
                                <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                        <Image source={require("../../../assets/newBack3.png")} style={{ width: 20, height: 20 }}></Image>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <Text style={{ fontFamily: 'Ubuntu-Medium', color: '#ffff', justifyContent: "center", textAlign: "center", fontSize: 20, position: "absolute", top: 10, left: 100, fontWeight: "bold" }}>Weekely Report</Text>
                        <View style={{ backgroundColor: '#126F85', width: 50, top: 13, borderRadius: 60, right: 25, position: "absolute", top: 8 }}>
                            <TouchableOpacity onPress={() => this.printReport()}>
                                <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                        <Image source={require("../../../assets/printer.png")} style={{ width: 20, height: 20 }}></Image>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <StatusBar barStyle="light-content" hidden={false} backgroundColor={Colors.PRIMARY_COLOR} />
                    <Modal style={[styles.modal4]} position={"bottom"} ref={"modal4"} onClosed={this.onClose} backdropPressToClose={false}>
                        <TouchableOpacity style={{ top: -20, justifyContent: "center", alignContent: "center", alignItems: "center" }} onPress={() => this.closemodal4()}>
                            <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: '#ffff', borderRadius: 100, }}>
                                <Image style={{ width: 40, height: 40 }} source={require('../../../assets/closemodl.png')} />
                            </View>
                        </TouchableOpacity>
                        <View style={{
                            flex: 1,
                            justifyContent: 'flex-end',
                            padding: PixelRatio.getPixelSizeForLayoutSize(12),
                        }}>
                            <View style={{ top: 20 }}>
                                <Text style={{ color: Colors.PRIMARY_COLOR, fontSize: 20, textAlign: 'left', fontWeight: 'bold', top: -10 }}>Weekely Sales Report</Text>

                                <TouchableOpacity
                                    style={[styles.buttonContainerProcess1, styles.ProcessButton]}
                                    onPress={() => this.printReceipt()}
                                >
                                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', }}>
                                        <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 20 }}>Print Receipt</Text>
                                        <Text style={{ fontSize: 10, fontWeight: 'bold', color: this.state.bluetoothConnected == 0 ? '#fc7b03' : this.state.bluetoothConnected == 1 ? Colors.BACKGROUD_WHITE : Colors.BACKGROUD_WHITE }}>{this.state.bluetoothConnected == 0 ? 'Printer Disconnected' : this.state.bluetoothConnected == 1 ? 'Connected' : 'Connected'}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>

                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 10 }}>
                        <DatePicker
                            style={{ width: 330, height: 45 }}
                            customStyles={{
                                placeholderText: { fontSize: 15 }, // placeHolder style
                                headerStyle: { backgroundColor: Colors.PRIMARY_COLOR },			// title container style
                                headerMarkTitle: {}, // title mark style 
                                headerDateTitle: {}, // title Date style
                                contentInput: {}, //content text container style
                                contentText: {}, //after selected text Style

                            }} // optional 
                            centerAlign // optional text will align center or not
                            // allowFontScaling={false} // optional
                            placeholder={'From date → To date'}
                            returnFormat={'YYYY-MM-DD'}
                            markText={"Most Sellings"}
                            onConfirm={value => this.pickerRange(value)}
                            mode={'range'}
                            ref={(ref) => this.picker = ref}
                            {...rest}
                            
                            customButton={this.customButton}
                        />
                        
                    </View>



                    <View style={{ height: 50, top: 30 }}>
                        <View style={{
                            marginLeft: 7, marginRight: 7, backgroundColor: Colors.PRIMARY_COLOR,
                            height: 50, flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderTopEndRadius: 13, borderTopLeftRadius: 13
                        }}>
                            <Text style={{ position: 'absolute', left: 15, fontWeight: 'bold', fontSize: 17, color: '#FFFFFF' }}>Name</Text>
                            <Text style={{ position: 'absolute', right: 15, fontWeight: 'bold', fontSize: 17, color: '#FFFFFF' }}>Amount</Text>
                            <Text style={{ fontWeight: 'bold', fontSize: 17, color: '#FFFFFF' }}>Qty</Text>
                        </View>
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'center', top: 30 }}>
                        <Spinner
                            isVisible={this.state.spinner}
                            type={'ThreeBounce'}
                            textStyle={{ color: 'white' }}
                            size={70}
                            color={Colors.FLAT_TEXT_COLOR}
                        />

                    </View>
                    <View style={{ width: '100%' }}>
                        {this.nodataImage()}
                    </View>
                    {/* <Content> */}


                    {/* <View  style={styles.gridView}> */}
                    {this.dateSelectimage()}
                    <FlatGrid
                        itemDimension={350}
                        items={this.state.data}
                        style={styles.gridView2}
                        spacing={8}
                        renderItem={({ item, index }) => (

                            <TouchableOpacity>
                                <View
                                    style={[
                                        styles.itemContainer,
                                        { backgroundColor: '#ffff' }
                                    ]}
                                >
                                    <Text style={styles.itemName1}>{item.name}</Text>
                                    <Text>       </Text>
                                    <Text style={styles.itemName}>{item.totalQty}</Text>
                                    <Text>       </Text>
                                    <Text style={styles.itemName2}>Rs. {item.totalPrice}</Text>

                                </View>
                            </TouchableOpacity>
                        )}
                    />
                    {/* </View> */}
                    {/* </Content> */}
                    {this.state.footer &&
                        <Footer>
                            <FooterTab style={{ backgroundColor: Colors.PRIMARY_COLOR }}>
                                <View style={{ height: '100%', width: '100%', alignItems: 'center', flex: 1, flexDirection: 'row' }}>
                                    <Text style={{ left: 11, fontWeight: 'bold', fontSize: 22, textShadowRadius: 3, color: 'white', fontFamily: 'sans-serif-condensed' }}>Amount :</Text>
                                    <Text style={{ fontWeight: 'bold', fontSize: 22, textShadowRadius: 3, color: 'white', fontFamily: 'sans-serif-condensed', position: "absolute", right: 10 }}>Rs. {this.financial(this.state.total)}</Text>
                                </View>
                            </FooterTab>
                        </Footer >
                    }
                </ImageBackground>
            </Container >
        )
    }
}