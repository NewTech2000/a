import {
    PixelRatio,
    StyleSheet,
} from 'react-native';
import Colors from '../../../resources/Colors';

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#0a1142',
    },
    content: {
        flex: 1,
        justifyContent: 'flex-end',
        padding: PixelRatio.getPixelSizeForLayoutSize(12),
    },
    progressBarStyle: {
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
    },
    form: {
        // top:-100,
        top: 20,
        flex: 1,
        justifyContent: 'flex-end',
    },
    item: {
        marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
        backgroundColor: '#00B3DE',
        borderColor: '#00B3DE',
    },
    input: {
        color: 'black',
        //   justifyContent:'center',
        //     alignItems:'center'
        left: 3
    },
    gridView: {
        // marginTop: -100,
        flex: 1,

    },
    buttonContainer: {
        flex: 1,
        // marginTop: PixelRatio.getPixelSizeForLayoutSize(10),
    },
    place: {
        width: 28,
        height: 28,
        // left:-10
    },
    button: {
        marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
        backgroundColor: '#00B3DE',
        height: 45
    },
    loginText: {
        color: '#ffff',
        fontSize: 20,
        fontWeight: 'bold',
    },
    itemContainer: {
        // borderColor:'#eeeeee',
        // borderWidth:2,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomEndRadius: 15,
        borderTopLeftRadius: 15,
        borderBottomLeftRadius: 2,
        borderTopEndRadius: 2,
        // padding: 5,
        height: 50,
        // margin: 5,
        elevation: 8,
        marginTop: 2,
    },
    touchableOpacityStyle: {
        // backgroundColor: "transparent",
        // backgroundColor: "yellow",
        alignItems: 'center',

        justifyContent: 'center',


    },
    headerStyle: {
        backgroundColor: Colors.PRIMARY_COLOR,
        shadowOpacity: 0.75,
        shadowRadius: 5,
        shadowOffset: { height: 0, width: 0 },

    },
    ToucherbleIconStyle: { color: '#ffff' },
    ToucherbleIconStyle2: {
        height: 30,
        width: 30
    },
    TextItemStyle: {
        width: '92%',
        height: 55,
    },
    dailyText:{
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 25,
        left: 100,
        fontWeight:'bold',
        top:-20,
        color: Colors.PRIMARY_COLOR,
    },
    header: {
        justifyContent: 'center',
        alignItems: 'center',
        left:50
    },
    headerTextStyle: {
        fontSize: 25,
        // left:70,
        // fontWeight:'bold',
        color: 'white',
    },
    ImageIconStyleText: {
        padding: 10,
        margin: 5,
        height: 20,
        width: 20,
        resizeMode: "stretch"
    },

    NodataImageStyle:{
        height: 150,
        width: 350,
    },
    InputStyle: {
        fontSize: 15,
    },
    iconlImage: {
        width: 60,
        height: 60
    },
    itemName: {
        fontSize: 16,
        // fontWeight: 'bold',
        // color: Colors.Golden_Brown,
        justifyContent: "center",
        alignItems: "center",
        textAlign: "center"
    },
    itemName1: {
        fontSize: 16,
        position: 'absolute',
        left: 15,
        // fontWeight: 'bold',
        // color: Colors.Golden_Brown,
        // fontWeight: "400",
        justifyContent: "center",
        alignItems: "center",
        textAlign: "center"
    },
    itemName2: {
        fontSize: 16,
        position: 'absolute',
        right: 15,
        // fontWeight: 'bold',
        // color: Colors.Golden_Brown,
        // fontWeight: "400",
        justifyContent: "center",
        alignItems: "center",
        textAlign: "center"
    },
});


export default styles;