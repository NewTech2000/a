
// import {PieChart} from 'react-native-mp-android-chart';


export default class Report extends Component {

    constructor(props) {
        super(props);

    }
//   render() {
//     return (
//       <View style={styles.container}>
//         {/* <PieChart
//           style={styles.chart}
//           logEnabled={true}
//           backgroundColor={'#f0f0f0'}
//           description={this.state.description}
//           data={this.state.data}
//           legend={this.state.legend}

//         };

//           drawSliceText={true}
//           usePercentValues={false}
//           centerText={'Pie center text!'}
//           centerTextRadiusPercent={100}
//           holeRadius={40}
//           holeColor={'#f0f0f0'}
//           transparentCircleRadius={45}
//           transparentCircleColor={'#f0f0f0'}
//           transparentCircleAlpha={50}
//           maxAngle={350}
//         /> */}
//       </View>
//     );
//   }
// }

    render() {
        return (
            <View style={styles.container}>
                <ImageBackground
                    source={require("../../../assets/newBack2.png")}
                    style={{ width: "100%", height: "100%" }}>
                   
                     <View style={{ backgroundColor: '#126F85' ,width:'100%',height:60,borderBottomLeftRadius:50}}>
                    <View style={{ backgroundColor: '#126F85' ,width:50,top:13,borderRadius:60,left:25,position:"absolute",top:8}}>       
                        <TouchableOpacity  onPress={() => this.props.navigation.navigate('ReportsScreen')}>
                            <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                    <Image source={require("../../../assets/newBack3.png")} style={{ width: 20, height: 20 }}></Image>
                                </View>
                            </View>
                        </TouchableOpacity>
                        </View>
                        <Text style={{  fontFamily: 'Ubuntu-Medium',color: '#ffff' ,justifyContent:"center",textAlign:"center" ,fontSize:20,position:"absolute",top:10,left:100,fontWeight:"bold"}}>Charts</Text>
                        <View style={{ backgroundColor: '#126F85' ,width:50,top:13,borderRadius:60,right:25,position:"absolute",top:8}}> 
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('')}>
                            <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                    <Image source={require("../../../assets/icons8-tune-96.png")} style={{ width: 20, height: 20 }}></Image>
                                </View>
                            </View>
                        </TouchableOpacity>
                  </View>
                    </View>
                    <StatusBar barStyle="light-content" hidden={false} backgroundColor={'#126F85'} />
                    
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('linechartscreen')}>
                    <Card style={{ top: 30, marginLeft: 10, marginRight: 10, borderColor: '#126F85', borderRadius: 20, height: 100,borderLeftWidth: 3,
                            borderRightWidth: 3,
                            borderBottomWidth: 3,
                            borderTopWidth: 3, }}>
                     
                        <Image source={require("../../../assets/weekly.png")} style={styles.ToucherbleIconStyle3} />
                          
                        <Text style={styles.dailyText}>Line chart</Text>
                    </Card>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Piechart')}>
                    <Card style={{ top: 30, marginLeft: 10, marginRight: 10, borderColor: '#126F85', borderRadius: 20, height: 100,borderLeftWidth: 3,
                            borderRightWidth: 3,
                            borderBottomWidth: 3,
                            borderTopWidth: 3, }}>
                     
                        <Image source={require("../../../assets/best.png")} style={styles.ToucherbleIconStyle3} />
                          
                        <Text style={styles.dailyText}>Pie Chart</Text>
                    </Card>
                    </TouchableOpacity>
                </ImageBackground>
            </View>
        );
    }
}