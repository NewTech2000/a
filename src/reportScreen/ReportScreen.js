
import React, { Component } from 'react';
import { Image, TouchableOpacity, ImageBackground,BackHandler, StatusBar,View } from 'react-native';
import {
    Text,
    Header,
    Left,
    Right,
    Card,
    CardItem,
    Body
} from 'native-base';
import NetInfo from "@react-native-community/netinfo";
import styles from './ReportScreenStyle';
import Colors from '../../resources/Colors';


export default class Report extends Component {

    constructor(props) {
        super(props);


        this.state = {


        };

    }
    onButtonPress = () => {

    }


    handleBackButton = () => {
        this.props.navigation.goBack();
        return true;
    };



    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }


 
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }
    
    render() {
        return (
            <View style={styles.container}>
                <ImageBackground
                    source={require("../../assets/newBack2.png")}
                    style={{ width: "100%", height: "100%" }}>
                    {/* <Header
                        style={styles.headerStyle}>
                        <Left>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('HomeScreen')}>
                                <Image source={require("../../assets/back.png")} style={styles.ToucherbleIconStyle2} />
                            </TouchableOpacity>
                        </Left>
                        <View style={styles.header}>
                            <Text style={styles.headerTextStyle}>Reports</Text>
                        </View>
                        <Right>
                            <TouchableOpacity >

                            </TouchableOpacity>
                        </Right>
                    </Header> */}
                    <View style={{ backgroundColor: '#126F85', width: '100%', height: 60, borderBottomLeftRadius: 50 }}>
                        <View style={{ backgroundColor: '#126F85', width: 50, top: 13, borderRadius: 60, left: 25, position: "absolute", top: 8 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('HomeScreen')}>
                                <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                        <Image source={require("../../assets/newBack3.png")} style={{ width: 20, height: 20 }}></Image>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <Text style={{  fontFamily: 'Ubuntu-Medium',color: '#ffff' ,justifyContent:"center",textAlign:"center" ,fontSize:20,position:"absolute",top:10,left:100,fontWeight:"bold"}}>Reports</Text>
                        <View style={{ backgroundColor: '#126F85' ,width:50,top:13,borderRadius:60,right:25,position:"absolute",top:8}}> 
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Piechartscreen')}>
                            {/* <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                    <Image source={require("../../assets/icons8-tune-96.png")} style={{ width: 20, height: 20 }}></Image>
                                </View>
                            </View> */}
                        </TouchableOpacity>
                  </View>
                    </View>
                    <StatusBar barStyle="light-content" hidden={false} backgroundColor={'#126F85'} />
                    <TouchableOpacity activeOpacity={0.8} onPress={() => this.props.navigation.navigate('DailyReportScreen')}>
                        <Card style={{
                            top: 30, marginLeft: 10, marginRight: 10, borderColor: '#126F85', borderRadius: 20, height: 100, borderLeftWidth: 3,
                            borderRightWidth: 3,
                            borderBottomWidth: 3,
                            borderTopWidth: 3,
                        }}>

                            <Image source={require("../../assets/daily.png")} style={styles.ToucherbleIconStyle3} />

                            <Text style={styles.dailyText}>Daily Report</Text>
                        </Card>
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={0.8} onPress={() => this.props.navigation.navigate('WeeklyReportScreen')}>
                        <Card style={{
                            top: 30, marginLeft: 10, marginRight: 10, borderColor: '#126F85', borderRadius: 20, height: 100, borderLeftWidth: 3,
                            borderRightWidth: 3,
                            borderBottomWidth: 3,
                            borderTopWidth: 3, }}>
                     
                        <Image source={require("../../assets/weekly.png")} style={styles.ToucherbleIconStyle3} />
                          
                        <Text style={styles.dailyText}>Custom Report</Text>
                    </Card>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.8} onPress={() => this.props.navigation.navigate('MostSelling')}>
                        <Card style={{
                            top: 30, marginLeft: 10, marginRight: 10, borderColor: '#126F85', borderRadius: 20, height: 100, borderLeftWidth: 3,
                            borderRightWidth: 3,
                            borderBottomWidth: 3,
                            borderTopWidth: 3,
                        }}>

                            <Image source={require("../../assets/best.png")} style={styles.ToucherbleIconStyle3} />

                            <Text style={styles.dailyText}>Most sellings</Text>
                        </Card>
                    </TouchableOpacity>
                </ImageBackground>
            </View>
        );
    }
}