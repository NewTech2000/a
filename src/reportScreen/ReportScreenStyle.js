import {
    PixelRatio,
    StyleSheet,
} from 'react-native';
import Colors from '../../resources/Colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: '#ffff',

    },
    headerStyle: {
        backgroundColor: Colors.PRIMARY_COLOR,
        shadowOpacity: 0.75,
        shadowRadius: 5,
        shadowOffset: { height: 0, width: 0 },

    },
    ToucherbleIconStyle2: {
        height: 30,
        width: 30
    },
    ToucherbleIconStyle3: {
        height: 50,
        width: 50,
        top:20,
        left:20,
    },
    dailyText:{
        justifyContent: 'center',
        alignItems: 'center',
        fontSize:hp('3.3%'),
        left: 100,
        fontWeight:'bold',
        top:-20,
        color: Colors.PRIMARY_COLOR,
    },
    header: {
        justifyContent: 'center',
        alignItems: 'center',
        left:70
    },
    headerTextStyle: {
        fontSize: hp('3.3%'),
        color: 'white',
    },
});


export default styles;