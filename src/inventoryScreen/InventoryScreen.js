import React, { Component } from 'react';
import { View, Image, TouchableOpacity, ImageBackground, BackHandler, StatusBar, Alert, FlatList, AsyncStorage, Switch,Dimensions } from 'react-native';
import {
    Item,
    Input,
    Text,
    Header,
    Left,
    Right,
    Content,
    Picker,
} from 'native-base';
import styles from './InventoryScreenStyles';
import Colors from '../../resources/Colors';
import Strings from '../../resources/Strings';
import Modal from 'react-native-modalbox';
import NetInfo from "@react-native-community/netinfo";
import Messages from '../../resources/Message';
import { FlatGrid } from 'react-native-super-grid';
import Spinner from "react-native-spinkit";
import ProgressCircle from 'react-native-progress-circle';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

export default class InventoryScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            token: "",
            spinner: false,
            searchText: "",
            switchValue: false,
            flatlist1Visible: true,
            flatlist1header: true,
            flatlist2Visible: false,
            flatlist2header: false,
            search: true,
            categoryDrop: false,
            catname: "",
            category: [],
            nodata: false
        }

    }

    onButtonPress = () => {

    }


    handleBackButton = () => {
        this.props.navigation.goBack();
        return true;
    };



    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }



    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        this.showData()


    }

    //getting all category types
    getAllCategories() {

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + "/api/product/category?page=0&limit=200&text=" + this.state.searchText,
                    {
                        method: "GET",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },

                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        console.log("categories" + JSON.stringify(responseJson));

                        this.setState({
                            category: responseJson.content
                        })
                        this.getInventory()
                    })

                    .catch(error => {
                        console.log(error);
                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.getAllCategories()

                            }
                        },
                    ],
                    { cancelable: true }
                );

            }
        });

    }


    //getting auth code from AsyncStorage
    showData = async () => {
        let myArray = await AsyncStorage.getItem('myArray');
        let d = JSON.parse(myArray);
        this.setState({
            token: d

        })
        this.getAllCategories()
        console.log(this.state.token);

    }
    search(text) {
        this.state.searchText = text;
        this.getInventory()
    }
    //getting all of inventory
    getInventory() {
        this.setState({
            spinner: true
        })
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + '/api/inventory/filter?category=' + this.state.catname + '&name=' + this.state.searchText + '&page=0&limit=800',
                    {
                        method: "GET",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },

                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        console.log("invoice" + JSON.stringify(responseJson));

                        this.setState({
                            data: responseJson.content,
                            spinner: false,

                        })
                        if (responseJson.content.length == 0) {
                            this.setState({
                                nodata: true
                            })
                        } else {
                            this.setState({
                                nodata: false,

                            })
                        }
                    })
                    .catch(error => {
                        console.log(error);
                        this.setState({
                            spinner: false
                        })


                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })
                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.getInventory()
                            }
                        },
                    ],
                    { cancelable: true }
                );

            }
        });

    }

    qtyvisible(value) {

        if (value == false) {
            this.setState({
                flatlist1Visible: true,
                flatlist1header: true,
                flatlist2Visible: false,
                flatlist2header: false,
            })
        } else {
            this.setState({
                flatlist1Visible: false,
                flatlist1header: false,
                flatlist2Visible: true,
                flatlist2header: true,
            })
        }
        this.setState({
            switchValue: value,
        })
    }
    visibleDropDown() {
        this.setState({
            search: false,
            categoryDrop: true
        })
    }

    InvisibleDropDown() {
        this.setState({
            catname: "",
            categoryDrop: false,
            search: true
        })
        this.getInventory()
    }

    //load category for dropdown
    loadCategoryTypes() {
        return this.state.category.map(data => (
            <Picker.Item label={data.name} value={data.name} key={data.name} />
        ));
    }
    //drop down value asign
    onPickerValueChange = (itemValue, index) => {
        this.setState({
            catname: itemValue
        })
        this.getInventory()
    }

    //no data iamge
    nodataImage() {
        if (this.state.nodata) {
            return (
                <View style={{ top: 80, justifyContent: 'center', alignItems: 'center', }}>
                    <Image style={{ width: 200, height: 200, borderRadius: 100 }}
                        source={require("../../assets/no_data_found.png")}
                    />

                </View>
            )
        }
    }
    openPopupmodal() {
        this.refs.modal3.open()
    }
    render() {
        return (

            <ImageBackground
                source={require("../../assets/newBack2.png")}
                style={{ width: "100%", height: "100%" }}>

                <StatusBar barStyle="light-content" hidden={false} backgroundColor='#126F85' />
                <View style={{ backgroundColor: '#126F85', width: '100%', height: 60, borderBottomLeftRadius: 50 }}>
                    <View style={{ backgroundColor: '#126F85', width: 50, top: 13, borderRadius: 60, left: 25, position: "absolute", top: 8 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('HomeScreen')}>
                            <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                    <Image source={require("../../assets/newBack3.png")} style={{ width: 20, height: 20 }}></Image>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Text style={{ fontFamily: 'Ubuntu-Medium', color: '#ffff', justifyContent: "center", textAlign: "center", fontSize: 20, position: "absolute", top: 10, left: 100, fontWeight: "bold" }}>Inventory</Text>
                    <View style={{ position: "absolute", width: 70, bottom: 10, right: 65 }}>
                        <Text numberOfLines={1} style={{ fontSize: 12, color: '#ffff', textAlign: "center", top: 8, }}>Hide image</Text>
                        <Switch
                            //  tintColor="transparent"
                            thumbTintColor={this.state.switchValue
                                ? Colors.BTN_COLOR : "#cccccc"}
                            style={{ left: 45, bottom: 10, transform: [{ scaleX: 1.5 }, { scaleY: 1.5 }] }}
                            value={this.state.switchValue}
                            onValueChange={(switchValue) => this.qtyvisible(switchValue)}
                        />
                    </View>
                </View>
                <View style={{ justifyContent: 'center', marginTop: -10, padding: 16, }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row', }}>
                        {this.state.search &&


                            <Item
                                style={styles.item}
                                rounded
                                last >
                                <Image
                                    style={{ width: 25, height: 25, left: 10 }}
                                    source={require('../../assets/search.png')}
                                />
                                <Input
                                    style={{ color: 'white', left: 20 }}
                                    placeholder="Search Here"
                                    placeholderTextColor="#ffffff80"
                                    autoCapitalize="none"
                                    onChangeText={text => this.search(text)}
                                    value={this.state.text}
                                />
                                <TouchableOpacity onPress={() => this.visibleDropDown()}>
                                    <View style={{ backgroundColor: Colors.FLAT_TEXT_COLOR, width: 50, height: 50, borderBottomRightRadius: 20, borderTopRightRadius: 20 }}>
                                        <View style={{ justifyContent: 'center', alignItems: 'center', top: 10 }}>
                                            <Image
                                                style={styles.place4}
                                                source={require('../../assets/down3.png')}
                                            />
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </Item>
                        }

                        {this.state.categoryDrop &&
                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Picker
                                    style={{ width: 200, color: '#ffffff80', left: 10, }}
                                    selectedValue={this.state.catname}

                                    onValueChange={this.onPickerValueChange.bind(this)}
                                >
                                    <Picker.Item label="Category" value="" />
                                    {this.loadCategoryTypes()}

                                </Picker>
                                <TouchableOpacity onPress={() => this.InvisibleDropDown()}>
                                    <View style={{ backgroundColor: Colors.FLAT_TEXT_COLOR, width: 50, height: 50, borderBottomRightRadius: 20, borderTopRightRadius: 20 }}>
                                        <View style={{ justifyContent: 'center', alignItems: 'center', top: 10 }}>
                                            <Image
                                                style={styles.place4}
                                                source={require('../../assets/search.png')}
                                            />
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </Item>
                        }
                    </View>
                </View>
                {/* pop up modal open  */}
                <Modal style={[styles.longpressmodal, styles.longpressmodal3]} position={"center"} ref={"modal3"} isDisabled={this.state.isDisabled}>
                    <View style={{bottom:100}}>
                        <Text style={{ color: '#126F85', fontSize: 18, fontWeight: 'bold', right: 5,}}>Inventory of Batch Numbers</Text>
                        <Text style={{ color: '#126F85', fontSize: hp('2.0%'), top: 13, fontWeight: 'bold', right: 5 }}>{"BatchNo."}                {"Qty"}               {"Price"}</Text>
                    </View>


                    {/* <FlatList
                        itemDimension={130}
                        data={this.state.batchDetails}
                        style={{
                            flex: 1,

                            marginBottom: 15,

                            marginTop: 25,
                        }}
                        renderItem={({ item, index }) => (
                            <ScrollView>
                                <TouchableOpacity activeOpacity={0.8} onPress={() => this.batchPriceAddToCart(item)}>
                                    <View style={[styles.itemContainermodal, { backgroundColor: 'white', borderColor: '#E5E4FD', borderWidth: 0.9, }]}>
                                        <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
                                            <Text style={{ color: '#74b5c4', fontSize: hp('2.0%'), top: 2, fontWeight: 'bold', right: 10 }}>{item.batchNumber}                    {item.saleDiscountPercentage}                   {item.salePrice}</Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </ScrollView>
                        )}

                    /> */}


                </Modal>
                {/* pop up modal close */}
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Spinner
                        isVisible={this.state.spinner}
                        type={'ThreeBounce'}
                        textStyle={{ color: 'white' }}
                        size={70}
                        color={Colors.FLAT_TEXT_COLOR}
                    />
                </View>
                {this.state.flatlist1header &&


                    <View style={{ height: 30 }}>
                        <View style={{
                            marginLeft: 7, marginRight: 7, borderColor: Colors.FLAT_TEXT_COLOR, borderWidth: 2,
                            height: 30, flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRadius: 10,
                        }}>
                            <Text style={{ position: 'absolute', left: 15, fontWeight: 'bold', fontSize: 17, color: Colors.PRIMARY_COLOR }}></Text>
                            <Text style={{ position: 'absolute', right: 38, fontWeight: 'bold', fontSize: 17, color: Colors.PRIMARY_COLOR }}>Qty</Text>
                            <Text style={{ fontWeight: 'bold', fontSize: 17, color: Colors.PRIMARY_COLOR, right: 47, }}>Name</Text>
                        </View>
                    </View>


                }
                {this.state.flatlist2header &&
                    <View style={{ height: 30 }}>
                        <View style={{
                            marginLeft: 7, marginRight: 7, borderColor: Colors.FLAT_TEXT_COLOR, borderWidth: 2,
                            height: 30, flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRadius: 10,
                        }}>
                            <Text style={{ position: 'absolute', left: 15, fontWeight: 'bold', fontSize: 17, color: Colors.PRIMARY_COLOR }}>Name</Text>
                            <Text style={{ position: 'absolute', right: 15, fontWeight: 'bold', fontSize: 17, color: Colors.PRIMARY_COLOR }}>Qty</Text>
                            <Text style={{ fontWeight: 'bold', fontSize: 17, color: Colors.PRIMARY_COLOR }}>Price(Rs.)</Text>
                        </View>
                    </View>
                }

                {this.nodataImage()}
                {this.state.flatlist1Visible &&


                    <FlatList
                        itemDimension={300}
                        data={this.state.data}
                        style={styles.gridView}
                        renderItem={({ item, index }) => (
                            <TouchableOpacity activeOpacity={0.8} style={{ justifyContent: 'center', alignItems: 'center' }} onPress={() => this.openPopupmodal()}>
                                <View style={[styles.itemContainer, { backgroundColor: Colors.BACKGROUD_WHITE }]}>
                                    {/* <View style={{position:"absolute",top:20,left:50}}> */}
                                    <View style={{ justifyContent: "center", alignContent: "flex-start", alignItems: "flex-start", }}>
                                        <Image source={{ uri: item.imageUrl }} style={{ height: 60, width: 63, }}></Image>
                                    </View>

                                    {/* </View> */}
                                    {/* <View style={{ position: "absolute", bottom: 22 }}>
                                        <Text style={styles.itemName}> {item.name}</Text>
                                    </View> */}
                                    <View style={{
                                        height: hp('11%'),
                                        width: wp('26%'), backgroundColor: "#fff", position: "absolute", right: -1, borderBottomRightRadius: 20, borderTopRightRadius: 20
                                    }}>
                                        <View style={{ top: 5, justifyContent: "center", alignContent: "center", alignItems: "center" }}>
                                            <ProgressCircle
                                                style={styles.progress}
                                                percent={(item.qty / 200) * 100}
                                                radius={30}
                                                borderWidth={5}
                                                color={Colors.PRIMARY_COLOR}
                                                shadowColor="#999"
                                                bgColor="#fff"
                                            >
                                                {/* <Text style={{ fontSize: 18,color: Colors.FLAT_TEXT_COLOR ,fontWeight:"bold"}}>Qty</Text> */}
                                                <Text style={{ fontSize: hp('1.5%') }}>{item.qty}</Text>
                                            </ProgressCircle>
                                        </View>

                                        {/* <Text style={styles.itemName2}>{item.qty}</Text> */}
                                    </View>
                                    <View style={{ justifyContent: "center", alignContent: "center", alignItems: "center", position: "absolute", left: 100 }}>
                                        <Text numberOfLines={1} style={styles.itemnametxt}>{item.name}</Text>
                                        <Text numberOfLines={1} style={styles.itempricetxt}>{"Rs." + item.price}</Text>
                                        <Text numberOfLines={1} style={styles.itemcategorytxt}>{item.category}</Text>
                                        <Text numberOfLines={1} style={styles.itemqtytxt}>{item.qtyMeasurement}</Text>
                                    </View>



                                </View>
                            </TouchableOpacity>

                        )}
                    />
                }

                {this.state.flatlist2Visible &&
                    <Content>


                        {/* <View  style={styles.gridView}> */}

                        <FlatGrid
                            itemDimension={350}
                            items={this.state.data}
                            // style={styles.gridView}
                            spacing={8}
                            renderItem={({ item, index }) => (


                                <TouchableOpacity onPress={() => this.openPopupmodal()}>
                                    <View
                                        style={[
                                            styles.itemContainer2,
                                            { backgroundColor: '#ffff' }
                                        ]}
                                    >
                                        <Text numberOfLines={1} style={styles.itemName5}>{item.name}</Text>
                                        <Text numberOfLines={1} style={styles.itemName7}>{item.category}</Text>
                                        <Text numberOfLines={1} style={styles.itemName8}>{item.qtyMeasurement}</Text>
                                        {/* <Text style={{ top: -100, fontSize: hp('2%'), color: Colors.FLAT_TEXT_COLOR, fontWeight: "bold", }}>{item.category}</Text>
                                        <Text style={{ top: -100, color: Colors.FLAT_TEXT_COLOR }}>{item.qtyMeasurement}</Text> */}
                                        <Text>       </Text>
                                        <Text style={styles.itemName4}>{item.price}</Text>
                                        <Text>       </Text>
                                        {/* <Text style={styles.itemName6}>{item.qty}</Text> */}
                                        <View style={{ position: "absolute", right: 15, justifyContent: "center", alignContent: "center", alignItems: "center" }}>
                                            <ProgressCircle
                                                style={styles.progress}
                                                percent={(item.qty / 200) * 100}
                                                radius={30}
                                                borderWidth={5}
                                                color={Colors.PRIMARY_COLOR}
                                                shadowColor="#999"
                                                bgColor="#fff"
                                            >
                                                {/* <Text style={{ fontSize: 18,color: Colors.FLAT_TEXT_COLOR ,fontWeight:"bold"}}>Qty</Text> */}
                                                <Text style={{ fontSize: 15 }}>{item.qty}</Text>
                                            </ProgressCircle>
                                        </View>

                                    </View>
                                </TouchableOpacity>
                            )}
                        />
                        {/* </View> */}
                    </Content>

                }
            </ImageBackground>
        );
    }
}