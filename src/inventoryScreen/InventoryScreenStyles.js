import {
  PixelRatio,
  StyleSheet,
} from 'react-native';
import Colors from '../../resources/Colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
  content: {
    flex: 1,
    justifyContent: 'flex-end',
    padding: PixelRatio.getPixelSizeForLayoutSize(12),
  },
  logoBack: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 150,
    width: 150,
    top: -30,
    borderRadius: 100,
    backgroundColor: '#ffffff90'
  },
  NodataImageStyle: {
    height: hp('15%'),
    width: wp('60%')
  },
  touchableOpacityStyle: {
  },
  iconlImage: {
    width: 60,
    height: 60
  },
  modal4: {
    height: 270,
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    backgroundColor: 'white'
  },
  quickBilling: {
    // height: 400,
    height: hp('55%'),
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    backgroundColor: 'white'
  },
  itemName: {
    fontSize: hp('2%'),
    color: Colors.FLAT_TEXT_COLOR,
    fontWeight: "bold",
    left: 100,
    top: -35,
    // justifyContent: "center",
    // alignItems: "center",
    // textAlign: "center"
  },
  // itemName2: {
  //   fontSize: hp('4%'),
  //   top:20,
  //   color:'#ffff',
  //   fontWeight: "bold",
  //    justifyContent: "center",
  //   alignItems: "center",
  //   textAlign: "center"

  // },
  itemName3: {
    fontSize: hp('2%'),
    color: Colors.FLAT_TEXT_COLOR,
    fontWeight: "bold",
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center"
  },

  inputTextContainer2: {
    justifyContent: 'center',
    alignItems: 'center',

  },
  gridView: {
    flex: 1,

  },
  gridViewModal: {
    flex: 1,
    backgroundColor: 'white',
    marginTop: hp('4%'), //changed item
    marginRight: 30,
    marginLeft: 10,

  },


  itemContainer: {
    borderRadius: 20,
    padding: 10,
    height: hp('11%'),
    width: wp('95%'),
    marginTop: 6,
    shadowColor: Colors.DARK_BLACK_TEXT_COLOR,
    shadowOpacity: 0.8,
    elevation: 7,
    shadowRadius: 30,
    shadowOffset: { width: 1, height: 13 }

  },
  itemContainer2: {
    // borderColor:'#eeeeee',
    // borderWidth:2,

    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomEndRadius: 15,
    borderTopLeftRadius: 15,
    borderBottomLeftRadius: 15,
    borderTopEndRadius: 12,
    // padding: 5,
    height: 70,
    // margin: 5,
    elevation: 3,
    marginTop: 2,
  },

  itemContent: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25,
    height: hp('20%'),
    width: wp('85%'),
    // marginBottom: '10%',
    // marginLeft: '10%',
    shadowColor: "#000",
    shadowOffset: {
      width: 1,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 10.65,

    elevation: 6,
  },
  button: {
    position: 'absolute',
    top: 20,
    padding: 10,
  },
  caption: {
    fontSize: hp('2.6%'),
    fontWeight: 'bold',
    alignItems: 'center',
  },
  welcome: {
    fontSize: hp('3%'),
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
  MainContainer: {
    justifyContent: 'center',
    flex: 1,
    paddingTop: 30,
  },
  imageThumbnail: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 100,
  },
  notificationViewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.FLAT_TEXT_COLOR,
    width: 25,
    height: 25,
    borderRadius: 25,
  },
  notificationTextStyle: {
    color: Colors.BACKGROUD_WHITE,
    fontSize: hp('1.5%'),
  },
  textInputStyle: {
    height: 50,
    borderRadius: 10,
    borderWidth: 1,
    paddingLeft: 10,
    borderColor: Colors.PRIMARY_COLOR,
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  viewStyle: {
    justifyContent: 'center',
    marginTop: -10,
    padding: 16,
  },
  ProcessText: {
    fontSize: hp('2.5%'),
    fontWeight: 'bold',
    justifyContent: 'center',
    alignItems: 'center',
    color: 'white'
  },
  ProcessButton: {
    backgroundColor: Colors.PRIMARY_COLOR
  },
  inputTextContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonContainerProcess: {
    height: hp('7%'),
    width: wp('90%'),
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
    borderRadius: 30,
    shadowColor: Colors.DARK_BLACK_TEXT_COLOR,
    shadowOpacity: 0.8,
    shadowRadius: 15,
    shadowOffset: { width: 1, height: 13 }
  },
  buttonContainerProcess1: {
    height: hp('7%'),
    width: wp('83%'),
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
    borderRadius: 30,
    shadowColor: Colors.DARK_BLACK_TEXT_COLOR,
    shadowOpacity: 0.8,
    elevation: 6,
    shadowRadius: 15,
    shadowOffset: { width: 1, height: 13 }
  },
  input: {
    color: 'white',
    left: 20,
    maxWidth: '82%',
  },
  item: {
    marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
    backgroundColor: Colors.PRIMARY_COLOR,
    borderColor: Colors.PRIMARY_COLOR,
  },
  form: {
    flex: 1,
    // justifyContent: 'flex-end',
  },
  TextItemStyle: {
    width: '92%',
    height: 55,
    borderBottomColor: Colors.PRIMARY_COLOR,
    borderBottomWidth: 2
  },
  NotSelectedText2: {
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    color: Colors.DARK_BLACK_TEXT_COLOR,
    fontSize: 15,
    fontWeight: 'bold',
  },
  InputStyle: {
    fontSize: 20,
    fontWeight: 'bold'
  },
  TextInputStyle2: {
    height: 40,
    borderBottomColor: Colors.DARK_BLACK_TEXT_COLOR,
    width: 200,
    marginBottom: 10,
    borderBottomWidth: 1
  },
  backGround: {
    backgroundColor: Colors.PRIMARY_COLOR,

  },

  headerTextStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 25,
    color: 'white',
  },
  header: {
    justifyContent: 'center',
    alignItems: 'center',
    left: 70
  },
  headerStyle: {
    backgroundColor: Colors.PRIMARY_COLOR,
    shadowOpacity: 0.75,
    shadowRadius: 5,
    shadowOffset: { height: 0, width: 0 },
  },
  ToucherbleIconStyle: { color: Colors.PRIMARY_COLOR },
  ToucherbleIconStyle2: { height: 30, width: 80 },
  ToucherbleIconStyle3: { height: 30, width: 30 },

  logoContainer: {

    justifyContent: 'center',
    alignItems: 'center',
    marginTop: PixelRatio.getPixelSizeForLayoutSize(40),
    marginBottom: PixelRatio.getPixelSizeForLayoutSize(40),
  },
  logoText: {
    color: Colors.DARK_BLACK_TEXT_COLOR,
    fontSize: 24,
    fontWeight: '700',
    marginTop: 10
  },
  logoText1: {
    color: Colors.DARK_BLACK_TEXT_COLOR,
    fontSize: 15,
    fontWeight: '700',

  },
  backgroundImage: { width: "100%", height: "100%" },

  logo: {
    left: -3,
    height: PixelRatio.getPixelSizeForLayoutSize(150),
    width: PixelRatio.getPixelSizeForLayoutSize(150),
    resizeMode: 'contain',
  },
  place: {
    width: 25,
    height: 25,
    left: 10
  },
  place4: {
    width: 25,
    height: 25,
  },
  buttonContainer: {
    flex: 1,
    marginTop: PixelRatio.getPixelSizeForLayoutSize(10),
  },
  forgotPasswordContainer: {
    alignItems: 'center',
    bottom: -150
  },
  forgotPasswordText: {
    color: 'white',
    fontSize: 16,
  },
  button: {
    marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  loginText: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
  },
  signupContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: PixelRatio.getPixelSizeForLayoutSize(3),
  },
  dontHaveAccountText: {
    color: '#bec0ce',
    fontSize: 16,
  },
  signupText: {
    color: '#000',
    fontSize: 10,
  },
  itemName5: {
    fontSize: 16,
    position: 'absolute',
    left: 15,
    bottom: 40,
    width: 100,
    color: Colors.FLAT_TEXT_COLOR,
    fontWeight: "bold",
    // fontWeight: 'bold',

    // fontWeight: "400",
    justifyContent: "center",
    alignItems: "center",
    textAlign: "left"
  },
  itemName7: {
    fontSize: 14,
    position: 'absolute',
    left: 15,
    width: 100,
    top: 45,
    color: Colors.FLAT_TEXT_COLOR,
    fontWeight: "bold",
    justifyContent: "center",
    alignItems: "center",
    textAlign: "left"
  },
  itemName8: {
    fontSize: 12,
    position: 'absolute',
    left: 15,
    top: 30,
    width: 100,
    color: Colors.FLAT_TEXT_COLOR,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "left"
  },
  itemName6: {
    fontSize: 16,
    position: 'absolute',
    right: 15,
    color: Colors.FLAT_TEXT_COLOR,
    // fontWeight: 'bold',

    // fontWeight: "400",
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center"
  },
  itemName4: {
    fontSize: 16,
    // fontWeight: 'bold',
    color: Colors.FLAT_TEXT_COLOR,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center"
  },
  itemnametxt: {
    fontSize: 16,
    width: 100,
    color: Colors.FLAT_TEXT_COLOR,
    fontWeight: "bold",
    textAlign: "left"
  },
  itemcategorytxt: {
    fontSize: 14,
    width: 100,
    color: Colors.FLAT_TEXT_COLOR,
    fontWeight: "bold",
    textAlign: "left"
  },
  itemqtytxt: {
    fontSize: 12,
    textAlign:'left',
    left:-40,
    color: Colors.FLAT_TEXT_COLOR,
    
  },
  itempricetxt: {
    fontSize: 12,
    width: 100,
    color: Colors.FLAT_TEXT_COLOR,
    textAlign: "left"
  },
  longpressmodal: {
    justifyContent: 'center',
    alignItems: 'center'
  },

  longpressmodal3: {
    height: hp('45%'),
    width: wp('85%'),
    borderRadius: 20,
  },
});


export default styles;
