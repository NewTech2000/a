import {
    PixelRatio,
    StyleSheet,
  } from 'react-native';
  import Colors from '../../../resources/Colors';
  import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
  import { Right } from 'native-base';
  
  const styles = StyleSheet.create({
    buttonContainerProcess: {
      height: hp('7%'),
      width: wp('90%'),
      flexDirection: "row",
      justifyContent: "center",
      alignItems: "center",
      marginTop: 10,
      borderRadius: 30,
      shadowColor: Colors.DARK_BLACK_TEXT_COLOR,
      shadowOpacity: 0.8,
      shadowRadius: 15,
      shadowOffset: { width: 1, height: 13 }
  
    },
    ProcessButton: {
      backgroundColor: Colors.PRIMARY_COLOR
    },
    notificationViewStyle: {
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: Colors.FLAT_TEXT_COLOR,
      width: 25,
      height: 25,
      borderRadius: 25,
    },
    notificationTextStyle: {
      color: Colors.BACKGROUD_WHITE,
      fontSize: hp('1.5%'),
    },
    ProcessText: {
      fontSize: hp('2.5%'),
      fontWeight: 'bold',
      justifyContent: 'center',
      alignItems: 'center',
      color: 'white'
    },
    inputTextContainer2: {
      justifyContent: 'center',
      alignItems: 'center',
  
    },
    item: {
      // marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
      //  backgroundColor: Colors.PRIMARY_COLOR,
    
      borderColor: Colors.PRIMARY_COLOR,
      borderWidth:1,
      

  },
  place: {
    width: 25,
    height: 25,
    left: 10
},
input: {
  color: Colors.PRIMARY_COLOR,
  fontWeight:"bold",
  left: 10,
  maxWidth:'90%',
  fontSize:15,
  backgroundColor:'white',
 
  // backgroundColor:'red'
},
gridView: {
  // marginTop: 1,
  flex: 1,
},
itemContainer: {
  borderRadius: 8,
  padding: 5,
  height: hp('5%'),
  width: wp('99%'),
  // left: 5,
  // right: 5,
  marginTop: 3,
  marginBottom:3,
  // marginLeft: -5,
  shadowColor: Colors.DARK_BLACK_TEXT_COLOR,
  shadowOpacity: 0.8,
  elevation: 7,
  shadowRadius: 30,
  shadowOffset: { width: 1, height: 13 }

},
place1: {
  width: 25,
  height: 25,
  justifyContent:"center",
  alignContent:"center",
  alignItems:"center"
 

},
  });
  
  
  export default styles;