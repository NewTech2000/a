import React, { Component, Fragment } from 'react';
import SearchableDropdown from 'react-native-searchable-dropdown';
import { View, Text, ImageBackground, TouchableOpacity, Image,BackHandler, StatusBar, FlatList, Alert ,AsyncStorage} from 'react-native';
import {
  Item,
  Input,
  Header,
  Left,
  Right,
  Picker,
  Label,
  Button,
} from 'native-base';
import Colors from '../../../resources/Colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import styles from './SupplierInvoiceStyle';
import Strings from '../../../resources/Strings';
import Messages from '../../../resources/Message';
import NetInfo from "@react-native-community/netinfo";
import Moment from 'moment';
import { fromLeft } from 'react-navigation-transitions';
var dateFormat = require('dateformat');
export default class SupplierInvoiceScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedItems: [],
      getItemlist: [],
      text: '',
      item: '',
      name: "",
      price: "",
      salesprice: "",
      cost: "",
      qty: "",
      name2: "",
      price2: "",
      salesprice2: "",
      supplierId: "",
      cost2: "",
      qty2: "",
      sampleArray: [],
      more: "",
      date: (dateFormat(new Date(), "yyyy-mm-dd h:MM:ss")),
      sample: [],
      batchNumber: '',
      productId: '',
      token:""

    }
    // this.state.getItemlist = this.props.navigation.state.params.item
    // this.state.sample = this.props.navigation.state.params.sample

    // console.log("ccccccccccccccccccc" + JSON.stringify(this.state.getItemlist))
    // this.state.name = this.state.getItemlist.name
    // this.state.cost = this.state.getItemlist.cost
    // this.state.productId = this.state.getItemlist.productId
    // this.state.price = this.state.getItemlist.price.toString()
    // this.state.salesprice = this.state.getItemlist.salePrice.toString()
    // this.state.qty = this.state.getItemlist.qty.toString()

  }




  componentDidUpdate() {
    this.state.getItemlist = this.props.navigation.getParam('item', '')
    this.state.sample = this.props.navigation.getParam('sample', '')
    this.state.supplierId = this.props.navigation.getParam('ssid', '')
    this.state.token = this.props.navigation.getParam('token', '')
    console.log("zzzzzzzzzzzzz" + JSON.stringify(this.state.getItemlist.cost));
    console.log("ccccccccccccccccccc" + JSON.stringify(this.state.sample))
    var name = this.state.getItemlist.name
    var cost = ""
    var productId = this.state.getItemlist.productId
    var price = this.state.getItemlist.price.toString()
    var salesprice = this.state.getItemlist.salePrice.toString()
    var qty = this.state.getItemlist.qty.toString()
    const { navigation } = this.props;
    this.focusListener = navigation.addListener('didFocus', () => {

      this.setState({ name: name, cost: cost, productId: productId, price: price, salesprice: salesprice, qty: qty });

    })
  }


  backButtonPress() {
    // this.props.navigation.navigate('SupplierScreen')
  }

  handleBackButton = () => {
       
    // backHandlerClickCount=0
    // if (this.state.backClickCount) {
    //     this.closeAll() 
    // }else if (this) {
       this.props.navigation.goBack(null);
        return true;
    // }

};
  
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
}

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    this.state.getItemlist = this.props.navigation.getParam('item', '')

    console.log("zzzzzzzzzzzzz" + JSON.stringify(this.state.getItemlist.ssid));
    console.log("zzzzzzzzzzzzz" + JSON.stringify(this.state.supplierId));
    this.state.supplierId = this.state.getItemlist.ssid
    console.log("mmmm" + this.state.date)
  }


  //place Order
  Supplierinvoice() {  
    this.setState({
      spinner: true,
    });
    var data = JSON.stringify({

      "additionalCostAmount": 0,
      "charges": [

      ],
      "creditDayPeriod": 0,
      "customFields": {},
      "description": "string",
      "invoiceDate": {
        "basic": this.state.date,
      },
      "invoiceDiscountAmount": 0,
      "invoiceDiscountPercentage": 0,
      "invoiceNumber": "string",
      "payments": [
      ],
      "products": this.state.sampleArray,
      "serialNumber": "string",
      "taxes": [

      ]



    });

    NetInfo.isConnected.fetch().then(isConnected => {
      if (isConnected === true) {
        fetch(
          Strings.BASE_URL + '/api/supplier/' + this.state.supplierId + '/invoice',
          {
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              'Authorization': this.state.token
            },
            body: data
          }
        )

          .then(resp => resp.json())
          .then(responseJson => {
            Messages.messageName(Strings.WARNING_SUCESS, Strings.SUPPLIER_INVOICE_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
            console.log("xxxxxxxxxxx" + JSON.stringify(responseJson));
            // this.props.navigation.navigate('DoneScreen', {
            //   total: 0,
            //   balance: 0

            // });
            this.props.navigation.navigate('SupplierScreen')
            this.setState({
              spinner: false
            });
          })

          .catch(error => {
            console.log(error);
            this.setState({
              spinner: false,
            });
            Messages.messageName(Strings.WARNING_FAILED, Strings.PLACE_ORDER_FAILED, Strings.ICON[2], Strings.TYPE[2], Colors.DARK_BLACK_TEXT_COLOR);

          });
      } else {
        Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
        this.setState({
          spinner: false
        })

        Alert.alert(
          'Connection Error!',
          'Please check your connection and try again',
          [
            {
              text: 'Retry', onPress: () => {
                this.invoice()

              }
            },
          ],
          { cancelable: true }
        );
      }
    });


  }
  addTotable() {
    this.setState({

    })
    this.state.sampleArray.push({

      "assumedCost": this.state.cost,
      "batchNumber": this.state.batchNumber,
      "cost": this.state.cost,
      "customFields": {},
      "name": this.state.name,
      "price": this.state.price,
      "productId": this.state.productId,
      "qty": this.state.qty,
      "salePrice": this.state.salesprice,
      "serialNumbers": [
        "string"
      ]

    }),

      console.log("vvvvvvvvvvvvvvvvvvv" + JSON.stringify(this.state.sampleArray))
  }
  navigateAdditem = () => {
    console.log("eeeeeeeeeeeeeeeeeee" + JSON.stringify(this.state.sampleArray))
    this.props.navigation.navigate('SupplierAdditemSreen', {
      sampleArray: this.state.sampleArray,
      ssid: this.state.supplierId
    });

  }
  // // this.props.navigation.navigate('SupplierAdditemSreen')
  // const { navigate } = this.props.navigation;
  // navigate('SupplierAdditemSreen', {
  //   // sampleArray: sampleArray,

  // })

  validation(){
    if(this.state.name == ""){
      Messages.messageName(Strings.SUPPLIER_ITEM_ADDED_FAIL, Strings.SUPPLIER_ITEM_NAME, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
    }else if(this.state.price == ""){
      Messages.messageName(Strings.SUPPLIER_ITEM_ADDED_FAIL, Strings.SUPPLIER_ITEM_PRICE, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
    }else if(this.state.batchNumber == ""){
      Messages.messageName(Strings.SUPPLIER_ITEM_ADDED_FAIL, Strings.SUPPLIER_ITEM_BATCHNUM, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
    }else if (this.state.salesprice == ''){
      Messages.messageName(Strings.SUPPLIER_ITEM_ADDED_FAIL, Strings.SUPPLIER_ITEM_SALEPRICE, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
    }else if(this.state.cost == ''){
      Messages.messageName(Strings.SUPPLIER_ITEM_ADDED_FAIL, Strings.SUPPLIER_ITEM_COST, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
    }else if(this.state.qty == ''){
      Messages.messageName(Strings.SUPPLIER_ITEM_ADDED_FAIL, Strings.SUPPLIER_ITEM_QTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
    }else{
      this.addTotable()
    }
  }
//  validation(){
//   if(this.state.cost == ""){
// Alert.alert("wwwwwwwwwww")
//   }else{
//     this.addTotable()
//   }
//  }
  render() {
    return (
      <ImageBackground
        source={require("../../../assets/newBack2.png")}
        style={{ width: "100%", height: "100%" }}>

        <View style={{ backgroundColor: '#126F85', width: '100%', height: 60, borderBottomLeftRadius: 50 }}>
          <View style={{ backgroundColor: '#126F85', width: 50, top: 13, borderRadius: 60, left: 25, position: "absolute", top: 8 }}>
            <TouchableOpacity onPress={() => this.backButtonPress()}>
              <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                  <Image source={require("../../../assets/newBack3.png")} style={{ width: 20, height: 20 }}></Image>
                </View>
              </View>
            </TouchableOpacity>
          </View>
          <Text style={{ fontFamily: 'Ubuntu-Medium', color: '#ffff', justifyContent: "center", textAlign: "center", fontSize: 20, position: "absolute", top: 10, left: 80, fontWeight: "bold" }}>Supplier Invoice</Text>

        </View>
        <StatusBar barStyle="light-content" hidden={false} backgroundColor={'#126F85'} />

        <View>
          <View style={styles.inputTextContainer2}>
            <TouchableOpacity
              style={[styles.buttonContainerProcess, styles.ProcessButton]}
              onPress={() => this.navigateAdditem()}>
              <Text style={styles.ProcessText}>Add product</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ padding: 15 }}>
          <Item
            style={styles.item}

            last
          >
            <Image
              style={styles.place}
              source={require('../../../assets/nameSup.png')}
            />
            <Input
              style={styles.input}
              placeholder="Name"
              placeholderTextColor=''
              autoCapitalize="none"
              editable={false}
              value={this.state.name}
              onChangeText={name => this.setState({ name })}
            />
          </Item>
          <Item
            style={styles.item}

            last
          >
            <Image
              style={styles.place}
              source={require('../../../assets/moneyBag.png')}
            />
            <Input
              style={styles.input}
              placeholder="price"
              placeholderTextColor=''
              autoCapitalize="none"
              value={this.state.price}
              onChangeText={price => this.setState({ price })}
            />
          </Item>
          <Item
            style={styles.item}

            last
          >
            <Image
              style={styles.place}
              source={require('../../../assets/moneyBag.png')}
            />
            <Input
              style={styles.input}
              placeholder="batchNumber"
              
              placeholderTextColor=''
              autoCapitalize="none"
              value={this.state.batchNumber}
              onChangeText={batchNumber => this.setState({ batchNumber })}
            />
          </Item>
          <Item
            style={styles.item}

            last
          >
            <Image
              style={styles.place}
              source={require('../../../assets/priceSup.png')}
            />
            <Input
              style={styles.input}
              placeholder="Sales Price"
              placeholderTextColor=''
              keyboardType='numeric'
              autoCapitalize="none"
              value={this.state.salesprice}
              onChangeText={salesprice => this.setState({ salesprice })}
            />
          </Item>
          <Item
            style={styles.item}

            last
          >
            <Image
              style={styles.place}
              source={require('../../../assets/costSup.png')}
            />
            <Input
              style={styles.input}
              placeholder="Cost"
              placeholderTextColor=''
              keyboardType='numeric'
              autoCapitalize="none"
              value={this.state.cost}
              onChangeText={cost => this.setState({ cost })}
            />
          </Item>
          <Item
            style={styles.item}

            last
          >
            <Image
              style={styles.place}
              source={require('../../../assets/qtySup.png')}
            />
            <Input
              style={styles.input}
              placeholder="Qty"
              placeholderTextColor=''
              keyboardType='numeric'
              autoCapitalize="none"
              value={this.state.qty}
              onChangeText={qty => this.setState({ qty })}
            />
          </Item>
        </View>
        <View style={{width:'80%',margin:10,alignSelf: 'flex-end',right: 0,}}>
        <TouchableOpacity onPress={() => this.validation()} style={{alignSelf: 'flex-end',right: 0,}}>
          <View style={{ backgroundColor: Colors.PRIMARY_COLOR, width: '80%', height: 30, padding: 10, borderRadius: 30,bottom: 8}} >
            <Text style={{ textAlign: "center", color: Colors.BACKGROUD_WHITE, fontSize: 15,bottom:5 }}>    Add     </Text>
          </View>
        </TouchableOpacity>
        </View>
        
        
        
        <View style={{ height: 30 }}>
          <View style={{
            marginLeft: 7, marginRight: 7, backgroundColor: '#e0e0e0',
            height: 30, flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center',
          }}>
            <Text style={{ fontWeight: 'bold', fontSize: 13, color: Colors.DARK_BLACK_TEXT_COLOR, textAlign: 'left', left: -60 }}>Name</Text>
            <Text style={{ fontWeight: 'bold', fontSize: 13, color: Colors.DARK_BLACK_TEXT_COLOR, left: -30 }}>price</Text>
            <Text style={{ fontWeight: 'bold', fontSize: 13, color: Colors.DARK_BLACK_TEXT_COLOR, left: -10 }}>Salesprice</Text>
            <Text style={{ fontWeight: 'bold', fontSize: 13, color: Colors.DARK_BLACK_TEXT_COLOR, left: 10 }}>Cost     Qty</Text>

          </View>
        </View>

        <FlatList
          itemDimension={300}
          data={this.state.sampleArray}
          extraData={this.state}
          style={styles.gridView}
          renderItem={({ item, index }) => (
            <TouchableOpacity activeOpacity={0.8}>
              <View style={{ justifyContent: 'center', alignItems: 'center', alignContent: "center" }}>
                <View style={[styles.itemContainer, { backgroundColor: Colors.BACKGROUD_WHITE, }]}>
                  <View style={{ flexDirection: "row" ,paddingRight:20,paddingLeft:20}}>
                    <View style={{width:60 ,paddingRight:10}}>
                    <Text numberOfLines={1} style={{ fontSize: 13, textAlign: 'left', }}>{item.name}</Text>
                    </View>
                    <View style={{width:60}}>
                    <Text style={{ fontSize: 13,  }}>{item.price}</Text>
                    </View>
                    <View style={{width:60}}>
                    <Text style={{ fontSize: 13,}}>{item.salePrice}</Text>
                    </View>
                   <View style={{width:60 ,paddingLeft:15}}>
                   <Text style={{ fontSize: 13,}}>{item.cost}</Text>
                   </View>
                   <View style={{width:60}}>
                   <Text style={{ fontSize: 13,}}>{item.qty}</Text>
                   </View>
                   
                   <TouchableOpacity style={{ alignItems: 'flex-end',}}>
                    <Image
                      style={styles.place1}
                      source={require('../../../assets/del1.png')}
                    />
                  </TouchableOpacity>
                  </View>
                  

                </View>
              </View>
            </TouchableOpacity>
          )}
        />
{/* <View style={{justifyContent:"center",alignContent:"center",alignItems:"center"}}> */}
<Button onPress={() => this.Supplierinvoice()} style={{backgroundColor:Colors.PRIMARY_COLOR,textAlign:"center",justifyContent:"center",alignContent:"center",alignItems:"center"}}>
  <Text style={{fontSize:18,fontWeight:"bold",color:'#ffff',textAlign:"center"}}>Submit</Text>
  </Button>
{/* </View> */}
        
      </ImageBackground>
    );
  }
}