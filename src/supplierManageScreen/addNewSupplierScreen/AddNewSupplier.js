import React, { Component } from 'react';
import { View, Image, ImageBackground, StatusBar, Alert, TouchableOpacity, BackHandler, TouchableHighlight, AsyncStorage } from 'react-native';
import {
    Button,
    Content,
    Form,
    Item,
    Input,
    Text,
    Label,
    Fab
} from 'native-base';
import styles from './AddNewSupplierStyles';
import Messages from '../../../resources/Message';
import Colors from '../../../resources/Colors';
import NetInfo from "@react-native-community/netinfo";
import Strings from '../../../resources/Strings';
import CompressImage from "react-native-compress-image";
import ImagePicker from "react-native-image-picker";
import Spinner from 'react-native-loading-spinner-overlay';
import { ScrollView } from "react-native-gesture-handler";
import { NavigationActions, StackActions } from 'react-navigation'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default class AddSupplier extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            address: "",
            background: '',
            username: '',
            password: '',
            confirmpassword: '',
            contact: '',
            nicnumber: '',
            photo: null,
            avatarSource: null,
            url: "",
            photoUri: '',
            uri: "",
            spinner: false,
            branchId: "",
            roleId: "",
            token: "",
            email: "",
            contactNumber: "",
            contactPerson: "",
            address: "",
            description: ""



        };
    }

    onButtonPress = () => {

    }


    handleBackButton = () => {
        this.props.navigation.goBack(null);
        return true;
    };



    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }


    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    async componentWillMount() {
        let user = await AsyncStorage.getItem('companyDetails');
        let parsed = JSON.parse(user);
        let myArray = await AsyncStorage.getItem('myArray');
        let d = JSON.parse(myArray);

        this.setState({
            branchId: parsed.branchId,
            roleId: parsed.role.id,
            token: d

        })
        console.log("dddddddddddddddddddddddd" + JSON.stringify(parsed.branchId))
        console.log("dddddddddddddddddddddddd" + JSON.stringify(parsed.role.id))


    }
    namevalidate(text) {
        const reg = /^[a-zA-Z ]{2,30}$/;
        if (reg.test(text) === false) {
            return false;
        } else {
            return true;
        }
    }

    validation() {
        if (this.namevalidate(this.state.name) === false) {
            Messages.messageName(Strings.SUPPLIER_ADDED_FAIL, Strings.SUPPLIER_NAME_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        }else if (this.state.address == "") {
            Messages.messageName(Strings.SUPPLIER_ADDED_FAIL, Strings.SUPPLIER_ADDRESS_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        } else if (this.state.contactNumber == "") {
            Messages.messageName(Strings.SUPPLIER_ADDED_FAIL, Strings.SUPPLIER_CONTACT_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        } else if (this.state.contactPerson == "") {
            Messages.messageName(Strings.SUPPLIER_ADDED_FAIL, Strings.SUPPLIER_CONTACT_PERSON_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        } else if (this.state.email ="") {
            Messages.messageName(Strings.SUPPLIER_ADDED_FAIL, Strings. SUPPLIER_EMAIL_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        } else if (this.state.description ="") {
            Messages.messageName(Strings.SUPPLIER_ADDED_FAIL, Strings.SUPPLIER_DESCRIPTION_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        }else {
            this.addSupplier()
        }


    }

    addSupplier() {
        this.setState({
            spinner: true,
        });
        var data = JSON.stringify({

            "address": this.state.address,
            "contactNumber": this.state.contactNumber,
            "contactPerson": this.state.contactPerson,
            "creditAmountLimit": 0,
            "creditDayLimit": 0,
            "description": this.state.description,
            "email": this.state.email,
            "name": this.state.name

        });

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(Strings.BASE_URL + '/api/supplier',
                    {
                        method: "POST",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },
                        body: data
                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        Messages.messageName(Strings.WARNING_SUCESS, Strings.SUPPLIER_ADDED_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                        console.log(JSON.stringify(responseJson));
                        const resetAction = StackActions.reset({
                            index: 0,
                            actions: [
                                NavigationActions.navigate({ routeName: 'SupplierScreen' })],

                        });
                        this.props.navigation.dispatch(resetAction);
                        this.setState({
                            spinner: false
                        });
                    })

                    .catch(error => {
                        console.log(error);
                        this.setState({
                            spinner: false,
                        });
                        Messages.messageName(Strings.WARNING_FAILED, Strings.SUPPLIER_ADDED_FAIL, Strings.ICON[2], Strings.TYPE[2], Colors.WARNING_ALERT);
                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false,
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.addSupplier()

                            }
                        },
                    ],
                    { cancelable: true }
                );


            }
        });

    }


    //upload Image
    handleUploadPhoto = () => {

        if (this.state.photo == null) {
            this.registerCustomer()
        } else {

            this.setState({
                spinner: true,
            });

            fetch(Strings.Image_URL + "/file-server", {
                method: "POST",
                body: this.createFormData(this.state.photo, { userId: "123" })
            })
                .then(response => response.json())
                .then(response => {



                    this.setState({
                        photo: null,
                        uri: response.url
                    });
                    this.setState({
                        spinner: false,
                    });
                    this.addUser()

                    console.log("upload succes", this.state.uri);

                })
                .catch(error => {
                    this.setState({
                        spinner: false,
                    });

                    console.log("upload error", error);
                    // alert("Upload failed!");
                });


        }

    };


    //chooseing image
    handleChoosePhoto = () => {
        const options = {
            noData: true,
        }
        ImagePicker.launchImageLibrary(options, response => {
            console.log("upload succes", response);
            if (response.uri) {
                this.setState({
                    photo: response

                })
                this.compress()
            }
        })
    }


    //split image url
    createFormData = (photo, body) => {
        const data = new FormData();

        data.append("document", {
            name: photo.fileName,
            type: photo.type,
            uri:
                Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "")
        });

        Object.keys(body).forEach(key => {
            data.append(key, body[key]);
        });
        return data;
    };

    //To compress a image
    compress() {
        CompressImage.createCompressedImage(this.state.photo.uri, "Compress/Images")

            .then(response => {
                this.state.photo.uri = response.uri;
                this.state.photo.path = response.path;
                this.state.photo.mime = response.mime;
            })

            .catch(err => {
                return Alert.alert(
                    Strings.COMPRESS_ERR
                );
            });

    }





    render() {
        return (

            <ImageBackground
                source={require("../../../assets/newBack2.png")}
                style={{ width: "100%", height: "100%" }}>
                <StatusBar barStyle="light-content" hidden={false} backgroundColor='#126F85' />
                <Spinner
                    visible={this.state.spinner}
                    textContent={'Loading...'}
                    textStyle={{ color: 'white' }}
                    overlayColor={"#0f0e0e90"}
                    animation={"fade"}
                    color={"#00CEFD"}
                />
                <View style={{ backgroundColor: '#126F85', width: '100%', height: 60, borderBottomLeftRadius: 50 }}>
                    <View style={{ backgroundColor: '#126F85', width: 50, top: 13, borderRadius: 60, left: 25, position: "absolute", top: 8 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('SupplierScreen')}>
                            <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                    <Image source={require("../../../assets/newBack3.png")} style={{ width: 20, height: 20 }}></Image>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Text style={{ fontFamily: 'Ubuntu-Medium', color: '#ffff', justifyContent: "center", textAlign: "center", fontSize: 20, position: "absolute", top: 10, left: 100, fontWeight: "bold" }}>New supplier</Text>
                </View>
                <ScrollView>
                    <Content contentContainerStyle={styles.content}>

                        <Form style={styles.form}>
                            {/* <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <TouchableHighlight style={{ borderRadius: 100, height: 100, width: 100, marginBottom: 10, marginTop: 20 }}
                                    onPress={() => this.handleChoosePhoto()}
                                >

                                    <Image
                                        source={this.state.photo == null ? require('../../../assets/nnn.jpg') : this.state.photo !== null ? this.state.photo : this.state.photo}
                                        style={{ width: 100, height: 100, borderRadius: 100, borderWidth: 2, }}
                                    />
                                </TouchableHighlight>

                            </View> */}


                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../../assets/user.png')}
                                />
                                <Input
                                    style={styles.input}
                                    placeholder="*Name"
                                    placeholderTextColor="#ffff"
                                    autoCapitalize="none"
                                    value={this.state.name}
                                    onChangeText={name => this.setState({ name: name })}
                                />
                            </Item>
                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../../assets/address.png')}
                                />
                                <Input
                                    style={styles.input}
                                    placeholder="address"
                                    placeholderTextColor="#ffff"
                                    value={this.state.address}
                                    onChangeText={address => this.setState({ address: address })}

                                />
                            </Item>
                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../../assets/mobilenum.png')}
                                />
                                <Input
                                    style={styles.input}
                                    keyboardType={"numeric"}
                                    placeholder="*contactNumber"
                                    placeholderTextColor="#ffff"
                                    value={this.state.contactNumber}
                                    onChangeText={contactNumber => this.setState({ contactNumber: contactNumber })}
                                />
                            </Item>
                            {/* <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../../assets/confirm.png')}
                                />
                                <Input
                                    style={styles.input}
                                    placeholder="Confirm Password"
                                    placeholderTextColor="#ffff"
                                    value={this.state.confirmpassword}
                                    onChangeText={confirmpassword => this.setState({ confirmpassword })}
                                    secureTextEntry
                                />
                            </Item> */}
                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../../assets/contactperson.png')}
                                />
                                <Input
                                    style={styles.input}
                                    placeholder="contactPerson"
                                    placeholderTextColor="#ffff"
                                    autoCapitalize="none"
                                    value={this.state.contactPerson}
                                    onChangeText={contactPerson => this.setState({ contactPerson: contactPerson })}
                                />
                            </Item>
                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../../assets/email.png')}
                                />
                                <Input
                                    style={styles.input}
                                    placeholder="email"
                                    placeholderTextColor="#ffff"
                                    value={this.state.email}
                                    onChangeText={email => this.setState({ email })}

                                />
                            </Item>
                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../../assets/description.png')}
                                />
                                <Input
                                    style={styles.input}
                                    placeholder="description"
                                    multiline={true}
                                    placeholderTextColor="#ffff"
                                    value={this.state.description}
                                    onChangeText={description => this.setState({ description: description })}

                                />
                            </Item>

                        </Form>
                        <Label></Label>
                        <Label></Label>

                        <View style={styles.buttonContainer}>
                            <Button
                                style={styles.button}
                                onPress={() => this.validation()}
                                hasText
                                block
                                large
                                dark
                                rounded
                            >
                                <Text style={styles.loginText}>Submit</Text>
                            </Button>




                        </View>

                    </Content>

                </ScrollView>

            </ImageBackground>

        );
    }
}