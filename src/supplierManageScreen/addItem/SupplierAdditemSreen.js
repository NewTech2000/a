import React, { Component } from 'react';
import { Image, TouchableOpacity, ImageBackground, StatusBar, TouchableHighlight, BackHandler, Alert, AsyncStorage, Vibration, PixelRatio, Dimensions } from 'react-native';
import {
    Item,
    Input,
    Text,
    Header,
    Left,
    Right,
    Fab,
    Picker,
    Switch,
    Toast,
    Label
} from 'native-base';
import styles from './SupplierAdditemStyle';
import Modal from 'react-native-modalbox';
import Colors from '../../../resources/Colors';
import NetInfo from "@react-native-community/netinfo";
import Strings from '../../../resources/Strings';
import Messages from '../../../resources/Message';
import { FlatGrid } from 'react-native-super-grid';
import { View } from 'react-native-animatable';
import CompressImage from "react-native-compress-image";
import AwesomeAlert from 'react-native-awesome-alerts';
import ImagePicker from "react-native-image-picker";
import { TextInputMask } from 'react-native-masked-text';
import Spinner from "react-native-spinkit";
import Dialog, {
    DialogTitle,
    DialogContent,
    DialogButton,
    ScaleAnimation,
} from 'react-native-popup-dialog';
import Spinner2 from 'react-native-loading-spinner-overlay';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import ImageResizer from 'react-native-image-resizer';

const DURATION = 40;
export default class ManageItem extends Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false,
            selectedItem: 'About',
            arrayholder: [],
            temp: 0,
            price: 0,
            tempPrice: 0,
            text: "",
            trancerdate: [],
            tempArray: [],
            itemType: '',
            avatarSource: "",
            image: null,
            photo: null,
            url: "",
            description: '',
            priceT: 0,
            ItemPrice: 100,
            cost: '',
            token: '',
            apiData: [],
            spinner: false,
            spinner2: false,
            searchText: "",
            productId: "",
            uri: "",
            showTheThing: true,
            pKey: 0,
            qty: 0,
            ItemCost: '',
            ItemName: '',
            ItemPrice: '',
            ItemQty: '',
            photoUri: '',
            category: [],
            catName: "",
            cat: "",
            ssid:"",
            nodata: false,
            resizedImageUri: '',
            batchNumber: "",
            showAlert: false,
            sampleArray: []



        }
        this.state.sampleArray = this.props.navigation.getParam('sampleArray', '')
        console.log("bbbbbbbbbbbbbbbb" + JSON.stringify(this.state.sampleArray))
        //  this.state.sample = this.props.navigation.state.params.sampleArray
    }


    componentDidUpdate() {
        this.state.sampleArray = this.props.navigation.getParam('sampleArray', '')
        this.state.ssid = this.props.navigation.getParam('ssid', '')
        console.log("zzzzzzzzzzzzz" + JSON.stringify(this.state.ssid));
        const { navigation } = this.props;
        this.focusListener = navigation.addListener('didFocus', () => {

            // this.setState({ tempPrice: tot, temp: qty, count: qty });

        })

           
}




//getting auth code from AsyncStorage
showData = async () => {
    let myArray = await AsyncStorage.getItem('myArray');
    let d = JSON.parse(myArray);
    this.setState({
        token: d,

    })

    this.getAllProduct()
    this.gettingCategories()
    console.log(this.state.token);

}

onValueChange(value) {
    this.setState({
        itemType: value
    });
}

onValueChange2(value) {
    this.setState({
        catName: value
    });
}

toggle() {
    this.props.navigation.navigate('HomeScreen')
}
editSelectedItem = (item) => {
    console.log(item)
    const { navigate } = this.props.navigation;
    navigate('SupplierInvoiceScreen', {
        item: item,
        sample:this.state.sampleArray,
        ssid:this.state.ssid,
        token:this.state.token

    })
}

navigateTOProcess() {
    if (this.state.photo.uri == this.state.photoUri) {
        this.state.uri = this.state.photo.uri
        console.log("vvvvvvvvvvvvvvvvv" + this.state.photoUri)
        console.log("aaaaaaaaaaaaaaaaaaa" + this.state.photo.uri)
        this.updateItem()
    } else {
        this.handleUploadPhoto()
    }
}


//show alert hide and show method
showAlert = () => {
    this.setState({
        showAlert: true
    });
    // this.refs.sideMenu.close()
};

hideAlert = () => {
    this.setState({
        showAlert: false
    });
};

handleBackButton = () => {
    this.props.navigation.goBack(null);
    return true;
};

componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
}


componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    this.showData()
}

renderNext3() {
    this.refs.view3.bounceIn(1000);
}

anima() {
    this.props.navigation.navigate('AddItemDetailsScreen')
}



animaNavigate() {
    this.setState({
        scaleAnimationDialog2: false,
    });
    this.props.navigation.navigate('CustomerScreen')
}


addNewBatch() {
    this.setState({
        spinner2: true,
    });
    var data = JSON.stringify({
        "assumedCost": this.state.cost,
        "batchNumber": this.state.batchNumber,
        "cost": this.state.cost,
        "price": this.state.price,
        "productId": this.state.pKey,
        "qty": this.state.qty,
        "salePrice": this.state.price,
        "serialNumbers": [
            "string"
        ]
    });

    NetInfo.isConnected.fetch().then(isConnected => {
        if (isConnected === true) {
            fetch(
                Strings.BASE_URL + "/api/product/batch",
                {
                    method: "POST",
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                        'Authorization': this.state.token
                    },
                    body: data
                }
            )

                .then(resp => resp.json())
                .then(responseJson => {
                    this.setState({
                        spinner2: false,
                        slideAnimationDialog: false,
                        batchNumber: "",
                        cost: "",
                        price: "",
                        qty: ""

                    });
                    this.refs.modal2.close()
                    Messages.messageName(Strings.WARNING_SUCESS, Strings.ADDED_NEW_BATCH_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                    console.log(JSON.stringify(responseJson));
                    this.getAllProduct()
                })

                .catch(error => {
                    this.setState({
                        spinner2: false,
                    });
                    console.log(error);
                    Messages.messageName(Strings.WARNING_SUCESS, Strings.ADDED_NEW_BATCH_FAIL, Strings.ICON[2], Strings.TYPE[2], Colors.SUCCESS_ALERT);


                });

        } else {
            Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
            this.setState({
                spinner2: false
            })

            Alert.alert(
                'Connection Error!',
                'Please check your connection and try again',
                [
                    {
                        text: 'Retry', onPress: () => {
                            this.saveItemPrice()

                        }
                    },
                ],
                { cancelable: true }
            );
        }
    });

}

search(text) {
    this.state.searchText = text;
    this.getAllProduct()
}

//get All Product 
getAllProduct() {
    this.setState({
        spinner: true,
    });

    NetInfo.isConnected.fetch().then(isConnected => {
        if (isConnected === true) {
            fetch(
                Strings.BASE_URL + "/api/inventory?page=0&limit=100&text=" + this.state.searchText,
                {
                    method: "GET",
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                        'Authorization': this.state.token
                    },

                }
            )
                .then(resp => resp.json())
                .then(responseJson => {
                    console.log("gettAll" + JSON.stringify(responseJson));
                    this.setState({
                        spinner: false,
                    });

                    this.setState({
                        arrayholder: responseJson.content
                    })

                    if (responseJson.content.length == 0) {
                        this.setState({
                            nodata: true
                        })
                    } else {
                        this.setState({
                            nodata: false
                        })
                    }

                })
                .catch(error => {
                    console.log(error);
                    this.setState({
                        spinner: false,
                    });

                });

        } else {
            Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
            this.setState({
                spinner: false
            })
            Alert.alert(
                'Connection Error!',
                'Please check your connection and try again',
                [
                    {
                        text: 'Retry', onPress: () => {
                            this.getAllProduct()
                        }
                    },
                ],
                { cancelable: true }
            );
        }
    });
}


onClose = () => {
    this.setState({
        showTheThing: true,
        showAlert: false,
        // photo: "",
        // catName: "",
        // ItemName: "",
        // itemType: "",
    })
}

//no data iamge
nodataImage() {
    if (this.state.nodata) {
        return (

            <View style={{ top: 60, justifyContent: 'center', alignItems: 'center', }}>
                <Image style={{ width: 200, height: 200, borderRadius: 100 }}
                    source={require("../../../assets/no_data_found.png")}
                />
            </View>
        )
    }
}

//Save Item
saveItem() {

    this.saveProduct()


}

//chooseing image
handleChoosePhoto = () => {
    const options = {
        noData: true,
    }
    ImagePicker.launchImageLibrary(options, response => {
        console.log("upload succes", response);
        if (response.uri) {
            this.setState({
                photo: response

            })
            this.compress()
        }
    })
}




//To compress a image
compress() {
    CompressImage.createCompressedImage(this.state.photo.uri, "Compress/Images")

        .then(response => {
            this.state.photo.uri = response.uri;
            this.state.photo.path = response.path;
            this.state.photo.mime = response.mime;
        })

        .catch(err => {
            // return Alert.alert(
            //     Strings.COMPRESS_ERR
            // );

        });

}

//upload Image
handleUploadPhoto = (string) => {
    this.setState({
        spinner2: true,
    });

    fetch(Strings.Image_URL + "/file-server", {
        method: "POST",
        body: this.createFormData(this.state.photo, { userId: "123" })
    })
        .then(response => response.json())
        .then(response => {


            // alert("Upload success!");
            this.setState({
                photo: null,
                uri: response.url
            });
            this.setState({
                spinner2: false,
            });


            console.log("upload succes", this.state.uri);
            if (string == 'save') {
                this.saveProduct()
            } else {
                this.updateItem()
            }


        })
        .catch(error => {
            this.setState({
                spinner: false,
            });

            console.log("upload error", error);
            // alert("Upload failed!");
        });
};

gettingCategories() {
    this.setState({
        spinner: true,
    });

    NetInfo.isConnected.fetch().then(isConnected => {
        if (isConnected === true) {
            fetch(
                Strings.BASE_URL + '/api/product/category/sub-category?page=0&limit=100&text=' + this.state.cat,
                {
                    method: "GET",
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                        'Authorization': this.state.token
                    },

                }
            )
                .then(resp => resp.json())
                .then(responseJson => {
                    console.log("fffffffffffffffffffff" + JSON.stringify(responseJson));
                    this.setState({
                        spinner: false,
                    });

                    this.setState({
                        category: responseJson.content
                    })

                })
                .catch(error => {
                    console.log(error);
                    this.setState({
                        spinner: false,
                    });

                });
        } else {
            Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
            this.setState({
                spinner: false
            })

            Alert.alert(
                'Connection Error!',
                'Please check your connection and try again',
                [
                    {
                        text: 'Retry', onPress: () => {
                            this.gettingCategories()

                        }
                    },
                ],
                { cancelable: true }
            );
        }
    }); 0

}

loadCategoryTypes() {
    var data = [];
    return this.state.category.map(data => (
        <Picker.Item label={data.category} value={data.category} key={data.category} />
    ));
}

//split image url
createFormData = (photo, body) => {
    const data = new FormData();

    data.append("document", {
        name: photo.fileName,
        type: photo.type,
        uri:
            Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "")
    });

    Object.keys(body).forEach(key => {
        data.append(key, body[key]);
    });
    console.log("qqqqqqqqqqqqqqqqqqqqqqqqqqq", data);
    return data;
};

onPressAlert() {
    Messages.messageName("Need", Strings.LONG_TAP, Strings.ICON[2], Strings.TYPE[2], Colors.WARNING_ALERT);
}



render() {
    const { showAlert } = this.state;
    return (
        <View style={styles.container}>
            <ImageBackground
                source={require("../../../assets/newBack2.png")}
                style={{ width: "100%", height: "100%" }}>
                <View style={{ backgroundColor: '#126F85', width: '100%', height: 60, borderBottomLeftRadius: 50 }}>
                    <View style={{ backgroundColor: '#126F85', width: 50, top: 13, left: 25, position: "absolute", top: 8 }}>
                        <TouchableOpacity onPress={() => this.editSelectedItem()}>
                            <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                    <Image source={require("../../../assets/newBack3.png")} style={{ width: 20, height: 20 }}></Image>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Text style={{ fontFamily: 'Ubuntu-Medium', color: '#ffff', justifyContent: "center", textAlign: "center", fontSize: 20, position: "absolute", top: 10, left: 100, fontWeight: "bold" }}>Supplier Products</Text>
                </View>

                <StatusBar barStyle="light-content" hidden={false} backgroundColor={'#126F85'} />
                <Spinner2
                    visible={this.state.spinner2}
                    textContent={'Loading...'}
                    textStyle={{ color: 'white' }}
                    overlayColor={"#0f0e0e90"}
                    animation={"fade"}
                    color={"#00CEFD"}
                />

                <View style={styles.viewStyle}>
                    <Item
                        style={styles.item}
                        rounded
                        last
                    >
                        <Image
                            style={styles.place}
                            source={require('../../../assets/search.png')}
                        />

                        <Input
                            style={styles.input}
                            placeholder="Search"
                            placeholderTextColor="#ffffff80"
                            autoCapitalize="none"
                            onChangeText={text => this.search(text)}
                        />
                    </Item>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Spinner
                        isVisible={this.state.spinner}
                        type={'ThreeBounce'}
                        textStyle={{ color: 'white' }}
                        size={70}
                        color={Colors.FLAT_TEXT_COLOR}
                    />

                </View>
                {this.nodataImage()}
                <FlatGrid
                    itemDimension={130}
                    items={this.state.arrayholder}
                    style={styles.gridView}
                    renderItem={({ item, index }) => (

                        <TouchableOpacity activeOpacity={0.8} style={styles.touchableOpacityStyle} onPress={() => this.editSelectedItem(item)} >
                            <View style={[styles.itemContainer, { backgroundColor: Colors.BACKGROUD_WHITE }]}>

                                <Text style={styles.itemName3}>LKR.<Text style={styles.itemName2}>{item.price}</Text></Text>

                                <Image
                                    name={item.name}
                                    style={styles.iconlImage}
                                    color={item.color}
                                    source={{ uri: item.imageUrl }}
                                />

                                <Text style={styles.itemName}>{item.name}</Text>



                            </View>
                        </TouchableOpacity>
                    )}
                />
                {this.state.showTheThing &&
                    <Fab
                        active={this.state.active}
                        direction="up"
                        containerStyle={{}}
                        style={styles.backGround}
                        position="bottomRight"
                        onPress={() => this.anima()}>
                        <Image source={require("../../../assets/plus.png")} style={styles.ToucherbleIconStyle3} />
                    </Fab>
                }
            </ImageBackground>
        </View>
    );
}
}
