import React, { Component } from 'react';
import { TextInput, Image, TouchableOpacity, ImageBackground, BackHandler, StatusBar, FlatList, AsyncStorage, Alert, Switch, Linking } from 'react-native';
import {
    Text,
    Header,
    Left,
    Right,
    Item,
    Input,
    Fab
} from 'native-base';
import styles from './SupplierScreenStyles';
import NetInfo from "@react-native-community/netinfo";
import Colors from '../../../resources/Colors';
import Strings from '../../../resources/Strings';
import { View } from 'react-native-animatable';
import Spinner from "react-native-spinkit";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default class SupplierScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {

            token: '',
            data: [],
           
            name:"",
            phone: '',
            spinner: false,
            searchText: "",
            nodata: false,
            switchValue: false,
            extra: {
                name:"",
                price:'',
                qty:'',
                salePrice:'',
                cost:''
            },
                

            
        };
       
    }

    navigatephone(phone) {
        Linking.openURL(`tel:${phone}`)
    }
    onButtonPress = () => {

    }


    handleBackButton = () => {
        this.props.navigation.goBack();
        return true;
    };



    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }


    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        this.showData()


    }
    //getting auth code from AsyncStorage
    showData = async () => {
        let myArray = await AsyncStorage.getItem('myArray');
        let d = JSON.parse(myArray);

        this.setState({
            token: d

        })

        this.gettingSuppliers()
        console.log(this.state.token);

    }

    navigateToEditScreen(item) {
        var item2={
            name:"",
            cost:"",
            productId:"",
            price:"",
            salesprice:"",
            qty:"",
            ssid:item.id,
        }
        console.log(JSON.stringify(item))
        const { navigate } = this.props.navigation;
        navigate('SupplierInvoiceScreen', {
          item:item2,
        })

    }

    navigateToAddUserForm() {
        this.props.navigation.navigate("AddSupplierScreen");
    }

    //getting all of invoices
    gettingSuppliers() {
        this.setState({
            spinner: true
        })
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + '/api/supplier?page=0&limit=400',
                    {
                        method: "GET",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },

                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        console.log("invoice" + JSON.stringify(responseJson.content));

                        this.setState({
                            data: responseJson.content.reverse(),
                            spinner: false
                        })
                        // this.state.data.pop()
                        if (this.state.data.length == 0) {
                            this.setState({
                                nodata: true
                            })
                        } else {
                            this.setState({
                                nodata: false
                            })
                        }

                    })

                    .catch(error => {
                        console.log(error);
                        this.setState({
                            spinner: false
                        })



                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.gettingSuppliers()

                            }
                        },
                    ],
                    { cancelable: true }
                );

            }
        });

    }


    //no data iamge
    nodataImage() {
        if (this.state.nodata) {
            return (

                <View style={{ top: 60, justifyContent: 'center', alignItems: 'center', }}>
                    <Image style={{ width: 200, height: 200, borderRadius: 100 }}
                        source={require("../../../assets/no_data_found.png")}
                    />

                </View>
            )
        }
    }

    render() {

        return (
            <View style={styles.container}>
                <ImageBackground
                    source={require("../../../assets/newBack2.png")}
                    style={{ width: "100%", height: "100%" }}>

                    <View style={{ backgroundColor: '#126F85', width: '100%', height: 60, borderBottomLeftRadius: 50 }}>
                        <View style={{ backgroundColor: '#126F85', width: 50, top: 13, borderRadius: 60, left: 25, position: "absolute", top: 8 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('HomeScreen')}>
                                <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                        <Image source={require("../../../assets/newBack3.png")} style={{ width: 20, height: 20 }}></Image>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <Text style={{ fontFamily: 'Ubuntu-Medium', color: '#ffff', justifyContent: "center", textAlign: "center", fontSize: 20, position: "absolute", top: 10, left: 100, fontWeight: "bold" }}>Manage suppliers</Text>
                        <View style={{ backgroundColor: '#126F85', width: 50, top: 13, borderRadius: 60, right: 25, position: "absolute", top: 8 }}>

                        </View>
                    </View>
                    <StatusBar barStyle="light-content" hidden={false} backgroundColor={'#126F85'} />

                    <View style={{ justifyContent: 'center', marginTop: -10, padding: 16, }}>

                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Spinner
                            isVisible={this.state.spinner}
                            type={'ThreeBounce'}
                            textStyle={{ color: 'white' }}
                            size={70}
                            color={Colors.FLAT_TEXT_COLOR}
                        />
                    </View>
                    {this.nodataImage()}

                    <FlatList
                        itemDimension={300}
                        data={this.state.data}
                        style={styles.gridView}
                        renderItem={({ item, index }) => (
                            <TouchableOpacity activeOpacity={0.8} style={{ justifyContent: 'center', alignItems: 'center' }} onPress={() => this.navigateToEditScreen(item)}>

                                <View style={[styles.itemContainer, { backgroundColor: Colors.BACKGROUD_WHITE }]}>
                                    <View style={{ height: 90 }}>
                                        <View style={{ width: 60, height: 60, borderRadius: 100, backgroundColor: 'rgba(0,0,0,0.2)', justifyContent: "center", alignContent: "center", alignItems: "center", borderColor: Colors.FLAT_TEXT_COLOR, borderWidth: 3, left: 10, position: "absolute", top: 15 }}>
                                            <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', }}>
                                                <Image source={require('../../../assets/addSubUser.png')} style={{ width: 55, height: 55, borderRadius: 100, }}></Image>
                                            </View>
                                        </View>
                                        <View style={{ width: wp('80%') }}>
                                            <Text numberOfLines={1} style={styles.itemName}>{item.name == "" ? "Not set" : item.name !== "" ? item.name : item.name}</Text>
                                        </View>
                                        
                                        
                                        <View style={{ width: wp('37%') }}>
                                            <Text style={styles.itemName}>Contact Person : {item.contactPerson}</Text>
                                        </View>
                                        <View style={{left:250,bottom:35, width:60,alignContent:'flex-end'}}>
                                        <TouchableOpacity onPress={() => this.navigatephone(item.contactNumber)}>
                                            <View style={{width:50,height:50,borderRadius:100,}}>
                                             <Image style={{width:30,height:30,justifyContent:"center",alignContent:"center",alignItems:"center",top:10,left:10}} source={require("../../../assets/suppliercontact1.png")}></Image>
                                            </View>
                                            {/* <Text style={styles.itemcontact}>{item.contactNumber}</Text> */}
                                        </TouchableOpacity>
                                        </View>
                                        {/* <View style={{ position: "absolute", width: 70, top: 65, right: 65 }}> */}
                                        {/* <Text numberOfLines={1} style={{ fontSize: 12, color: '#ffff', textAlign: "center", top: 8, }}>Hide image</Text> */}
                                        {/* <Switch
                                                //  tintColor="transparent"
                                                thumbTintColor={this.state.switchValue
                                                    ? Colors.BTN_COLOR : "#cccccc"}
                                                style={{ left: 45, bottom: 10, transform: [{ scaleX: 1.3 }, { scaleY: 1.3 }] }}
                                                value={this.state.switchValue}
                                                onValueChange={(switchValue) => this.switchValue(switchValue)}
                                            /> */}
                                        {/* </View> */}
                                    </View>

                                    {/* <Text style={styles.itemName}>       : {item.invoiceDate.basic}</Text>
                                    <Text style={styles.itemName2}>Total Amount  : {item.grandTotalAmount}</Text> */}

                                </View>
                            </TouchableOpacity>

                        )}
                    />

                    <Fab
                        active={this.state.active}
                        direction="up"
                        containerStyle={{}}
                        style={styles.backGround}
                        position="bottomRight"
                        onPress={() => this.navigateToAddUserForm()}>
                        <Image source={require("../../../assets/plus.png")} style={styles.ToucherbleIconStyle3} />
                    </Fab>

                </ImageBackground>
            </View>



        );
    }
}