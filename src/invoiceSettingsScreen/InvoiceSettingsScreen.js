import React, { Component } from 'react';
import { TextInput, Image, TouchableOpacity, ImageBackground, BackHandler,StatusBar, FlatList, TouchableHighlight, Alert, AsyncStorage, Vibration, PixelRatio ,Dimensions} from 'react-native';
import {
    Button,
    Container,
    Content,
    Form,
    Item,
    Input,
    Text,
    Header,
    Left,
    Right,
    Icon,
    Drawer,
    Fab,
    Picker,
    Toast,
    Label
} from 'native-base';
import styles from './InvoiceSettingsStyles';
import Colors from '../../resources/Colors';
import Strings from '../../resources/Strings';
import NetInfo from "@react-native-community/netinfo";
import Messages from '../../resources/Message';
import { View } from 'react-native-animatable';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

export default class ManageItem extends Component {
    constructor(props) {
        super(props);
        this.state = {

            token: '',
            selected: '',
            data: [],
            spinner: false,
            Last: "",
            Prefix: "",

        };

    }

    onButtonPress = () => {

    }


    handleBackButton = () => {
        this.props.navigation.goBack();
        return true;
    };



    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }


 
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        this.showData()

       
    }


    //getting auth code from AsyncStorage
    showData = async () => {
        let myArray = await AsyncStorage.getItem('myArray');
        let d = JSON.parse(myArray);


        this.setState({
            token: d

        })

        console.log(this.state.token);

    }


    onValueChange(value) {
        this.setState({
            selected: value
        });
    }
    //Validation
    Validation(){
        if(this.state.Last==''){
            Messages.messageName(Strings.WARNING, Strings.INVOICE_NUMBER, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        }else if(this.state.Prefix==''){
            Messages.messageName(Strings.WARNING, Strings.PREFIX, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        }else{
            this.createCustomerInvoiceNumber()
        }

    }

    //create customer invoice number  ex :- (BC001)
    createCustomerInvoiceNumber() {
        this.setState({
            spinner: true,
        });
        var data = JSON.stringify({
            "lastKey": this.state.Last,
            "prefix": this.state.Prefix

        });

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + '/api/uni-key/CUSTOMER_INVOICE_NUMBER/unique',
                    {
                        method: "POST",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },
                        body: data
                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        Messages.messageName(Strings.WARNING_SUCESS, Strings.INVOICE_NUMBER_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                        console.log(JSON.stringify(responseJson));

                    })


                    .catch(error => {
                        console.log(error);
                        Messages.messageName(Strings.WARNING_SUCESS, Strings.INVOICE_NUMBER_FAIL, Strings.ICON[2], Strings.TYPE[2], Colors.SUCCESS_ALERT);
                    });

            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.createCustomerInvoiceNumber()

                            }
                        },
                    ],
                    { cancelable: true }
                );
            }
        });

    }

    render() {

        return (
            <View style={styles.container}>
                <ImageBackground
                    source={require("../../assets/newBack2.png")}
                    style={{ width: "100%", height: "100%" }}>


                    <View style={{ backgroundColor: '#126F85', width: '100%', height: 60, borderBottomLeftRadius: 50 }}>
                        <View style={{ backgroundColor: '#126F85', width: 50, top: 13, borderRadius: 60, left: 25, position: "absolute", top: 8 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('InvoiceScreen')}>
                                <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                        <Image source={require("../../assets/newBack3.png")} style={{ width: 20, height: 20 }}></Image>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <Text style={{ fontFamily: 'Ubuntu-Medium', color: '#ffff', justifyContent: "center", textAlign: "center", fontSize: 20, position: "absolute", top: 10, left: 100, fontWeight: "bold" }}>Invoice Settings</Text>
                    </View>
                    <StatusBar barStyle="light-content" hidden={false} backgroundColor={'#126F85'} />
                    <Content contentContainerStyle={styles.content}>

                        <Form style={styles.form}>
                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={{ width: 25, height: 25, left: 10 }}
                                    source={require('../../assets/branch3.png')}
                                />
                                <Picker
                                    note
                                    mode="dropdown"
                                    style={{ width: 100, color: '#ffffff80', left: 10 }}
                                    selectedValue={this.state.selected}
                                    onValueChange={this.onValueChange.bind(this)}
                                >
                                    <Picker.Item label="Select Branch" value="" />
                                    <Picker.Item label="Own" value="null" />

                                </Picker>
                            </Item>
                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={{ width: 25, height: 25, left: 10 }}
                                    source={require('../../assets/nextinv.png')}
                                />
                                <Input
                                    style={styles.input}
                                    keyboardType={'numeric'}
                                    placeholder="Next Invoice No"
                                    placeholderTextColor="#ffffff80"
                                    autoCapitalize="none"
                                    value={this.state.Last}
                                    onChangeText={Last => this.setState({ Last })}
                                />
                            </Item>
                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={{ width: 25, height: 25, left: 10 }}
                                    source={require('../../assets/prefix.png')}
                                />
                                <Input
                                    style={styles.input}
                                    placeholder="prefix"
                                    placeholderTextColor="#ffffff80"
                                    autoCapitalize="none"
                                    value={this.state.Prefix}
                                    onChangeText={Prefix => this.setState({ Prefix })}
                                />
                            </Item>
                            <View style={styles.buttonContainer}>
                                <Button
                                    style={styles.button}

                                    onPress={() => this.Validation()}
                                    hasText
                                    block
                                    large
                                    dark
                                    rounded
                                >
                                    <Text style={styles.loginText}>Save</Text>
                                </Button>

                            </View>
                        </Form>


                    </Content>

                </ImageBackground>
            </View>



        );
    }
}