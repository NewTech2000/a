import React, { Component } from 'react';
import { View, TextInput, Image, TouchableOpacity, ImageBackground, StatusBar, Alert, BackHandler, AsyncStorage, Vibration, PixelRatio,Dimensions } from 'react-native';
import {
    Item,
    Input,
    Text,
    Header,
    Left,
    Right,
    Fab,
    Label,
} from 'native-base';
import styles from './ManageCustomerScreenStyle';
import Colors from '../../resources/Colors';
import AwesomeAlert from 'react-native-awesome-alerts';
import Modal from 'react-native-modalbox';
import NetInfo from "@react-native-community/netinfo";
import Strings from '../../resources/Strings';
import Messages from '../../resources/Message';
import Spinner from "react-native-spinkit";
import { FlatGrid } from 'react-native-super-grid';
import { showMessage, } from "react-native-flash-message";
import { ScrollView } from 'react-native-gesture-handler';

const DURATION = 40;
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
export default class ManageCustomer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            customBackgroundDialog: false,
            defaultAnimationDialog: false,
            scaleAnimationDialog: false,
            scaleAnimationDialog2: false,
            slideAnimationDialog: false,

            token: '',
            data: [],
            name: "",
            cName: '',
            nic: '',
            Cnic: '',
            contact: '',
            Ccontact: '',
            address: '',
            Caddress: '',
            email: '',
            Cemail: '',
            spinner: false,
            searchText: "",
            showTheThing: true,
            cid: 0,
            Ccid: "",
            nodata: false,
            moreFields: false,
            moreButton: true,
            showAlert: false,
        };

    }

    //getting auth code from AsyncStorage
    showData = async () => {
        let myArray = await AsyncStorage.getItem('myArray');
        let d = JSON.parse(myArray);

        this.setState({
            token: d

        })
        console.log(this.state.token);
        this.getAllCustomers()

    }


    //no data iamge
    nodataImage() {
        if (this.state.nodata) {
            return (
                <View style={{ top: 60, justifyContent: 'center', alignItems: 'center', }}>
                    <Image style={{ width: 200, height: 200, borderRadius: 100 }}
                        source={require("../../assets/no_data_found.png")}
                    />

                </View>
            )
        }
    }

    //when close model , visible button
    onClose = () => {
        this.setState({
            showTheThing: true
        })
    }



    anima() {
        this.setState({
            showTheThing: false
        })
        this.refs.modal2.open()
    }

    Test(name) {
        const cusName = name
        this.props.navigation.navigate('CartScreen', { text1: cusName });
    }

    search(text) {
        this.state.searchText = text;
        this.getAllCustomers()
    }

    //get all customers from api
    getAllCustomers() {
        this.setState({
            spinner: true
        })
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + "/api/customer?page=0&limit=1000&text=" + this.state.searchText,
                    {
                        method: "GET",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },

                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        this.setState({
                            spinner: false
                        })
                        console.log("customers" + JSON.stringify(responseJson));

                        this.setState({
                            data: responseJson.content
                        })
                        if (responseJson.content.length == 0) {
                            this.setState({
                                nodata: true
                            })
                        } else {
                            this.setState({
                                nodata: false
                            })
                        }

                    })


                    .catch(error => {
                        console.log(error);

                        this.setState({
                            spinner: false
                        })

                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.getAllCustomers()

                            }
                        },
                    ],
                    { cancelable: true }
                );
            }
        });

    }

    visibleField() {
        this.setState({
            moreFields: true,
            moreButton: false
        })
    }

    visibleFalseField() {
        this.setState({
            moreFields: false,
            moreButton: true
        })
    }

    //show alert hide and show method

    showAlert = () => {
        this.setState({
            showAlert: true
        });
        // this.refs.sideMenu.close()
    };

    hideAlert = () => {
        this.setState({
            showAlert: false
        });
    };

    //Email Validation
    emailvalidate(text) {
        const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(text) === false) {
            return false;
        } else {
            return true;
        }
    }

    //Mobile Validation
    mobilevalidate(text) {
        const reg = /^(?:0|94|\+94)?(?:(11|21|23|24|25|26|27|31|32|33|34|35|36|37|38|41|45|47|51|52|54|55|57|63|65|66|67|81|912|91)(0|2|3|4|5|7|9)|7(0|1|2|5|6|7|8)\d)\d{6}$/;
        if (reg.test(text) === false) {
            return false;
        } else {
            return true;
        }
    }
    namevalidate(text) {
        const reg = /^[a-zA-Z ]{2,30}$/;
        if (reg.test(text) === false) {
            return false;
        } else {
            return true;
        }
    }

    nicvalidate(text) {
        const reg = /^([0-9]{9}[x|X|v|V]|[0-9]{12})$/;
        if (reg.test(text) === false) {
            return false;
        } else {
            return true;
        }
    }

    //save customer

    Validation() {

        if (this.state.moreFields == false) {
            if (this.namevalidate(this.state.name) === false) {
                Messages.messageName(Strings.WARNING_FAILED, Strings.CUSTOMER_NAME, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);

            } else if (this.mobilevalidate(this.state.contact) === false) {
                Messages.messageName(Strings.WARNING_FAILED, Strings.CUSTOMER_CONTACT, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
            } else {
                this.saveCustomer()
            }

        } else {
            if (this.state.moreFields == true) {
                if (this.namevalidate(this.state.name) === false) {
                    Messages.messageName(Strings.WARNING_FAILED, Strings.CUSTOMER_NAME, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);

                } else if (this.mobilevalidate(this.state.contact) === false) {
                    Messages.messageName(Strings.WARNING_FAILED, Strings.CUSTOMER_CONTACT, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);

                } else if (this.nicvalidate(this.state.nic) === false) {
                    Messages.messageName(Strings.WARNING_FAILED, Strings.CUSTOMER_NIC, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);

                } else if (this.emailvalidate(this.state.email) === false) {
                    Messages.messageName(Strings.WARNING_FAILED, Strings.CUSTOMER_EMAIL, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);

                } else if (this.state.address == "") {
                    Messages.messageName(Strings.WARNING_FAILED, Strings.CUSTOMER_ADDRESS, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);

                } else {
                    this.saveCustomer()
                }
            }
        }
    }

    saveCustomer() {
        this.setState({
            spinner: true,
        });
        var data = JSON.stringify({

            "address": this.state.address,
            "birthDate": {
                "basic": "1990-5-5"

            },
            "contactNumber": this.state.contact,
            "creditAmountLimit": 0,
            "creditDayLimit": 0,
            "description": "string",
            "email": this.state.email,
            "name": this.state.name,
            "nicNumber": this.state.nic,
            "totalCreditAmount": 0

        });

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + '/api/customer',
                    {
                        method: "POST",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },
                        body: data
                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        this.getAllCustomers()
                        this.refs.modal2.close()
                        Messages.messageName(Strings.WARNING_SUCESS, Strings.CUSTOMER_ADDED_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                        console.log(JSON.stringify(responseJson));

                        this.setState({
                            scaleAnimationDialog: false,
                            spinner: false,
                            contact: "",
                            email: "",
                            name: "",
                            nic: "",
                            address: "",
                        });
                    })

                    .catch(error => {
                        console.log(error);
                        this.setState({
                            spinner: false,
                        });
                        Messages.messageName(Strings.WARNING_SUCESS, Strings.CUSTOMER_ADDED_Fail, Strings.ICON[2], Strings.TYPE[2], Colors.SUCCESS_ALERT);


                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.saveCustomer()

                            }
                        },
                    ],
                    { cancelable: true }
                );
            }
        });

    }





    //delete customer
    deleteCustomer() {
        // Alert.alert(
        //     'Confirm',
        //     'Are you sure you want to do this ?',
        //     [
        //         { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        //         {
        //             text: 'Yes', onPress: () => {

        this.setState({
            spinner: true,
            showAlert: false,


        });

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + '/api/customer/' + this.state.Ccid,
                    {
                        method: "DELETE",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },
                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        this.setState({
                            spinner: false,


                        });

                        Messages.messageName(Strings.WARNING_SUCESS, Strings.ITEM_DELETE_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.WARNING_ALERT);
                        console.log(JSON.stringify(responseJson));
                        this.getAllProduct()
                        this.refs.modal4.close()
                    })

                    .catch(error => {
                        this.setState({
                            spinner: false,
                        });
                        console.log(error);
                        this.refs.modal4.close()
                        this.getAllCustomers()
                        // Messages.messageName(Strings.WARNING_SUCESS, Strings.ITEM_DELETE_FAIL, Strings.ICON[2], Strings.TYPE[2]);
                        Messages.messageName(Strings.WARNING_SUCESS, Strings.CUSTOMER_DELETE_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                        console.log(JSON.stringify(responseJson));

                    });

            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false,
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.deleteCustomer()

                            }
                        },
                    ],
                    { cancelable: true }
                );
            }
        });

        //             }
        //         },
        //     ],
        //     { cancelable: true }
        // );
    }

    onPressAlert() {
        Messages.messageName("Need", Strings.LONG_TAP, Strings.ICON[2], Strings.TYPE[2], Colors.DARK_BLACK_TEXT_COLOR);
    }

    closeModel2() {
        this.refs.modal2.close()
    }

    closeOneItemModel4() {
        this.refs.modal4.close()
    }
    closeOneItemModel2() {
        this.refs.modal2.close()
    }
    editSelectedCustomer = (item) => {

        this.setState({
            Ccid: item.id,
            cName: item.name,
            Ccontact: item.contactNumber,
            Cemail: item.email,
            Cnic: item.nicNumber,
            Caddress: item.address
        })
        this.setState({
            showTheThing: false
        })
        this.refs.modal4.open()
        Vibration.vibrate(DURATION);
    }


    // Validation Delete Customer Form
    ValidationUpdate() {

        if (this.namevalidate(this.state.cName) === false) {
            Messages.messageName(Strings.WARNING_FAILED, Strings.CUSTOMER_NAME, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);

        } else if (this.mobilevalidate(this.state.Ccontact) === false) {
            Messages.messageName(Strings.WARNING_FAILED, Strings.CUSTOMER_CONTACT, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);

        } else if (this.nicvalidate(this.state.Cnic) === false) {
            Messages.messageName(Strings.WARNING_FAILED, Strings.CUSTOMER_NIC, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);

        } else if (this.emailvalidate(this.state.Cemail) === false) {
            Messages.messageName(Strings.WARNING_FAILED, Strings.CUSTOMER_EMAIL, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);

        } else if (this.state.Caddress == "") {
            Messages.messageName(Strings.WARNING_FAILED, Strings.CUSTOMER_ADDRESS, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        } else {
            this.UpdateCustomer()
        }
    }

    //update customer
    UpdateCustomer() {
        this.setState({
            spinner: true,

        });
        var data = JSON.stringify({

            address: this.state.Caddress,
            birthDate: {
                "basic": "1990-5-5"

            },
            contactNumber: this.state.Ccontact,
            creditAmountLimit: 0,
            creditDayLimit: 0,
            description: "good",
            email: this.state.Cemail,
            name: this.state.cName,
            nicNumber: this.state.Cnic,
            totalCreditAmount: 0

        });
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + '/api/customer/' + this.state.Ccid,
                    {
                        method: "PUT",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },
                        body: data
                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        this.getAllCustomers()
                        this.refs.modal2.close()
                        Messages.messageName(Strings.WARNING_SUCESS, Strings.CUSTOMER_UPDATE_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.WARNING_ALERT);
                        console.log(JSON.stringify(responseJson));

                        this.setState({
                            scaleAnimationDialog: false,
                            spinner: false,
                            contact: "",
                            name: "",
                            email: "",
                            address: "",
                            nic: "",
                        });
                    })

                    .catch(error => {
                        this.refs.modal4.close()
                        this.getAllCustomers()
                        Messages.messageName(Strings.WARNING_SUCESS, Strings.CUSTOMER_UPDATE_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                        this.setState({
                            spinner: false,
                        });
                        console.log(JSON.stringify(responseJson));
                        console.log(error);

                        // Messages.messageName(Strings.WARNING_SUCESS, Strings.CUSTOMER_UPDATE_FAIL, Strings.ICON[2], Strings.TYPE[2]);


                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.UpdateCustoer()

                            }
                        },
                    ],
                    { cancelable: true }
                );
            }
        });

    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Back Handler Press
    onButtonPress = () => {

    }


    handleBackButton = () => {
        this.props.navigation.goBack();
        return true;
    };



    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }



    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        this.showData()


    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    render() {

        const { showAlert } = this.state;

        return (
            <ImageBackground
                source={require("../../assets/newBack2.png")}
                style={{ width: "100%", height: "100%" }}>
                <View style={{ backgroundColor: '#126F85', width: '100%', height: 60, borderBottomLeftRadius: 50 }}>
                    <View style={{ backgroundColor: '#126F85', width: 50, top: 13, borderRadius: 60, left: 25, position: "absolute", top: 8 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('HomeScreen')}>
                            <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                    <Image source={require("../../assets/newBack3.png")} style={{ width: 20, height: 20 }}></Image>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Text style={{ fontFamily: 'Ubuntu-Medium', color: '#ffff', justifyContent: "center", textAlign: "center", fontSize: 20, position: "absolute", top: 10, left: 100, fontWeight: "bold" }}>Manage customer</Text>
                </View>
                <StatusBar barStyle="light-content" hidden={false} backgroundColor={'#126F85'} />

                {/* Delete Customer Modal */}


                <Modal style={[styles.modalDelete]} position={"bottom"} ref={"modal4"} onClosed={this.onClose} >
                    <View style={{ backgroundColor: '#126F85', width: '100%', height: 60, borderBottomLeftRadius: 50 }}>
                        <View style={{ backgroundColor: '#126F85', width: 50, top: 13, left: 25, position: "absolute", top: 8 }}>
                            <TouchableOpacity onPress={() => this.closeOneItemModel4()}>
                                <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                        <Image source={require("../../assets/delete1.png")} style={{ width: 20, height: 20 }}></Image>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <Text style={{ fontFamily: 'Ubuntu-Medium', color: '#ffff', justifyContent: "center", textAlign: "center", fontSize: 20, position: "absolute", top: 10, left: 100, fontWeight: "bold" }}>Edit Customer</Text>
                    </View>
                    <View style={{ position: "absolute", right: 20, top: 8 }}>
                        <TouchableOpacity onPress={() => this.showAlert()}>
                            <View style={{ justifyContent: 'center', alignItems: 'center', elevation: 10, width: 30, height: 30, backgroundColor: Colors.PRIMARY_COLOR, borderRadius: 100 }}>
                                <Image style={{ width: 20, height: 20 }} source={require('../../assets/re.png')} />
                            </View>
                        </TouchableOpacity>
                    </View>
                    {/* <View style={{ width: 50, justifyContent: 'flex-end', alignItems: 'flex-end', position: "absolute", right: 10, top: 70 }}>
                            <TouchableOpacity style={{}} onPress={() => this.showAlert()}>

                                <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: Colors.PRIMARY_COLOR, borderRadius: 100 }}>
                                    <Image style={{ width: 20, height: 20 }} source={require('../../assets/re.png')} />
                                </View>
                            </TouchableOpacity>
                        </View> */}

                    <View style={{
                        flex: 1,
                        justifyContent: 'flex-end',
                        bottom: 100,
                        padding: PixelRatio.getPixelSizeForLayoutSize(12),
                    }}>

                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/description.png')}
                            />
                            <Input
                                style={styles.input}
                                placeholder="Name"
                                placeholderTextColor="#ffffff80"
                                autoCapitalize="none"
                                value={this.state.cName}
                                onChangeText={cName => this.setState({ cName })}
                            />
                        </Item>
                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/mobilenum.png')}
                            />
                            <Input
                                style={styles.input}
                                keyboardType="numeric"
                                maxLength={10}
                                placeholder="Contact"
                                value={this.state.Ccontact}
                                placeholderTextColor="#ffffff80"
                                autoCapitalize="none"
                                onChangeText={Ccontact => this.setState({ Ccontact })}
                            />
                        </Item>
                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/nic.png')}
                            />
                            <Input
                                style={styles.input}
                                placeholder="Nic"
                                value={this.state.Cnic}
                                placeholderTextColor="#ffffff80"
                                autoCapitalize="none"
                                onChangeText={Cnic => this.setState({ Cnic })}
                            />
                        </Item>
                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/price.png')}
                            />
                            <Input
                                style={styles.input}
                                placeholder="Email"
                                placeholderTextColor="#ffffff80"
                                autoCapitalize="none"
                                value={this.state.Cemail}
                                onChangeText={Cemail => this.setState({ Cemail })}
                            />
                        </Item>
                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/address.png')}
                            />
                            <Input
                                style={styles.input}
                                placeholder="Address"
                                multiline={true}
                                placeholderTextColor="#ffffff80"
                                autoCapitalize="none"
                                value={this.state.Caddress}
                                onChangeText={Caddress => this.setState({ Caddress })}
                            />
                        </Item>
                    </View>

                    <View style={styles.inputTextContainerDelete}>
                        <TouchableOpacity
                            style={[styles.buttonContainerProcess, styles.ProcessButton]}
                            onPress={() => this.ValidationUpdate()}
                        >
                            <Text style={styles.ProcessText}>Update</Text>
                        </TouchableOpacity>
                    </View>

                    <AwesomeAlert
                        show={showAlert}
                        showProgress={false}
                        title="Confirm"
                        message="       Are you sure you want to delete this ?      "
                        closeOnTouchOutside={false}
                        closeOnHardwareBackPress={false}
                        showCancelButton={true}
                        showConfirmButton={true}
                        cancelText="    No    "
                        confirmText="   Yes   "
                        confirmButtonColor="#126F85"
                        onCancelPressed={() => {
                            this.hideAlert();
                        }}
                        onConfirmPressed={() => {
                            this.deleteCustomer();
                        }}
                    />
                </Modal>




                {/* Add Customer Modal */}


                <Modal style={[styles.modal4]} position={"bottom"} ref={"modal2"} onClosed={this.onClose} backdropPressToClose={false}>
                    <TouchableOpacity style={{ top: -20, justifyContent: "center", alignContent: "center", alignItems: "center" }} onPress={() => this.closeModel2()}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: '#ffff', borderRadius: 100, }}>
                            <Image style={{ width: 40, height: 40 }} source={require('../../assets/closemodl.png')} />
                        </View>
                    </TouchableOpacity>
                    <ScrollView>
                        <View style={{
                            flex: 1,
                            // justifyContent: 'flex-end',
                            padding: PixelRatio.getPixelSizeForLayoutSize(12),
                        }}>
                            {/* <View style={{ position: "absolute", width: 50, right: 150, }}>
                            <TouchableOpacity onPress={() => this.closeModel2()}>
                                <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: '#ffff', borderRadius: 100, }}>
                                    <Image style={{ width: 40, height: 40 }} source={require('../../assets/closemodl.png')} />
                                </View>
                            </TouchableOpacity>
                        </View> */}






                            <View style={{ bottom: 10 }}>
                                <Text style={{ color: Colors.PRIMARY_COLOR, fontSize: 20, textAlign: 'left', fontWeight: 'bold', top: -10 }}>Add Customer</Text>
                                <Item
                                    style={styles.item}
                                    rounded
                                    last
                                >
                                    <Image
                                        style={styles.place}
                                        source={require('../../assets/description.png')}
                                    />
                                    <Input
                                        style={styles.input}
                                        placeholder="Name"
                                        placeholderTextColor="#ffffff80"
                                        autoCapitalize="none"
                                        value={this.state.name}
                                        onChangeText={name => this.setState({ name })}
                                    />
                                </Item>
                                <Item
                                    style={styles.item}
                                    rounded
                                    last
                                >
                                    <Image
                                        style={styles.place}
                                        source={require('../../assets/mobilenum.png')}
                                    />
                                    <Input
                                        style={styles.input}
                                        keyboardType="numeric"
                                        maxLength={10}
                                        placeholder="Contact"
                                        placeholderTextColor="#ffffff80"
                                        autoCapitalize="none"
                                        value={this.state.contact}
                                        onChangeText={contact => this.setState({ contact })}
                                    />
                                </Item>

                                {this.state.moreButton &&
                                    <View>
                                        <Label></Label>
                                        <TouchableOpacity onPress={() => this.visibleField()}>
                                            <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
                                                <Text style={{ fontWeight: 'bold', color: Colors.PRIMARY_COLOR }}>____________More Details____________</Text>
                                            </View>
                                        </TouchableOpacity>
                                        <Label></Label>
                                    </View>
                                }

                                {this.state.moreFields &&
                                    <View>
                                        <Item
                                            style={styles.item}
                                            rounded
                                            last
                                        >
                                            <Image
                                                style={styles.place}
                                                source={require('../../assets/nic.png')}
                                            />
                                            <Input
                                                style={styles.input}
                                                placeholder="Nic"
                                                placeholderTextColor="#ffffff80"
                                                autoCapitalize="none"
                                                value={this.state.nic}
                                                onChangeText={nic => this.setState({ nic })}
                                            />
                                        </Item>
                                        <Item
                                            style={styles.item}
                                            rounded
                                            last
                                        >
                                            <Image
                                                style={styles.place}
                                                source={require('../../assets/email.png')}
                                            />
                                            <Input
                                                style={styles.input}
                                                placeholder="Email"
                                                placeholderTextColor="#ffffff80"
                                                autoCapitalize="none"
                                                value={this.state.email}
                                                onChangeText={email => this.setState({ email })}
                                            />
                                        </Item>
                                        <Item
                                            style={styles.item}
                                            rounded
                                            last
                                        >
                                            <Image
                                                style={styles.place}
                                                source={require('../../assets/address.png')}
                                            />
                                            <Input
                                                style={styles.input}
                                                placeholder="Address"
                                                multiline={true}
                                                placeholderTextColor="#ffffff80"
                                                autoCapitalize="none"
                                                value={this.state.address}
                                                onChangeText={address => this.setState({ address })}
                                            />
                                        </Item>
                                        <View>
                                            <Label></Label>
                                            {/* <Label></Label> */}
                                            <TouchableOpacity onPress={() => this.visibleFalseField()}>
                                                <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
                                                    <Text style={{ fontWeight: 'bold', color: Colors.PRIMARY_COLOR }}>____________Close____________</Text>
                                                </View>
                                            </TouchableOpacity>
                                            {/* <Label></Label> */}
                                        </View>
                                    </View>



                                }

                            </View>

                            <View style={styles.inputTextContainer2}>
                                <TouchableOpacity
                                    style={[styles.buttonContainerProcess, styles.ProcessButton]}
                                    onPress={() => this.Validation()}
                                >
                                    <Text style={styles.ProcessText}>Save</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ScrollView>

                </Modal>

                {/* End Cusdtomer Add Modal */}


                <View style={styles.viewStyle2}>
                    <View style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        flexDirection: 'row',
                    }}>
                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/search.png')}
                            />

                            <Input
                                style={styles.input}
                                placeholder="Search"
                                placeholderTextColor="#ffffff80"
                                autoCapitalize="none"
                                onChangeText={text => this.search(text)}
                            // value={this.state.text}
                            />
                        </Item>
                    </View>
                </View>
                <Label></Label>


                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Spinner
                        // style={{top:40}}
                        isVisible={this.state.spinner}
                        type={'ThreeBounce'}
                        textStyle={{ color: 'white' }}
                        size={70}
                        color={Colors.FLAT_TEXT_COLOR}
                    />
                </View>
                {this.nodataImage()}
                <FlatGrid
                    itemDimension={130}
                    items={this.state.data}
                    style={styles.gridView}
                    renderItem={({ item, index }) => (
                        <TouchableOpacity activeOpacity={0.8} onPress={() => this.onPressAlert()} onLongPress={() => this.editSelectedCustomer(item)}>
                            <View style={[styles.itemContainer, { backgroundColor: Colors.BACKGROUD_WHITE }]}>
                                <Text numberOfLines={1} style={styles.itemName}>{item.name}</Text>
                                <Text style={styles.itemName2}>{item.contactNumber}</Text>

                            </View>
                        </TouchableOpacity>
                    )}
                />

                {this.state.showTheThing &&
                    <Fab
                        active={this.state.active}
                        direction="up"
                        containerStyle={{}}
                        style={styles.backGround}
                        position="bottomRight"
                        onPress={() => this.anima()}>
                        <Image source={require("../../assets/plus.png")} style={styles.ToucherbleIconStyle2} />
                    </Fab>
                }
            </ImageBackground>

        );
    }
}










