import {
    PixelRatio,
    StyleSheet,
} from 'react-native';
import Colors from '../../resources/Colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#0a1142',
    },
    content: {
        flex: 1,
        justifyContent: 'flex-end',
        padding: PixelRatio.getPixelSizeForLayoutSize(12),
    },
    logoBack: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 150,
        width: 150,
        top: -30,
        borderRadius: 100,
        backgroundColor: '#ffffff90'
    },
    viewStyle2: {
        justifyContent: 'center',
        top:10
        // marginTop: 10,
        // padding: ,
    },
    modalDelete: {
        // height: 500,
        height: hp('100%'),
        // borderTopRightRadius: 30,
        // borderTopLeftRadius: 30,
        backgroundColor: 'white'
    },
    modal4: {
        height: hp('68%'),
        borderTopRightRadius: 30,
        borderTopLeftRadius: 30,
        backgroundColor: 'white'
    },


    gridView: {
        flex: 1,
    },
    itemContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 30,
        borderColor: Colors.PRIMARY_COLOR,
        borderWidth: 2,
        padding: 10,
        height: 100,
        shadowColor: Colors.DARK_BLACK_TEXT_COLOR,
        shadowOpacity: 0.8,
        elevation: 7,
        shadowRadius: 30,
        shadowOffset: { width: 1, height: 13 }

    },
    itemName: {
        fontSize: hp('2.3%'),
        color: 'black',
        fontWeight: 'bold',
    },
    itemName2: {
        fontSize: hp('1.8%'),
        color: 'black',
        fontWeight: 'bold',
    },
    itemCode: {
        fontWeight: '600',
        fontSize: 12,
        color: '#fff',
    },
    MainContainer: {
        justifyContent: 'center',
        flex: 1,
        paddingTop: 30,
    },
    imageThumbnail: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 100,
    },
    textInputStyle: {
        height: 50,
        borderWidth: 1,
        paddingLeft: 10,
        borderRadius: 10,
        borderColor: Colors.PRIMARY_COLOR,
        backgroundColor: Colors.PRIMARY_COLOR,
    },
    viewStyle: {
        justifyContent: 'center',
        padding: 16,
    },
    ProcessText: {
        fontSize: 18,
        fontWeight: 'bold',
        justifyContent: 'center',
        alignItems: 'center',
        color: 'white'
    },
    ProcessButton: {
        backgroundColor: Colors.PRIMARY_COLOR
    },
    inputTextContainer: {
        left: 19
    },
    inputTextContainer2: {
        justifyContent: 'center',
        alignItems: 'center',
        bottom: 20,
    },

    inputTextContainerDelete: {
        justifyContent: 'center',
        alignItems: 'center',
        bottom:60,
       
    },


    inputTextContainer3: {
        justifyContent: 'center',
        alignItems: 'center',
        top: 30

    },
   buttonContainerProcess: {
        height: 50,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 30,
        marginTop: 20,
        width: wp('85%'),
        borderRadius: 30,
        shadowColor: Colors.DARK_BLACK_TEXT_COLOR,
        shadowOpacity: 0.8,
        elevation: 6,
        shadowRadius: 15,
        shadowOffset: { width: 1, height: 13 }

    },
    input: {
        color: 'white',
        left: 20,
        maxWidth: '82%',
    },
    item: {
        marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
        backgroundColor: Colors.PRIMARY_COLOR,
        borderColor: Colors.PRIMARY_COLOR,
    },
    form: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    TextInputStyle2: {
        height: 40,
        borderBottomColor: Colors.DARK_BLACK_TEXT_COLOR,
        width: 200,
        marginBottom: 10,
        borderBottomWidth: 1
    },
    backGround: {
        backgroundColor: Colors.PRIMARY_COLOR,

    },

    headerTextStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: hp('3.3%'),
        color: 'white',
    },
    header: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerStyle: {
        backgroundColor: Colors.PRIMARY_COLOR,
        shadowOpacity: 0.75,
        shadowRadius: 5,
        shadowOffset: { height: 0, width: 0 },

    },
    ToucherbleIconStyle: { color: Colors.PRIMARY_COLOR },
    ToucherbleIconStyle2: { height: 30, width: 30 },

    logoContainer: {

        justifyContent: 'center',
        alignItems: 'center',
        marginTop: PixelRatio.getPixelSizeForLayoutSize(40),
        marginBottom: PixelRatio.getPixelSizeForLayoutSize(40),
    },
    logoText: {
        color: Colors.DARK_BLACK_TEXT_COLOR,
        fontSize: hp('2.4%'),
        fontWeight: '700',
        marginTop: 10
    },
    logoText1: {
        color: Colors.DARK_BLACK_TEXT_COLOR,
        fontSize: 15,
        fontWeight: '700',

    },
    backgroundImage: { width: "100%", height: "100%" },

    logo: {
        left: -3,
        height: PixelRatio.getPixelSizeForLayoutSize(150),
        width: PixelRatio.getPixelSizeForLayoutSize(150),
        resizeMode: 'contain',
    },
    place: {
        width: 25,
        height: 25,
        left: 10
    },
    buttonContainer: {
        flex: 1,
        marginTop: PixelRatio.getPixelSizeForLayoutSize(10),
    },
    forgotPasswordContainer: {
        alignItems: 'center',
        bottom: -150
    },
    forgotPasswordText: {
        color: 'white',
        fontSize: 16,
    },
    button: {
        marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
        backgroundColor: Colors.PRIMARY_COLOR,
    },
    loginText: {
        color: '#fff',
        fontSize: 20,
        fontWeight: 'bold',
    },
    signupContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: PixelRatio.getPixelSizeForLayoutSize(3),
    },
    dontHaveAccountText: {
        color: '#bec0ce',
        fontSize: 16,
    },
    signupText: {
        color: '#000',
        fontSize: 10,
    },
});


export default styles;