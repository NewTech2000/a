import React, { Component } from 'react';
import { View, Image, TouchableOpacity, StatusBar, AsyncStorage, Alert, BackHandler } from 'react-native';
import {
    Button,
    Content,
    Form,
    Item,
    Input,
    Text,
    Label,
} from 'native-base';

import styles from './LoginScreenStyle';
import Messages from '../../resources/Message';
import Strings from '../../resources/Strings';
import NetInfo from "@react-native-community/netinfo";
import Colors from '../../resources/Colors';
import Spinner from 'react-native-loading-spinner-overlay';
import { ScrollView } from "react-native-gesture-handler";
import { NavigationActions, StackActions } from 'react-navigation'

export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userName: "",
            password: "",
            token: "",
            merchantData: null,
            spinner: false,
            productImage:true,
            companyDetails: []
        };
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        // this.props.navigation.goBack(null);
        // return true;
        // Alert.alert("working")
    }

    //validate name & pasword field
    validation() {
        if (this.state.userName == "") {
            Messages.messageName(Strings.WARNING, Strings.USER_NAME_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        } else if (this.state.password == "") {
            Messages.messageName(Strings.WARNING, Strings.PASSWORD_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        } else {
            this.gettingAuth()
        }
    }


    //store auth code in AsyncStorage & navigate to home screen
    _storeData = async () => {
        this.setState({
            spinner: false,
        });
        let obj = {
            userName: this.state.userName,
            password: this.state.password,
        }
        try {
            await AsyncStorage.setItem('companyDetails', JSON.stringify(this.state.companyDetails));
            await AsyncStorage.setItem('loginDetails2', JSON.stringify(obj))
            AsyncStorage.setItem('productImage', JSON.stringify(this.state.productImage));
            await AsyncStorage.setItem('myArray', JSON.stringify(this.state.token), () => {
                Messages.messageName(Strings.WARNING_SUCESS, Strings.LOGIN_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'HomeScreen' })],
                });
                this.props.navigation.dispatch(resetAction);
            });

        } catch (error) {
            console.error(error);
        }
    }

    //store user name and password when user is not activeted
    _storUserDetails = async () => {
        this.setState({
            spinner: false,
        });
        let obj = {
            userName: this.state.userName,
            password: this.state.password,
        }
        try {
            await AsyncStorage.setItem('loginDetails2', JSON.stringify(obj))
            await AsyncStorage.setItem('productImage', JSON.stringify(true));
            await AsyncStorage.setItem('loginDetails', JSON.stringify(obj), () => {
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'PendingScreen' })],
                });
                this.props.navigation.dispatch(resetAction);
            });

        } catch (error) {
            console.error(error);
        }
    }

    //check availability user 
    gettingAuth() {
        this.setState({
            spinner: true,
        });
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + '/api/auth?username=' + this.state.userName + '&password=' + this.state.password,
                    {
                        method: "POST",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",

                        },

                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        console.log("Login" + JSON.stringify(responseJson))
                        this.setState({
                            spinner: false,
                        });

                        if (responseJson.errorCode == "USER_NOT_FOUND") {
                            Messages.messageName(Strings.WARNING, Strings.USER_NOT_FOUNd, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);

                        } else if (responseJson.errorCode == "INVALID_CREDENTIALS") {
                            Messages.messageName(Strings.WARNING, Strings.USER_DID_NOT_MATCH, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
                        } else if (responseJson.errorCode == "COMPANY_IS_NOT_ACTIVATED") {
                            this._storUserDetails()
                            Messages.messageName(Strings.WARNING, Strings.USER_NOT_ACTIVE, Strings.ICON[0], Strings.TYPE[0], Colors.PRIMARY_COLOR);
                        }

                        console.log("Login" + JSON.stringify(responseJson.errorMessage))
                        console.log("Login" + JSON.stringify(responseJson.user.company.address))
                        console.log("Login" + JSON.stringify(responseJson.user.company.contactNumber))
                        this.state.companyDetails = responseJson.user
                        console.log("xxxxxxxxxxxxxxxx" + JSON.stringify(this.state.companyDetails))
                        this.state.token = responseJson.accessToken
                        console.log(JSON.stringify(this.state.token));

                        if (this.state.token == null) {
                            // Messages.messageName(Strings.WARNING_SUCESS, Strings.USER_NOT_FOUNd, Strings.ICON[2], Strings.TYPE[2]);
                        } else {
                            this._storeData()
                        }

                    })

                    .catch(error => {
                        console.log(error);
                        this.setState({
                            spinner: false,
                        });

                    });

            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.gettingAuth()

                            }
                        },
                    ],
                    { cancelable: true }
                );
            }
        });
    }

    render() {
        return (
            <ScrollView >
                <StatusBar barStyle="dark-content" hidden={false} backgroundColor={"#85CCD9"} />
                <Spinner
                    visible={this.state.spinner}
                    textContent={'Loading...'}
                    textStyle={{ color: 'white' }}
                    overlayColor={"#0f0e0e90"}
                    animation={"fade"}
                    color={"#00CEFD"}
                />
                <View style={{ width: '100%', height: 300, backgroundColor: '#85CCD9', borderBottomLeftRadius: 100 }}>
                    <View style={styles.logoContainer}>
                        <View style={styles.logoBack}>
                            <Image
                                style={styles.logo}
                                source={require('../../assets/LOGO-FINAL-05.png')}
                            />
                        </View>
                        {/* <Text style={styles.logoText}>Welcome!</Text> */}
                        <Text style={styles.logoText1}>#F E E L   T H E   G R O W T H</Text>
                    </View>
                </View>
                <Content contentContainerStyle={styles.content}>


                    <Label></Label>
                    <Form style={styles.form}>
                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/user.png')}
                            />
                            <Input
                                style={styles.input}
                                placeholder="Username"
                                placeholderTextColor="#ffffff80"
                                autoCapitalize="none"
                                onChangeText={userName => this.setState({ userName })}
                            />
                        </Item>
                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/password.png')}
                            />
                            <Input
                                style={styles.input}
                                placeholder="Password"
                                placeholderTextColor="#ffffff80"
                                onChangeText={password => this.setState({ password })}
                                secureTextEntry
                            />
                        </Item>
                    </Form>

                    <View style={styles.buttonContainer}>
                        <Button
                            style={styles.button}
                            onPress={() => this.validation()}
                            hasText
                            block
                            large
                            dark
                            rounded
                        >
                            <Text style={styles.loginText}>LOGIN</Text>
                        </Button>


                        <View style={styles.signupContainer}>
                            <View style={styles.forgotPasswordContainer}>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate("BusinessCategoryScreen")}>
                                    <Text style={styles.signupText}>Become a member</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Content>
            </ScrollView>
        );
    }
}



