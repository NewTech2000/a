import {
  PixelRatio,
  StyleSheet,
} from 'react-native';
import Colors from '../../resources/Colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#0a1142',
  },
  content: {
    flex: 1,
    justifyContent: 'flex-end',
    padding: PixelRatio.getPixelSizeForLayoutSize(12),
  },
  logoBack: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 150,
    width: 150,
    top: -30,
    borderRadius: 100,
    backgroundColor: '#ffffff90'
  },

  logoContainer: {

    justifyContent: 'center',
    alignItems: 'center',
    top: 60
  },
  logoText: {
    color: Colors.DARK_BLACK_TEXT_COLOR,
    fontSize: hp('3.5%'),
    fontWeight: '700',
    marginTop: 10
  },
  logoText1: {
    color: Colors.DARK_BLACK_TEXT_COLOR,
    fontSize: hp('1.6%'),
    fontWeight: '700',
  },
  backgroundImage: { width: "100%", height: "100%" },

  logo: {
    left: -3,
    width: wp('90%'),
    resizeMode: 'contain',
  },
  form: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  item: {
    marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
    backgroundColor: Colors.PRIMARY_COLOR,
    borderColor: Colors.PRIMARY_COLOR,
  },
  input: {
    color: 'white',
    left: 20,
    maxWidth:'82%'
  },
  place: {
    width: 25,
    height: 25,

  },
  buttonContainer: {
    flex: 1,
    marginTop: PixelRatio.getPixelSizeForLayoutSize(10),
  },
  forgotPasswordContainer: {
    alignItems: 'center',
    marginTop: PixelRatio.getPixelSizeForLayoutSize(2),
  },
  forgotPasswordText: {
    color: 'white',
    fontSize: hp('2%'),
  },
  button: {
    marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  loginText: {
    color: '#ffff',
    fontSize: hp('3%'),
    fontWeight: 'bold',
  },
  signupContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: PixelRatio.getPixelSizeForLayoutSize(3),
  },
  dontHaveAccountText: {
    color: '#bec0ce',
    fontSize: 16,
  },
  signupText: {
    color: '#000',
    fontWeight:'bold',
    fontSize: 16,
    // textDecorationLine: 'underline'
  },
});


export default styles;