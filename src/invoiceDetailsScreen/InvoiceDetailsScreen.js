
import React, { Component } from 'react';
import { TextInput, Image, TouchableOpacity, ImageBackground, BackHandler
    ,StatusBar, Alert, PixelRatio, AsyncStorage,Dimensions } from 'react-native';
import {
    Button,
    Text,
    Header,
    Left,
    Right,
    Label
} from 'native-base';
import styles from './InvoiceDetailsScreenStyle';
import { ScrollView } from 'react-native-gesture-handler'
import Colors from '../../resources/Colors';
import Strings from '../../resources/Strings';
import Message from '../../resources/Message';
import NetInfo from "@react-native-community/netinfo";
import { View } from 'react-native-animatable';
import Spinner from "react-native-spinkit";
import Moment from 'moment';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { BluetoothEscposPrinter, BluetoothManager } from "react-native-bluetooth-escpos-printer";
var dateFormat = require('dateformat');

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

export default class ManageItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            token: '',
            data: [],
            spinner: false,
            searchText: "",
            id: "",
            invoiceId: "",
            invoiceDate: "",
            itemList: [],
            nodata: false,
            bluetoothConnected: 0,
            companyName: "",
            companyContactNumber: "",
            companyAddress: "",
            barcode: false,
            discount:0,
            customer:"",
            paymentMethod:[],
            paidAmount:"",
            payableAmount:"",
            BLPAddress:""
            

        };

    }

    bluetoothConnectedChecking() {
        BluetoothManager.connect(this.state.BLPAddress)
            .then((s) => {

                console.log("connected")
                this.setState({
                    bluetoothConnected: 1,

                })
            },
                (err) => {
                    console.log("connected" + err)
                    this.setState({
                        bluetoothConnected: 0
                    })
                })
    }



    //getting auth code from AsyncStorage
    showData = async () => {
        let user = await AsyncStorage.getItem('companyDetails');
        let parsed = JSON.parse(user);
        let myArray1 = await AsyncStorage.getItem('barcode');
        let d1 = JSON.parse(myArray1);
        let myArray5 = await AsyncStorage.getItem('BLPAddress');
        let d5 = JSON.parse(myArray5);
        this.setState({
            barcode: d1,
            BLPAddress:d5,
            companyName: parsed.company.name,
            companyAddress: parsed.company.address,
            companyContactNumber: parsed.company.customSettings.contact

        })

        let myArray = await AsyncStorage.getItem('myArray');
        let d = JSON.parse(myArray);


        this.setState({
            token: d

        })
        if(this.state.BLPAddress !== null){
            this.bluetoothConnectedChecking()
        }
        this.getInvoiceDetailsById()
        console.log(this.state.token);

    }


    //getting invoice details
    getInvoiceDetailsById() {
        this.setState({
            spinner: true
        })
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + '/api/customer/invoice/' + this.state.invoiceId,
                    {
                        method: "GET",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },

                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        console.log("invoiceData" + JSON.stringify(responseJson));

                        this.setState({
                            invoiceDate: responseJson.invoiceDate.basic,
                            data: responseJson,
                            itemList: responseJson.items,
                            customer:responseJson.customer,
                            paidAmount:responseJson.paidAmount,
                            payableAmount:responseJson.payableAmount,
                            paymentMethod:responseJson.payments,
                            spinner: false
                        })
                        // if (responseJson.content.length == 0) {
                        //     this.setState({
                        //         nodata: true
                        //     })
                        // } else {
                        //     this.setState({
                        //         nodata: false
                        //     })
                        // }

                    })

                    .catch(error => {
                        console.log(error);
                        this.setState({
                            spinner: false
                        })

                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.getInvoiceDetailsById()

                            }
                        },
                    ],
                    { cancelable: true }
                );


            }
        });

    }

    //no data iamge
    nodataImage() {
        if (this.state.nodata) {
            return (
                <View style={{ top: 50, justifyContent: 'center', alignItems: 'center' }}>
                    <Image style={{ width: 200, height: 100 }}
                        source={require("../../assets/norecord-found.png")}
                    />

                </View>
            )
        }
    }

    
    onButtonPress = () => {

    }


    handleBackButton = () => {
        this.props.navigation.goBack(null);
        return true;
    };


    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }



    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        this.showData()


    }

    //getting invoice details
    componentWillMount() {
        this.state.invoiceId = this.props.navigation.getParam('id', '')
        this.setState({
            date: Moment().format('YYYY-MM-D HH:mm:ss')
        })
        this.showData()
    }

    //print receipt confiug
    printRecept = async () => {
        try {

            await BluetoothEscposPrinter.printerInit();
            await BluetoothEscposPrinter.printerLeftSpace(0);
            await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.CENTER);
            await BluetoothEscposPrinter.setBlob(0);
            await BluetoothEscposPrinter.printText("COPY\r\n", {
                encoding: 'GBK',
                codepage: 0,
                widthtimes: 1,
                heigthtimes: 1,
                fonttype: 1
            });
            await BluetoothEscposPrinter.printText("--------------------------------\r\n", {});
            await BluetoothEscposPrinter.printText(this.state.companyName + "\r\n", { encoding: 'GBK', fonttype: 11 });
            await BluetoothEscposPrinter.setBlob(0);
            await BluetoothEscposPrinter.printText(this.state.companyAddress + "\r\n", {
                encoding: 'GBK',
                codepage: 0,
                widthtimes: 0,
                heigthtimes: 0,
                fonttype: 1
            });
            await BluetoothEscposPrinter.printText("Tel :" + this.state.companyContactNumber + "\r\n", {});
            await BluetoothEscposPrinter.printText("--------------------------------\r\n", {});
            await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.LEFT);
            await BluetoothEscposPrinter.printText("Date  : " + this.state.invoiceDate + "\r\n", {});
            await BluetoothEscposPrinter.printText("Customer : " + this.state.custName + "\n\r", {});
            await BluetoothEscposPrinter.printText("cashier : Admin\r\n", {});
            await BluetoothEscposPrinter.printText("Invoice Number :" + this.state.data.invoiceNumber + "\r\n", {});
            await BluetoothEscposPrinter.printText("--------------------------------\r\n", {});
            let columnWidths = [12, 6, 6, 8];
            await BluetoothEscposPrinter.printColumn(columnWidths,
                [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.CENTER, BluetoothEscposPrinter.ALIGN.CENTER, BluetoothEscposPrinter.ALIGN.RIGHT],
                ['Name', "Qty", 'Price', 'Total',], {});
            for (let index = 0; index < this.state.itemList.length; index++) {
                await BluetoothEscposPrinter.printText("\r\n", {});
                await BluetoothEscposPrinter.printColumn(columnWidths,
                    [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.CENTER, BluetoothEscposPrinter.ALIGN.RIGHT],
                    [this.state.itemList[index].name + "", this.state.itemList[index].qty + "", this.state.itemList[index].salePrice + "", this.state.itemList[index].price + ""]
                    , {});

            }
            await BluetoothEscposPrinter.printText("\r\n", {});
            await BluetoothEscposPrinter.printText("--------------------------------\r\n", {});
            await BluetoothEscposPrinter.printText("\r\n", {});
            // await BluetoothEscposPrinter.printText("No of items : " + this.state.qtyCount + "\r\n", {});
            await BluetoothEscposPrinter.printText("Sub Total  :" + this.state.data.subTotalAmount + "\r\n", {});
            await BluetoothEscposPrinter.printText("Discount   : " + this.state.discount + "%" + "\r\n", {});
            await BluetoothEscposPrinter.setBlob(0);
            await BluetoothEscposPrinter.printText("Grand Total :" + this.state.data.grandTotalAmount + ".00" + "\r\n", {
                encoding: 'GBK',
                codepage: 0,
                widthtimes: 3,
                heigthtimes: 3,
                fonttype: 1
            });

            await BluetoothEscposPrinter.printText("paid amount :" + this.state.data.cashGivenAmount + "\r\n", {});
            await BluetoothEscposPrinter.printText("Balance    :" + this.state.data.balanceGivenAmount + "\r\n", {});
            await BluetoothEscposPrinter.printText("--------------------------------\r\n", {});
            {
                this.state.barcode &&
                    await BluetoothEscposPrinter.printBarCode("123456789012", BluetoothEscposPrinter.BARCODETYPE.JAN13, 3, 120, 0, 2);
            }
            await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.CENTER);
            await BluetoothEscposPrinter.printText("Thank you Come again.....!\r\n\r\n\r\n", {});
            await BluetoothEscposPrinter.printText("--------------------------------\r\n", {});
            await BluetoothEscposPrinter.setBlob(0);
            await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.CENTER);
            await BluetoothEscposPrinter.printText("Powered by - Billa cloud\r\n", {
                encoding: 'GBK',
                fonttype: 11
            });
            await BluetoothEscposPrinter.setBlob(0);
            await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.CENTER);
            await BluetoothEscposPrinter.printText("0714770888\r\n", {
                encoding: 'GBK',
                fonttype: 11
            });
            await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.LEFT);
            await BluetoothEscposPrinter.printText("\r\n\r\n\r\n", {});
        } catch (e) {
            console.log(JSON.stringify(e.code));

            if (e.code == "EUNSPECIFIED") {

                Alert.alert(
                    'Connection Error!',
                    'there is something wrong with printer connection',
                    [
                        { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                        {
                            text: 'Navigate to Settings', onPress: () => {


                                this.props.navigation.navigate('PrinterSettingsForInvoice')
                            }
                        },
                    ],
                    { cancelable: true }
                );

            }


        }
    }


    render() {

        return (

            <View style={styles.container}>
                <ImageBackground
                    source={require("../../assets/newBack2.png")}
                    style={{ width: "100%", height: "100%" }}>

                    <View style={{ backgroundColor: '#126F85', width: '100%', height: 60, borderBottomLeftRadius: 50 }}>
                        <View style={{ backgroundColor: '#126F85', width: 50, top: 13, borderRadius: 60, left: 25, position: "absolute", top: 8 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('InvoiceScreen')}>
                                <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                        <Image source={require("../../assets/newBack3.png")} style={{ width: 20, height: 20 }}></Image>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <Text style={{ fontFamily: 'Ubuntu-Medium', color: '#ffff', justifyContent: "center", textAlign: "center", fontSize: 20, position: "absolute", top: 10, left: 100, fontWeight: "bold" }}>Invoice Details</Text>
                        <View style={{ backgroundColor: '#126F85', width: 50, top: 13, borderRadius: 60, right: 25, position: "absolute", top: 8 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('PrinterSettingsForInvoice')}>
                                <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                        <Image source={require("../../assets/printer.png")} style={{ width: 20, height: 20 }}></Image>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <ScrollView>
                        <StatusBar barStyle="light-content" hidden={false} backgroundColor={'#126F85'} />
                        <Label></Label>
                        <View style={{ justifyContent: 'center', alignItems: 'center', }}>
                            <Spinner
                                isVisible={this.state.spinner}
                                type={'ThreeBounce'}
                                textStyle={{ color: 'white' }}
                                size={70}
                                color={Colors.FLAT_TEXT_COLOR}
                            />
                        </View>
                        {this.nodataImage()}

                        <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 10, }}>
                         <View style={{ alignItems: 'flex-start', alignContent: 'flex-start', position: 'absolute', left: 28 }}>
                                <Text style={styles.itemName2}>Invoice Number : {this.state.data.invoiceNumber}</Text>
                                 <Text style={styles.itemName2}>Created Date   : {this.state.invoiceDate}</Text>
                           <Text style={styles.itemName2}>Payment Method : {this.state.paymentMethod.length==0? "payment pending" : this.state.paymentMethod.length !== 0 ? this.state.paymentMethod[0].paymentMethod: this.state.paymentMethod[0].paymentMethod}</Text>
                                    <Text style={styles.itemName2}>Customer  : {this.state.customer==null? "walking customer": this.state.customer !== null ? this.state.customer.name:this.state.customer.name }</Text>
                            </View> 

                            <Label></Label>
                            <Text style={styles.itemName}>Name                     Qty                   Price</Text>
                        </View>
                        <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 10, }}>
                            <View style={{ padding: 10, width: '90%', top: 60, }}>
                                {
                                    this.state.itemList.map((item, index) => (
                                        <TouchableOpacity
                                            key={item.id}
                                            style={styles.container1}
                                        >
                                            <View style={{ flexDirection: "row", alignContent: 'flex-start', justifyContent: 'flex-start', width: '100%' }}>
                                                <Text style={{ position: "absolute", justifyContent: 'flex-start' }}>
                                                    {item.name}
                                                </Text>
                                                <Text style={{ textAlign: "center", position: "absolute", left: 150 }}>
                                                    {item.qty}
                                                </Text>
                                                <Text style={{ textAlign: "center", left: 250, }}>
                                                    {item.price}
                                                </Text>
                                            </View>
                                        </TouchableOpacity>
                                    ))
                                }
                            </View>
                        </View>


                        <Label></Label>
                        <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 100, }}>
                            <View style={{ alignItems: 'flex-start', alignContent: 'flex-start', position: 'absolute', left: 28 }}>
                                <Text style={styles.itemName2}>Sub total :{this.state.data.subTotalAmount}</Text>
                                <Text style={styles.itemName2}>Discount  :{this.state.data.totalDiscountAmount}</Text>
                                <Text style={styles.itemName5}>Grand total :{this.state.data.grandTotalAmount}</Text>
                                <Text style={styles.itemName2}>Paid amount :{this.state.paidAmount}</Text>
                                <Text style={styles.itemName2}>payable amount :{this.state.payableAmount}</Text>
                                {/* <Text style={styles.itemName2}>Balance    :{this.state.data.balanceGivenAmount}</Text> */}
                            </View>
                        </View>



                        <Label></Label>
                        <Label></Label>
                        <View style={{
                            justifyContent: 'center', alignContent: 'center', alignItems: 'center', flex: 1,
                            justifyContent: 'flex-start',
                            top: 20,
                            marginTop: 100,
                            padding: PixelRatio.getPixelSizeForLayoutSize(12),

                        }}>
                            <TouchableOpacity onPress={() => this.printRecept()} >
                                <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>

                                    <View style={{ borderColor: Colors.PRIMARY_COLOR, borderWidth: 2, width: wp('80%'), height: hp('8%'), borderRadius: 30 }}>
                                        <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 10 }}>
                                            <Text style={styles.NotSelectedText2}>Print Receipt</Text>
                                            <Text style={{ fontSize: 10, fontWeight: 'bold', color: this.state.bluetoothConnected == 0 ? '#fc7b03' : this.state.bluetoothConnected == 1 ? Colors.PRIMARY_COLOR : Colors.PRIMARY_COLOR }}>{this.state.bluetoothConnected == 0 ? 'Printer Disconnected' : this.state.bluetoothConnected == 1 ? 'Connected' : 'Connected'}</Text>
                                        </View>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>

                </ImageBackground>
            </View>



        );
    }
}