import {
    PixelRatio,
    StyleSheet,
  } from 'react-native';
  import Colors from '../../../resources/Colors';
  import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
  
  const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#0a1142',
      },
      content: {
        flex: 1,
        // justifyContent: 'flex-end',
        padding: PixelRatio.getPixelSizeForLayoutSize(12),
        
        bottom:40
      },
      logoBack: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 150,
        width: 150,
        borderRadius: 100,
        backgroundColor: '#ffffff90'
      },
  
      backGround: {
        backgroundColor: Colors.PRIMARY_COLOR,
    
      },
      logoContainer: {
        margin:50
      },
      logoText: {
        color: Colors.DARK_BLACK_TEXT_COLOR,
        fontSize: 24,
        left:20,
        fontWeight: '700',
      },
      logoText1: {
        color: Colors.DARK_BLACK_TEXT_COLOR,
        fontSize: 15,
        fontWeight: '700',
      },
      backgroundImage: { width: "100%", height: "100%" },
    
      logo: {
        left: -3,
        height: PixelRatio.getPixelSizeForLayoutSize(150),
        width: PixelRatio.getPixelSizeForLayoutSize(150),
        resizeMode: 'contain',
      },
      form: {
        flex: 1,
        justifyContent: 'flex-end',
        // top: -70
      },
      item: {
        marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
        backgroundColor: Colors.PRIMARY_COLOR,
        borderColor: Colors.PRIMARY_COLOR,
      },
      input: {
        color: 'white',
        // left: 20,
        maxWidth:'82%',
      },
      place: {
        width: 25,
        height: 25,
    
      },
      buttonContainer: {
        flex: 1,
        bottom:20
        // top: -70
      },
      forgotPasswordContainer: {
        alignItems: 'center',
        marginTop: PixelRatio.getPixelSizeForLayoutSize(2),
      },
      forgotPasswordText: {
        color: 'white',
        fontSize: 16,
      },
      button: {
        marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
        backgroundColor: Colors.PRIMARY_COLOR,
      },
      loginText: {
        color: '#ffff',
        fontSize: 20,
        fontWeight: 'bold',
      },
      signupContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: PixelRatio.getPixelSizeForLayoutSize(3),
      },
      dontHaveAccountText: {
        color: '#bec0ce',
        fontSize: 16,
      },
      signupText: {
        color: '#000',
        fontSize: 16,
      },
  });
  
  
   export default styles;