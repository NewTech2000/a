import React, { Component } from 'react';
import { View, Image, ImageBackground, StatusBar, Alert, BackHandler,TouchableOpacity,
     TouchableHighlight, AsyncStorage, Switch,Dimensions } from 'react-native';
import {
    Button,
    Content,
    Form,
    Item,
    Input,
    Text,
    Label,
    Fab
} from 'native-base';
import styles from './EditUserFormStyle';
import Messages from '../../../resources/Message';
import Colors from '../../../resources/Colors';
import NetInfo from "@react-native-community/netinfo";
import Strings from '../../../resources/Strings';
import CompressImage from "react-native-compress-image";
import ImagePicker from "react-native-image-picker";
import Spinner from 'react-native-loading-spinner-overlay';
import { ScrollView } from "react-native-gesture-handler";
import { NavigationActions, StackActions } from 'react-navigation'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

export default class AddSubUserScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            address: "",
            background: '',
            username: '',
            password: '',
            confirmpassword: '',
            contact: '',
            nicnumber: '',
            photo: null,
            avatarSource: null,
            url: "",
            photoUri: '',
            uri: "",
            id: "",
            spinner: false,
            imageUrl: "",
            branchId: "",
            roleId: "",
            token: "",
            switchValue: false,
            switchValueActivated: false,
            switchValueDeactivated: true



        };
        this.state.id = this.props.navigation.state.params.id
        this.state.address = this.props.navigation.state.params.address
        this.state.imageUrl = this.props.navigation.state.params.image
        this.state.photo = { uri: this.props.navigation.state.params.image }
        this.state.nicnumber = this.props.navigation.state.params.nic
        this.state.username = this.props.navigation.state.params.userName
        this.state.name = this.props.navigation.state.params.name

    }

    onButtonPress = () => {

    }


    handleBackButton = () => {
        this.props.navigation.goBack();
        return true;
    };



    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }


    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        this.state.switchValue = this.props.navigation.getParam('enabled', false)

        if (this.state.switchValue == true) {
            this.setState({
                switchValueActivated: true,
                switchValueDeactivated: false
            })
        } else {
            this.setState({
                switchValueActivated: false,
                switchValueDeactivated: true
            })
        }

    }

    async componentWillMount() {
        let user = await AsyncStorage.getItem('companyDetails');
        let parsed = JSON.parse(user);
        let myArray = await AsyncStorage.getItem('myArray');
        let d = JSON.parse(myArray);

        this.setState({
            branchId: parsed.branchId,
            roleId: parsed.role.id,
            token: d

        })
        console.log("dddddddddddddddddddddddd" + JSON.stringify(parsed.branchId))
        console.log("dddddddddddddddddddddddd" + JSON.stringify(parsed.role.id))


    }


    validation() {
        // if (this.state.name == "") {
        //     Messages.messageName(Strings.WARNING, Strings.NAME_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        // } else if (this.state.username == "") {
        //     Messages.messageName(Strings.WARNING, Strings.USER_NAME_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        // } else if (this.state.contact == "") {
        //     Messages.messageName(Strings.WARNING, Strings.MOBILE_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        // } else if (this.state.nicnumber == "") {
        //     Messages.messageName(Strings.WARNING, Strings.NIC_WARNING, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        // } else if (this.state.address == "") {
        //     Messages.messageName(Strings.WARNING, Strings.ADDRESS_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        // }
        // else if (this.state.photo == null) {
        //     Messages.messageName(Strings.WARNING, Strings.IMAGE_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        // }
        // else {
                if (this.state.photo.uri == this.state.imageUrl) {
                    this.state.uri = this.state.photo.uri
                    this.UpdateUser()
                } else {
                    this.handleUploadPhoto()
                }
        
            
      

        // }


    }



    UpdateUser() {

        this.setState({
            spinner: true,
        });
        var data = JSON.stringify({

            "address": this.state.address,
            "branchId": this.state.branchId,
            "contact": this.state.contact,
            "dateOfBirth": {
                "basic": "1990-10-10"
            },
            "description": "string",
            "imageUrl": this.state.uri,
            "name": this.state.name,
            "nicNumber": this.state.nicnumber,
            "roleId": this.state.roleId,
            "username": this.state.username

        });

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + "/api/user/" + this.state.id,
                    {
                        method: "PUT",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },
                        body: data
                    }
                )


                    .then(resp => resp.json())
                    .then(responseJson => {
                        this.setState({
                            spinner: false,
                        });
                        const resetAction = StackActions.reset({
                            index: 0,
                            actions: [
                                NavigationActions.navigate({ routeName: 'SubusersScreen' })],
                
                        });
                        this.props.navigation.dispatch(resetAction);
                        console.log(JSON.stringify(responseJson));

                    })

                    .catch(error => {
                        this.setState({
                            spinner: false,
                        });
                        const resetAction = StackActions.reset({
                            index: 0,
                            actions: [
                                NavigationActions.navigate({ routeName: 'SubusersScreen' })],
                
                        });
                        this.props.navigation.dispatch(resetAction);
                        console.log(error);

                    });


            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

            }
        });


    }


    //upload Image
    handleUploadPhoto = () => {



        this.setState({
            spinner: true,
        });

        fetch(Strings.Image_URL + "/file-server", {
            method: "POST",
            body: this.createFormData(this.state.photo, { userId: "123" })
        })
            .then(response => response.json())
            .then(response => {



                this.setState({
                    photo: null,
                    uri: response.url
                });
                this.setState({
                    spinner: false,
                });
                this.UpdateUser()

                console.log("upload succes", this.state.uri);

            })
            .catch(error => {
                this.setState({
                    spinner: false,
                });

                console.log("upload error", error);
                // alert("Upload failed!");
            });




    };


    //chooseing image
    handleChoosePhoto = () => {
        const options = {
            noData: true,
        }
        ImagePicker.launchImageLibrary(options, response => {
            console.log("upload succes", response);
            if (response.uri) {
                this.setState({
                    photo: response

                })
                this.compress()
            }
        })
    }


    //split image url
    createFormData = (photo, body) => {
        const data = new FormData();

        data.append("document", {
            name: photo.fileName,
            type: photo.type,
            uri:
                Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "")
        });

        Object.keys(body).forEach(key => {
            data.append(key, body[key]);
        });
        return data;
    };

    //To compress a image
    compress() {
        CompressImage.createCompressedImage(this.state.photo.uri, "Compress/Images")

            .then(response => {
                this.state.photo.uri = response.uri;
                this.state.photo.path = response.path;
                this.state.photo.mime = response.mime;
            })

            .catch(err => {
                return Alert.alert(
                    Strings.COMPRESS_ERR
                );
            });

    }


    updateUserStatus(value) {

    }

    switchValue(value) {

        this.updateUserStatus(value)

        if (value == true) {
            this.setState({
                switchValueActivated: true,
                switchValueDeactivated: false
            })
        } else {
            this.setState({
                switchValueActivated: false,
                switchValueDeactivated: true
            })
        }

        this.setState({
            switchValue: value,
        })
    }


    render() {
        return (

            <ImageBackground
                source={require("../../../assets/newBack2.png")}
                style={{ width: "100%", height: "100%" }}>
                <StatusBar barStyle="light-content" hidden={false} backgroundColor='#126F85' />
                <Spinner
                    visible={this.state.spinner}
                    textContent={'Loading...'}
                    textStyle={{ color: 'white' }}
                    overlayColor={"#0f0e0e90"}
                    animation={"fade"}
                    color={"#00CEFD"}
                />
                <View style={{ backgroundColor: '#126F85', width: '100%', height: 60, borderBottomLeftRadius: 50 }}>
                    <View style={{ backgroundColor: '#126F85', width: 50, top: 13, borderRadius: 60, left: 25, position: "absolute", top: 8 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('SubusersScreen')}>
                            <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                    <Image source={require("../../../assets/newBack3.png")} style={{ width: 20, height: 20 }}></Image>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Text style={{ fontFamily: 'Ubuntu-Medium', color: '#ffff', justifyContent: "center", textAlign: "center", fontSize: 20, position: "absolute", top: 10, left: 100, fontWeight: "bold" }}>Edit users</Text>
                    <View style={{ position: "absolute", width: 70, bottom: 10, right: 65 }}>
                        {this.state.switchValueActivated &&
                            <Text numberOfLines={1} style={{ fontSize: 12, color: '#ffff', textAlign: "center", top: 10, }}>Activated</Text>
                        }
                        {this.state.switchValueDeactivated &&
                            < Text numberOfLines={1} style={{ fontSize: 12, color: '#ffff', textAlign: "center", top: 10, left: -10 }}>Deactivated</Text>
                        }
                        <Switch
                            //  tintColor="transparent"
                            thumbTintColor={this.state.switchValue
                                ? Colors.BTN_COLOR : "#cccccc"}
                            style={{ left: 40, bottom: 10, transform: [{ scaleX: 1.5 }, { scaleY: 1.5 }] }}
                            value={this.state.switchValue}
                            onValueChange={(switchValue) => this.switchValue(switchValue)}
                        />
                    </View>
                </View>
                <ScrollView>
                    <Content contentContainerStyle={styles.content}>

                        <Form style={styles.form}>
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <TouchableHighlight style={{ borderRadius: 100, height: 100, width: 100, marginBottom: 10, marginTop: 20 }}
                                    onPress={() => this.handleChoosePhoto()}
                                >

                                    <Image
                                        source={this.state.imageUrl == null ? require('../../../assets/nnn.jpg') : this.state.imageUrl !== null ? this.state.photo : this.state.photo}
                                        style={{ width: 100, height: 100, borderRadius: 100, borderColor: Colors.PRIMARY_COLOR, borderWidth: 2, }}
                                    />

                                </TouchableHighlight>

                            </View>


                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../../assets/owner.png')}
                                />
                                <Input
                                    style={styles.input}
                                    placeholder="Name"
                                    placeholderTextColor="#ffffff80"
                                    autoCapitalize="none"
                                    value={this.state.name}
                                    onChangeText={name => this.setState({ name })}
                                />
                            </Item>
                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../../assets/user.png')}
                                />
                                <Input
                                    style={styles.input}
                                    placeholder="Username"
                                    placeholderTextColor="#ffffff80"
                                    value={this.state.username}
                                    onChangeText={username => this.setState({ username })}

                                />
                            </Item>

                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../../assets/contact.png')}
                                />
                                <Input
                                    style={styles.input}
                                    keyboardType="numeric"
                                    maxLength={10}
                                    placeholder="Contact"
                                    placeholderTextColor="#ffffff80"
                                    autoCapitalize="none"
                                    value={this.state.contact}
                                    onChangeText={contact => this.setState({ contact })}
                                />
                            </Item>
                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../../assets/invoice.png')}
                                />
                                <Input
                                    style={styles.input}
                                    placeholder="NicNumber"
                                    placeholderTextColor="#ffffff80"
                                    value={this.state.nicnumber}
                                    onChangeText={nicnumber => this.setState({ nicnumber })}

                                />
                            </Item>
                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../../assets/address.png')}
                                />
                                <Input
                                    style={styles.input}
                                    placeholder="Address"
                                    placeholderTextColor="#ffffff80"
                                    value={this.state.address}
                                    onChangeText={address => this.setState({ address })}

                                />
                            </Item>

                        </Form>
                        <Label></Label>
                        <Label></Label>
                        <View style={styles.buttonContainer}>
                            <Button
                                style={styles.button}
                                onPress={() => this.validation()}
                                hasText
                                block
                                large
                                dark
                                rounded
                            >
                                <Text style={styles.loginText}>Submit</Text>
                            </Button>


                            <Label></Label>
                            <Label></Label>

                        </View>

                    </Content>

                </ScrollView>

            </ImageBackground >

        );
    }
}