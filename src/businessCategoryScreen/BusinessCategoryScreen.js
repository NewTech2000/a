import React, { Component } from 'react';
import {
    View,
    AsyncStorage,
    StatusBar,
    ActivityIndicator,
    Image,
    ScrollView,
    StyleSheet,
    ImageBackground,
    TouchableOpacity,
    Alert,BackHandler,
    Dimensions
} from 'react-native';
import { FlatGrid } from 'react-native-super-grid';
import NetInfo from "@react-native-community/netinfo";
import Colors from '../../resources/Colors';
import { NavigationActions, StackActions } from 'react-navigation';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Text,
} from 'native-base';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

export default class BusinessCategoryScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            imageArray: [],
        }
    }

    onButtonPress = () => {
 
    }
 
    handleBackButton = () => {
        this.props.navigation.goBack(null);
        return true;
    };
 
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    navigateToRegisterFormOne(item) {
        this.props.navigation.navigate('CompanyDetailsScreen', {
            name: item.name,
        });
    }


    render() {

        const items = [
            { name: 'Salon', url: require('../../assets/saloon.png'), background: "../../assets/bakery.jpg" },
            { name: 'Bakery', url: require('../../assets/bakery.png') },
            { name: 'Juice Bar', url: require('../../assets/juicebar.png') },
            { name: 'Coffee Shop', url: require('../../assets/coffeeshop.png') },
            { name: 'Phone Shop', url: require('../../assets/phoneshop.png') },
            { name: 'Paint Shop', url: require('../../assets/paintshop.png') },
            { name: 'restaurant', url: require('../../assets/rest.png') }
        ];
        

        return (

                <ImageBackground
                    source={require("../../assets/newBack2.png")}
                    style={{ width: "100%", height: "100%" }}>
                  <StatusBar barStyle="dark-content" hidden={false} backgroundColor={"#85CCD9"} />
                    <View style={{backgroundColor:'#85CCD9',width:'100%',height:200,paddingLeft: 23,borderBottomLeftRadius:90}}>
                    <View style={{ backgroundColor: '#85CCD9', width: 50, top: 13, left: 25, position: "absolute", top: 8 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('LoginScreen')}>
                                <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                        <Image source={require("../../assets/newBack3.png")} style={{ width: 20, height: 20 }}></Image>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                    <View style={{ top: 30, left: 15, justifyContent: 'center' ,}}>
                        <Text style={{ fontSize: hp('4%'), fontFamily: 'Ubuntu-Medium',fontWeight:"bold",color: '#ffff' ,marginRight:100,paddingRight:40,position:"absolute",top:10}}>Please  <Text style={{color: Colors.DARK_BLACK_TEXT_COLOR, fontSize: hp('4%'),fontWeight:"bold",position:"absolute",marginLeft:100,top:10}}>pick</Text></Text>                      
                        <Text style={{ fontSize: hp('4%'),color: '#ffff' ,position:"absolute",top:50}}>your business</Text>
                        <Text style={{ color: Colors.PRIMARY_COLOR, fontSize: hp('4%'),fontWeight:"bold",position:"absolute" ,top:85,left:2 }}>category</Text>
                    </View>
                    </View>
                    <FlatGrid
                        itemDimension={130}
                        items={items}
                        style={{
                            flex: 1,
                            marginLeft:28,marginRight:38
                        }}
                        renderItem={({ item, index }) => (
                         
                            <TouchableOpacity activeOpacity={0.8} onPress={() => this.navigateToRegisterFormOne(item)}>
                                <View style={[styles.itemContainer, { backgroundColor: 'white', borderColor: '#E5E4FD', borderWidth: 1.1, }]}>
                                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
                                        <Image source={item.url} style={{ width: 70, height: 70 }} ></Image>
                                        <Text style={{ fontSize: hp('1.8%'), top: 5 ,fontWeight:'bold'}}>  {item.name}</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                           
                        )}

                    />

                
                </ImageBackground>

          
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    itemContainer: {
        justifyContent: 'center',
        marginLeft:10,
        height: hp('17%'),
        width: wp('33%'),
        marginTop: '20%',
        // margin: 20,
      
        borderRadius: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 10.65,

        elevation: 3,
        // top: 20,
    },
})