import {
  PixelRatio,
  StyleSheet,
} from 'react-native';
import Colors from '../../resources/Colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffff',
  },
  content: {
    flex: 1,
    justifyContent: 'flex-end',
    padding: PixelRatio.getPixelSizeForLayoutSize(12),
  },
  logoBack: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 150,
    width: 150,
    top: -30,
    borderRadius: 100,
    backgroundColor: '#ffffff90'
  },
  modal4: {
    height: hp('100%'),
    // borderTopRightRadius: 30,
    // borderTopLeftRadius: 30,
    backgroundColor: 'white'
  },
  modal: {
    height: 350,
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    backgroundColor: 'white'
  },

  gridView: {
    flex: 1,
    marginBottom:20,
   
  },
  grid: {
    flex: 1,
    marginBottom:20,
    top:60
   
  },
  gridView2: {
    flex: 1,
    marginBottom:20,
    bottom:30
  },
  itemContainer: {
    borderRadius: 20,
    padding: 10,
    height: hp('8%'),
    width: wp('98%'),
    marginTop: 6,
    marginBottom:3,
    shadowColor: Colors.DARK_BLACK_TEXT_COLOR,
    shadowOpacity: 0.8,
    elevation: 3,
    shadowRadius: 30,
    shadowOffset: { width: 1, height: 13 }

  },
  itemContainer3: {
    borderRadius: 20,
    padding: 10,
    height: hp('7%'),
    width: wp('98%'),
    marginTop: 6,
    marginBottom:6,
    shadowColor: Colors.DARK_BLACK_TEXT_COLOR,
    shadowOpacity: 0.8,
    elevation: 3,
    shadowRadius: 30,
    shadowOffset: { width: 1, height: 13 }

  },
  itemContainer1: {
    borderRadius: 20,
    padding: 10,
    height: hp('15%'),
    width: wp('80%'),
    marginTop: 6,
    marginBottom:6,
    shadowColor: Colors.DARK_BLACK_TEXT_COLOR,
    shadowOpacity: 0.8,
    elevation: 7,
    shadowRadius: 30,
    shadowOffset: { width: 1, height: 13 }


  },
  itemContainer2: {
    borderRadius: 20,
    paddingTop: 10,
    height: hp('15%'),
    width: wp('98%'),
    marginTop: 6,
    marginBottom:6,
    shadowColor: Colors.DARK_BLACK_TEXT_COLOR,
    shadowOpacity: 0.8,
    elevation: 7,
    
    shadowRadius: 30,
    shadowOffset: { width: 1, height: 13 }


  },
  itemName: {
    fontSize: 15,
    color: Colors.DARK_BLACK_TEXT_COLOR,
    fontWeight: 'bold',
  },
  itemtelno: {
    fontSize: 12,
    color: Colors.DARK_BLACK_TEXT_COLOR,
    fontWeight: 'bold',
  },
  itemName2: {
    fontSize: 15,
    color: 'black',
    fontWeight: 'bold',
  },
  itemAmount: {
    fontSize: 15,
    bottom:20,
    position:"absolute",
    color: Colors.DARK_BLACK_TEXT_COLOR,
    fontWeight: 'bold',
    left:150
  },
  itemAmountpay: {
    fontSize: 15,
    bottom:10,
    position:"absolute",
    color: Colors.DARK_BLACK_TEXT_COLOR,
    fontWeight: 'bold',
    left:150
  },
  itempaid: {
    fontSize: 15,
    bottom:10,
    textAlign:"center",
    position:"absolute",
    color: Colors.DARK_BLACK_TEXT_COLOR,
    fontWeight: 'bold',
    right:100
  },
  itemcreditlimit: {
    fontSize: 17,
    bottom:20,
    position:"absolute",
    color: Colors.DARK_BLACK_TEXT_COLOR,
    fontWeight: 'bold',
    right:30
  },
  itemdue: {
    fontSize: 15,
    bottom:10,
    position:"absolute",
    color: Colors.DARK_BLACK_TEXT_COLOR,
    fontWeight: 'bold',
    right:2
  },
  itemAmount1: {
    fontSize: 15,
    color: Colors.PRIMARY_COLOR,
    fontWeight: 'bold',
  },
  itemAmount2: {
    fontSize: 18,
    color: Colors.DARK_BLACK_TEXT_COLOR,
    // fontWeight: 'bold',
  },
  MainContainer: {
    justifyContent: 'center',
    flex: 1,
    paddingTop: 30,
  },
  imageThumbnail: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 100,
  },
  textInputStyle: {
    height: 50,
    color: Colors.BACKGROUD_WHITE,
    borderWidth: 1,
    paddingLeft: 20,
    borderRadius: 30,
    borderColor: Colors.PRIMARY_COLOR,
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  viewStyle: {
    
    justifyContent: 'center',
    padding: 16,
  },
  ProcessText: {
    fontSize: 18,
    fontWeight: 'bold',
    justifyContent: 'center',
    alignItems: 'center',
    color: 'white'
  },
  ProcessButton: {
    backgroundColor: Colors.PRIMARY_COLOR
  },
  inputTextContainer: {
    left: 19
  },
  inputTextContainer2: {
    justifyContent: 'center',
    alignItems: 'center'

  },
  buttonContainerProcess: {
    height: 50,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 30,
    marginTop: 20,
    width: 300,
    borderRadius: 30,
    shadowColor: Colors.DARK_BLACK_TEXT_COLOR,
    shadowOpacity: 0.8,
    elevation: 6,
    shadowRadius: 15,
    shadowOffset: { width: 1, height: 13 }

  },
  input: {
    color: 'white',
    left: 20
  },
  item: {
    marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
    backgroundColor: Colors.PRIMARY_COLOR,
    borderColor: Colors.PRIMARY_COLOR,
  },
  form: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  TextInputStyle2: {
    height: 40,
    borderBottomColor: Colors.DARK_BLACK_TEXT_COLOR,
    width: 200,
    marginBottom: 10,
    borderBottomWidth: 1
  },
  backGround: {
    backgroundColor: Colors.PRIMARY_COLOR,

  },

  headerTextStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 20,
    color: 'white',
  },
  header: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  headerStyle: {
    backgroundColor: Colors.PRIMARY_COLOR,
    shadowOpacity: 0.75,
    shadowRadius: 5,
    shadowOffset: { height: 0, width: 0 },

  },
  ToucherbleIconStyle: { color: Colors.PRIMARY_COLOR },
  ToucherbleIconStyle2: { height: 30, width: 30 },

  logoContainer: {

    justifyContent: 'center',
    alignItems: 'center',
    marginTop: PixelRatio.getPixelSizeForLayoutSize(40),
    marginBottom: PixelRatio.getPixelSizeForLayoutSize(40),
  },
  logoText: {
    color: Colors.DARK_BLACK_TEXT_COLOR,
    fontSize: 24,
    fontWeight: '700',
    marginTop: 10
  },
  logoText1: {
    color: Colors.DARK_BLACK_TEXT_COLOR,
    fontSize: 15,
    fontWeight: '700',

  },
  backgroundImage: { width: "100%", height: "100%" },

  logo: {
    left: -3,
    height: PixelRatio.getPixelSizeForLayoutSize(150),
    width: PixelRatio.getPixelSizeForLayoutSize(150),
    resizeMode: 'contain',
  },
  place: {
    width: 25,
    height: 25,
    left:10,
  },
  buttonContainer: {
    flex: 1,
    marginTop: PixelRatio.getPixelSizeForLayoutSize(10),
  },
  forgotPasswordContainer: {
    alignItems: 'center',
    bottom: -150
  },
  forgotPasswordText: {
    color: 'white',
    fontSize: 16,
  },
  button: {
    marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  loginText: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
  },
  signupContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: PixelRatio.getPixelSizeForLayoutSize(3),
  },
  dontHaveAccountText: {
    color: '#bec0ce',
    fontSize: 16,
  },
  signupText: {
    color: '#000',
    fontSize: 10,
  },
  
});


export default styles;