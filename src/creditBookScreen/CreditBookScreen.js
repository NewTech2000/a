import React, { Component } from 'react';
import { View, TextInput, Image, TouchableOpacity, ImageBackground, StatusBar, BackHandler, FlatList, AsyncStorage, Vibration, PixelRatio, Alert, Keyboard, SafeAreaView ,Dimensions} from 'react-native';
import {
    Item,
    Input,
    Text,
    Header,
    Left,
    Right,
    Label,
} from 'native-base';
import styles from './CreditBookScreenStyle';
import Colors from '../../resources/Colors';
import NetInfo from "@react-native-community/netinfo";
import Modal from 'react-native-modalbox';
import Strings from '../../resources/Strings';
import Messages from '../../resources/Message';
import Spinner from "react-native-spinkit";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
const DURATION = 40;
import Moment from 'moment';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

export default class CreditBook extends Component {
    constructor(props) {
        super(props);
        this.state = {
            customBackgroundDialog: false,
            defaultAnimationDialog: false,
            scaleAnimationDialog: false,
            scaleAnimationDialog2: false,
            slideAnimationDialog: false,
            name: "",
            token: '',
            data: [],
            nic: '',
            contact: '',
            address: '',
            email: '',
            spinner: false,
            searchText: "",
            showTheThing: true,
            cid: 0,
            cName: '',
            date: "",
            date2: "",
            Ccontact: '',
            Cnic: '',
            Cemail: '',
            Caddress: '',
            creditData: [],
            customerId: '',
            Invoiceamount: 0,
            invoiceNumber: '',
            description: '',
            customer: '',
            invoiceId: 0,
            nodata: false,
            temporyamount: 0,
        };

    }
    financial(x) {
        return Number.parseFloat(x).toFixed(2);
    }
    //getting auth code from AsyncStorage
    showData = async () => {
        let myArray = await AsyncStorage.getItem('myArray');
        let d = JSON.parse(myArray);

        this.setState({
            token: d

        })
        console.log(this.state.token);
        this.getAllCustomers()

    }

    //close model
    onClose = () => {
        this.setState({
            showTheThing: true,
            customer: ""
        })
    }
    onButtonPress = () => {

    }


    handleBackButton = () => {
        this.props.navigation.goBack();
        return true;
    };



    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        this.showData()
        this.setState({
            date: Moment().format('YYYY-MM-DD'),


        })

    }



    Test(name) {
        const cusName = name
        this.props.navigation.navigate('CartScreen', { text1: cusName });
    }

    //asign search word
    search(text) {
        this.state.searchText = text;
        this.getAllCustomers()
    }

    //getting customer list from api
    getAllCustomers() {
        this.setState({
            spinner: true
        })
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + "/api/customer/due?page=0&limit=800&text=" + this.state.searchText,
                    {
                        method: "GET",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },

                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        this.setState({
                            spinner: false
                        })
                        console.log("customers" + JSON.stringify(responseJson));

                        this.setState({
                            data: responseJson.content
                        })

                        if (responseJson.content.length == 0) {
                            this.setState({
                                nodata: true
                            })
                        } else {
                            this.setState({
                                nodata: false
                            })
                        }
                    })
                    .catch(error => {
                        console.log(error);

                        this.setState({
                            spinner: false
                        })

                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.getAllCustomers()

                            }
                        },
                    ],
                    { cancelable: true }
                );


            }
        });

    }


    //getting customer credit details by customer id
    getCustomerCreditDetails(id) {
        this.setState({
            customerId: id
        })
        console.log("id" + id);
        this.setState({
            spinner: true
        })

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + '/api/customer/' + id + '/credit?page=0&limit=500',
                    {
                        method: "GET",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },

                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        this.setState({
                            spinner: false
                        })
                        console.log("credit" + JSON.stringify(responseJson));

                        this.refs.modal2.open()
                        this.setState({
                            creditData: responseJson.content.reverse()
                        })



                    })


                    .catch(error => {
                        console.log(error);

                        this.setState({
                            spinner: false
                        })

                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.getCustomerCreditDetails()

                            }
                        },
                    ],
                    { cancelable: true }
                );


            }
        });

    }


    //no data iamge
    nodataImage() {
        if (this.state.nodata) {
            return (
                <View style={{ top: 60, justifyContent: 'center', alignItems: 'center', }}>
                    <Image style={{ width: 200, height: 200, borderRadius: 100 }}
                        source={require("../../assets/no_data_found.png")}
                    />

                </View>
            )
        }
    }


    //settle customer due payments
    invoicePayment() {
        console.log(JSON.stringify(this.state.Invoiceamount+"        "+this.state.invoiceNumber+"        "+this.state.invoiceId));
        this.setState({
            spinner: true,
        });
        var data = JSON.stringify({

            "amount": this.state.Invoiceamount,
            "description": this.state.description,
            "invoiceIds": [
                this.state.invoiceId
            ],
            "invoiceNumbers": this.state.invoiceNumber,
            "paymentDate": {
                "basic": this.state.date

            }

        });

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + '/api/customer/' + this.state.customerId + '/payment',
                    {
                        method: "POST",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },
                        body: data
                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        Messages.messageName(Strings.WARNING_SUCESS, Strings.CUSTOMER_PAYMENT_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                        console.log(JSON.stringify(responseJson));
                        this.getAllCustomers()
                        this.refs.modal.close()
                        this.setState({
                            spinner: false
                        });
                    })


                    .catch(error => {
                        console.log(error);
                        this.setState({
                            spinner: false,
                        });
                        Messages.messageName(Strings.WARNING_FAILED, Strings.CUSTOMER_PAYMENT, Strings.ICON[2], Strings.TYPE[2], Colors.WARNING_ALERT);


                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.invoicePayment()

                            }
                        },
                    ],
                    { cancelable: true }
                );


            }
        });

    }


    //open customer details model
    creditDetails = (item) => {
        this.setState({
            customer: item.name.toString()

        })
        this.getCustomerCreditDetails(item.id)
        Vibration.vibrate(DURATION);
    }

    //open payment model
    navigateToPaymentModel(item) {

        var paybale = item.amount - item.paidAmount
        this.setState({
            invoiceId: item.id,
            Invoiceamount: paybale.toString(),
            temporyamount: paybale.toString(),
            date2: item.creditDate.basic,
            invoiceNumber: item.invoiceNumbers.toString()
        })
        console.log("credit" + item.id);
        this.refs.modal2.close()
        this.refs.modal.open()
    }

    closequickmodal() {
        this.refs.modal2.close()
    }

    validatedue() {
        var a = parseInt(this.state.Invoiceamount)
        var b = parseInt(this.state.temporyamount)
        if (a > b) {
            Messages.messageName(Strings.WARNING_FAILED, Strings.CUSTOMER_PAYMENT, Strings.ICON[2], Strings.TYPE[2], Colors.WARNING_ALERT);
        } else {
            this.invoicePayment()
        }
    }

    //long tap alert
    onPressAlert() {
        Messages.messageName("Need", Strings.LONG_TAP2, Strings.ICON[2], Strings.TYPE[0], Colors.DARK_BLACK_TEXT_COLOR);
    }

    closemodal() {
        this.refs.modal.close()
    }

    render() {

        return (
            <View style={styles.container}>
                <ImageBackground
                    source={require("../../assets/newBack2.png")}
                    style={{ width: "100%", height: "100%" }}>
                    {/* <Header
                        style={styles.headerStyle}>
                        <Left>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('HomeScreen')}>
                                <Image source={require("../../assets/back.png")} style={styles.ToucherbleIconStyle2} />
                            </TouchableOpacity>
                        </Left>
                        <View style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}>
                            <Text style={styles.headerTextStyle}>Credit Book</Text>
                        </View>
                        <Right>
                            <TouchableOpacity >

                            </TouchableOpacity>
                        </Right>
                    </Header> */}
                    <View style={{ backgroundColor: '#126F85', width: '100%', height: 60, borderBottomLeftRadius: 50 }}>

                        <View style={{ backgroundColor: '#126F85', width: 50, top: 13, borderRadius: 60, left: 25, position: "absolute", top: 8 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('HomeScreen')}>
                                <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                        <Image source={require("../../assets/newBack3.png")} style={{ width: 20, height: 20 }}></Image>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <Text style={{ fontFamily: 'Ubuntu-Medium', color: '#ffff', justifyContent: "center", textAlign: "center", fontSize: 20, position: "absolute", top: 10, left: 100, fontWeight: "bold" }}>Credit Book</Text>
                    </View>
                    <StatusBar barStyle="light-content" hidden={false} backgroundColor={'#126F85'} />

                    <Modal style={[styles.modal4]} position={"bottom"} ref={"modal2"} onClosed={this.onClose} backdropPressToClose={false} >

                        <View>
                            <View style={{ position: "absolute", top: 25, right: 20 }}>
                                <TouchableOpacity onPress={() => this.closequickmodal()}>
                                    <View style={{ width: 35, height: 35, borderRadius: 30, backgroundColor: '#126F85', elevation: 10 }}>
                                        <Image style={{ width: 25, height: 25, top: 6, left: 5 }} source={require('../../assets/delete1.png')}></Image>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={{ position: "absolute", top: 20, left: 20 }}>
                                <Text style={{ color: Colors.PRIMARY_COLOR, fontSize: 20, textAlign: 'left', fontWeight: 'bold', }}>Customer Credit Details</Text>
                                <Text style={{ color: Colors.DARK_BLACK_TEXT_COLOR, fontSize: 18, textAlign: 'left', fontWeight: 'bold', }}>Name : {this.state.customer}</Text>
                            </View>

                        </View>
                        {/* table */}



                        {/* ddddd */}
                        <Label></Label>
                        <View style={{ height: 50, top: 60 }}>
                            <View style={{
                                marginLeft: 7, marginRight: 7, backgroundColor: Colors.PRIMARY_COLOR,
                                height: 50, flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderTopEndRadius: 13, borderTopLeftRadius: 13
                            }}>
                                <Text style={{ position: 'absolute', left: 15, fontWeight: 'bold', fontSize: 17, color: '#FFFFFF', }}>Invoice No</Text>
                                <Text style={{ position: 'absolute', fontWeight: 'bold', fontSize: 17, color: '#FFFFFF', left: 130 }}>Amount</Text>
                                <Text style={{ position: 'absolute', fontWeight: 'bold', fontSize: 17, color: '#FFFFFF', right: 15 }}>Due</Text>
                                <Text style={{ position: 'absolute', fontWeight: 'bold', fontSize: 17, color: '#FFFFFF', right: 80 }}>Paid</Text>

                            </View>
                        </View>
                        <FlatList
                            itemDimension={300}
                            data={this.state.creditData}
                            style={styles.grid}
                            renderItem={({ item, index }) => (
                                <TouchableOpacity activeOpacity={0.8} style={{ justifyContent: 'center', alignItems: 'center' }} onPress={() => this.navigateToPaymentModel(item)}>
                                    <View style={[styles.itemContainer3, { backgroundColor: Colors.BACKGROUD_WHITE }]}>
                                        {/* <Image style={styles.cardImage}
                                        source={{ uri: Strings.BASE_URL + '/' + item.logo }}
                                    /> */}
                                        <View style={{ width: 100 }}>
                                            <Text numberOfLines={1} style={[styles.itemName,]}>{item.invoiceNumbers}</Text>
                                        </View>
                                        <View style={{ width: 80 }}>
                                            <Text numberOfLines={1} style={[styles.itemtelno,]}>{item.creditDate.basic}</Text>
                                        </View>
                                        <Text style={styles.itemAmountpay}>{this.financial(item.amount)}</Text>
                                        <Text style={styles.itempaid}>{this.financial(item.paidAmount)}</Text>
                                        <SafeAreaView>
                                            <Text style={styles.itemdue}>{this.financial(item.amount - item.paidAmount)}</Text>
                                        </SafeAreaView>


                                    </View>
                                </TouchableOpacity>

                            )}
                        />
                        {/* <View style={{
                            flex: 1,
                            justifyContent: "center",
                            top:40,
                            padding: PixelRatio.getPixelSizeForLayoutSize(9),
                        }}>




                            <FlatList
                                itemDimension={0}
                                data={this.state.creditData}
                                // style={styles.gridView2}
                                renderItem={({ item, index }) => (
                                    <TouchableOpacity activeOpacity={0.8} onPress={() => this.navigateToPaymentModel(item)}>
                                        <View style={[styles.itemContainer2, { backgroundColor: Colors.BACKGROUD_WHITE }]}>
                                            <Text style={[styles.itemName, { left: 10 }]}>Credit Date    : {item.creditDate.basic}</Text>
                                            <View style={{ backgroundColor: '#e0e0e0', width: '100%', height: hp('9%'), borderBottomLeftRadius: 20, borderBottomRightRadius: 20, top: 12 }}>
                                                <View style={{ flexDirection: 'row', top: 10 }}>
                                                    <Text style={styles.itemAmount1}>  Amount(Rs)  </Text>
                                                    <Text style={styles.itemAmount1}>      Paid(Rs)</Text>
                                                    <Text style={styles.itemAmount1}>       Due(Rs)</Text>
                                                </View>
                                                <View style={{ flexDirection: 'row', top: 10 }}>
                                                    <Text style={styles.itemAmount2}>    {item.amount}</Text>
                                                    <Text style={styles.itemAmount2}>                 {item.paidAmount}</Text>
                                                    <Text style={styles.itemAmount2}>             {item.amount - item.paidAmount} </Text>
                                                </View>
                                            </View>



                                        </View>
                                    </TouchableOpacity>
                                )}
                            />
                        </View> */}

                    </Modal>

                    <Modal style={[styles.modal]} position={"bottom"} ref={"modal"} onClosed={this.onClose} backdropPressToClose={false}>
                        <TouchableOpacity style={{ top: -20, justifyContent: "center", alignContent: "center", alignItems: "center" }} onPress={() => this.closemodal()}>
                            <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: '#ffff', borderRadius: 100, }}>
                                <Image style={{ width: 40, height: 40 }} source={require('../../assets/closemodl.png')} />
                            </View>
                        </TouchableOpacity>
                        <View style={{
                            flex: 1,
                            bottom: 30,
                            justifyContent: 'flex-start',
                            padding: PixelRatio.getPixelSizeForLayoutSize(8),
                        }}>

                            <Text style={{ color: Colors.PRIMARY_COLOR, fontSize: 20, textAlign: 'left', fontWeight: 'bold', top: -10 }}>Payment</Text>
                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../assets/type.png')}
                                />
                                <Input
                                    style={styles.input}
                                    placeholder="Invoice Numbers"

                                    placeholderTextColor="#ffffff80"
                                    autoCapitalize="none"
                                    editable={false}
                                    value={this.state.invoiceNumber}
                                    onChangeText={invoiceNumber => this.setState({ invoiceNumber })}
                                />
                            </Item>
                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../assets/price.png')}
                                />
                                <Input
                                    style={styles.input}
                                    placeholder="Amount"

                                    keyboardType={"number-pad"}
                                    placeholderTextColor="#ffffff80"
                                    autoCapitalize="none"
                                    value={this.state.Invoiceamount}
                                    onChangeText={Invoiceamount => this.setState({ Invoiceamount })}
                                />
                            </Item>
                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../assets/description.png')}
                                />
                                <Input
                                    style={styles.input}
                                    multiline={true}

                                    placeholder="Description"
                                    placeholderTextColor="#ffffff80"
                                    autoCapitalize="none"
                                    value={this.state.description}
                                    onChangeText={description => this.setState({ description })}
                                />
                            </Item>
                            <View style={{ justifyContent: "center", alignContent: "center", alignItems: "center", right: 2 }}>
                                <TouchableOpacity
                                    style={[styles.buttonContainerProcess, styles.ProcessButton]}
                                    onPress={() => this.validatedue()}
                                >
                                    <Text style={styles.ProcessText}>Pay</Text>
                                </TouchableOpacity>
                            </View>


                        </View>
                    </Modal>


                    <View style={{ justifyContent: 'center', marginTop: -10, paddingTop: 10 }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row', }}>
                            <Item
                                style={styles.item}
                                rounded
                                last >
                                <Image
                                    style={{ width: 25, height: 25, left: 10 }}
                                    source={require('../../assets/search.png')}
                                />
                                <Input
                                    style={{ color: 'white', left: 20, maxWidth: '82%' }}
                                    placeholder="Search Here"

                                    placeholderTextColor="#ffffff80"
                                    autoCapitalize="none"
                                    onChangeText={text => this.search(text)}
                                    value={this.state.text}
                                />
                            </Item>
                        </View>
                    </View>
                    <View style={{ height: 50 }}>
                        <View style={{
                            marginLeft: 7, marginRight: 7,
                            height: 50, flex: 1, borderWidth: 1, borderColor: Colors.FLAT_TEXT_COLOR, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderTopEndRadius: 13, borderTopLeftRadius: 13
                        }}>
                            <Text style={{ position: 'absolute', left: 15, fontWeight: 'bold', fontSize: 17, color: Colors.FLAT_TEXT_COLOR, }}>Name</Text>
                            <Text style={{ position: 'absolute', fontWeight: 'bold', fontSize: 17, color: Colors.FLAT_TEXT_COLOR, left: 130 }}>Amount</Text>
                            <Text style={{ position: 'absolute', fontWeight: 'bold', fontSize: 17, color: Colors.FLAT_TEXT_COLOR, right: 15 }}>Outstanding</Text>

                        </View>
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Spinner
                            isVisible={this.state.spinner}
                            type={'ThreeBounce'}
                            textStyle={{ color: 'white' }}
                            size={70}
                            color={Colors.FLAT_TEXT_COLOR}
                        />
                    </View>
                    {this.nodataImage()}

                    <FlatList
                        itemDimension={300}
                        data={this.state.data}
                        style={styles.gridView}
                        renderItem={({ item, index }) => (
                            <TouchableOpacity activeOpacity={0.8} style={{ justifyContent: 'center', alignItems: 'center' }} onPress={() => this.creditDetails(item)}>
                                <View style={[styles.itemContainer, { backgroundColor: Colors.BACKGROUD_WHITE }]}>
                                    {/* <Image style={styles.cardImage}
                                        source={{ uri: Strings.BASE_URL + '/' + item.logo }}
                                    /> */}
                                    <View style={{ width: 100 }}>
                                        <Text numberOfLines={1} style={styles.itemName}>{item.name}</Text>
                                    </View>
                                    <Text style={styles.itemtelno}>{item.contactNumber}</Text>
                                    <Text style={styles.itemAmount}>{this.financial(item.totalCreditAmount)}</Text>
                                    <Text style={styles.itemcreditlimit}>{this.financial(item.creditDayLimit)}</Text>

                                </View>
                            </TouchableOpacity>

                        )}
                    />

                </ImageBackground>
            </View>



        );
    }
}