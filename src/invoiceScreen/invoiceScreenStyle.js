import {
  PixelRatio,
  StyleSheet,
} from 'react-native';
import Colors from '../../resources/Colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
  content: {
    flex: 1,
    justifyContent: 'flex-end',
    padding: PixelRatio.getPixelSizeForLayoutSize(12),
  },
  logoBack: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 150,
    width: 150,
    top: -30,
    borderRadius: 100,
    backgroundColor: '#ffffff90'
  },
  touchableOpacityStyle: {
    alignItems: 'center',

    justifyContent: 'center',


  },
  iconlImage: {
    width: 60,
    height: 60
  },
  itemName: {
    fontSize: hp('2.2%'),
    color: Colors.FLAT_TEXT_COLOR,
    fontWeight: "bold",
    top: 10,
  },
  itemName2: {
    fontSize: hp('2.4%'),
    top: 10,
    color: Colors.DARK_BLACK_TEXT_COLOR,
    fontWeight: "bold",

  },
  itemName3: {
    fontSize: hp('2.2%'),
    color: Colors.FLAT_TEXT_COLOR,
    fontWeight: "bold",
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center"
  },

  inputTextContainer2: {
    justifyContent: 'center',
    alignItems: 'center'

  },
  gridView: {
    flex: 1,
  },
  itemContainer: {
    borderRadius: 20,
    padding: 10,
    height: hp('14%'),
    width: wp('90%'),
    marginTop: 6,
    shadowColor: Colors.DARK_BLACK_TEXT_COLOR,
    shadowOpacity: 0.8,
    elevation: 7,
    shadowRadius: 30,
    shadowOffset: { width: 1, height: 13 }

  },
  button: {
    position: 'absolute',
    top: 20,
    padding: 10,
  },
  caption: {
    fontSize: hp('2.6%'),
    fontWeight: 'bold',
    alignItems: 'center',
  },

  welcome: {
    fontSize: hp('2.6%'),
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
  MainContainer: {
    justifyContent: 'center',
    flex: 1,
    paddingTop: 30,
  },
  imageThumbnail: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 100,
  },
  notificationViewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.FLAT_TEXT_COLOR,
    width: 25,
    height: 25,
    borderRadius: 25,
  },
  notificationTextStyle: {
    color: Colors.BACKGROUD_WHITE,
    fontSize: 12,
  },
  textInputStyle: {
    height: 50,
    borderRadius: 10,
    borderWidth: 1,
    paddingLeft: 10,
    color: Colors.BACKGROUD_WHITE,
    fontWeight: 'bold',
    borderColor: Colors.PRIMARY_COLOR,
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  viewStyle: {
    justifyContent: 'center',
    marginTop: -10,
    padding: 16,
  },
  ProcessText: {
    fontSize: 18,
    fontWeight: 'bold',
    justifyContent: 'center',
    alignItems: 'center',
    color: 'white'
  },
  ProcessButton: {
    backgroundColor: Colors.PRIMARY_COLOR
  },
  inputTextContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  modal4: {
    height: 550,
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    backgroundColor: 'white'
  },
  buttonContainerProcess: {
    height: 50,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
    width: 325,
    borderRadius: 30,
    shadowColor: Colors.DARK_BLACK_TEXT_COLOR,
    shadowOpacity: 0.8,
    elevation: 6,
    shadowRadius: 15,
    shadowOffset: { width: 1, height: 13 }

  },
  input: {
    color: 'white',
    left: 20
  },
  item: {
    marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
    backgroundColor: Colors.PRIMARY_COLOR,
    borderColor: Colors.PRIMARY_COLOR,
  },
  form: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  TextInputStyle2: {
    height: 40,
    borderBottomColor: Colors.DARK_BLACK_TEXT_COLOR,
    width: 200,
    marginBottom: 10,
    borderBottomWidth: 1
  },
  backGround: {
    backgroundColor: Colors.PRIMARY_COLOR,

  },

  headerTextStyle: {
    left: 50,
    fontSize: 25,
    color: 'white',
  },
  header: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  headerStyle: {
    backgroundColor: Colors.PRIMARY_COLOR,
    shadowOpacity: 0.75,
    shadowRadius: 5,
    shadowOffset: { height: 0, width: 0 },

  },
  ToucherbleIconStyle: { color: Colors.PRIMARY_COLOR },
  ToucherbleIconStyle2: { height: 30, width: 30 },
  ToucherbleIconStyle3: { height: 30, width: 30 },

  logoContainer: {

    justifyContent: 'center',
    alignItems: 'center',
    marginTop: PixelRatio.getPixelSizeForLayoutSize(40),
    marginBottom: PixelRatio.getPixelSizeForLayoutSize(40),
  },
  logoText: {
    color: Colors.DARK_BLACK_TEXT_COLOR,
    fontSize: 24,
    fontWeight: '700',
    marginTop: 10
  },
  logoText1: {
    color: Colors.DARK_BLACK_TEXT_COLOR,
    fontSize: 15,
    fontWeight: '700',

  },
  backgroundImage: { width: "100%", height: "100%" },

  logo: {
    left: -3,
    height: PixelRatio.getPixelSizeForLayoutSize(150),
    width: PixelRatio.getPixelSizeForLayoutSize(150),
    resizeMode: 'contain',
  },
  place: {
    width: 25,
    height: 25,
    left: 10
  },
  buttonContainer: {
    flex: 1,
    marginTop: PixelRatio.getPixelSizeForLayoutSize(10),
  },
  forgotPasswordContainer: {
    alignItems: 'center',
    bottom: -150
  },
  forgotPasswordText: {
    color: 'white',
    fontSize: 16,
  },
  button: {
    marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  loginText: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
  },
  signupContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: PixelRatio.getPixelSizeForLayoutSize(3),
  },
  dontHaveAccountText: {
    color: '#bec0ce',
    fontSize: 16,
  },
  signupText: {
    color: '#000',
    fontSize: 10,
  },
});


export default styles;