
import React, { Component } from 'react';
import { TextInput, Image, TouchableOpacity, ImageBackground, StatusBar, BackHandler, FlatList, AsyncStorage, Alert, Dimensions} from 'react-native';
import {
    Text,
    Header,
    Left,
    Right,
    Item,
    Input,
} from 'native-base';
import styles from './invoiceScreenStyle';
import NetInfo from "@react-native-community/netinfo";
import Colors from '../../resources/Colors';
import Messages from '../../resources/Message';
import Strings from '../../resources/Strings';
import { View } from 'react-native-animatable';
import Spinner from "react-native-spinkit";

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

export default class ManageItem extends Component {
    constructor(props) {
        super(props);
        this.state = {

            token: '',
            data: [],
            spinner: false,
            searchText: "",
            nodata: false,
        };

    }

    //assaign search word
    search(text) {
        this.state.searchText = text;
        this.getInvoice()
    }

    onButtonPress = () => {

    }


    handleBackButton = () => {
        this.props.navigation.goBack(null);
        return true;
    };



    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }



    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        this.showData()


    }
    //getting auth code from AsyncStorage
    showData = async () => {
        let myArray = await AsyncStorage.getItem('myArray');
        let d = JSON.parse(myArray);


        this.setState({
            token: d

        })

        this.getInvoice()
        console.log(this.state.token);

    }

    //navigate to invoice show screen
    navigateToInvoiceDetailsScreen(item) {
        const { navigate } = this.props.navigation;
        navigate('InvoiceDetailsScreen', {
            id: item.id,
        })


    }


    //getting all of invoices
    getInvoice() {
        this.setState({
            spinner: true
        })
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + '/api/customer/invoice?page=0&limit=10000&text=' + this.state.searchText,
                    {
                        method: "GET",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },

                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        console.log("invoice" + JSON.stringify(responseJson.content));

                        this.setState({
                            data: responseJson.content.reverse(),
                            spinner: false
                        })
                        if (responseJson.content.length == 0) {
                            this.setState({
                                nodata: true
                            })
                        } else {
                            this.setState({
                                nodata: false
                            })
                        }


                    })


                    .catch(error => {
                        console.log(error);
                        this.setState({
                            spinner: false
                        })



                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.getInvoice()

                            }
                        },
                    ],
                    { cancelable: true }
                );


            }
        });

    }


    //no data iamge
    nodataImage() {
        if (this.state.nodata) {
            return (

                <View style={{ top: 60, justifyContent: 'center', alignItems: 'center', }}>
                    <Image style={{ width: 200, height: 200, borderRadius: 100 }}
                        source={require("../../assets/no_data_found.png")}
                    />

                </View>
            )
        }
    }


    render() {

        return (
            <View style={styles.container}>
                <ImageBackground
                    source={require("../../assets/newBack2.png")}
                    style={{ width: "100%", height: "100%" }}>

                    <View style={{ backgroundColor: '#126F85', width: '100%', height: 60, borderBottomLeftRadius: 50 }}>
                        <View style={{ backgroundColor: '#126F85', width: 50, top: 13, borderRadius: 60, left: 25, position: "absolute", top: 8 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('HomeScreen')}>
                                <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                        <Image source={require("../../assets/newBack3.png")} style={{ width: 20, height: 20 }}></Image>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <Text style={{ fontFamily: 'Ubuntu-Medium', color: '#ffff', justifyContent: "center", textAlign: "center", fontSize: 20, position: "absolute", top: 10, left: 100, fontWeight: "bold" }}>Invoice Details</Text>
                        <View style={{ backgroundColor: '#126F85', width: 50, top: 13, borderRadius: 60, right: 25, position: "absolute", top: 8 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('InvoiceSettingsScreen')}>
                                <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                        <Image source={require("../../assets/setting.png")} style={{ width: 20, height: 20 }}></Image>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <StatusBar barStyle="light-content" hidden={false} backgroundColor={'#126F85'} />

                    <View style={{ justifyContent: 'center', marginTop: -10, padding: 16, }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row', }}>
                            <Item
                                style={styles.item}
                                rounded
                                last >
                                <Image
                                    style={{ width: 25, height: 25, left: 10 }}
                                    source={require('../../assets/search.png')}
                                />
                                <Input
                                    style={{ color: 'white', left: 20 }}
                                    placeholder="Search Here"
                                    placeholderTextColor="#ffffff80"
                                    autoCapitalize="none"
                                    onChangeText={text => this.search(text)}
                                    value={this.state.text}
                                />
                            </Item>
                        </View>
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Spinner
                            isVisible={this.state.spinner}
                            type={'ThreeBounce'}
                            textStyle={{ color: 'white' }}
                            size={70}
                            color={Colors.FLAT_TEXT_COLOR}
                        />
                    </View>
                    {this.nodataImage()}

                    <FlatList
                        itemDimension={300}
                        data={this.state.data}
                        style={styles.gridView}
                        renderItem={({ item, index }) => (
                            <TouchableOpacity activeOpacity={0.8} style={{ justifyContent: 'center', alignItems: 'center' }} onPress={() => this.navigateToInvoiceDetailsScreen(item)}>
                                <View style={[styles.itemContainer, { backgroundColor: Colors.BACKGROUD_WHITE }]}>
                                    <Text style={styles.itemName}>Invoice Number : {item.invoiceNumber}</Text>
                                    <Text style={styles.itemName}>Invoice Date       : {item.invoiceDate.basic}</Text>
                                    <Text style={styles.itemName2}>Total Amount   : {item.grandTotalAmount}</Text>


                                </View>
                            </TouchableOpacity>

                        )}
                    />

                </ImageBackground>
            </View>



        );
    }
}