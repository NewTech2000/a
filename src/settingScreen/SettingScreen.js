import React, { Component } from 'react';
import {
    ActivityIndicator,
    Platform,
    StyleSheet,
    Text,
    Image,
    ImageBackground,
    View,
    ScrollView,
    DeviceEventEmitter,
    NativeEventEmitter,
    Switch,
    TouchableOpacity,
    StatusBar,
    Dimensions,
    AsyncStorage,
    BackHandler,
    PixelRatio,
    ToastAndroid,
    Alert
} from 'react-native';
import { BluetoothEscposPrinter, BluetoothManager, BluetoothTscPrinter } from "react-native-bluetooth-escpos-printer";
import {
    Header,
    Left,
    Right,
    Label,
    Card,
    Button
} from 'native-base';
import styles from './SettingScreenStyles'
import Colors from '../../resources/Colors'

export default class BluetoothSettings extends Component {


    _listeners = [];

    constructor() {
        super();
        this.state = {
            switchValue: false,
            barcode: false,
            devices: null,
            pairedDs: [],
            foundDs: [],
            bleOpend: false,
            loading: true,
            boundAddress: '',
            debugMsg: '',
            name: '',
            customItem: false,
            productImage: false,
            receiptandcharge: false
        }
    }



    //getting QuickSettings status code from AsyncStorage
    showData = async () => {
        let myArray = await AsyncStorage.getItem('quickSettings');
        let d = JSON.parse(myArray);
        let myArray1 = await AsyncStorage.getItem('barcode');
        let d1 = JSON.parse(myArray1);
        let myArray2 = await AsyncStorage.getItem('customItem');
        let d2 = JSON.parse(myArray2);
        let myArray3 = await AsyncStorage.getItem('productImage');
        let d3 = JSON.parse(myArray3);
        let myArray4 = await AsyncStorage.getItem('receiptandcharge');
        let d4 = JSON.parse(myArray4);


        this.setState({
            switchValue: d,
            barcode: d1,
            customItem: d2,
            productImage: d3,
            receiptandcharge: d4
        })

        console.log(JSON.stringify(d3));

    }


    //store barcode status code in AsyncStorage 
    _storeDataBarcode = async (value) => {
        // Alert.alert(value+"")

        this.setState({
            barcode: value
        })

        try {
            AsyncStorage.setItem('barcode', JSON.stringify(value));

        } catch (error) {
            console.error(error);
        }
    }

    //store customItem status code in AsyncStorage 
    _storeDataCustomItem = async (value) => {
        // Alert.alert(value+"")

        this.setState({
            customItem: value
        })

        try {
            AsyncStorage.setItem('customItem', JSON.stringify(value));

        } catch (error) {
            console.error(error);
        }
    }


    //store productImage status code in AsyncStorage 
    _storeDataProductImage = async (value) => {
        // Alert.alert(value+"")

        this.setState({
            productImage: value
        })

        try {
            AsyncStorage.setItem('productImage', JSON.stringify(value));

        } catch (error) {
            console.error(error);
        }
    }




    //store quickSettings status code in AsyncStorage 
    _storeData = async (value) => {
        // Alert.alert(value+"")

        this.setState({
            switchValue: value
        })

        try {
            AsyncStorage.setItem('quickSettings', JSON.stringify(value));

        } catch (error) {
            console.error(error);
        }
    }

    //store receiptandcharge status code in AsyncStorage 
    _storeReceiptandCharge = async (value) => {
        // Alert.alert(value+"")

        this.setState({
            receiptandcharge: value
        })

        try {
            AsyncStorage.setItem('receiptandcharge', JSON.stringify(value));

        } catch (error) {
            console.error(error);
        }
    }


    //store BLPAddress status code in AsyncStorage 
    _addtoBLPAddressAddress = async () => {

        try {
            AsyncStorage.setItem('BLPAddress', JSON.stringify(this.state.boundAddress));

        } catch (error) {
            console.error(error);
        }
    }

    onButtonPress = () => {

    }


    handleBackButton = () => {
        this.props.navigation.goBack();
        return true;
    };



    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }



    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        this.showData()
        //alert(BluetoothManager)
        BluetoothManager.isBluetoothEnabled().then((enabled) => {
            this.setState({
                bleOpend: Boolean(enabled),
                loading: false
            })
        }, (err) => {
            err
        });

        if (Platform.OS === 'ios') {
            let bluetoothManagerEmitter = new NativeEventEmitter(BluetoothManager);
            this._listeners.push(bluetoothManagerEmitter.addListener(BluetoothManager.EVENT_DEVICE_ALREADY_PAIRED,
                (rsp) => {
                    this._deviceAlreadPaired(rsp)
                }));
            this._listeners.push(bluetoothManagerEmitter.addListener(BluetoothManager.EVENT_DEVICE_FOUND, (rsp) => {
                this._deviceFoundEvent(rsp)
            }));
            this._listeners.push(bluetoothManagerEmitter.addListener(BluetoothManager.EVENT_CONNECTION_LOST, () => {
                this.setState({
                    name: '',
                    boundAddress: ''
                });
            }));
        } else if (Platform.OS === 'android') {
            this._listeners.push(DeviceEventEmitter.addListener(
                BluetoothManager.EVENT_DEVICE_ALREADY_PAIRED, (rsp) => {
                    this._deviceAlreadPaired(rsp)
                }));
            this._listeners.push(DeviceEventEmitter.addListener(
                BluetoothManager.EVENT_DEVICE_FOUND, (rsp) => {
                    this._deviceFoundEvent(rsp)
                }));
            this._listeners.push(DeviceEventEmitter.addListener(
                BluetoothManager.EVENT_CONNECTION_LOST, () => {
                    this.setState({
                        name: '',
                        boundAddress: ''
                    });
                }
            ));
            this._listeners.push(DeviceEventEmitter.addListener(
                BluetoothManager.EVENT_BLUETOOTH_NOT_SUPPORT, () => {
                    ToastAndroid.show("Device Not Support Bluetooth !", ToastAndroid.LONG);
                }
            ))
        }
    }



    _deviceAlreadPaired(rsp) {
        var ds = null;
        if (typeof (rsp.devices) == 'object') {
            ds = rsp.devices;
        } else {
            try {
                ds = JSON.parse(rsp.devices);
            } catch (e) {
            }
        }
        if (ds && ds.length) {
            let pared = this.state.pairedDs;
            pared = pared.concat(ds || []);
            this.setState({
                pairedDs: pared
            });
        }
    }

    _deviceFoundEvent(rsp) {//alert(JSON.stringify(rsp))
        var r = null;
        try {
            if (typeof (rsp.device) == "object") {
                r = rsp.device;
            } else {
                r = JSON.parse(rsp.device);
            }
        } catch (e) {//alert(e.message);
            //ignore
        }
        //alert('f')
        if (r) {
            let found = this.state.foundDs || [];
            if (found.findIndex) {
                let duplicated = found.findIndex(function (x) {
                    return x.address == r.address
                });
                //CHECK DEPLICATED HERE...
                if (duplicated == -1) {
                    found.push(r);
                    this.setState({
                        foundDs: found
                    });
                }
            }
        }
    }

    _renderRow(rows) {
        let items = [];
        for (let i in rows) {
            let row = rows[i];
            if (row.address) {
                items.push(
                    <TouchableOpacity key={new Date().getTime() + i} stlye={styles.wtf} onPress={() => {
                        this.setState({
                            loading: true
                        });
                        BluetoothManager.connect(row.address)
                            .then((s) => {
                                this.setState({
                                    loading: false,
                                    boundAddress: row.address,
                                    name: row.name || "UNKNOWN"
                                })
                                this._addtoBLPAddressAddress()
                            }, (e) => {
                                this.setState({
                                    loading: false
                                })
                                alert(e);
                            })

                    }}><View style={{ borderColor: '#126F85', marginTop: 10, flex: 1, padding: 5, height: 50, borderWidth: 2, borderRadius: 15, }}>
                            <Text style={styles.name}>{row.name || "UNKNOWN"}</Text><Text
                                style={styles.address}>{row.address}</Text>
                        </View>
                    </TouchableOpacity>
                );
            }
        }
        return items;
    }

    render() {
        return (
            <ImageBackground
                source={require("../../assets/newBack2.png")}
                style={{ width: "100%", height: "100%" }}>

                <View style={{ backgroundColor: '#126F85', width: '100%', height: 60, borderBottomLeftRadius: 50 }}>
                    <View style={{ backgroundColor: '#126F85', width: 50, top: 13, borderRadius: 60, left: 25, position: "absolute", top: 8 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('HomeScreen')}>
                            <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                    <Image source={require("../../assets/newBack3.png")} style={{ width: 20, height: 20 }}></Image>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Text style={{ fontFamily: 'Ubuntu-Medium', color: '#ffff', justifyContent: "center", textAlign: "center", fontSize: 20, position: "absolute", top: 10, left: 100, fontWeight: "bold" }}>Settings</Text>
                </View>

                <ScrollView style={styles.container}>
                    <StatusBar barStyle="light-content" hidden={false} backgroundColor={'#126F85'} />
                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 20 }}>
                        <View style={styles.togleButtonView}>

                            <Text style={{ fontSize: 20, color: Colors.DARK_BLACK_TEXT_COLOR, fontWeight: 'bold', left: 10, top: 20 }}>Quick billing</Text>
                            <Switch
                                style={{ left: -10 }}
                                value={this.state.switchValue}
                                onValueChange={(switchValue) => this._storeData(switchValue)} />
                        </View>
                    </View>
                    <Label></Label>
                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 20 }}>
                        <View style={styles.togleButtonView}>

                            <Text style={{ fontSize: 20, color: Colors.DARK_BLACK_TEXT_COLOR, fontWeight: 'bold', left: 10, top: 20 }}>Receipt & charge</Text>
                            <Switch
                                style={{ left: -10 }}
                                value={this.state.receiptandcharge}
                                onValueChange={(switchValue) => this._storeReceiptandCharge(switchValue)} />
                        </View>
                    </View>
                    <Label></Label>
                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 20 }}>
                        <View style={styles.togleButtonView}>

                            <Text style={{ fontSize: 20, color: Colors.DARK_BLACK_TEXT_COLOR, fontWeight: 'bold', left: 10, top: 20 }}>Custom item</Text>
                            <Switch
                                style={{ left: -10 }}
                                value={this.state.customItem}
                                onValueChange={(switchValue) => this._storeDataCustomItem(switchValue)} />
                        </View>
                    </View>
                    <Label></Label>
                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 20 }}>
                        <View style={styles.togleButtonView}>

                            <Text style={{ fontSize: 20, color: Colors.DARK_BLACK_TEXT_COLOR, fontWeight: 'bold', left: 10, top: 20 }}>Barcode</Text>
                            <Switch
                                style={{ left: -10 }}
                                value={this.state.barcode}
                                onValueChange={(switchValue) => this._storeDataBarcode(switchValue)} />
                        </View>
                    </View>
                    <Label></Label>
                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 20 }}>
                        <View style={styles.togleButtonView}>

                            <Text style={{ fontSize: 20, color: Colors.DARK_BLACK_TEXT_COLOR, fontWeight: 'bold', left: 10, top: 20 }}>Product image</Text>
                            <Switch
                                style={{ left: -10 }}
                                value={this.state.productImage}
                                onValueChange={(switchValue) => this._storeDataProductImage(switchValue)} />
                        </View>
                    </View>
                    <Label></Label>
                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 20 }}>
                        <View style={styles.togleButtonView}>

                            <Text style={{ fontSize: 20, color: Colors.DARK_BLACK_TEXT_COLOR, fontWeight: 'bold', left: 10, top: 20 }}>Bluetooth</Text>
                            <Switch value={this.state.bleOpend} style={{ left: -10 }} onValueChange={(v) => {
                                this.setState({
                                    loading: true
                                })
                                if (!v) {
                                    BluetoothManager.disableBluetooth().then(() => {
                                        this.setState({
                                            bleOpend: false,
                                            loading: false,
                                            foundDs: [],
                                            pairedDs: []
                                        });
                                    }, (err) => { alert(err) });

                                } else {
                                    BluetoothManager.enableBluetooth().then((r) => {
                                        var paired = [];
                                        if (r && r.length > 0) {
                                            for (var i = 0; i < r.length; i++) {
                                                try {
                                                    paired.push(JSON.parse(r[i]));
                                                } catch (e) {
                                                    //ignore
                                                }
                                            }
                                        }
                                        this.setState({
                                            bleOpend: true,
                                            loading: false,
                                            pairedDs: paired
                                        })
                                    }, (err) => {
                                        this.setState({
                                            loading: false
                                        })
                                        alert(err)
                                    });
                                }
                            }} />
                        </View>
                    </View>

                    <Label></Label>
                        <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 20 }}>
                            <View style={styles.togleButtonView}> 

                                <Text style={{ fontSize: 20, color: Colors.DARK_BLACK_TEXT_COLOR, fontWeight: 'bold', left: 10, top: 20 }}>Printer Settings</Text>
                                <Switch
                                    style={{ left: -10 }}
                                    // value={this.state.productImage}
                                    // onValueChange={}
                                     />
                            </View>
                        </View>
                        <Label></Label>


                    <View style={{
                        flex: 1,
                        justifyContent: 'flex-end',
                        padding: PixelRatio.getPixelSizeForLayoutSize(12),
                    }}>

                       
                        <View>
                            <Button
                                style={styles.button}
                                onPress={() => this._scan()}
                                hasText
                                block
                                large
                                dark
                                disabled={this.state.loading || !this.state.bleOpend}
                                rounded
                            >
                                <Text style={styles.loginText}>SCAN</Text>
                            </Button>

                            {/* <Button disabled={this.state.loading || !this.state.bleOpend} onPress={() => {
                            this._scan();
                        }} title="Scan" /> */}
                        </View>


                        <Text style={styles.title}>Connected:<Text style={{ color: "blue" }}>{!this.state.name ? 'No Devices' : this.state.name}</Text></Text>
                        <Text style={styles.title}>Found(tap to connect):</Text>
                        {this.state.loading ? (<ActivityIndicator animating={true} />) : null}
                        <View style={{ flex: 1, flexDirection: "column" }}>
                            {
                                this._renderRow(this.state.foundDs)
                            }
                        </View>
                        <Text style={styles.title}>Paired:</Text>
                    </View>
                    {this.state.loading ? (<ActivityIndicator animating={true} />) : null}
                    <View style={{ flex: 1, flexDirection: "column", justifyContent: "center", marginLeft: 20, marginRight: 20 }}>
                        {
                            this._renderRow(this.state.pairedDs)
                        }
                    </View>

                    <View style={{ flexDirection: "row", justifyContent: "space-around", paddingVertical: 30 }}>
                        <Button disabled={this.state.loading || !(this.state.bleOpend && this.state.boundAddress.length > 0)}
                            title="Connected" onPress={() => {
                                alert(this.state.name + "" + this.state.boundAddress)
                            }} />

                    </View>

                </ScrollView>
            </ImageBackground>

        );
    }

    _selfTest() {
        this.setState({
            loading: true
        }, () => {
            BluetoothEscposPrinter.selfTest(() => {
            });

            this.setState({
                loading: false
            })
        })
    }

    _scan() {
        this.setState({
            loading: true
        })
        BluetoothManager.scanDevices()
            .then((s) => {
                var ss = s;
                var found = ss.found;
                try {
                    found = JSON.parse(found);//@FIX_it: the parse action too weired..
                } catch (e) {
                    //ignore
                }
                var fds = this.state.foundDs;
                if (found && found.length) {
                    fds = found;
                }
                this.setState({
                    foundDs: fds,
                    loading: false
                });
            }, (er) => {
                this.setState({
                    loading: false
                })
                alert('error' + JSON.stringify(er));
            });
    }


}
