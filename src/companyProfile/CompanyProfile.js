import React, { Component } from 'react';
import { View, StyleSheet, ImageBackground, StatusBar, BackHandler,TouchableOpacity, Image, TouchableHighlight, Alert, AsyncStorage, Vibration, PixelRatio, ScrollView,Dimensions } from 'react-native';
import {
    Item,
    Input,
    Text,
    Button,
    Label
} from 'native-base';
import Colors from '../../resources/Colors';
import Strings from '../../resources/Strings';
import Messages from '../../resources/Message';
import CompressImage from "react-native-compress-image";
import ImagePicker from "react-native-image-picker";
import NetInfo from "@react-native-community/netinfo";
import Spinner from "react-native-spinkit";
import styles from './CompanyProfileStyles';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

export default class CompanyProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            selectedItem: 'About',
            arrayholder: [],
            temp: 0,
            text: '',
            avatarSource: null,
            image: null,
            photo: null,
            url: "",
            description: '',
            token: '',
            apiData: [],
            spinner: false,
            productId: "",
            uri: "",
            showTheThing: true,
            pKey: 0,
            photoUri: '',
            name: "",
            address: "",
            domain: "",
            businessType: '',
            background: '',
            username: '',
            password: '',
            companyName: '',
            companyContact: '',
            CompanyDomain: '',
            branchName: '',
            branchContact: '',
            branchAddress: '',
            nicNumber: '',
            userContact: "",
            companydesription: '',
            companyaddress: '',
            photo: null,
            uri:""


        };

    }
    onButtonPress = () => {

    }


    handleBackButton = () => {
        this.props.navigation.goBack(null);
        return true;
    };t



    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }


 
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
      

       
    }

    async componentWillMount() {
        let user = await AsyncStorage.getItem('companyDetails');
        let myArray = await AsyncStorage.getItem('myArray');
        let d = JSON.parse(myArray);
        this.setState({
            token: d

        })
        let parsed = JSON.parse(user);
        this.setState({
            name: parsed.name,
            address: parsed.address,
            nicNumber: parsed.nicNumber,
            description: parsed.description,
            username: parsed.username,
            userContact: parsed.userContact,
            domain: parsed.company.domain,
            companydesription: parsed.company.description,
            companyaddress: parsed.company.address,
            companyContact: parsed.company.customSettings.contact,
            businessType: parsed.company.customSettings.type,
            photo: { uri: parsed.imageUrl, },
            avatarSource: parsed.imageUrl,


        })
        console.log("sss" + this.state.name)
        console.log(parsed)
        // console.log("ddddd"+this.state.companyName)

    }

    // //upload Image
    // handleUploadPhoto = () => {

    //     if(this.state.avatarSource == this.state.photo.uri){
    //         this.setState({
    //             uri:this.state.avatarSource
    //         })
    //         this.updateUser()
    //     }else{
    //     this.setState({
    //         spinner: true,
    //     });

    //     fetch(Strings.Image_URL + "/api/files", {
    //         method: "POST",
    //         body: this.createFormData(this.state.photo, { userId: "123" })
    //     })
    //         .then(response => response.json())
    //         .then(response => {



    //             this.setState({
    //                 photo: null,
    //                 uri: response.url
    //             });
    //             this.setState({
    //                 spinner: false,
    //             });
    //             this.updateUser()

    //             console.log("upload succes", this.state.uri);

    //         })
    //         .catch(error => {
    //             this.setState({
    //                 spinner: false,
    //             });

    //             console.log("upload error", error);
    //             // alert("Upload failed!");
    //         });
    //     }

    // };


    //chooseing image
    handleChoosePhoto = () => {
        const options = {
            noData: true,
        }
        ImagePicker.launchImageLibrary(options, response => {
            console.log("upload succes", response);
            if (response.uri) {
                this.setState({
                    photo: response

                })
                this.compress()
            }
        })
    }


    //split image url
    createFormData = (photo, body) => {
        const data = new FormData();

        data.append("document", {
            name: photo.fileName,
            type: photo.type,
            uri:
                Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "")
        });

        Object.keys(body).forEach(key => {
            data.append(key, body[key]);
        });
        return data;
    };

    //To compress a image
    compress() {
        CompressImage.createCompressedImage(this.state.photo.uri, "Compress/Images")

            .then(response => {
                this.state.photo.uri = response.uri;
                this.state.photo.path = response.path;
                this.state.photo.mime = response.mime;
            })

            .catch(err => {
                return Alert.alert(
                    Strings.COMPRESS_ERR
                );
            });

    }



    onValueChange(value) {
        this.setState({
            businessType: value
        });
    }
    updateUser() {
        this.setState({
            spinner: true,
        });

        var data = JSON.stringify({
            "address": this.state.address,
            "branchId": 0,
            "contact": this.state.companyContact,
            "dateOfBirth": {
              "basic": "1990-10-12"
            },
            "description": this.state.description,
            "imageUrl": this.state.uri,
            "name":  this.state.name,
            "nicNumber": this.state.nicNumber,
            "roleId": 2,
            "username": this.state.username
        });

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + "/api/user/"+this.state.id,
                    {
                        method: "PUT",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },
                        body: data
                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        this.setState({
                            spinner: false,

                        });

                        Messages.messageName(Strings.WARNING_SUCESS, Strings.PROFILE_UPDATE_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                        console.log("dddddddddddddddddd"+JSON.stringify(responseJson));
                    })

                    .catch(error => {
                        this.setState({
                            spinner: false,
                        });
                        console.log(error);
                        Messages.messageName(Strings.WARNING_FAILED, Strings.PROFILE_UPDATE_FAIL, Strings.ICON[2], Strings.TYPE[2], Colors.SUCCESS_ALERT);


                    });

            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner2: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.saveItemPrice()

                            }
                        },
                    ],
                    { cancelable: true }
                );
            }
        });

    }



       //upload Image
       handleUploadPhoto = () => {

        if (this.state.photo == null) {
            // this.registerCustomer()
        } else {

            this.setState({
                spinner: true,
            });

            fetch(Strings.Image_URL + "/api/files", {
                method: "POST",
                body: this.createFormData(this.state.photo, { userId: "123" })
            })
                .then(response => response.json())
                .then(response => {



                    this.setState({
                        photo: null,
                        uri: response.url
                    });
                    this.setState({
                        spinner: false,
                    });
                   
                    console.log("upload succes", this.state.uri);
                    this.updateUser()

                })
                .catch(error => {
                    this.setState({
                        spinner: false,
                    });

                    console.log("upload error", error);
                    // alert("Upload failed!");
                });


        }

    };


    //chooseing image
    handleChoosePhoto = () => {
        const options = {
            noData: true,
        }
        this.setState({
            spinner: true,
        });
        
        ImagePicker.launchImageLibrary(options, response => {


            console.log("upload succes", response);
            if (response.uri) {
                this.setState({
                    photo: response

                })
                this.compress()
            }
        })
    }


    //split image url
    createFormData = (photo, body) => {
        const data = new FormData();

        data.append("document", {
            name: photo.fileName,
            type: photo.type,
            uri:
                Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "")
        });

        Object.keys(body).forEach(key => {
            data.append(key, body[key]);
        });
        return data;
    };

    //To compress a image
    compress() {
        CompressImage.createCompressedImage(this.state.photo.uri, "Compress/Images")

            .then(response => {
                this.state.photo.uri = response.uri;
                this.state.photo.path = response.path;
                this.state.photo.mime = response.mime;
            })

            .catch(err => {
                return Alert.alert(
                    Strings.COMPRESS_ERR
                );
            });

    }




    render() {

        return (
            <ImageBackground
                source={require("../../assets/newBack2.png")}
                style={{ width: "100%", height: "100%" }}>

                <View style={{ backgroundColor: '#126F85', width: '100%', height: 60, borderBottomLeftRadius: 50 }}>
                    <View style={{ backgroundColor: '#126F85', width: 50, top: 13, borderRadius: 60, left: 25, position: "absolute", top: 8 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('HomeScreen')}>
                            <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                    <Image source={require("../../assets/newBack3.png")} style={{ width: 20, height: 20 }}></Image>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Text style={{ fontFamily: 'Ubuntu-Medium', color: '#ffff', justifyContent: "center", textAlign: "center", fontSize: 20, position: "absolute", top: 10, left: 100, fontWeight: "bold" }}>User profile</Text>
                    <View style={{ backgroundColor: '#126F85', width: 50, top: 13, borderRadius: 60, right: 25, position: "absolute", top: 8 }}>
                        {/* <TouchableOpacity onPress={() => this.updateuser()}>
                            <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                    <Image source={require("../../assets/profilesave.png")} style={{ width: 20, height: 20 }}></Image>
                                </View>
                            </View>
                        </TouchableOpacity> */}
                    </View>
                </View>

                <StatusBar barStyle="light-content" hidden={false} backgroundColor={'#126F85'} />
                <ScrollView>
                    {/* //user Details Start */}
                    {/* <Text style={{ fontFamily: 'Ubuntu-Medium', color: '#126f85', justifyContent: "center", textAlign: "center", fontSize: 18, position: "absolute", top: 10, fontWeight: "bold", left: 130 }}>User Details</Text> */}

                    <View style={{
                        flex: 1,
                        justifyContent: 'flex-end',
                        padding: PixelRatio.getPixelSizeForLayoutSize(12),
                    }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <TouchableHighlight style={{ borderRadius: 100, height: 100, width: 100, marginBottom: 10, marginTop: 20 }}
                                onPress={() => this.handleChoosePhoto()}
                            >
                                <Image
                                    source={this.state.photo}
                                    style={{ width: 100, height: 100, borderRadius: 100, borderColor: Colors.PRIMARY_COLOR, borderWidth: 2, }}
                                />
                            </TouchableHighlight>
                        </View>

                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/owner.png')}
                            />
                            <Input
                                style={styles.input}
                                placeholder="Full Name"
                                placeholderTextColor="#ffffff80"
                                autoCapitalize="none"
                                value={this.state.name}
                                onChangeText={name => this.setState({ name })}
                            />
                        </Item>
                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/address.png')}
                            />
                            <Input
                                style={styles.input}
                                placeholder="Address"
                                placeholderTextColor="#ffffff80"
                                autoCapitalize="none"
                                value={this.state.address}
                                onChangeText={address => this.setState({ address })}
                            />
                        </Item>
                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/contact.png')}
                            />
                            <Input
                                keyboardType={"numeric"}
                                maxLength={10}
                                style={styles.input}
                                placeholder="Contact Number"
                                placeholderTextColor="#ffffff80"
                                autoCapitalize="none"
                                value={this.state.companyContact}
                                onChangeText={companyContact => this.setState({ companyContact })}
                            />

                        </Item>
                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/invoice.png')}
                            />
                            <Input

                                style={styles.input}
                                placeholder="NIC Number"
                                placeholderTextColor="#ffffff80"
                                autoCapitalize="none"
                                value={this.state.nicNumber}
                                onChangeText={nicNumber => this.setState({ nicNumber })}
                            />
                        </Item>
                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/description.png')}
                            />
                            <Input
                                style={styles.input}
                                placeholder="Description"
                                placeholderTextColor="#ffffff80"
                                autoCapitalize="none"
                                value={this.state.description}
                                onChangeText={description => this.setState({ description })}

                            />
                        </Item>
                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/user.png')}
                            />
                            <Input
                                style={styles.input}
                                placeholder="User Name"
                                placeholderTextColor="#ffffff80"
                                autoCapitalize="none"
                                value={this.state.username}
                                onChangeText={username => this.setState({ username })}
                            />
                        </Item>
                        <Label></Label>
                        <Button
                            style={styles.button}
                            onPress={() => this.handleUploadPhoto()}
                            hasText
                            block
                            large
                            dark
                            rounded
                        >
                            <Text style={styles.updateText}>UPDATE</Text>
                        </Button>
                    </View>

                    {/* End of UserDetails */}
                    {/* <View style={{ width: '100%', borderBottomColor: "#126F85", height: 5, borderBottomWidth: 3, bottom: 20 }}></View> */}

                    {/* Start of Company Details */}
                    {/* <View style={{
                        flex: 1,
                        justifyContent: 'flex-end',
                        padding: PixelRatio.getPixelSizeForLayoutSize(12),
                    }}>
                        <Text style={{ fontFamily: 'Ubuntu-Medium', color: '#126f85', justifyContent: "center", textAlign: "center", fontSize: 18, position: "absolute", top: 5, fontWeight: "bold", left: 110 }}>Company Details</Text>
                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/owner.png')}
                            />
                            <Input
                                style={styles.input}
                                placeholder="Company Name"
                                placeholderTextColor="#ffff"
                                autoCapitalize="none"
                                value={this.state.name}
                                onChangeText={name => this.setState({ name })}
                            />
                        </Item>
                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/business.png')}
                            />
                            <Input
                                style={styles.input}
                                placeholder="Domain Name"
                                placeholderTextColor="#ffff"
                                autoCapitalize="none"
                                value={this.state.domain}
                                onChangeText={domain => this.setState({ domain })}

                            />
                        </Item>


                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/description.png')}
                            />
                            <Input
                                style={styles.input}
                                placeholder="Description"
                                placeholderTextColor="#ffff"
                                autoCapitalize="none"
                                value={this.state.companydesription}
                                onChangeText={companydesription => this.setState({ companydesription })}
                            />
                        </Item>
                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/address.png')}
                            />
                            <Input
                                style={styles.input}
                                placeholder="Company Address"
                                placeholderTextColor="#ffff"
                                autoCapitalize="none"
                                value={this.state.companyaddress}
                                onChangeText={companyaddress => this.setState({ companyaddress })}
                            />
                        </Item>
                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/type.png')}
                            />
                            <Picker
                                style={{ width: 200, color: '#ffff', left: 10 }}
                                selectedValue={this.state.businessType}
                                onValueChange={this.onValueChange.bind(this)}

                            > */}
                    {/* <Picker.Item label={this.state.businessType} value={this.state.businessType} key={this.state.businessType} /> */}
                    {/* <Picker.Item label="Salon" value="Salon" />
                                <Picker.Item label="Juice Bar" value="Juice Bar" />
                                <Picker.Item label="Bakery" value="Bakery" />

                            </Picker>
                        </Item>
                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/contact.png')}
                            />
                            <Input
                                keyboardType={"numeric"}
                                maxLength={10}
                                style={styles.input}
                                placeholder="ContactNumber"
                                placeholderTextColor="#ffff"
                                autoCapitalize="none"
                                value={this.state.companyContact}
                                onChangeText={companyContact => this.setState({ companyContact })}
                            />
                        </Item>
                    </View> */}
                    {/* End Of Company Details */}
                    {/* <View style={{ width: '100%', borderBottomColor: "#126F85", height: 5, borderBottomWidth: 3, bottom: 20 }}></View> */}
                    {/* start of Branch Detail */}
                    {/* <View style={{
                        flex: 1,
                        justifyContent: 'flex-end',
                        padding: PixelRatio.getPixelSizeForLayoutSize(12),
                    }}>
                        <Text style={{ fontFamily: 'Ubuntu-Medium', color: '#126f85', justifyContent: "center", textAlign: "center", fontSize: 18, position: "absolute", top: 5, fontWeight: "bold", left: 110 }}>Branch Details</Text>
                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/branch3.png')}
                            />
                            <Input
                                style={styles.input}
                                placeholder="Branch Name"
                                placeholderTextColor="#ffff"
                                autoCapitalize="none"
                                value={this.state.name}
                                onChangeText={name => this.setState({ name })}
                            />
                        </Item>
                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/address.png')}
                            />
                            <Input
                                style={styles.input}
                                placeholder="Branch Address"
                                placeholderTextColor="#ffff"
                                autoCapitalize="none"

                            />
                        </Item>


                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/description.png')}
                            />
                            <Input
                                style={styles.input}
                                placeholder="Description"
                                placeholderTextColor="#ffff"
                                autoCapitalize="none"

                            />
                        </Item>

                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/contact.png')}
                            />
                            <Input
                                keyboardType={"numeric"}
                                maxLength={10}
                                style={styles.input}
                                placeholder="ContactNumber"
                                placeholderTextColor="#ffff"
                                autoCapitalize="none"

                            />
                        </Item> */}
                    {/* </View> */}
                </ScrollView>

            </ImageBackground>
        );
    }
}