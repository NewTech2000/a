import {
    PixelRatio,
    StyleSheet,
} from 'react-native';
import Colors from '../../resources/Colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#0a1142',
    },
    content: {
        flex: 1,
        justifyContent: 'flex-end',
        padding: PixelRatio.getPixelSizeForLayoutSize(12),
    },
    buttonContainer: {
        flex: 1,
        marginTop: PixelRatio.getPixelSizeForLayoutSize(10),
      },
    logoBack: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 150,
        width: 150,
        top: -30,
        borderRadius: 100,
        backgroundColor: '#ffffff90'
    },
    modal4: {
        height: 500,
        borderTopRightRadius: 30,
        borderTopLeftRadius: 30,
        backgroundColor: 'white'
    },



    gridView: {
        flex: 1,
    },
    itemContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 30,
        borderColor: Colors.PRIMARY_COLOR,
        borderWidth: 2,
        padding: 10,
        height: 100,
        shadowColor: Colors.DARK_BLACK_TEXT_COLOR,
        shadowOpacity: 0.8,
        elevation: 7,
        shadowRadius: 30,
        shadowOffset: { width: 1, height: 13 }

    },
    itemName: {
        fontSize: hp('2%'),
        color: 'black',
        fontWeight: 'bold',
    },
    itemName2: {
        fontSize: hp('2%'),
        color: 'black',
        fontWeight: 'bold',
    },
    itemCode: {
        fontWeight: '600',
        fontSize: hp('1.6%'),
        color: '#fff',
    },
    MainContainer: {
        justifyContent: 'center',
        flex: 1,
        paddingTop: 30,
    },
    imageThumbnail: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 100,
    },
    textInputStyle: {
        height: 50,
        borderWidth: 1,
        paddingLeft: 10,
        borderRadius: 10,
        color: Colors.BACKGROUD_WHITE,
        borderColor: Colors.PRIMARY_COLOR,
        backgroundColor: Colors.PRIMARY_COLOR,
    },
    viewStyle: {
        justifyContent: 'center',
        padding: 16,
    },
    ProcessText: {
        fontSize: hp('2.4%'),
        fontWeight: 'bold',
        justifyContent: 'center',
        alignItems: 'center',
        color: 'white'
    },
    ProcessButton: {
        backgroundColor: Colors.PRIMARY_COLOR
    },
    inputTextContainer: {
        left: 19
    },
    inputTextContainer2: {
        justifyContent: 'center',
        alignItems: 'center'

    },
    buttonContainerProcess: {
        height: hp('7%'),
        width: wp('80%'),
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 30,
        marginTop: 20,
        borderRadius: 30,
        shadowColor: Colors.DARK_BLACK_TEXT_COLOR,
        shadowOpacity: 0.8,
        elevation: 6,
        shadowRadius: 15,
        shadowOffset: { width: 1, height: 13 }

    },
    input: {
        color: Colors.BACKGROUD_WHITE,
        left: 20,
        maxWidth:('82%')
    },
    item: {
        marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
        backgroundColor: Colors.PRIMARY_COLOR,
        borderColor: Colors.PRIMARY_COLOR,
    },
    form: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    TextInputStyle2: {
        height: 40,
        borderBottomColor: Colors.DARK_BLACK_TEXT_COLOR,
        width: 200,
        marginBottom: 10,
        borderBottomWidth: 1
    },
    backGround: {
        backgroundColor: Colors.PRIMARY_COLOR,

    },

    headerTextStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: hp('3.3%'),
        left: 60,
        color: 'white',
    },
    header: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerStyle: {
        backgroundColor: Colors.PRIMARY_COLOR,
        shadowOpacity: 0.75,
        shadowRadius: 5,
        shadowOffset: { height: 0, width: 0 },

    },
    ToucherbleIconStyle: { color: Colors.PRIMARY_COLOR },
    ToucherbleIconStyle2: { height: 30, width: 30 },

    logoContainer: {

        justifyContent: 'center',
        alignItems: 'center',
        marginTop: PixelRatio.getPixelSizeForLayoutSize(40),
        marginBottom: PixelRatio.getPixelSizeForLayoutSize(40),
    },
    logoText: {
        color: Colors.DARK_BLACK_TEXT_COLOR,
        fontSize: hp('3.2%'),
        fontWeight: '700',
        marginTop: 10
    },
    logoText1: {
        color: Colors.DARK_BLACK_TEXT_COLOR,
        fontSize: hp('2%'),
        fontWeight: '700',

    },
    backgroundImage: { width: "100%", height: "100%" },

    logo: {
        left: -3,
        height: PixelRatio.getPixelSizeForLayoutSize(150),
        width: PixelRatio.getPixelSizeForLayoutSize(150),
        resizeMode: 'contain',
    },
    place: {
        width: 25,
        height: 20,
        left: 10
    },
    buttonContainer1: {
        flex: 1,
        // marginTop: PixelRatio.getPixelSizeForLayoutSize(10),
    },
    forgotPasswordContainer: {
        alignItems: 'center',
        bottom: -150
    },
    forgotPasswordText: {
        color: 'white',
        fontSize: hp('2.2%'),
    },
    button: {
        marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
        backgroundColor: Colors.PRIMARY_COLOR,
    },
    updateText: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',
    },
    signupContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: PixelRatio.getPixelSizeForLayoutSize(3),
    },
    dontHaveAccountText: {
        color: '#bec0ce',
        fontSize: hp('2.2%'),
    },
    signupText: {
        color: '#000',
        fontSize: 10,
    },
});


export default styles;