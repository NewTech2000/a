import React, { Component } from 'react';
import { View, TextInput, Image, TouchableOpacity, ImageBackground, BackHandler, StatusBar, Alert, FlatList, ScrollView, AsyncStorage, PixelRatio, Animated } from 'react-native';
import {
    Button,
    Container,
    Content,
    Form,
    Item,
    Input,
    Header,
    Left,
    Right,
    Text,
    DatePicker,
    Label,
    Picker
} from 'native-base';
import Colors from '../../resources/Colors';
import Strings from '../../resources/Strings';
import Messages from '../../resources/Message';
import Spinner from 'react-native-loading-spinner-overlay';
import styles from './PaymentScreenStyle';
import Modal from 'react-native-modalbox';
import NetInfo from "@react-native-community/netinfo";
import Moment from 'moment';
import VirtualKeyboard from 'react-native-virtual-keyboard';
import { BluetoothEscposPrinter, BluetoothManager } from "react-native-bluetooth-escpos-printer";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/AntDesign';
import { CreditCardInput, LiteCreditCardInput } from "react-native-credit-card-input";
import Icon2 from 'react-native-vector-icons/FontAwesome5';
import { SafeAreaView } from 'react-navigation';
var dateFormat = require('dateformat');

export default class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            text: '',
            text2: '',
            text3: '',
            grandTotal: 0,
            subTotal: '',
            discount: 0,
            sub: '',
            grand: '',
            cus: "",
            backgroundColor1: Colors.BACKGROUD_WHITE,
            backgroundColor2: Colors.PRIMARY_COLOR,
            textColor: Colors.DARK_BLACK_TEXT_COLOR,
            customerColor: Colors.BACKGROUD_WHITE,
            dis: '',
            amount: '',
            balance: 0,
            spinner: false,
            token: "",
            customer: "",
            btnSelected: 1,
            receivedPrice: 0,
            customerId: "",
            custName: "",
            orderDetails: [],
            payment: [],
            qtyCount: 0,
            date: "",
            underButton: true,
            invoiceNumber: "",
            tot: '',
            invoiceDescription: "",
            bluetoothConnected: 0,
            companyName: '',
            companyAddress: '',
            companyContactNumber: '',
            image: 0,
            charge: true,
            addToPayment: false,
            paymenttypeVisible: false,
            cashType: "CASH",
            cardType: "CARD",
            creditBook: "CreditBook",
            checkType: "CHEQUE",
            grandTotal2: 0,
            calculateBalance: "",
            cardpaymentVisible: true,
            cardAddPaymentVisible: false,
            checkpaymentVisible: true,
            checkAddPaymentVisible: false,
            descriptioncheck: '',
            expiredate: "",
            chequenumber: '',
            bank: '',
            cardnumber: '',
            cardStatus: '',
            descriptioncard: '',
            balanaceVisible: true,
            barcode: false,
            finalCashPayment: [],
            finalCardPayment: [],
            finalChequePayment: [],
            bluetoothConnected: 0,
            bank2: "",
            switchValue: false,
            disAmount: 0,
            receiptandcharge: false,
            printerStatus: false,
            dayPeriod: "",
            BLPAddress:"",
            backClickCount: 0,
            modalVisible: false,

        }
        //  var expiredate = new Date();
        // this.state = { chosenDate: new Date() };
        // this.setDate = this.setDate.bind(this);


    }


    financial(x) {
        return Number.parseFloat(x).toFixed(2);
    }


    componentDidUpdate() {
        this.state.customerId = this.props.navigation.getParam('customerId', '')
        this.state.custName = this.props.navigation.getParam('customerName', 'Unknown')

    }

    bluetoothConnectedChecking() {
        BluetoothManager.connect(this.state.BLPAddress)
            .then((s) => {

                console.log("connected")
                this.setState({
                    bluetoothConnected: 1,

                })
            },
                (err) => {
                    console.log("connected" + err)
                    this.setState({
                        bluetoothConnected: 0
                    })
                })
    }

    /////////////////////////////////////////////////////////////// this is error code

    _spring() {
        this.setState({ backClickCount: 1 }, () => {
            //     Animated.sequence([
            //         Animated.spring(
            //             this.springValue,
            //             {
            //                 toValue: -.15,
            //                 friction: 5,
            //                 duration: 300,
            //                 useNativeDriver: true,
            //             }
            //         ),
            //         Animated.timing(
            //             this.springValue,
            //             {
            //                 toValue: 100,
            //                 duration: 300,
            //                 useNativeDriver: true,
            //             }
            //         ),

            //     ]).start(() => {
            //         this.setState({ backClickCount: 0 });
            //     });
        });

    }
    closeAll() {
        this.closeOneItemModel2();
        this.closeOneItemModel3();
        this.closeOneItemModel4();
        this.closeOneItemModel5();
        // this.closeOneItemModel6();
    }
    onButtonPress = () => {

    }


    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }
    handleBackButton = () => {
       
        backHandlerClickCount=0
        if (this.state.backClickCount) {
            this.closeAll() 
        }else if (this) {
          this.closeAll() & this.props.navigation.goBack(null);
            return true;
        }

    };


    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    _onChange = (formData) => this.setState({ cardnumber: formData.values.number, cardStatus: formData.status.number });
    _onFocus = (field) => console.log("focusing", field);
    _setUseLiteCreditCardInput = (useLiteCreditCardInput) => this.setState({ useLiteCreditCardInput });


    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        // BluetoothManager.connect(this.state.BLPAddress)
        //     .then((s) => {
        //         this.setState({
        //             bluetoothConnected: 1
        //         })
        //     },
        //         (err) => {
        //             this.setState({
        //                 bluetoothConnected: 0
        //             })
        //         })



        this.state.grandTotal = this.props.navigation.getParam('grandTot', 'nothing sent')
        this.state.grandTotal2 = this.props.navigation.getParam('grandTot', 'nothing sent')
        this.state.receivedPrice = this.props.navigation.getParam('grandTot', 'nothing sent')
        this.state.orderDetails = this.props.navigation.getParam('orderDetails', 'nothing sent')
        this.state.subTotal = this.props.navigation.getParam('subTot', 'nothing sent')
        this.state.discount = this.props.navigation.getParam('discount', 0)
        this.state.switchValue = this.props.navigation.getParam('switchValue', false)
        this.state.disAmount = this.props.navigation.getParam('disAmount', 0)
        var temp;
        console.log("sssssss" + JSON.stringify(this.state.disAmount + "" + this.state.discount));
        for (let index = 0; index < this.state.orderDetails.length; index++) {
            this.state.qtyCount = this.state.qtyCount + this.state.orderDetails[index].qty
            var TPrice;
            if (this.state.discount > 0) {
                if (this.state.switchValue == true) {
                    temp = this.state.discount * 100 / this.state.subTotal
                    TPrice = this.state.subTotal * temp / 100
                    console.log(TPrice)
                    this.setState({
                        disAmount: TPrice,
                        discount: temp
                    })
                    console.log(TPrice)
                } else {
                    TPrice = this.state.subTotal * this.state.discount / 100
                    this.setState({
                        disAmount: TPrice
                    })
                    console.log(TPrice)

                }
            }

        }

        this.showData()

        this.setState({
            date: Moment().format('YYYY-MM-D HH:mm:ss')
        })

        this.setState({
            sub: this.state.subTotal,
            dis: this.state.discount,
            grand: this.state.grandTotal,
            tot: this.state.grandTotal.toString()

        })

    }



    ////////////////////////////////////////////////////////////////////////////////////////////////

    async componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        
        let user = await AsyncStorage.getItem('companyDetails');
        let parsed = JSON.parse(user);
        let myArray1 = await AsyncStorage.getItem('barcode');
        let d1 = JSON.parse(myArray1);
        let myArray4 = await AsyncStorage.getItem('receiptandcharge');
        let d4 = JSON.parse(myArray4);
        let myArray5 = await AsyncStorage.getItem('BLPAddress');
        let d5 = JSON.parse(myArray5);
        if (d4 == undefined) {
            this.setState({
                barcode: false
            })
        } else {
            this.setState({
                barcode: d4
            })
        }
        this.setState({
            receiptandcharge: d4,
            BLPAddress:d5,
            companyName: parsed.company.name,
            companyAddress: parsed.company.address,
            companyContactNumber: parsed.company.customSettings.contact

        })

        console.log("nnnnnnnnnnnnnnnnnnnnnnnnn" + JSON.stringify(this.state.BLPAddress))
        if(this.state.BLPAddress !== null){
            this.bluetoothConnectedChecking()
        }
    }

    generateUniqueInvoiceNumber(text) {
        this.setState({
            spinner: true
        })

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + "/api/customer-invoice-number/unique/generate",
                    {
                        method: "GET",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },

                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        console.log("InvoiceNumber" + JSON.stringify(responseJson));

                        if (responseJson.errorMessage == 'Resource does not exists') {
                            this.setState({
                                spinner: false,
                            })
                            this.props.navigation.navigate('InvoiceSettingsScreenForPayment')
                        } else {

                            this.setState({
                                invoiceNumber: responseJson.prefix + responseJson.nextInvoiceNumber,
                                spinner: false
                            })

                            if (text == "credit") {
                                this.refs.creditBook.open()
                                // this.invoice()
                            } else if (text == "receipt") {
                                this.receipt()
                            } else {
                                this.invoice()
                            }


                        }


                    })

                    .catch(error => {
                        console.log(error);
                        this.setState({
                            spinner: false,
                        })


                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false,
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.generateUniqueInvoiceNumber()

                            }
                        },
                    ],
                    { cancelable: true }
                );
            }
        });

    }


    printRecept() {
        this.generateUniqueInvoiceNumber('receipt')
    }

    //print receipt confiug
    receipt = async () => {
        this.setState({
            spinner: true,
        });
        try {
            await BluetoothEscposPrinter.printerInit();
            await BluetoothEscposPrinter.printerLeftSpace(0);
            await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.CENTER);
            await BluetoothEscposPrinter.setBlob(0);
            // await BluetoothEscposPrinter.printText(this.state.companyName+"\r\n", {
            //     encoding: 'GBK',
            //     codepage: 0,
            //     widthtimes: 1,
            //     heigthtimes: 1,
            //     fonttype: 1
            // });
            await BluetoothEscposPrinter.printText("--------------------------------\r\n", {});
            await BluetoothEscposPrinter.printText(this.state.companyName + "\r\n", { encoding: 'GBK', fonttype: 11 });
            await BluetoothEscposPrinter.setBlob(0);
            await BluetoothEscposPrinter.printText(this.state.companyAddress + "\r\n", {
                encoding: 'GBK',
                codepage: 0,
                widthtimes: 0,
                heigthtimes: 0,
                fonttype: 1
            });
            await BluetoothEscposPrinter.printText("Tel :" + this.state.companyContactNumber + "\r\n", {});
            await BluetoothEscposPrinter.printText("--------------------------------\r\n", {});
            await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.LEFT);
            await BluetoothEscposPrinter.printText("Date  : " + (dateFormat(new Date(), "yyyy-mm-dd h:MM:ss")) + "\r\n", {});
            await BluetoothEscposPrinter.printText("Customer : " + this.state.custName + "\n\r", {});
            await BluetoothEscposPrinter.printText("cashier : Admin\r\n", {});
            await BluetoothEscposPrinter.printText("Invoice Number :" + this.state.invoiceNumber + "\r\n", {});
            await BluetoothEscposPrinter.printText("--------------------------------\r\n", {});
            let columnWidths = [12, 6, 6, 8];
            await BluetoothEscposPrinter.printColumn(columnWidths,
                [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.CENTER, BluetoothEscposPrinter.ALIGN.CENTER, BluetoothEscposPrinter.ALIGN.RIGHT],
                ['Name', "Qty", 'Price', 'Total',], {});
            for (let index = 0; index < this.state.orderDetails.length; index++) {
                await BluetoothEscposPrinter.printText("\r\n", {});
                await BluetoothEscposPrinter.printColumn(columnWidths,
                    [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.CENTER, BluetoothEscposPrinter.ALIGN.RIGHT],
                    [this.state.orderDetails[index].name + "", this.state.orderDetails[index].qty + "", this.state.orderDetails[index].salePrice + "", this.state.orderDetails[index].total + ""]
                    , {});
                if (this.state.orderDetails[index].discountPercentage > 0) {
                    if (this.state.orderDetails[index].customFields.type == "value") {
                        await BluetoothEscposPrinter.printText("dis. Rs." + this.state.orderDetails[index].discountPercentage, {});
                    } else {
                        await BluetoothEscposPrinter.printText("dis. " + this.state.orderDetails[index].discountPercentage + "%", {});
                    }

                }

            }
            await BluetoothEscposPrinter.printText("\r\n", {});
            await BluetoothEscposPrinter.printText("--------------------------------\r\n", {});
            await BluetoothEscposPrinter.printText("\r\n", {});
            await BluetoothEscposPrinter.printText("No of items : " + this.state.qtyCount + "\r\n", {});
            await BluetoothEscposPrinter.printText("Sub Total  :" + this.financial(this.state.subTotal) + "\r\n", {});
            await BluetoothEscposPrinter.printText("Discount   : " + this.state.discount + "%" + "\r\n", {});
            await BluetoothEscposPrinter.setBlob(0);
            await BluetoothEscposPrinter.printText("Net Total :Rs." + this.financial(this.state.grandTotal) + "\r\n", {
                encoding: 'GBK',
                codepage: 0,
                widthtimes: 3,
                heigthtimes: 3,
                fonttype: 1
            });

            await BluetoothEscposPrinter.printText("paid amount : Rs." + this.state.receivedPrice + "\r\n", {});
            await BluetoothEscposPrinter.printText("Balance    : Rs." + this.financial(this.state.balance) + "\r\n", {});
            await BluetoothEscposPrinter.printText("--------------------------------\r\n", {});
            {
                this.state.barcode &&
                    await BluetoothEscposPrinter.printBarCode("123456789012", BluetoothEscposPrinter.BARCODETYPE.JAN13, 3, 120, 0, 2);
            }
            await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.CENTER);
            await BluetoothEscposPrinter.printText("Thank you Come again.....!\r\n\r\n\r\n", {});
            await BluetoothEscposPrinter.printText("--------------------------------\r\n", {});
            await BluetoothEscposPrinter.setBlob(0);
            await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.CENTER);
            await BluetoothEscposPrinter.printText("Powered by - Billa cloud\r\n", {
                encoding: 'GBK',
                fonttype: 11
            });
            await BluetoothEscposPrinter.setBlob(0);
            await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.CENTER);
            await BluetoothEscposPrinter.printText("0714770888\r\n", {
                encoding: 'GBK',
                fonttype: 11
            });
            await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.LEFT);
            await BluetoothEscposPrinter.printText("\r\n\r\n\r\n", {});
        } catch (e) {
            console.log(JSON.stringify(e.code));

            if (e.code == "EUNSPECIFIED") {
                this.setState({
                    printerStatus: true,
                    spinner: false
                })
                Alert.alert(
                    'Connection Error!',
                    'there is something wrong with printer connection',
                    [
                        { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                        {
                            text: 'Navigate to Settings', onPress: () => {

                                this.props.navigation.navigate('PrinterSettingScreen')
                                this.refs.modal4.close()

                            }
                        },
                    ],
                    { cancelable: true }
                );

            }

        }
        if (this.state.printerStatus == false) {
            if (this.state.receiptandcharge == true) {
                this.setState({
                    spinner: false,
                });
                this.navigateToDone()
                // }else{
                // Alert.alert("working")
            } else {
                this.setState({
                    spinner: false,
                });
            }
            // }

        }
    }

    calculateDis(x) {
        return;
    }

    //getting Authorization code
    showData = async () => {
        let myArray = await AsyncStorage.getItem('myArray');
        let d = JSON.parse(myArray);

        this.setState({
            token: d

        })
        console.log(this.state.token);

    }

    //place Order
    invoice() {



        console.log("vvvvvvvvvvvv" + JSON.stringify(this.state.finalCashPayment))
        this.setState({
            spinner: true,
        });
        var data = JSON.stringify({
            "additionalCostAmount": 0,
            "charges": [],
            "creditDayPeriod": 30,
            "customFields": { "cashGivenAmount": this.state.receivedPrice },
            "customerId": this.state.customerId,
            "description": "string",
            "invoiceDate": {
                "basic": this.state.date,
            },
            "invoiceDiscountAmount": this.state.disAmount,
            "invoiceDiscountPercentage": this.state.discount,
            "invoiceNumber": this.state.invoiceNumber,
            "items": this.state.orderDetails,
            "payments": this.state.finalCashPayment,
            "serviceChargeAmount": 0,
            "taxes": [],


        });

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + '/api/customer/invoice',
                    {
                        method: "POST",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },
                        body: data
                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        Messages.messageName(Strings.WARNING_SUCESS, Strings.PLACE_ORDER_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                        console.log("xxxxxxxxxxx" + JSON.stringify(responseJson));
                        const balance = this.state.receivedPrice - this.state.grandTotal
                        this.props.navigation.navigate('DoneScreen', {
                            total: this.state.grandTotal,
                            balance: balance

                        });
                        this.setState({
                            spinner: false
                        });
                    })

                    .catch(error => {
                        console.log(error);
                        this.setState({
                            spinner: false,
                        });
                        Messages.messageName(Strings.WARNING_FAILED, Strings.PLACE_ORDER_FAILED, Strings.ICON[2], Strings.TYPE[2], Colors.DARK_BLACK_TEXT_COLOR);

                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.invoice()

                            }
                        },
                    ],
                    { cancelable: true }
                );
            }
        });


    }

    navigateToDone() {
        if (this.state.payment.length == 0) {
            
            if (this.state.customerId !== "") {
                Alert.alert(
                    'Creditbook message',
                    'Customer has been already selected!',
                    [
                        { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                        {
                            text: 'Add to creditbook', onPress: () => {
                               
                                this.addToCreditBook()


                            }
                        },
                    ],
                    { cancelable: true }
                );

            } else {
            this.state.finalCashPayment.push({ "amount": this.state.grandTotal2, "description": "unknown", "paymentMethod": this.state.cashType })
            console.log("vvvvvvvvvvvvvvvvvvvvvv" + JSON.stringify((this.state.finalCashPayment)))
            this.invoice()
            }
        } else {
            var finalAmountCash = 0;
            var finalAmountCard = 0;
            var finalAmountCheque = 0;
            for (let index = 0; index < this.state.payment.length; index++) {

                if (this.state.payment[index].paymentMethod == "CASH") {
                    finalAmountCash = finalAmountCash + this.state.payment[index].amount

                } else if (this.state.payment[index].paymentMethod == 'CARD') {
                    finalAmountCard = finalAmountCard + this.state.payment[index].amount

                } else if (this.state.payment[index].paymentMethod == 'CHEQUE') {
                    finalAmountCheque = finalAmountCheque + this.state.payment[index].amount
                }

            }

            console.log(finalAmountCash)
            console.log(finalAmountCard)
            console.log(finalAmountCheque)
            if (finalAmountCard > 0) {
                this.state.finalCashPayment.push({ "amount": finalAmountCard, "description": "unknown", "paymentMethod": this.state.cardType })
            }
            if (finalAmountCheque > 0) {
                this.state.finalCashPayment.push({ "amount": finalAmountCheque, "description": "unknown", "paymentMethod": this.state.checkType })
            }

            if (this.state.customerId !== "") {
                Alert.alert(
                    'Creditbook message',
                    'Customer has been already selected!',
                    [
                        { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                        {
                            text: 'Add to creditbook', onPress: () => {
                                // if(){

                                // }
                                this.state.finalCashPayment.push({ "amount": finalAmountCash, "description": "unknown", "paymentMethod": this.state.cashType })
                                console.log("zzzzzzzzzzzzzzzzzzzzzzzzzzzzzz" + JSON.stringify((this.state.finalCashPayment)))
                                this.addToCreditBook()


                            }
                        },
                    ],
                    { cancelable: true }
                );

            } else {
                if (this.state.grandTotal2 > finalAmountCash) {
                    var final = finalAmountCash + this.state.grandTotal2
                    this.state.finalCashPayment.push({ "amount": final, "description": "unknown", "paymentMethod": this.state.cashType })
                    console.log("ssssssssss"+JSON.stringify((this.state.finalCashPayment)))
               
                }
                this.invoice()
            }

        }

        // if (this.state.invoiceNumber == "") {
        //     this.generateUniqueInvoiceNumber()
        //     // Alert.alert("empty")
        // } else {

        // Alert.alert("no")

        // }

    }

    calculateTotal(text) {
        this.state.receivedPrice = text;
        const balance = this.state.receivedPrice - this.state.grandTotal2

        if (text == "") {
            this.setState({
                balance: 0,
                tot: 0,
                charge: true,
                addToPayment: false,
                textColor: Colors.DARK_BLACK_TEXT_COLOR,

            })
        } else {

            if (text < this.state.grandTotal2) {
                this.setState({
                    backgroundColor2: Colors.BTN_COLOR,
                    textColor: Colors.ERR_COLOR,
                    charge: false,
                    addToPayment: true,
                    cardpaymentVisible: false,
                    cardAddPaymentVisible: true,
                    checkpaymentVisible: false,
                    checkAddPaymentVisible: true

                })
            } else {
                this.setState({
                    backgroundColor2: Colors.PRIMARY_COLOR,
                    textColor: Colors.DARK_BLACK_TEXT_COLOR,
                    charge: true,
                    addToPayment: false,
                    cardpaymentVisible: true,
                    cardAddPaymentVisible: true,
                    checkpaymentVisible: true,
                    checkAddPaymentVisible: false
                })
            }

            this.setState({
                balance: balance,
                tot: balance.toString().substring(1),

            })
        }

    }
    calculateCardTotal(text2) {
        this.state.receivedPrice = text2;
        const balance = this.state.receivedPrice - this.state.grandTotal2

        if (text2 == "") {
            this.setState({
                balance: 0,
                tot: 0,
                cardpaymentVisible: true,
                cardAddPaymentVisible: false,
                textColor: Colors.DARK_BLACK_TEXT_COLOR,

            })
        } else {

            if (text2 < this.state.grandTotal2) {
                this.setState({
                    backgroundColor2: Colors.BTN_COLOR,
                    textColor: Colors.ERR_COLOR,

                    cardpaymentVisible: false,
                    cardAddPaymentVisible: true,

                })
            } else {
                this.setState({
                    backgroundColor2: Colors.PRIMARY_COLOR,
                    textColor: Colors.DARK_BLACK_TEXT_COLOR,

                    cardpaymentVisible: true,
                    cardAddPaymentVisible: false,

                })
            }

        }

    }
    calculateCheckTotal(text3) {
        this.state.receivedPrice = text3;
        const balance = this.state.receivedPrice - this.state.grandTotal2
        if (text3 == "") {
            this.setState({
                checkpaymentVisible: true,
                checkAddPaymentVisible: false,
                textColor: Colors.DARK_BLACK_TEXT_COLOR,

            })
        } else {
            if (text3 < this.state.grandTotal2) {
                this.setState({
                    backgroundColor2: Colors.BTN_COLOR,
                    textColor: Colors.ERR_COLOR,
                    checkpaymentVisible: false,
                    checkAddPaymentVisible: true,

                })
            } else {
                this.setState({
                    backgroundColor2: Colors.PRIMARY_COLOR,
                    textColor: Colors.DARK_BLACK_TEXT_COLOR,
                    checkpaymentVisible: true,
                    checkAddPaymentVisible: false,

                })
            }
        }

    }

    cheque() {
        this.refs.cheque.open()
        this.setState({
            underButton: false,
            checkpaymentVisible: true,
            checkAddPaymentVisible: false,
            btnSelected: 3,
            image: 3,
            balance: Math.abs(this.state.receivedPrice - this.state.grandTotal),
            textColor: Colors.DARK_BLACK_TEXT_COLOR,

        })

    }
    checkValidation() {
        var a = parseInt(this.state.receivedPrice)
        if (this.state.bank2 == "") {
            Messages.messageName(Strings.WARNING, Strings.BANK_ADD_FAIL, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        } else if (this.state.chequenumber == "") {
            Messages.messageName(Strings.WARNING, Strings.CHECKNUM_ADD_FAIL, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        } else if (a == 0) {
            Messages.messageName(Strings.WARNING, Strings.INVALID_VALUE, Strings.ICON[0], Strings.TYPE[0], Colors.DARK_BLACK_TEXT_COLOR);
        } else if (a > this.state.grandTotal2) {
            Messages.messageName(Strings.WARNING, Strings.HIGH_VALUE, Strings.ICON[0], Strings.TYPE[0], Colors.DARK_BLACK_TEXT_COLOR);
        } else if (this.state.expiredate == "") {
            Messages.messageName(Strings.WARNING, Strings.CHECKEXPIREDATE_ADD_FAIL, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        } else if (this.state.descriptioncheck == "") {
            Messages.messageName(Strings.WARNING, Strings.CHECKDESCRIP_ADD_FAIL, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        } else {
            if (this.state.payment.length == 0) {
                this.state.finalCashPayment.push({ "amount": this.state.grandTotal2, "description": "unknown", "paymentMethod": this.state.checkType })
                console.log(JSON.stringify((this.state.finalCashPayment)))
            } else {
                var finalAmountCash = 0;
                var finalAmountCard = 0;
                var finalAmountCheque = 0;
                for (let index = 0; index < this.state.payment.length; index++) {

                    if (this.state.payment[index].paymentMethod == "CASH") {
                        finalAmountCash = finalAmountCash + this.state.payment[index].amount

                    } else if (this.state.payment[index].paymentMethod == 'CARD') {
                        finalAmountCard = finalAmountCard + this.state.payment[index].amount

                    } else if (this.state.payment[index].paymentMethod == 'CHEQUE') {
                        finalAmountCheque = finalAmountCheque + this.state.payment[index].amount
                    }

                }

                console.log(finalAmountCash)
                console.log(finalAmountCard)
                console.log(finalAmountCheque)
                if (finalAmountCash > 0) {
                    this.state.finalCashPayment.push({ "amount": finalAmountCash, "description": "unknown", "paymentMethod": this.state.cashType })
                }
                if (finalAmountCard > 0) {
                    this.state.finalCashPayment.push({ "amount": finalAmountCard, "description": "unknown", "paymentMethod": this.state.cardType })
                }
                if (this.state.customerId !== "") {
                    Alert.alert(
                        'Creditbook message',
                        'Customer has been already selected!',
                        [
                            { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                            {
                                text: 'Add to creditbook', onPress: () => {
                                    // if(){
    
                                    // }
                                    this.state.finalCashPayment.push({ "amount": finalAmountCheque, "description": "unknown", "paymentMethod": this.state.checkType })
                                    console.log("zzzzzzzzzzzzzzzzzzzzzzzzzzzzzz" + JSON.stringify((this.state.finalCashPayment)))
                                    this.addToCreditBook()
    
    
                                }
                            },
                        ],
                        { cancelable: true }
                    );
    
                } else {

                if (this.state.grandTotal2 > finalAmountCheque) {
                    var final = finalAmountCheque + this.state.grandTotal2
                    this.state.finalCashPayment.push({ "amount": final, "description": "unknown", "paymentMethod": this.state.checkType })
                    console.log(JSON.stringify((this.state.finalCashPayment)))
                    this.invoice()
                }
            }
            }
        }

    }

    detectCardType(number) {
        var re = {
            electron: /^(4026|417500|4405|4508|4844|4913|4917)\d+$/,
            maestro: /^(5018|5020|5038|5612|5893|6304|6759|6761|6762|6763|0604|6390)\d+$/,
            dankort: /^(5019)\d+$/,
            interpayment: /^(636)\d+$/,
            unionpay: /^(62|88)\d+$/,
            visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
            mastercard: /^5[1-5][0-9]{14}$/,
            amex: /^3[47][0-9]{13}$/,
            diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
            discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
            jcb: /^(?:2131|1800|35\d{3})\d{11}$/
        }

        for (var key in re) {
            if (re[key].test(number) === false) {
                return false
            } else {
                return true
            }
        }
    }

    cardValidation() {
        var a = parseInt(this.state.receivedPrice)
        if (this.state.bank == "") {
            Messages.messageName(Strings.WARNING, Strings.BANK_ADD_FAIL, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        } else if (this.state.cardnumber == "" || this.state.cardnumber.length < 19 || this.state.cardStatus == "invalid") {
            Messages.messageName(Strings.WARNING, Strings.CARDNUM_ADD_FAIL, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        } else if (a == 0) {
            Messages.messageName(Strings.WARNING, Strings.INVALID_VALUE, Strings.ICON[0], Strings.TYPE[0], Colors.DARK_BLACK_TEXT_COLOR);
        } else if (a > this.state.grandTotal2) {
            Messages.messageName(Strings.WARNING, Strings.HIGH_VALUE, Strings.ICON[0], Strings.TYPE[0], Colors.DARK_BLACK_TEXT_COLOR);
        } else {

            if (this.state.payment.length == 0) {
                this.state.finalCashPayment.push({ "amount": this.state.grandTotal2, "description": "unknown", "paymentMethod": this.state.cardType })
                console.log(JSON.stringify((this.state.finalCashPayment)))
            } else {
                var finalAmountCash = 0;
                var finalAmountCard = 0;
                var finalAmountCheque = 0;
                for (let index = 0; index < this.state.payment.length; index++) {

                    if (this.state.payment[index].paymentMethod == "CASH") {
                        finalAmountCash = finalAmountCash + this.state.payment[index].amount

                    } else if (this.state.payment[index].paymentMethod == 'CARD') {
                        finalAmountCard = finalAmountCard + this.state.payment[index].amount

                    } else if (this.state.payment[index].paymentMethod == 'CHEQUE') {
                        finalAmountCheque = finalAmountCheque + this.state.payment[index].amount
                    }

                }

                console.log(finalAmountCash)
                console.log(finalAmountCard)
                console.log(finalAmountCheque)
                if (finalAmountCash > 0) {
                    this.state.finalCashPayment.push({ "amount": finalAmountCash, "description": "unknown", "paymentMethod": this.state.cashType })
                }
                if (finalAmountCheque > 0) {
                    this.state.finalCashPayment.push({ "amount": finalAmountCheque, "description": "unknown", "paymentMethod": this.state.checkType })
                }

                if (this.state.customerId !== "") {
                    Alert.alert(
                        'Creditbook message',
                        'Customer has been already selected!',
                        [
                            { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                            {
                                text: 'Add to creditbook', onPress: () => {
                                    // if(){
    
                                    // }
                                    this.state.finalCashPayment.push({ "amount": finalAmountCard, "description": "unknown", "paymentMethod": this.state.cardType })
                                    console.log("zzzzzzzzzzzzzzzzzzzzzzzzzzzzzz" + JSON.stringify((this.state.finalCashPayment)))
                                    this.addToCreditBook()
    
    
                                }
                            },
                        ],
                        { cancelable: true }
                    );
    
                }else{

                if (this.state.grandTotal2 > finalAmountCard) {
                    var final = finalAmountCard + this.state.grandTotal2
                    this.state.finalCashPayment.push({ "amount": final, "description": "unknown", "paymentMethod": this.state.cardType })
                    console.log(JSON.stringify((this.state.finalCashPayment)))
                    this.invoice()
                }
            }

            }
           
        }


    }
    others() {
        Alert.alert("Coming Soon!")

    }

    Card() {

        this.refs.card.open()
        this.setState({
            cardpaymentVisible: true,
            cardAddPaymentVisible: false,
            underButton: false,
            btnSelected: 2,
            image: 2,
            balance: Math.abs(this.state.receivedPrice - this.state.grandTotal),
            textColor: Colors.DARK_BLACK_TEXT_COLOR,

        })

    }

    navigateToCustomerForm() {
        const { navigate } = this.props.navigation;

        navigate('CustomerScreen', {
            customerId: this.state.customerId

        })

    }

    static navigationOptions = {
        title: 'PaymentScreen',
    };

    focusAmount() {
        this.setState({
            underButton: false
        })
    }

    amountTouchEnd() {
        this.setState({
            underButton: false
        })
    }

    onClose = () => {
        this.setState({
            underButton: true,
            balance: 0,
            tot: 0,
            charge: true,
            addToPayment: false,
            textColor: Colors.DARK_BLACK_TEXT_COLOR,

        })
    }

    // Add to Credit Book
    addToCreditBook() {

        console.log("vvvvvvvvvvvv" + JSON.stringify(this.state.customerId))
        this.setState({
            spinner: true,
        });

         if(this.state.payment.length==0){
            var data = JSON.stringify({
                "additionalCostAmount": 0,
                "charges": [],
                "creditDayPeriod": this.state.creditDayPeriod,
                "customFields": { "cashGivenAmount": this.state.receivedPrice },
                "customerId": this.state.customerId,
                "description": "string",
                "invoiceDate": {
                    "basic": this.state.date,
                },
                "invoiceDiscountAmount": this.state.disAmount,
                "invoiceDiscountPercentage": this.state.discount,
                "invoiceNumber": this.state.invoiceNumber,
                "items": this.state.orderDetails,
                "payments": [],
                "serviceChargeAmount": 0,
                "taxes": [],
    
    
            });
         }else{
            var data = JSON.stringify({
                "additionalCostAmount": 0,
                "charges": [],
                "creditDayPeriod": this.state.creditDayPeriod,
                "customFields": { "cashGivenAmount": this.state.receivedPrice },
                "customerId": this.state.customerId,
                "description": "string",
                "invoiceDate": {
                    "basic": this.state.date,
                },
                "invoiceDiscountAmount": this.state.disAmount,
                "invoiceDiscountPercentage": this.state.discount,
                "invoiceNumber": this.state.invoiceNumber,
                "items": this.state.orderDetails,
                "payments": this.state.finalCashPayment,
                "serviceChargeAmount": 0,
                "taxes": [],
    
    
            });
         }
       

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + '/api/customer/invoice',
                    {
                        method: "POST",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },
                        body: data
                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        Messages.messageName(Strings.WARNING_SUCESS, Strings.CREDIT_BOOK_ADDED_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                        console.log("xxxxxxxxxxx" + JSON.stringify(responseJson));
                        const balance = this.state.receivedPrice - this.state.grandTotal
                        this.props.navigation.navigate('DoneScreen', {
                            total: this.state.grandTotal,
                            balance: balance

                        });
                        this.setState({
                            spinner: false
                        });
                    })

                    .catch(error => {
                        console.log(error);
                        this.setState({
                            spinner: false,
                        });
                        Messages.messageName(Strings.WARNING_SUCESS, Strings.CREDIT_BOOK_ADDED_FAIL, Strings.ICON[2], Strings.TYPE[2], Colors.DARK_BLACK_TEXT_COLOR);

                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.addToCreditBook()

                            }
                        },
                    ],
                    { cancelable: true }
                );
            }
        });

        // this.setState({
        //     spinner: true,
        // });
        // var data = JSON.stringify({

        //     "amount": this.state.tot,
        //     "creditDate": {
        //         "basic": this.state.date
        //     },
        //     "customerId": this.state.customerId,
        //     "description": this.state.invoiceDescription,
        //     "invoiceNumbers": [
        //         this.state.invoiceNumber
        //     ]

        // });

        // NetInfo.isConnected.fetch().then(isConnected => {
        //     if (isConnected === true) {
        //         fetch(
        //             Strings.BASE_URL + '/api/customer/' + this.state.customerId + '/credit',
        //             {
        //                 method: "POST",
        //                 headers: {
        //                     Accept: "application/json",
        //                     "Content-Type": "application/json",
        //                     'Authorization': this.state.token
        //                 },
        //                 body: data
        //             }
        //         )

        //             .then(resp => resp.json())
        //             .then(responseJson => {
        //                 Messages.messageName(Strings.WARNING_SUCESS, Strings.CREDIT_BOOK_ADDED_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
        //                 console.log(JSON.stringify(responseJson));
        //                 const balance = this.state.receivedPrice - this.state.grandTotal
        //                 this.props.navigation.navigate('DoneScreen', {
        //                     total: this.state.grandTotal,
        //                     balance: balance

        //                 });
        //                 this.setState({
        //                     spinner: false
        //                 });
        //             })

        //             .catch(error => {
        //                 console.log(error);
        //                 this.setState({
        //                     spinner: false,
        //                 });
        //                 Messages.messageName(Strings.WARNING_SUCESS, Strings.CREDIT_BOOK_ADDED_FAIL, Strings.ICON[2], Strings.TYPE[2], Colors.DARK_BLACK_TEXT_COLOR);

        //             });
        //     } else {
        //         Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
        //         this.setState({
        //             spinner: false
        //         })

        //         Alert.alert(
        //             'Connection Error!',
        //             'Please check your connection and try again',
        //             [
        //                 {
        //                     text: 'Retry', onPress: () => {
        //                         this.addToCreditBook()

        //                     }
        //                 },
        //             ],
        //             { cancelable: true }
        //         );
        //     }
        // });

    }


    addToCreditBookValidation(){
        if(this.state.payment.length>0){
            this.invoice()
        }else{
            this.addToCreditBook()
        }
    }

    //Add to Credit Book Visible
    addToCreditBookVisible() {
        if (this.state.custName == 'Unknown' || this.state.custName == "") {
            Alert.alert("Please Select Customer First!")
        } else {
            Alert.alert(
                'Confirm',
                'Are you sure you want to do this ?',
                [
                    { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                    {
                        text: 'Yes', onPress: () => {

                            if (this.state.grandTotal > this.state.grandTotal2) {
                                this.setState({
                                    balance: this.state.calculateBalance
                                })
                                this.refs.creditBook.open()
                                this.setState({
                                    btnSelected: 2,
                                    image: 2,
                                    underButton: false
                                })
                            } else {
                                this.refs.creditBook.open()
                                this.setState({
                                    btnSelected: 2,

                                    image: 2,
                                    underButton: false
                                })
                            }


                        }
                    },
                ],
                { cancelable: true }
            );
            this.setState({ btnSelected: 4 })
        }
    }

    test() {
        this.state.orderDetails.map((userData) => {
            console.log(userData);
        });
    }

    showkeyboardModel() {

        this.setState({

            underButton: false,
            text: this.state.text,

        })
        this.bluetoothConnectedChecking()
        this.refs.modal4.open()

    }

    closeOneItemModel2() {
        this.refs.modal4.close()
    }
    closeOneItemModel3() {
        this.refs.cheque.close()
    }
    closeOneItemModel4() {
        this.refs.card.close()
    }
    closeOneItemModel5() {
        this.refs.creditBook.close()
    }
    closeOneItemModel6() {
        this.refs.modal4.close()
    }

    chequeBalanaceCalculate() {
        var a = parseInt(this.state.receivedPrice)
        if (a == 0) {
            Messages.messageName(Strings.WARNING, Strings.INVALID_VALUE, Strings.ICON[0], Strings.TYPE[0], Colors.DARK_BLACK_TEXT_COLOR);
        } else {
            const temp = {
                "amount": Math.abs(a),
                "description": "unknown",
                "paymentMethod": this.state.checkType,
            }
            this.state.payment.push(temp)
            var receivedBalance = 0;
            var finalBalanace = 0;
            for (let index = 0; index < this.state.payment.length; index++) {
                receivedBalance = receivedBalance + this.state.payment[index].amount
            }
            finalBalanace = this.state.grandTotal - receivedBalance
            console.log(JSON.stringify(this.state.payment))

            this.setState({
                paymenttypeVisible: true,
                balanaceVisible: true,
                grandTotal2: Math.abs(finalBalanace),
                balance: 0,
                receivedPrice: 0,
                calculateBalance: finalBalanace,
                textColor: Colors.DARK_BLACK_TEXT_COLOR,

            }

            )
            this.refs.cheque.close()
        }

    }

    onValueChange(value) {
        this.setState({
            bank: value
        });
    }

    onValueChange2(value) {
        this.setState({
            bank2: value
        });
    }

    cardPaymenatBalanceCalculate() {

        var a = parseInt(this.state.receivedPrice)
        if (a == 0) {
            Messages.messageName(Strings.WARNING, Strings.INVALID_VALUE, Strings.ICON[0], Strings.TYPE[0], Colors.DARK_BLACK_TEXT_COLOR);
        } else if (this.state.bank == "") {
            Messages.messageName(Strings.WARNING, Strings.BANK_ADD_FAIL, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        } else if (this.state.cardnumber == "" || this.state.cardnumber.length < 19 || this.state.cardStatus == "invalid") {
            Messages.messageName(Strings.WARNING, Strings.CARDNUM_ADD_FAIL, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        } else {
            const temp = {
                "amount": Math.abs(a),
                "description": "unknown",
                "paymentMethod": this.state.cardType,
            }
            this.state.payment.push(temp)
            var receivedBalance = 0;
            var finalBalanace = 0;
            for (let index = 0; index < this.state.payment.length; index++) {
                receivedBalance = receivedBalance + this.state.payment[index].amount
            }
            finalBalanace = this.state.grandTotal - receivedBalance
            console.log(JSON.stringify(this.state.payment))

            this.setState({
                paymenttypeVisible: true,
                balanaceVisible: true,
                receivedPrice: 0,
                grandTotal2: Math.abs(finalBalanace),
                balance: 0,
                calculateBalance: finalBalanace,
                textColor: Colors.DARK_BLACK_TEXT_COLOR,

            }

            )
            this.refs.card.close()
        }

    }

    //payment history list
    clickAddTopayment() {
        var a = parseInt(this.state.receivedPrice)
        if (a == 0) {
            Messages.messageName(Strings.WARNING, Strings.INVALID_VALUE, Strings.ICON[0], Strings.TYPE[0], Colors.DARK_BLACK_TEXT_COLOR);
        } else {
            const temp = {
                "amount": Math.abs(a),
                "description": "unknown",
                "paymentMethod": this.state.cashType,
            }

            this.state.payment.push(temp)
            var receivedBalance = 0;
            var finalBalanace = 0;
            for (let index = 0; index < this.state.payment.length; index++) {
                receivedBalance = receivedBalance + this.state.payment[index].amount

            }
            finalBalanace = this.state.grandTotal - receivedBalance
            console.log(JSON.stringify(this.state.payment))

            this.setState({
                paymenttypeVisible: true,
                balanaceVisible: true,
                charge: true,
                addToPayment: false,
                receivedPrice: finalBalanace,
                grandTotal2: Math.abs(finalBalanace),
                balance: 0,
                calculateBalance: finalBalanace,
                textColor: Colors.DARK_BLACK_TEXT_COLOR,

            }

            )
            this.refs.modal4.close()
        }

    }

    //when delete payment from payment history,this will work
    reCalculateBalance(index) {

        let addedDetails = this.state.payment.filter((e, i) => i !== index);
        var receivedBalance = 0;
        var finalBalanace = 0;

        for (let index = 0; index < addedDetails.length; index++) {
            receivedBalance = receivedBalance + addedDetails[index].amount
        }

        finalBalanace = this.state.grandTotal - receivedBalance
        this.setState({
            payment: addedDetails,
            calculateBalance: finalBalanace,
            grandTotal2: Math.abs(finalBalanace),

        })

        if (addedDetails.length == 0) {
            this.setState({
                balanaceVisible: false,
                balance: 0,
                charge: true,
                addToPayment: false,
                receivedPrice: this.state.grandTotal
            })

        }

    }

    cash() {
        this.bluetoothConnectedChecking()
        this.refs.modal4.open()
        this.setState({ btnSelected: 1, image: 0, underButton: false, })
    }
    // setDate(newDate) {
    //     this.setState({
    //         expiredate: Moment(newDate).format('YYYY-MM-D')
    //     });
    //     console.log(this.state.expiredate);

    // }
    render() {
        const { navigate } = this.props.navigation;
        return (

            <ImageBackground
                source={require("../../assets/whiteback.jpg")}
                style={{ width: "100%", height: "100%" }}>

                <View style={{ backgroundColor: '#126F85', width: '100%', height: 60, borderBottomLeftRadius: 50 }}>
                    <View style={{ backgroundColor: '#126F85', width: 50, top: 13, borderRadius: 60, left: 25, position: "absolute", top: 8 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('CartScreen')}>
                            <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                    <Image source={require("../../assets/newBack3.png")} style={{ width: 20, height: 20 }}></Image>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Text style={{ fontFamily: 'Ubuntu-Medium', color: '#ffff', justifyContent: "center", textAlign: "center", fontSize: 20, position: "absolute", top: 10, left: 100, fontWeight: "bold" }}>Payment</Text>
                    <View style={{ backgroundColor: '#126F85', width: 70, top: 15, borderRadius: 6, right: 50, position: "absolute", top: 8 }}>
                        <TouchableOpacity onPress={() => this.navigateToCustomerForm()} activeOpacity={0.5}>
                            <View style={{ width: 87, height: 34, borderRadius: 6, backgroundColor: '#126F85', elevation: 10, borderColor: '#ffff', borderWidth: 1 }}>
                                <View style={{ justifyContent: 'flex-end', top: 5 }} numberOfLines={1}>
                                    <Image source={require("../../assets/user.png")} style={{ width: 20, height: 20, left: 2 }}></Image>

                                    <View style={{ width: 57, position: "absolute", bottom: 5, left: 22 }}>
                                        <Text numberOfLines={1} style={{ fontSize: 9, color: '#ffff', textAlign: "center", }}>{this.props.navigation.state.params.customerName
                                            ? this.props.navigation.state.params.customerName
                                            : 'SelectCustomer'}</Text>
                                    </View>

                                    <StatusBar barStyle="light-content" hidden={false} backgroundColor={'#126F85'} />

                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>

                <StatusBar barStyle="light-content" hidden={false} backgroundColor={'#126F85'} />
                <Spinner
                    visible={this.state.spinner}
                    textContent={'Loading...'}
                    textStyle={{ color: 'white' }}
                    overlayColor={"#0f0e0e90"}
                    animation={"fade"}
                    color={"#00CEFD"}
                />
                <Modal style={[styles.cheque]} position={"bottom"} ref={"cheque"} backdropPressToClose={true} onClosed={this.onClose} >
                    {/* <View style={{ position: "absolute", width: 50, right: 150, }}>
                        <TouchableOpacity style={{ justifyContent: 'flex-end', alignItems: 'flex-end', top: -20 }} onPress={() => this.closeOneItemModel3()}>
                            <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: '#ffff', borderRadius: 100, }}>
                                <Image style={{ width: 40, height: 40 }} source={require('../../assets/closemodl.png')} />
                            </View>
                        </TouchableOpacity>
                    </View> */}
                    <TouchableOpacity style={{ top: -20, justifyContent: "center", alignContent: "center", alignItems: "center" }} onPress={() => this.closeOneItemModel3()}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: '#ffff', borderRadius: 100, }}>
                            <Image style={{ width: 40, height: 40 }} source={require('../../assets/closemodl.png')} />
                        </View>
                    </TouchableOpacity>
                    <ScrollView style={{ bottom: 30, marginBottom: -20 }}>
                        <View style={{
                            flex: 1,
                            // justifyContent: 'flex-end',
                            padding: PixelRatio.getPixelSizeForLayoutSize(12),
                        }}>

                            <View style={{ bottom: 10 }}>

                                <Text style={{ color: Colors.PRIMARY_COLOR, fontSize: 20, textAlign: 'left', fontWeight: 'bold', top: -10, marginLeft: 10 }}>Cheque Details</Text>
                                <Item
                                    style={styles.item2}
                                    rounded
                                    last
                                >
                                    <Image
                                        style={styles.place}
                                        source={require('../../assets/bank.png')}
                                    />
                                    <Picker
                                        style={{ width: 200, color: '#ffff', left: 10 }}
                                        selectedValue={this.state.bank2}
                                        onValueChange={this.onValueChange2.bind(this)}
                                    >
                                        <Picker.Item label="Select Bank" value="" />
                                        <Picker.Item label="Sampath Bank PLC" value="Sampath Bank PLC" />
                                        <Picker.Item label="Commercial Bank of Ceylon PLC " value="Commercial Bank of Ceylon PLC " />
                                        <Picker.Item label="Nations Trust Bank" value="Nations Trust Bank" />
                                        <Picker.Item label="Hatton National Bank" value="Hatton National Bank" />
                                        <Picker.Item label="Seylan Bank" value="Seylan Bank" />
                                        <Picker.Item label="Bank of Ceylon" value="Bank of Ceylon" />
                                        <Picker.Item label="DFCC Bank" value="DFCC Bank" />
                                        <Picker.Item label="Pan Asia Bank" value="Pan Asia Bank" />
                                        <Picker.Item label="People's Bank" value="People's Bank" />
                                        <Picker.Item label="National Savings Bank" value="National Savings Bank" />
                                        <Picker.Item label="Cargills Bank" value="Cargills Bank" />
                                        <Picker.Item label="HSBC" value="HSBC" />
                                        <Picker.Item label="Regional Development Bank" value="Regional Development Bank" />

                                    </Picker>
                                </Item>

                                <Item
                                    style={styles.item2}
                                    rounded
                                    last
                                >
                                    <Image
                                        style={styles.place}
                                        source={require('../../assets/invoice.png')}
                                    />
                                    <Input
                                        keyboardType={"numeric"}
                                        style={styles.input}
                                        placeholder="Cheque Number"
                                        placeholderTextColor="#ffff"
                                        autoCapitalize="none"
                                        onChangeText={chequenumber => this.setState({ chequenumber })}
                                    />
                                </Item>

                                <Item
                                    style={styles.item2}
                                    rounded
                                    last
                                >
                                    <Image
                                        style={styles.place}
                                        source={require('../../assets/price.png')}
                                    />
                                    <Input
                                        keyboardType={"numeric"}
                                        style={styles.input}
                                        placeholder={"Rs:" + this.financial(this.state.grandTotal2)}
                                        placeholderTextColor="#ffff"
                                        autoCapitalize="none"
                                        onChangeText={text3 => this.calculateCheckTotal(text3)}
                                    />
                                </Item>

                                <Item
                                    style={styles.item2}
                                    rounded
                                    last
                                >
                                    <Image
                                        style={styles.place}
                                        source={require('../../assets/date.png')}
                                    />
                                    <Input
                                        keyboardType={"numeric"}
                                        style={styles.input}
                                        placeholder="Expire Date"
                                        placeholderTextColor="#ffff"
                                        autoCapitalize="none"
                                        onChangeText={expiredate => this.setState({ expiredate })}
                                    />
                                </Item>

                                <Item
                                    style={styles.item2}
                                    rounded
                                    last
                                >
                                    <Image
                                        style={styles.place}
                                        source={require('../../assets/description.png')}
                                    />
                                    <Input

                                        multiline={true}
                                        style={styles.input}
                                        placeholder="Description"
                                        placeholderTextColor="#ffff"
                                        autoCapitalize="none"
                                        onChangeText={descriptioncheck => this.setState({ descriptioncheck })}
                                    />
                                </Item>
                                {this.state.checkpaymentVisible &&
                                    <View style={{ justifyContent: "center", alignItems: "center", alignContent: "center" }}>
                                        <TouchableOpacity
                                            style={[styles.buttonContainerProcess1, styles.ProcessButton]}
                                            onPress={() => this.checkValidation()}
                                        >
                                            <Text style={styles.ProcessText}>Charge</Text>
                                        </TouchableOpacity>
                                    </View>
                                }
                                {this.state.checkAddPaymentVisible &&
                                    <View style={{ justifyContent: "center", alignItems: "center", alignContent: "center" }}>
                                        <TouchableOpacity
                                            style={[styles.buttonContainerProcess1, styles.ProcessButton]}
                                            onPress={() => this.chequeBalanaceCalculate()}
                                        >
                                            <Text style={styles.ProcessText}>Add To Payment</Text>
                                        </TouchableOpacity>
                                    </View>
                                }

                            </View>

                        </View>
                    </ScrollView>
                </Modal>

                <Modal style={[styles.card]} position={"bottom"} ref={"card"} onClosed={this.onClose} backdropPressToClose={false} onBackButtonPress={() => this.closeAll()} onRequestClose={() => {
                    console.log("back");
                    this.closeAll();
                }}>
                    {/* <View style={{ position: "absolute", width: 50, right: 150, }}>
                        <TouchableOpacity style={{ justifyContent: 'flex-end', alignItems: 'flex-end', top: -20 }} onPress={() => this.closeOneItemModel4()}>
                            <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: '#ffff', borderRadius: 100, }}>
                                <Image style={{ width: 40, height: 40 }} source={require('../../assets/closemodl.png')} />
                            </View>
                        </TouchableOpacity>
                    </View> */}
                    <TouchableOpacity style={{ top: -20, justifyContent: "center", alignContent: "center", alignItems: "center" }} onPress={() => this.closeOneItemModel4()}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: '#ffff', borderRadius: 100, }}>
                            <Image style={{ width: 40, height: 40 }} source={require('../../assets/closemodl.png')} />
                        </View>
                    </TouchableOpacity>
                    <View style={{
                        flex: 1,
                        // justifyContent: 'flex-end',
                        padding: PixelRatio.getPixelSizeForLayoutSize(12),
                    }}>
                        <View style={{ bottom: 60 }}>

                            <Text style={{ color: Colors.PRIMARY_COLOR, fontSize: 20, textAlign: 'left', fontWeight: 'bold', top: -10, marginLeft: 10 }}>Card Details</Text>

                            <Item
                                style={styles.item2}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../assets/bank.png')}
                                />
                                <Picker
                                    style={{ width: 200, color: '#ffffff80', left: 10 }}
                                    selectedValue={this.state.bank}
                                    onValueChange={this.onValueChange.bind(this)}
                                >
                                    <Picker.Item label="Select Bank" value="" />
                                    <Picker.Item label="Sampath Bank PLC" value="Sampath Bank PLC" />
                                    <Picker.Item label="Commercial Bank of Ceylon PLC " value="Commercial Bank of Ceylon PLC " />
                                    <Picker.Item label="Nations Trust Bank" value="Nations Trust Bank" />
                                    <Picker.Item label="Hatton National Bank" value="Hatton National Bank" />
                                    <Picker.Item label="Seylan Bank" value="Seylan Bank" />
                                    <Picker.Item label="Bank of Ceylon" value="Bank of Ceylon" />
                                    <Picker.Item label="DFCC Bank" value="DFCC Bank" />
                                    <Picker.Item label="Pan Asia Bank" value="Pan Asia Bank" />
                                    <Picker.Item label="People's Bank" value="People's Bank" />
                                    <Picker.Item label="National Savings Bank" value="National Savings Bank" />
                                    <Picker.Item label="Cargills Bank" value="Cargills Bank" />
                                    <Picker.Item label="HSBC" value="HSBC" />
                                    <Picker.Item label="Regional Development Bank" value="Regional Development Bank" />

                                </Picker>
                            </Item>

                            <Item
                                style={styles.item2}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../assets/invoice.png')}
                                />

                                <SafeAreaView style={{ width: '90%' }}>
                                    <LiteCreditCardInput
                                        autoFocus
                                        inputStyle={styles.input6}
                                        inputContainerStyle={styles.crediticon}
                                        validColor={"black"}
                                        invalidColor={"red"}
                                        placeholderColor={"#ffffff80"}

                                        onFocus={this._onFocus}
                                        onChange={this._onChange} />
                                </SafeAreaView>

                            </Item>

                            <Item
                                style={styles.item2}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../assets/price.png')}
                                />
                                <Input
                                    keyboardType={"numeric"}
                                    style={styles.input}
                                    placeholder={"Rs:" + this.financial(this.state.grandTotal2)}
                                    placeholderTextColor="#ffffff80"
                                    autoCapitalize="none"
                                    onChangeText={text2 => this.calculateCardTotal(text2)}
                                />
                            </Item>
                            {this.state.cardpaymentVisible &&
                                <View style={{ justifyContent: "center", alignContent: "center", alignItems: "center" }}>
                                    <TouchableOpacity
                                        style={[styles.buttonContainerProcess1, styles.ProcessButton]}
                                        onPress={() => this.cardValidation()}
                                    >
                                        <Text style={styles.ProcessText}>Charge</Text>
                                    </TouchableOpacity>
                                </View>
                            }

                            {this.state.cardAddPaymentVisible &&
                                <View style={{ justifyContent: "center", alignContent: "center", alignItems: "center" }}>
                                    <TouchableOpacity
                                        style={[styles.buttonContainerProcess1, styles.ProcessButton]}
                                        onPress={() => this.cardPaymenatBalanceCalculate()}
                                    >
                                        <Text style={styles.ProcessText}>Add To Payment</Text>
                                    </TouchableOpacity>
                                </View>
                            }
                        </View>

                    </View>
                </Modal>
                <Modal style={[styles.creditBook]} position={"bottom"} ref={"creditBook"} onClosed={this.onClose} backdropPressToClose={true} onRequestClose={() => {
                    console.log("back");
                    this.closeAll();
                }}>
                    {/* <View style={{ position: "absolute", width: 50, right: 150, }}>
                        <TouchableOpacity style={{ justifyContent: 'flex-end', alignItems: 'flex-end', top: -20 }} onPress={() => this.closeOneItemModel5()}>
                            <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: '#ffff', borderRadius: 100, }}>
                                <Image style={{ width: 40, height: 40 }} source={require('../../assets/closemodl.png')} />
                            </View>
                        </TouchableOpacity>
                    </View> */}
                    <TouchableOpacity style={{ top: -20, justifyContent: "center", alignContent: "center", alignItems: "center" }} onPress={() => this.closeOneItemModel5()}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: '#ffff', borderRadius: 100, }}>
                            <Image style={{ width: 40, height: 40 }} source={require('../../assets/closemodl.png')} />
                        </View>
                    </TouchableOpacity>
                    <ScrollView style={{ marginBottom: 20 }}>
                        <View style={{
                            flex: 1,
                            // justifyContent: 'flex-end',
                            padding: PixelRatio.getPixelSizeForLayoutSize(12),
                        }}>

                            <View style={{}}>

                                <View style={{ flexDirection: 'row', top: -10 }}>
                                    <Text style={{ color: Colors.PRIMARY_COLOR, fontSize: 20, textAlign: 'left', fontWeight: 'bold', top: -10, marginLeft: 10 }}>CreditBook</Text>

                                </View>




                                <Item
                                    style={styles.item2}
                                    rounded
                                    last
                                >
                                    <Image
                                        style={styles.place}
                                        source={require('../../assets/invoice.png')}
                                    />
                                    <Input
                                        style={styles.input2}
                                        placeholder="Invoice Number"
                                        editable={false}
                                        placeholderTextColor="#ffffff80"
                                        autoCapitalize="none"
                                        value={this.state.invoiceNumber}
                                        onChangeText={invoiceNumber => this.setState({ invoiceNumber })}
                                    />
                                </Item>

                                <Item
                                    style={styles.item2}
                                    rounded
                                    last
                                >
                                    <Image
                                        style={styles.place}
                                        source={require('../../assets/price.png')}
                                    />
                                    <Input
                                        style={styles.input2}
                                        placeholder={this.financial(this.state.balance)}
                                        placeholderTextColor="#ffffff80"
                                        editable={false}
                                        autoCapitalize="none"
                                        value={this.state.tot}
                                        onChangeText={tot => this.setState({ tot })}
                                    />
                                </Item>

                                <Item
                                    style={styles.item2}
                                    rounded
                                    last
                                >
                                    <Image
                                        style={styles.place}
                                        source={require('../../assets/description.png')}
                                    />
                                    <Input
                                        style={styles.input2}
                                        placeholder="creditDayPeriod"
                                        placeholderTextColor="#ffffff80"
                                        autoCapitalize="none"
                                        keyboardType='numeric'
                                        onChangeText={creditDayPeriod => this.setState({ creditDayPeriod: creditDayPeriod })}
                                    />
                                </Item>


                            </View>
                            <View style={{ top: 10 }}>
                                <TouchableOpacity
                                    style={[styles.buttonContainerProcess1, styles.ProcessButton]}
                                    onPress={() => this.addToCreditBookValidation()}
                                >
                                    <Text style={styles.ProcessText}>Submit</Text>
                                </TouchableOpacity>

                            </View>

                        </View>
                    </ScrollView>
                </Modal>

                <Modal style={[styles.quickBilling]} position={"bottom"} ref={"modal4"} onClosed={this.onClose} backdropPressToClose={false} onRequestClose={() => {
                    console.log("back");
                    this.closeAll();
                }}>
                    {/* <View style={{ position: "absolute", width: 50,justifyContent:"center",alignContent:"center",alignItems:"center"}}>
                       
                    </View> */}
                    <TouchableOpacity style={{ top: -20, justifyContent: "center", alignContent: "center", alignItems: "center" }} onPress={() => this.closeOneItemModel6()}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: '#ffff', borderRadius: 100, }}>
                            <Image style={{ width: 40, height: 40 }} source={require('../../assets/closemodl.png')} />
                        </View>
                    </TouchableOpacity>
                    <View style={{
                        flex: 1,

                        padding: PixelRatio.getPixelSizeForLayoutSize(12),
                    }}>

                        <View style={{ bottom: 40 }}>

                            <View style={{ flexDirection: 'row', top: -10 }}>
                                <Text style={{ color: Colors.PRIMARY_COLOR, fontSize: 20, textAlign: 'left', fontWeight: 'bold', top: -10, marginLeft: 10 }}>Cash Details</Text>

                            </View>
                            <Form style={styles.form2}>
                                <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>

                                    <Label></Label>
                                </View>
                                <Item style={styles.TextItemStyle2}>
                                    <Input style={styles.InputStyle}
                                        placeholder={"Rs : " + this.financial(this.state.grandTotal2)}
                                        onTouchStart={() => this.focusAmount()}
                                        autoFocus={true}
                                        onEndEditing={() => this.amountTouchEnd()}
                                        textAlign={'center'}
                                        // value={this.state.grandTotal2.toString()}
                                        keyboardType={"numeric"}
                                        onChangeText={text => this.calculateTotal(text)}
                                    />
                                </Item>
                                <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 10 }}>
                                    <Label></Label>
                                    <Text style={{ fontSize: 15, color: this.state.textColor }}>Balance : Rs.{this.financial(this.state.balance)}</Text>
                                </View>
                            </Form>
                        </View>
                        {this.state.charge &&
                            <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 70 }}>
                                <Button
                                    style={{ borderColor: Colors.PRIMARY_COLOR, borderWidth: 2, }}
                                    onPress={() => this.navigateToDone()}
                                    block
                                    large
                                    light
                                    rounded
                                >
                                    <Text style={styles.loginText2}>Charge  Rs.{this.financial(this.state.grandTotal)}</Text>

                                </Button>
                            </View>
                        }
                        {this.state.addToPayment &&
                            <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 70 }}>
                                <Button
                                    style={{ borderColor: Colors.PRIMARY_COLOR, borderWidth: 2, }}
                                    onPress={() => this.clickAddTopayment()}
                                    block
                                    large
                                    light
                                    rounded
                                >
                                    <Text style={styles.loginText2}>Add To Payment</Text>
                                </Button>
                            </View>
                        }
                    </View>

                    <TouchableOpacity
                        onPress={() => this.printRecept()}
                        activeOpacity={0.9}
                        style={{
                            height: 70,
                            backgroundColor: Colors.PRIMARY_COLOR
                        }}

                    >

                        <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 17 }}>
                            {/* <ImageBackground source={require('../../assets/print.png')} style={{width:45,height:45}}></ImageBackground> */}
                            <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 20 }}>Print Receipt</Text>
                            <Text style={{ fontSize: 10, fontWeight: 'bold', color: this.state.bluetoothConnected == 0 ? '#fc7b03' : this.state.bluetoothConnected == 1 ? Colors.BACKGROUD_WHITE : Colors.BACKGROUD_WHITE }}>{this.state.bluetoothConnected == 0 ? 'Printer Disconnected' : this.state.bluetoothConnected == 1 ? 'Connected' : 'Connected'}</Text>
                        </View>
                    </TouchableOpacity>

                </Modal>

                <Content contentContainerStyle={styles.content}>
                    <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: 'transparent' }}>

                        <Item

                            style={{
                                justifyContent: "center",
                                borderColor: '#ffff',
                                width: 330,
                                height: 200,
                                backgroundColor: '#ffff',
                                borderRadius: 10
                            }}
                            rounded
                            last
                        >

                            <Text style={{ color: '#126F85', textAlign: "center", fontSize: 35, position: "absolute", }}>
                                Total
                                </Text>
                            <Text
                                style={styles.input4}
                            > Rs : {this.financial(this.state.grandTotal)}</Text>
                        </Item>
                        {/* </ImageBackground> */}

                    </View>

                    {this.state.paymenttypeVisible &&
                        <View style={{ width: '100%', height: 140, }}>

                            <View style={{

                                // backgroundColor: '#ebebe0',
                                alignItems: 'center',
                                width: '100%'
                            }}>
                                <ScrollView>
                                    {
                                        this.state.payment.map((item, index) => (

                                            <TouchableOpacity
                                                key={item.id}
                                                style={{
                                                    padding: 10,
                                                    marginTop: 10,
                                                    backgroundColor: '#eeee',
                                                    alignItems: 'center',
                                                    width: '100%'
                                                }}
                                            >
                                                <View style={{ flexDirection: "row", alignContent: 'flex-start', justifyContent: 'flex-start', width: '100%', }}>
                                                    <TouchableOpacity onPress={() => this.reCalculateBalance(index)}>
                                                        <Image source={require('../../assets/close.png')} style={{ width: 30, height: 30, }}></Image>
                                                    </TouchableOpacity>
                                                    <View style={{ width: '100%', marginTop: 5 }}>
                                                        <Text style={{ position: "absolute", justifyContent: 'flex-end', color: '#126F85', textAlign: "center", fontWeight: "bold" }}>
                                                            {item.paymentMethod}
                                                        </Text>
                                                        <Text style={{ textAlign: "center", position: "absolute", right: 60 }}>
                                                            {"Rs." + item.amount + ".00"}
                                                        </Text>
                                                    </View>

                                                </View>
                                            </TouchableOpacity>

                                        ))

                                    }
                                </ScrollView>
                            </View>

                            <View style={{
                                padding: 10,

                                width: '100%'
                            }}>
                                {this.state.balanaceVisible &&
                                    <Text style={{ color: 'red', textAlign: "right", }}>
                                        {"Balance:Rs." + this.financial(this.state.calculateBalance)}
                                    </Text>
                                }
                            </View>

                        </View>
                    }

                </Content>

                {this.state.underButton &&
                    <View style={{ position: "absolute", bottom: 4, width: '100%', }}>
                        <View style={styles.buttonContainer1}>

                            <Button
                                style={(this.state.btnSelected == 1) ? styles.btnSelected : styles.notSelected}
                                onPress={() => this.cash()}
                                hasText
                                block
                                large
                                dark
                            // rounded
                            >
                                <Icon2 name='money-bill' style={{ fontSize: 30, color: this.state.image == 0 ? '#126F85' : this.state.image == 1 ? 'gray' : 'gray', justifyContent: "center", position: "absolute", top: 20 }} />

                                <Text style={(this.state.btnSelected == 1) ? styles.btnSelectedText : styles.NotSelectedText}>     Cash      </Text>

                            </Button>

                            <Button
                                style={(this.state.btnSelected == 2) ? styles.btnSelected : styles.notSelected}
                                onPress={() => this.Card()}
                                hasText
                                block
                                large
                                dark
                            // rounded
                            >

                                <Icon2 name='cc-visa' style={{ fontSize: 30, color: this.state.image == 2 ? '#126F85' : this.state.image == 0 ? 'gray' : 'gray', justifyContent: "center", position: "absolute", top: 20 }} />
                                <Text style={(this.state.btnSelected == 2) ? styles.btnSelectedText : styles.NotSelectedText}>     Card       </Text>

                            </Button>

                            <Button
                                style={(this.state.btnSelected == 3) ? styles.btnSelected : styles.notSelected}
                                onPress={() => this.cheque()}
                                hasText
                                block
                                large
                                dark
                            //  rounded
                            >

                                <Icon2 name='money-check-alt' style={{ fontSize: 30, color: this.state.image == 3 ? '#126F85' : this.state.image == 0 ? 'gray' : 'gray', justifyContent: "center", position: "absolute", top: 20 }} />
                                <Text style={(this.state.btnSelected == 3) ? styles.btnSelectedText : styles.NotSelectedText}>Cheque</Text>

                            </Button>

                        </View>

                        <View style={styles.buttonContainer4}>

                            <Button
                                style={(this.state.btnSelected == 4) ? styles.btnSelected : styles.notSelected}
                                onPress={() => this.addToCreditBookVisible()}
                                hasText
                                block
                                large
                                dark
                            // rounded
                            >

                                <Icon2 name='book' style={{ fontSize: 30, color: this.state.image == 4 ? '#126F85' : this.state.image == 0 ? 'gray' : 'gray', justifyContent: "center", position: "absolute", top: 20 }} />
                                <Text style={(this.state.btnSelected == 4) ? styles.btnSelectedText : styles.NotSelectedText}>CreditBook</Text>

                            </Button>

                            <Button
                                style={(this.state.btnSelected == 5) ? styles.btnSelected : styles.notSelected}
                                onPress={() => this.others()}
                                hasText
                                block
                                large
                                dark
                            // rounded
                            >

                                <Icon2 name='th' style={{ fontSize: 30, color: this.state.image == 5 ? '#126F85' : this.state.image == 0 ? 'gray' : 'gray', justifyContent: "center", position: "absolute", top: 20 }} />
                                <Text style={(this.state.btnSelected == 5) ? styles.btnSelectedText : styles.NotSelectedText}>Others</Text>

                            </Button>

                        </View>
                    </View>
                }

                <View style={styles.buttonContainer}>

                    <TouchableOpacity
                        activeOpacity={0.9}
                        style={{
                            height: 70,
                            width: '100%',
                            top: 10,
                            backgroundColor: Colors.PRIMARY_COLOR,

                        }}
                        // onPress={() => this.navigateToDone()}
                        onPress={() => this.showkeyboardModel()}

                    >
                        <View style={{ justifyContent: 'center', alignItems: 'center', top: 18 }}>
                            <Text style={styles.loginText}>Checkout</Text>

                        </View>
                    </TouchableOpacity>

                </View>

            </ImageBackground>

        );
    }
    changeText(newText) {
        this.setState({ text: newText });
    } qa

}
