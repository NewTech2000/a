import {
  PixelRatio,
  StyleSheet,
} from 'react-native';
import Colors from '../../resources/Colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
 
const styles = StyleSheet.create({
  container: {
    flex: 1,
 
  },
  content: {
    flex: 1,
    justifyContent: 'flex-start',
    width:'100%',
    // padding: PixelRatio.getPixelSizeForLayoutSize(12),
 
  },
   input6: {
    fontSize: 16,
    fontWeight:'bold',
    color: "white",
    paddingLeft:10,
   
  },
  logoBack: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 150,
    width: 150,
    top: -30,
    borderRadius: 100,
    backgroundColor: '#ffffff90'
  },
 
  gridView: {
    flex: 1,
  },
  itemContainer: {
    borderRadius: 20,
    padding: 10,
    height: hp('10%'),
    width: wp('90%'),
    marginTop: 6,
    shadowColor: Colors.DARK_BLACK_TEXT_COLOR,
    shadowOpacity: 0.8,
    elevation: 7,
    shadowRadius: 30,
    shadowOffset: { width: 1, height: 13 }
 
  },
  TextItemStyle: {
    width: '85%',
    height: 55,
    borderBottomColor: Colors.PRIMARY_COLOR,
     borderBottomWidth: 2,
     justifyContent:"center",
     alignContent:"center",
     alignItems:"center"
  },
  ImageIconStyleText: {
    padding: 10,
    margin: 5,
    height: 20,
    width: 20,
    resizeMode: "stretch"
  },
  InputStyle: {
    fontSize: 20,
    fontWeight: 'bold'
  },
  button: {
    height: 70,
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  button2: {
    height: 70,
    borderTopLeftRadius: 15,
    borderColor: Colors.PRIMARY_COLOR,
    borderWidth: 2,
    backgroundColor: Colors.BACKGROUD_WHITE,
    width: 200,
  },
  button3: {
    borderColor: Colors.PRIMARY_COLOR,
    borderWidth: 2,
    height: 70,
    width: 200,
    borderTopRightRadius: 15,
    backgroundColor: Colors.BACKGROUD_WHITE,
  },
  buttonContainer1: {
    // justifyContent: 'center',
    // alignItems: 'center',
    flexDirection: 'row',
    width:'100%', 
    top:-60,
  },
  buttonContainer4: {
    // justifyContent: 'center',
    // alignItems: 'center',
    flexDirection: 'row',
    width:'100%', 
    top:-60,
  },
  buttonContainer2: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    width:'100%', 
    top:-45
  },
  button1: {
    position: 'absolute',
    top: 95,
    padding: 10,
  },
  btnSelected: {
    height: 100,
    width:  wp('35%'),
    borderWidth:1,
    borderColor:Colors.PRIMARY_COLOR,
     backgroundColor: '#ffff', 
     justifyContent:"center",   
  },
  btnCreditSelected: {
    height: 80,
    width: 80,
    borderRadius:100,
    backgroundColor: 'rgba(0,128,128,0.4)'
  },
  btnCreditNoSelected: {
    height: 80,
    width: 80,
    elevation:10,
    borderRadius:100,
    backgroundColor: '#ffff',
    borderColor:'#e0e0e0',
    borderWidth:1,
  },
  notSelected: {
    height: 100,
    color: Colors.DARK_BLACK_TEXT_COLOR,
    // opacity:0.9,
    width:  wp('35%'),
    borderWidth:1,
    borderColor:'#eeeeee',
    backgroundColor: Colors.BACKGROUD_WHITE
  },
  btnSelectedText: {
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    color: Colors.PRIMARY_COLOR,
    fontSize: 11,
    fontWeight: 'bold',
    top:28,
    textAlign:"center"
  },
  btnSelectedText2: {
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    textAlign:"center",
    color: Colors.BACKGROUD_WHITE,
    fontSize: 15,
    fontWeight: 'bold',
   
  },
  NotSelectedText: {
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    color: Colors.DARK_BLACK_TEXT_COLOR,
    fontSize: 11,
    fontWeight: 'bold',
    top:28,
    textAlign:"center"
  },
  NotSelectedText2: {
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    textAlign:"center",
    color: Colors.PRIMARY_COLOR,
    fontSize: 15,
    fontWeight: 'bold',
   
  },
  loginText: {
    justifyContent: 'center',
    alignItems: 'center',
    color: '#ffff',
    fontSize: 18,
    fontWeight: 'bold',
  },
  buttonContainerProcess1: {
    height: hp('7%'),
    width: wp('81%'),
    // flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
    borderRadius: 30,
    shadowColor: Colors.DARK_BLACK_TEXT_COLOR,
    shadowOpacity: 0.8,
    elevation: 6,
    shadowRadius: 15,
    shadowOffset: { width: 1, height: 13 }
 
  },
  cheque: {
    // height: 480,
    height:hp('63%'),
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    backgroundColor: 'white'
  },
  card: {
    height: hp('48%'),
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    backgroundColor: 'white'
  },
  creditBook: {
    // height: 360,
    height:('50%'),
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    backgroundColor: 'white'
  },
  caption: {
    fontSize: 20,
    fontWeight: 'bold',
    alignItems: 'center',
  },
 
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  itemName: {
    fontSize: 15,
    color: '#fff',
    fontWeight: 'bold',
  },
  itemName2: {
    fontSize: 20,
    color: '#fff',
    fontWeight: 'bold',
  },
  itemName3: {
 
    backgroundColor: 'gray',
    fontSize: 20,
    color: '#fff',
    fontWeight: 'bold',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
  MainContainer: {
    justifyContent: 'center',
    flex: 1,
    paddingTop: 30,
  },
  imageThumbnail: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 100,
  },
  textInputStyle: {
    height: 40,
    borderWidth: 1,
    paddingLeft: 10,
    borderColor: Colors.PRIMARY_COLOR,
    backgroundColor: 'white',
  },
  viewStyle: {
    justifyContent: 'center',
    padding: 16,
  },
  ProcessText: {
    fontSize: 18,
    fontWeight: 'bold',
    justifyContent: 'center',
    alignItems: 'center',
    color: 'white'
  },
  ProcessButton: {
    backgroundColor: Colors.PRIMARY_COLOR
  },
  inputTextContainer: {
    left: 15
  },
  buttonContainerProcess: {
    height: 40,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 30,
    marginTop: 20,
    width: 180,
    borderRadius: 10,
    shadowColor: Colors.DARK_BLACK_TEXT_COLOR,
    shadowOpacity: 0.8,
    elevation: 6,
    shadowRadius: 15,
    shadowOffset: { width: 1, height: 13 }
 
  },
  input: {
    color: '#ffff',
    fontWeight: 'bold',
    textAlign:"left",
    paddingLeft:30
   
 
  },

  date: {
    color: '#ffff',
    fontWeight: 'bold',
    textAlign:"left",
    paddingLeft:20
   
 
  },
  input4: {
    color: '#616161',
    textAlign:"center",
    fontSize:50,
    top:'15%',
    

    
    fontFamily:'sans-serif-thin'
 
  },
  input2: {
    color: 'white',
    fontWeight: 'bold',
    left: 20
  },
  item: {
    borderColor: Colors.PRIMARY_COLOR,
    width: 200
  },
  item2: {
    marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
    backgroundColor: Colors.PRIMARY_COLOR,
    borderColor: Colors.PRIMARY_COLOR,
  },
  TextStyle: {
    fontSize: 23,
    textAlign: 'center',
    color: '#f00',
  },
  itemAmount: {
    backgroundColor: Colors.BACKGROUD_WHITE,
    borderColor: Colors.PRIMARY_COLOR,
    borderLeftWidth: 5,
    borderRightWidth: 5,
    borderBottomWidth: 5,
    borderTopWidth: 5
 
  },
  place: {
    width: 25,
    height: 25,
    left: 10,
    
  },
  place1: {
    width: 30,
    height: 30,
 
  },
  totalTextStyle: {
    color: Colors.DARK_BLACK_TEXT_COLOR,
    fontSize: 20,
    fontWeight: 'bold',
    top: 10
  },
  form: {
    flex: 1,
    top:100,
  },
  txtColor: {
    fontSize: 20,
    fontWeight: 'bold',
    left: 10,
    color: Colors.DARK_BLACK_TEXT_COLOR
  },
  TextInputStyle2: {
    height: 40,
    borderBottomColor: Colors.DARK_BLACK_TEXT_COLOR,
    width: 200,
    marginBottom: 10,
    borderBottomWidth: 1
  },
  backGround: {
    backgroundColor: Colors.PRIMARY_COLOR,
 
  },
 
  headerTextStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 25,
    left: 70,
    color: 'white',
  },
  header: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  headerStyle: {
    backgroundColor: Colors.PRIMARY_COLOR,
    shadowOpacity: 0.75,
    shadowRadius: 5,
    shadowOffset: { height: 0, width: 0 },
 
  },
  ToucherbleIconStyle: { color: Colors.PRIMARY_COLOR },
  ToucherbleIconStyle2: { height: 30, width: 30 },
  ToucherbleIconStyle4: { height: 40, width: 40, left: 135 },
  ToucherbleIconStyle3: { height: 20, width: 20 },
 
  container1: { flex: 1, padding: 16, paddingTop: 30, },
  head: { height: 40, backgroundColor: Colors.PRIMARY_COLOR },
  text: { margin: 6, color: 'white' },
  row: { flexDirection: 'row', backgroundColor: Colors.FLAT_TEXT_COLOR },
  btn: { width: 58, height: 18, },
  btnText: { textAlign: 'center', color: '#fff' },
  buttonContainer: {
    width:'100%',
    justifyContent:"center",
    alignItems:"center",
    position:"absolute",
    bottom:1,
    // right:8,
   
    
  },
  container2: {
    // backgroundColor: '#F5FCFF',
  },
  calButton: {
    backgroundColor: Colors.PRIMARY_COLOR
  },
  textInput: {
    height: 34,
    fontSize: 22,
    fontWeight: 'bold'
 
  },
  modal4: {
    height: hp('40%'),
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    backgroundColor: 'white'
  },
 
  buttonContainerProcess5: {
    height: hp('7%'),
    width: wp('80%'),
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
    borderRadius: 30,
    shadowColor: Colors.DARK_BLACK_TEXT_COLOR,
    shadowOpacity: 0.8,
    elevation: 6,
    shadowRadius: 15,
    shadowOffset: { width: 1, height: 13 }
 
  },
  ProcessButton6: {
    backgroundColor: Colors.PRIMARY_COLOR
  },
  ProcessText4: {
    fontSize: 18,
    fontWeight: 'bold',
    justifyContent: 'center',
    alignItems: 'center',
    color: 'white'
  },
  quickBilling: {
    height:hp('50%'),
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    backgroundColor: 'white'
  },
  form2: {
    flex: 1,
    // justifyContent: 'flex-end',
  },
  TextItemStyle2: {
    width: '92%',
    height: 55,
    borderBottomColor: Colors.PRIMARY_COLOR,
    borderBottomWidth: 2
  },
  loginText2: {
    color: '#126F85',
    fontSize: 20,
    fontWeight: 'bold',
  },
  crediticon:{
    width:50,
    paddingRight:20,
    marginRight:30
  },
  scale:{
    width: 300,
    height: 190
  },
});
 
export default styles;