import React, { Component } from 'react';
import { Image, TouchableOpacity, ImageBackground, StatusBar, Alert, AsyncStorage, Vibration, BackHandler, ScrollView, PixelRatio, DeviceEventEmitter, Dimensions } from 'react-native';
import {
    Item,
    Input,
    Text,
    Header,
    Left,
    Right,
    Drawer,
    Fab,
    Picker,
    Form,
    Label,
    Button
} from 'native-base';
import AwesomeAlert from 'react-native-awesome-alerts';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Spinner from "react-native-spinkit";
import Modal from 'react-native-modalbox';
import styles from './HomeScreenStyle';
import Colors from '../../resources/Colors';
import Strings from '../../resources/Strings';
import Messages from '../../resources/Message';
import { FlatGrid } from 'react-native-super-grid';
import NetInfo from "@react-native-community/netinfo";
import { View } from 'react-native-animatable';
import SideBar from '../sideBarScreen/SideBarScreen';
import Toast from 'react-native-tiny-toast'
import { NavigationActions, StackActions, SafeAreaView } from 'react-navigation'
import Moment from 'moment';
import Spinner2 from 'react-native-loading-spinner-overlay';
import AlertPro from "react-native-alert-pro";
var dateFormat = require('dateformat');
import { BluetoothEscposPrinter, BluetoothManager } from "react-native-bluetooth-escpos-printer";
import { FlatList } from 'react-native-gesture-handler';

const DURATION = 30;
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
import BackgroundTimer from 'react-native-background-timer';

export default class Basic extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            selectedItem: 'About',
            arrayholder: [],
            customBackgroundDialog: false,
            count: 0,
            temp: 0,
            price: 0,
            tempPrice: 0,
            text: '',
            trancerdate: [],
            tempArray: [],
            itemType: '',
            avatarSource: "",
            image: null,
            spinner: false,
            url: "",
            description: '',
            priceT: 0,
            cost: '',
            nodata: false,
            searchText: "",
            showTheThing: true,
            arrayDetails: [],
            category: [],
            search: true,
            categoryDrop: false,
            catname: "",
            backDetails: "",
            total: '',
            page: 0,
            quick: false,
            companyName: '',
            companyAddress: '',
            companyContactNumber: '',
            custName: 'unknown',
            invoiceNumber: '',
            qtyCount: '',
            subTotal: 0,
            discount: 0,
            grandTotal: 0,
            receivedPrice: 0,
            balance: 0,
            date: "",
            userName: "",
            posts: [],
            backgroundColor2: Colors.PRIMARY_COLOR,
            textColor: Colors.DARK_BLACK_TEXT_COLOR,
            receivedPrice: 0,
            tot: '',
            uri: "",
            balance: 0,
            batchDetails: [],
            invoiceNumber: '',
            totalDiscount: 0,
            spinner2: false,
            bluetoothConnected: 0,
            token: "",
            barcode: false,
            printerStatus: false,
            productName: "",
            switchValue: false,
            temporaryItem: false,
            showAlert: false,
            showAlert1: false,
            backClickCount: 0,
            backClickCount1: 0,
            BLPAddress: "",

            items: [
                { name: 'Manage Customer', url: require('../../assets/cus_man.png'), Label: 'Customer' },
                { name: 'Manage Category', url: require('../../assets/cat_man.png'), Label: 'Category' },
                { name: 'Manage Products', url: require('../../assets/man_item.png'), Label: 'Item' },
                { name: 'Invoice Details', url: require('../../assets/inv_details.png'), Label: 'Details' },
                { name: 'Credit Book', url: require('../../assets/cheque.png'), Label: 'Credit' },
                { name: 'Reports', url: require('../../assets/combo.png'), Label: 'Report' },
                { name: 'Inventory', url: require('../../assets/inventory.png'), Label: 'Inventory' },
                { name: 'Add Users', url: require('../../assets/addusers.png'), Label: 'AddUsers' },
                { name: 'Suppliers', url: require('../../assets/supplier.png'), Label: 'supplier' },
                { name: 'Settings', url: require('../../assets/service.png'), Label: 'Setting' },
                { name: 'About Us', url: require('../../assets/info.png'), Label: 'About' },
                { name: 'Visit BillaCloud.com', url: require('../../assets/visit.png'), Label: 'Visit' },

            ],

        };

    }

    // validation
    validation() {
        if (this.state.description == "") {
            Messages.messageName(Strings.WARNING, Strings.DESCRIPTION_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.DARK_BLACK_TEXT_COLOR);
        } else if (this.state.priceT == "") {
            Messages.messageName(Strings.WARNING, Strings.PRICE_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.DARK_BLACK_TEXT_COLOR);
        } else {
            this.saveItem()
        }
    }




    //log out 
    logOut() {
        AsyncStorage.removeItem('myArray');
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'LoginScreen' })],
        });

        this.props.navigation.dispatch(resetAction);

        Messages.messageName(Strings.WARNING_SUCESS, Strings.LOG_OUT, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);

        //             }
        //         },
        //     ],
        //     { cancelable: true }
        // );
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    //Back Handler Press
    onButtonPress() {
    }



    handleBackButton = () => {
        // this.showAlert1();
        // setTimeout(() => {
        //     backClickCount = 1;
        //   }, 1000);
        // this.state.backClickCount += 0

        // this.closeDrawer(); 
        // return true;


        // this.showAlert1();
        // return true;

        if (this.onOpenedz) {
            this.closeDrawer();
            return true;
        } else if (this.closeDrawer) {
            this.showAlert1();
            return true;
        }
        // this.closeDrawer(); 

    }


    closeOneItemModel6() {
        this.refs.modal4.close()
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        this.setState({
            date: Moment().format('YYYY-MM-D HH:mm:ss')
        })
        this.state.customer = this.props.navigation.getParam('customer', 'nothing sent')
        this.showData()
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    bluetoothConnectedChecking() {
        BluetoothManager.connect(this.state.BLPAddress)
            .then((s) => {

                console.log("connected")
                this.setState({
                    bluetoothConnected: 1,

                })
            },
                (err) => {
                    console.log("connected" + err)
                    this.setState({
                        bluetoothConnected: 0
                    })
                })
    }

    async componentWillMount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
        let user = await AsyncStorage.getItem('companyDetails');
        let myArray1 = await AsyncStorage.getItem('barcode');
        let d1 = JSON.parse(myArray1);
        let myArray2 = await AsyncStorage.getItem('customItem');
        let d2 = JSON.parse(myArray2);
        let parsed = JSON.parse(user);
        // console.log(JSON.stringify(parsed.company.name))
        console.log("ssssssssssssssssssssssssssssssss" + JSON.stringify(parsed))
        this.setState({
            barcode: d1,
            temporaryItem: d2,
            userName: parsed.name,
            companyName: parsed.company.name,
            companyAddress: parsed.address,
            companyContactNumber: parsed.company.customSettings.contact,
            uri: parsed.imageUrl,

        })

        // console.log("dddddddddddddddddddddddd" + JSON.stringify(this.state.tempArray))
        // console.log("wwwwwwwwwwwwwwwwwwww" + this.state.companyName)
        // console.log("wwwwwwwwwwwwwwwwwwww" + this.state.uri)

    }

    //show alert hide and show method

    showAlert = () => {
        this.setState({
            showAlert: true
        });
        // this.refs.sideMenu.close()
    };

    showAlert1 = () => {
        // this.closeDrawer();
        this.setState({
            showAlert1: true
        });
        // this.refs.sideMenu.close()
    };

    hideAlert = () => {
        this.setState({
            showAlert: false,
        });
    };

    hideAlert1 = () => {
        this.setState({
            showAlert1: false
        });
    };

    //navigate To Screens
    navigateToScreen(route) {
        if (route === 'Customer') {
            this.props.navigation.navigate("ManageCustomerScreen");
        } else if (route === 'Category') {
            this.props.navigation.navigate("CategoryScreen");
        } else if (route === 'Item') {
            this.props.navigation.navigate("ManageItemScreen");
        } else if (route === 'Details') {
            this.props.navigation.navigate("InvoiceScreen");
        } else if (route === 'Credit') {
            this.props.navigation.navigate("CreditBookScreen");
        }
        else if (route === 'Report') {
            this.props.navigation.navigate("ReportsScreen");
        } else if (route === 'Inventory') {
            this.props.navigation.navigate("InventoryScreen");
        } else if (route === 'AddUsers') {
            this.props.navigation.navigate("SubusersScreen");
        }
        else if (route === 'supplier') {
            this.props.navigation.navigate("SupplierScreen");
        }
        else if (route === 'Setting') {
            this.props.navigation.navigate("SettingScreen");
        } else if (route === 'About') {
            // this.props.navigation.navigate("SettingScreen");
        }

    }

    //when close drawer
    closeDrawer = () => {
        this.refs.sideMenu.close()
        this.bluetoothConnectedChecking()
        this.getAllProduct()
        this.getAllCategories()

    };

    //when open drawer
    openDrawer = () => {
        this.refs.sideMenu.open()
        this.setState({
            showTheThing: false
        })
    };

    //asign category type
    onValueChange(value) {
        this.setState({
            itemType: value
        });
    }

    //when back to home screen , asign new vales to (arrayDetails) array
    componentDidUpdate() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        this.state.arrayDetails = this.props.navigation.getParam('details', '')
        this.state.totalDiscount = this.props.navigation.getParam('totalDiscount', '')
        this.state.switchValue = this.props.navigation.getParam('switchValue', '')
        this.state.subTotal = this.props.navigation.getParam('subTot', 0)

        console.log("zzzzzzzzzzzzz" + JSON.stringify(this.state.subTotal));
        if (this.state.arrayDetails.length > 0) {
            this.state.tempArray = this.state.arrayDetails
        } else {
            console.log('hava' + this.state.arrayDetails.length);
            const { navigation } = this.props;
            this.focusListener = navigation.addListener('didFocus', () => {
                var tot = 0;
                var qty = 0;
                var temp = 0;

                if (this.state.totalDiscount > 0) {
                    if (this.state.switchValue == true) {
                        for (let index = 0; index < this.state.arrayDetails.length; index++) {
                            temp = temp + this.state.arrayDetails[index].total
                            tot = temp - this.state.totalDiscount
                            qty = qty + this.state.arrayDetails[index].qty;

                        }
                    } else {
                        for (let index = 0; index < this.state.arrayDetails.length; index++) {
                            tot = tot + this.state.arrayDetails[index].total - (this.state.arrayDetails[index].total * this.state.totalDiscount / 100)
                            qty = qty + this.state.arrayDetails[index].qty;

                        }
                    }

                    console.log(tot);
                    this.setState({ tempPrice: tot, temp: qty, count: qty });
                } else {


                    for (let index = 0; index < this.state.arrayDetails.length; index++) {
                        if (this.state.arrayDetails[index].discountPercentage <= 100) {
                            tot = tot + this.state.arrayDetails[index].total;
                        } else {
                            tot = tot + this.state.arrayDetails[index].total;
                        }
                        qty = qty + this.state.arrayDetails[index].qty;
                    }

                    console.log(tot);
                    this.setState({ tempPrice: tot, temp: qty, count: qty });

                }

            });

        }
    }

    //getting auth code from AsyncStorage
    showData = async () => {
        let myArray = await AsyncStorage.getItem('myArray');
        let d = JSON.parse(myArray);
        this.setState({
            token: d

        })

        console.log("mmmmmmmmmmmmmmmmmmmmmmmmm" + this.state.token);

        this.getAllProduct()
        this.getAllCategories()
    }

    //when press grid item (Vibrate)
    StartVibrationFunction = () => {
        Vibration.vibrate(DURATION);

    }






    renderNext3() {
        this.refs.view3.pulse(200);
    }

    renderNext4() {
        this.refs.view4.fadeOutUpBig(500);
        this.renderNext3();
    }

    //when open model, plus button invisible 
    onClose = () => {
        this.setState({
            showTheThing: true
        })
    }

    //open model
    anima() {
        this.setState({ showTheThing: false })
        this.refs.modal4.open()
    }



    animaNavigate() {
        this.setState({
            scaleAnimationDialog2: false,
        });
        this.props.navigation.navigate('CustomerScreen')
    }

    //asign search word
    search(text) {
        this.state.searchText = text;
        this.getAllProduct()
    }

    //getting all category types
    getAllCategories() {

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + "/api/product/category?page=0&limit=500&text=" + this.state.searchText,
                    {
                        method: "GET",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },

                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        console.log("categories" + JSON.stringify(responseJson));

                        this.setState({
                            category: responseJson.content
                        })

                    })

                    .catch(error => {
                        console.log(error);
                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.getAllCategories()

                            }
                        },
                    ],
                    { cancelable: true }
                );

            }
        });

    }

    //get All of products
    async  getAllProduct() {
        let myArray2 = await AsyncStorage.getItem('quickSettings');
        let myArray1 = await AsyncStorage.getItem('barcode');
        let d1 = JSON.parse(myArray1);
        let a = JSON.parse(myArray2);
        let myArray3 = await AsyncStorage.getItem('customItem');
        let d3 = JSON.parse(myArray3);
        let myArray5 = await AsyncStorage.getItem('BLPAddress');
        let d5 = JSON.parse(myArray5);
        console.log("sssssssssssssssssssss" + d5)

        this.setState({
            quick: a,
            BLPAddress: d5,
            temporaryItem: d3,
            barcode: d1,
        })
        if (this.state.BLPAddress !== null) {
            this.bluetoothConnectedChecking()

        }

        this.setState({
            spinner: true,
        });

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + '/api/inventory/filter?category=' + this.state.catname + '&name=' + this.state.searchText + '&page=0&limit=100',
                    {
                        method: "GET",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },

                    }
                )
                    .then(resp => resp.json())
                    .then(responseJson => {
                        this.setState({
                            spinner: false,
                            page: 0,

                        });

                        console.log("ssssssssssssssssssssssssss" + JSON.stringify(responseJson));

                        if (responseJson.errorCode == "ACCESS_TOKEN_EXPIRED") {
                            Alert.alert("Session expired !")
                            const resetAction = StackActions.reset({
                                index: 0,
                                actions: [NavigationActions.navigate({ routeName: 'LoginScreen' })],
                            });
                            this.props.navigation.dispatch(resetAction);

                        } else if (responseJson.errorCode == "INVALID_ACCESS_TOKEN") {
                            Alert.alert("Session expired !")
                            const resetAction = StackActions.reset({
                                index: 0,
                                actions: [NavigationActions.navigate({ routeName: 'LoginScreen' })],
                            });
                            this.props.navigation.dispatch(resetAction);

                        } else if (responseJson.errorCode == "COMPANY_IS_NOT_ACTIVATED") {
                            Messages.messageName(Strings.WARNING, Strings.TEMPERY_USER_DEACTIVE, Strings.ICON[0], Strings.TYPE[0], Colors.PRIMARY_COLOR);
                            const resetAction = StackActions.reset({
                                index: 0,
                                actions: [NavigationActions.navigate({ routeName: 'DeactivateScreen' })],
                            });
                            this.props.navigation.dispatch(resetAction);
                        }
                        this.setState({
                            arrayholder: responseJson.content
                        })

                        if (responseJson.content.length == 0) {
                            this.setState({
                                nodata: true
                            })
                        } else {
                            this.setState({
                                nodata: false
                            })
                        }

                    })
                    .catch(error => {
                        console.log(error);
                        Messages.messageName(Strings.WARNING_CONNECTION, Strings.INTERNAL_ERROR, Strings.ICON[2], Strings.TYPE[2]);
                        this.setState({
                            spinner: false,
                        });

                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.getAllProduct()

                            }
                        },
                    ],
                    { cancelable: true }
                );

            }
        });

    }

    financial(x) {
        return Number.parseFloat(x).toFixed(2);
    }

    //when increment qty count ,update total & qty count
    countQty(item) {
        var PriceY = item.price
        this.StartVibrationFunction()
        const temp = {
            batchId: 0,
            cost: 0,
            customFields: {},
            discountAmount: 0,
            discountPercentage: 0,
            itemType: "PRODUCT",
            name: item.name,
            price: item.price,
            itemId: item.productId,
            qty: 1,
            salePrice: item.price,
            serialNumbers: ["string"],
            total: item.price,
            fullTotal:item.price
        }
        this.renderNext3();
        this.state.temp = this.state.temp + 1
        this.setState({
            count: this.state.temp,
        })
        var obj = temp
        item = {};
        var myArray = this.state.tempArray
        var included = false;
        let tempTot = 0;
        var tempTot1 = 0;
        for (let index = 0; index < myArray.length; index++) {
            if (myArray[index].itemId == obj.itemId && myArray[index].batchId == obj.batchId) {

                if (this.state.totalDiscount > 0) {
                    if (this.state.switchValue == true) {
                        included = true;
                        myArray[index].qty = myArray[index].qty + 1;
                        myArray[index].total = myArray[index].qty * obj.price
                        myArray[index].fullTotal = myArray[index].qty * obj.price
                        myArray[index].salePrice = obj.price
                        for (let index = 0; index < myArray.length; index++) {
                            tempTot1 = tempTot1 + myArray[index].total

                        }
                        tempTot = tempTot1 - this.state.totalDiscount

                    } else {
                        included = true;
                        myArray[index].qty = myArray[index].qty + 1;
                        myArray[index].total = myArray[index].qty * obj.price
                        myArray[index].fullTotal = myArray[index].qty * obj.price
                        myArray[index].salePrice = obj.price
                        for (let index = 0; index < myArray.length; index++) {
                            tempTot = tempTot + myArray[index].total - (myArray[index].total * this.state.totalDiscount / 100)

                        }
                    }

                    this.setState({
                        tempPrice: tempTot
                    })
                } else {
                    if (myArray[index].discountPercentage > 1 || myArray[index].discountAmount > 1) {
                        if (myArray[index].customFields.type == "value") {
                            Alert.alert("working")
                            var temp1 = 0;
                            var tot1 = 0;
                            var tott1 = 0;
                            included = true;
                            myArray[index].qty = myArray[index].qty + 1
                            tott1 = obj.price * myArray[index].qty
                            myArray[index].fullTotal = myArray[index].qty * obj.price
                            temp1 = temp1 + tott1
                            myArray[index].total = temp1 - myArray[index].discountAmount
                            myArray[index].salePrice = myArray[index].price- myArray[index].discountAmount

                            var tot = 0;
                            for (let index = 0; index < myArray.length; index++) {
                                tot = tot + myArray[index].total;
                            }
                            this.state.tempPrice = tot

                        } else {
                            included = true;
                            myArray[index].qty = myArray[index].qty + 1
                            let tot3 = obj.price * myArray[index].qty
                            myArray[index].fullTotal = myArray[index].qty * obj.price
                            let finalAmount =tot3 - (tot3* myArray[index].discountPercentage / 100)
                            myArray[index].total = finalAmount
                            myArray[index].salePrice = myArray[index].price - (myArray[index].price* myArray[index].discountPercentage / 100)
                            var tot = 0;
                            for (let index = 0; index < myArray.length; index++) {
                                tot = tot + myArray[index].total;
                            }
                            this.state.tempPrice = tot
                        }

                    } else {

                        this.state.tempPrice = this.state.tempPrice + PriceY
                        console.log("no")
                        included = true;
                        myArray[index].qty = myArray[index].qty + 1
                        myArray[index].total = obj.price * myArray[index].qty
                        myArray[index].fullTotal = myArray[index].qty * obj.price
                        myArray[index].salePrice = myArray[index].price
                    }
                }




            }
        }

        this.setState({
            tempArray: myArray
        })

        if (!included) {
            if (this.state.totalDiscount > 0) {
                if (this.state.switchValue == true) {
                    var TPrice;
                    TPrice = this.state.tempPrice
                    TPrice = TPrice + PriceY
                    // Alert.alert("workign"+TPrice)
                    this.state.tempPrice = TPrice
                    this.state.tempArray.push(this.clone(obj));
                } else {
                    TPrice = PriceY + this.state.subTotal
                    this.state.tempPrice = TPrice - (TPrice * this.state.totalDiscount / 100)
                    this.state.tempArray.push(this.clone(obj));
                }


            } else {
                this.state.tempPrice = this.state.tempPrice + PriceY
                this.state.tempArray.push(this.clone(obj));

            }


        }

    }

    clone(obj) {
        return JSON.parse(JSON.stringify(obj))
    }

    //Navigate to cart Screen
    navigateToCart() {
        console.log(JSON.stringify(this.state.tempArray))
        if (this.state.temp <= 0) {
            Alert.alert("Your Cart is Empty!")
        } else {
            const { navigate } = this.props.navigation;
            navigate('CartScreen', {
                text: this.state.tempArray,
                totalDiscount: this.state.totalDiscount,
                switchValue: this.state.switchValue

            })

        }
        this.state.tempArray = []
    }



    //Save tempory item 
    saveItem() {
        Messages.messageName(Strings.WARNING_SUCESS, Strings.ITEM_ADDED_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
        const temp = {
            batchId: 0,
            cost: 0,
            customFields: {},
            discountAmount: 0,
            discountPercentage: 0,
            name: this.state.description,
            price: parseInt(this.state.priceT),
            itemId: 0,
            qty: 1,
            salePrice: parseInt(this.state.priceT),
            serialNumbers: ["string"],
            total: parseInt(this.state.priceT)
        }
        this.state.arrayholder.unshift(temp);
        this.refs.modal4.close()
        console.log("aaaa 1" + JSON.stringify(this.state.arrayholder))
        this.setState({
            slideAnimationDialog: false,
        });
    }

    //no data iamge
    nodataImage() {
        if (this.state.nodata) {
            return (

                <View style={{ top: 60, justifyContent: 'center', alignItems: 'center', }}>
                    <Image style={{ width: 200, height: 200, borderRadius: 100 }}
                        source={require("../../assets/no_data_found.png")}
                    />
                    {/* <Text>Add New Records </Text>
                    <Text>go to</Text>
                    <Text>SideBar/Manage Item</Text> */}
                </View>
            )
        }
    }

    visibleDropDown() {
        this.setState({
            search: false,
            categoryDrop: true
        })
    }

    InvisibleDropDown() {
        this.setState({
            catname: "",
            categoryDrop: false,
            search: true
        })
        this.getAllProduct()
    }

    //load category for dropdown
    loadCategoryTypes() {
        return this.state.category.map(data => (
            <Picker.Item label={data.name} value={data.name} key={data.name} />
        ));
    }

    //drop down value asign
    onPickerValueChange = (itemValue, index) => {
        this.setState({
            catname: itemValue
        })
        this.getAllProduct()
    }

    addRecords = (page) => {
        // assuming this.state.dataPosts hold all the records
        console.log(page)
        // assuming this.state.dataPosts hold all the records

    }

    onScrollHandler = () => {
        this.setState({
            page: this.state.page + 1
        }, () => {
            this.addRecords(this.state.page);
        });

    }

    openQuickBillingModel() {

        if (this.state.temp <= 0) {
            Alert.alert("Your Cart is Empty!")
        } else {
            this.refs.quickBilling.open()
            this.setState({
                showTheThing: false
            })

        }
    }

    generateUniqueInvoiceNumber(value) {
        this.setState({
            spinner2: true,
        });
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + "/api/customer-invoice-number/unique/generate",
                    {
                        method: "GET",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },

                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        console.log("InvoiceNumber" + JSON.stringify(responseJson));
                        this.setState({
                            spinner2: false,
                            invoiceNumber: responseJson.prefix + responseJson.nextInvoiceNumber
                        })
                        if (value == "quickPrint") {
                            this.printRecept()

                        } else {
                            this.invoice()
                        }

                    })

                    .catch(error => {
                        console.log(error);
                        Messages.messageName(Strings.WARNING_CONNECTION, Strings.INTERNAL_ERROR, Strings.ICON[2], Strings.TYPE[2], Colors.DARK_BLACK_TEXT_COLOR);
                        this.setState({
                            spinner2: true,
                        });

                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2], Colors.DARK_BLACK_TEXT_COLOR);
                this.setState({
                    spinner2: true,
                });

            }
        });
    }

    calculateTotal(text) {
        this.state.receivedPrice = text;
        const balance = this.state.receivedPrice - this.state.tempPrice

        if (text < this.state.tempPrice) {
            this.setState({
                backgroundColor2: Colors.BTN_COLOR,
                textColor: Colors.ERR_COLOR,

            })
        } else {
            this.setState({
                backgroundColor2: Colors.PRIMARY_COLOR,
                textColor: Colors.DARK_BLACK_TEXT_COLOR,
            })
        }

        this.setState({
            balance: balance,
            tot: balance.toString().substring(1)

        })



    }



    navigateToDone(value) {

        if (this.state.backgroundColor2 == Colors.BTN_COLOR) {
            Messages.messageName(Strings.WARNING_SUCESS, Strings.LOWER_AMOUNT, Strings.ICON[0], Strings.TYPE[0], Colors.DARK_BLACK_TEXT_COLOR);
        } else {

            this.invoice()
        }

    }

    // Quick place Order
    invoice() {
        this.setState({
            spinner2: true,
        });
        var data = JSON.stringify({

            "additionalCostAmount": 0,
            "charges": [],
            "creditDayPeriod": 30,
            "customFields": { "cashGivenAmount": this.state.receivedPrice },
            "customerId": this.state.customerId,
            "description": "string",
            "invoiceDate": {
                "basic": this.state.date,
            },
            "invoiceDiscountAmount": 0,
            "invoiceDiscountPercentage": 0,
            "invoiceNumber": this.state.invoiceNumber,
            "items": this.state.tempArray,
            "payments": [
                {
                    "amount": this.state.tempPrice,
                    "description": "string",
                    "paymentMethod": "CASH"
                }
            ],
            "serviceChargeAmount": 0,
            "taxes": [],

        });

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + '/api/customer/invoice',
                    {
                        method: "POST",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },
                        body: data
                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        Messages.messageName(Strings.WARNING_SUCESS, Strings.PLACE_ORDER_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                        console.log(JSON.stringify(responseJson));
                        const balance = this.state.receivedPrice - this.state.tempPrice
                        this.props.navigation.navigate('DoneScreen', {
                            total: this.state.tempPrice,
                            balance: balance

                        });
                        this.setState({
                            spinner2: false
                        });
                    })

                    .catch(error => {
                        console.log(error);
                        Messages.messageName(Strings.WARNING_CONNECTION, Strings.INTERNAL_ERROR, Strings.ICON[2], Strings.TYPE[2], Colors.DARK_BLACK_TEXT_COLOR);
                        this.setState({
                            spinner2: false,
                        });

                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2], Colors.DARK_BLACK_TEXT_COLOR);
                this.setState({
                    spinner2: false
                })

            }
        });

    }

    QuickprintRecept() {
        this.navigateToDone("quickPrint")
    }
    closequickmodal() {
        this.refs.quickBilling.close()
    }

    gettingProductBatch(vale) {
        this.setState({
            spinner: true,
        });

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + '/api/product/' + vale + '/batch?page=0&limit=400',
                    {
                        method: "GET",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },

                    }
                )
                    .then(resp => resp.json())
                    .then(responseJson => {
                        console.log("aaaaaaaaaaaaaaaaaa" + JSON.stringify(responseJson.content))
                        this.setState({
                            spinner: false,
                        });
                        this.refs.modal3.open()
                        this.setState({
                            batchDetails: responseJson.content
                        })

                    })
                    .catch(error => {
                        console.log(error);
                        this.setState({
                            spinner: false,
                        });

                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.gettingCategories()

                            }
                        },
                    ],
                    { cancelable: true }
                );

            }
        });

    }


    showBatchListModel(item) {
        // console.log("ssssssssss"+JSON.stringify(item))
        this.setState({
            productName: item.name
        })
        this.gettingProductBatch(item.productId)

    }

    //print receipt confiug
    printRecept = async () => {
        try {
            await BluetoothEscposPrinter.printerInit();
            await BluetoothEscposPrinter.printerLeftSpace(0);
            await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.CENTER);
            await BluetoothEscposPrinter.setBlob(0);
            await BluetoothEscposPrinter.printText("--------------------------------\r\n", {});
            await BluetoothEscposPrinter.printText(this.state.companyName + "\r\n", { encoding: 'GBK', fonttype: 10 });
            await BluetoothEscposPrinter.setBlob(0);
            await BluetoothEscposPrinter.printText(this.state.companyAddress + "\r\n", {
                encoding: 'GBK',
                codepage: 0,
                widthtimes: 0,
                heigthtimes: 0,
                fonttype: 1
            });
            await BluetoothEscposPrinter.printText("Tel :" + this.state.companyContactNumber + "\r\n", {});
            await BluetoothEscposPrinter.printText("______________________________________\r\n", {});
            await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.LEFT);
            await BluetoothEscposPrinter.printText("Date  : " + (dateFormat(new Date(), "yyyy-mm-dd h:MM:ss")) + "\r\n", {});
            await BluetoothEscposPrinter.printText("Customer : " + this.state.custName + "\n\r", {});
            await BluetoothEscposPrinter.printText("cashier : Admin\r\n", {});
            await BluetoothEscposPrinter.printText("Invoice Number :" + this.state.invoiceNumber + "\r\n", {});
            await BluetoothEscposPrinter.printText("_________________________________________\r\n", {});
            let columnWidths = [12, 6, 6, 8];
            await BluetoothEscposPrinter.printColumn(columnWidths,
                [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.CENTER, BluetoothEscposPrinter.ALIGN.CENTER, BluetoothEscposPrinter.ALIGN.RIGHT],
                ['Name', "Qty", 'Price', 'Total',], {});
            for (let index = 0; index < this.state.tempArray.length; index++) {
                await BluetoothEscposPrinter.printText("\r\n", {});
                await BluetoothEscposPrinter.printColumn(columnWidths,
                    [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.CENTER, BluetoothEscposPrinter.ALIGN.RIGHT],
                    [this.state.tempArray[index].name + "", this.state.tempArray[index].qty + "", this.state.tempArray[index].salePrice + "", this.state.tempArray[index].price + ""]
                    , {});

            }
            await BluetoothEscposPrinter.printText("\r\n", {});
            await BluetoothEscposPrinter.printText("_________________________________________\r\n", {});
            await BluetoothEscposPrinter.printText("\r\n", {});
            await BluetoothEscposPrinter.printText("No of items : " + this.state.count + "\r\n", {});
            await BluetoothEscposPrinter.printText("Sub Total  : " + this.state.tempPrice + "\r\n", {});
            await BluetoothEscposPrinter.printText("Discount   : " + this.state.discount + "%" + "\r\n", {});
            await BluetoothEscposPrinter.setBlob(0);
            await BluetoothEscposPrinter.printText("Grand Total  : " + this.state.tempPrice + "\r\n", {
                encoding: 'GBK',
                codepage: 0,
                widthtimes: 3,
                heigthtimes: 3,
                fonttype: 1
            });
            await BluetoothEscposPrinter.printText("paid amount :" + this.state.receivedPrice + "\r\n", {});
            await BluetoothEscposPrinter.printText("Balance    :" + this.state.balance + "\r\n", {});
            await BluetoothEscposPrinter.printText("--------------------------------\r\n", {});
            {
                this.state.barcode &&
                    await BluetoothEscposPrinter.printBarCode("123456789012", BluetoothEscposPrinter.BARCODETYPE.JAN13, 3, 120, 0, 2);
            }
            await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.CENTER);
            await BluetoothEscposPrinter.printText("Thank you Come again.....!\r\n\r\n\r\n", {});
            await BluetoothEscposPrinter.printText("--------------------------------\r\n", {});
            await BluetoothEscposPrinter.printText("IT Partner - Commercial Technologies 0774653543\r\n", {});
            await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.LEFT);
            await BluetoothEscposPrinter.printText("\r\n\r\n\r\n", {});
        } catch (e) {
            // Alert.alert(e.message || "ERROR");

            if (e.code == "EUNSPECIFIED") {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.PRINTER_ERROE, Strings.ICON[2], Strings.TYPE[2], Colors.DARK_BLACK_TEXT_COLOR);
                this.setState({
                    printerStatus: true
                })
            }



        }

        if (this.state.printerStatus == false) {
            this.invoice()
        }
    }

    showDefault = () => {
        Toast.show('Please long tap', { duration: 1000 })
    }



    navigateToAddItemScreen() {
        this.props.navigation.navigate("ManageItemScreen");
    }

    //open model
    addTemporaryItem(id) {
        // Alert.alert(id + "")
        this.setState({ showTheThing: false })
        this.refs.modal4.open()
    }

    batchPriceAddToCart(item) {
        var PriceY = item.price
        this.StartVibrationFunction()
        const temp = {
            batchId: item.batchNumber,
            cost: item.cost,
            customFields: {},
            discountAmount: 0,
            discountPercentage: 0,
            itemType: "PRODUCT",
            name: this.state.productName,
            price: item.price,
            itemId: item.productId,
            qty: 1,
            salePrice: item.price,
            serialNumbers: ["string"],
            total: item.price,
            fullTotal:item.price
        }
        this.renderNext3();
        this.refs.modal3.close()
        this.state.temp = this.state.temp + 1
        this.setState({
            count: this.state.temp,
        })
        var obj = temp
        item = {};
        var myArray = this.state.tempArray
        var included = false;
        let tempTot = 0;
        var tempTot1 = 0;
        for (let index = 0; index < myArray.length; index++) {
            if (myArray[index].itemId == obj.itemId && myArray[index].batchId == obj.batchId) {

                if (this.state.totalDiscount > 0) {
                    if (this.state.switchValue == true) {
                        included = true;
                        myArray[index].qty = myArray[index].qty + 1;
                        myArray[index].total = myArray[index].qty * obj.price
                        myArray[index].fullTotal = myArray[index].qty * obj.price
                        myArray[index].salePrice = obj.price
                        for (let index = 0; index < myArray.length; index++) {
                            tempTot1 = tempTot1 + myArray[index].total

                        }
                        tempTot = tempTot1 - this.state.totalDiscount

                    } else {
                        included = true;
                        myArray[index].qty = myArray[index].qty + 1;
                        myArray[index].total = myArray[index].qty * obj.price
                        myArray[index].fullTotal = myArray[index].qty * obj.price
                        myArray[index].salePrice = obj.price
                        for (let index = 0; index < myArray.length; index++) {
                            tempTot = tempTot + myArray[index].total - (myArray[index].total * this.state.totalDiscount / 100)

                        }
                    }

                    this.setState({
                        tempPrice: tempTot
                    })
                } else {
                    if (myArray[index].discountPercentage > 1 || myArray[index].discountAmount > 1) {
                        if (myArray[index].customFields.type == "value") {
                            Alert.alert("working")
                            var temp1 = 0;
                            var tot1 = 0;
                            var tott1 = 0;
                            included = true;
                            myArray[index].qty = myArray[index].qty + 1
                            tott1 = obj.price * myArray[index].qty
                            myArray[index].fullTotal = myArray[index].qty * obj.price
                            temp1 = temp1 + tott1
                            myArray[index].total = temp1 - myArray[index].discountAmount
                            // myArray[index].salePrice = myArray[index].price- myArray[index].discountAmount

                            var tot = 0;
                            for (let index = 0; index < myArray.length; index++) {
                                tot = tot + myArray[index].total;
                            }
                            this.state.tempPrice = tot

                        } else {
                            
                            included = true;
                            myArray[index].qty = myArray[index].qty + 1
                            let tot3 = obj.price * myArray[index].qty
                            myArray[index].fullTotal = myArray[index].qty * obj.price
                            let finalAmount =tot3 - (tot3* myArray[index].discountPercentage / 100)
                            myArray[index].total = finalAmount
                            myArray[index].salePrice = myArray[index].price - (myArray[index].price* myArray[index].discountPercentage / 100)
                            // var tot = 0;
                            // for (let index = 0; index < myArray.length; index++) {
                            //     tot =  myArray[index].total;
                            // }
                            this.state.tempPrice = myArray[index].total;
                        }
                        console.log("cccccccccc"+JSON.stringify(myArray))

                    } else {
                        this.state.tempPrice = this.state.tempPrice + PriceY
                        console.log("no")
                        included = true;
                        myArray[index].qty = myArray[index].qty + 1
                        myArray[index].total = obj.price * myArray[index].qty
                        myArray[index].fullTotal = myArray[index].qty * obj.price
                        myArray[index].salePrice = myArray[index].price
                    }
                }




            }
        }

        this.setState({
            tempArray: myArray
        })

        if (!included) {
            // this.state.tempPrice = this.state.tempPrice + PriceY
            // this.state.tempArray.push(this.clone(obj));
            if (this.state.totalDiscount > 0) {
                if (this.state.switchValue == true) {
                    var TPrice = 0;
                    TPrice = this.state.tempPrice
                    TPrice = TPrice + PriceY
                    this.state.tempPrice = TPrice
                    this.state.tempArray.push(this.clone(obj));
                } else {
                    TPrice = PriceY + this.state.subTotal
                    this.state.tempPrice = TPrice - (TPrice * this.state.totalDiscount / 100)
                    this.state.tempArray.push(this.clone(obj));
                }


            } else {
                this.state.tempPrice = this.state.tempPrice + PriceY
                this.state.tempArray.push(this.clone(obj));

            }


        }



    }


    render() {

        const { showAlert, showAlert1 } = this.state;

        return (
            <View style={styles.container}>
                <ImageBackground
                    source={require("../../assets/newBack2.png")}
                    style={{ width: "100%", height: "100%" }}>
                    <Header
                        style={styles.headerStyle}>
                        <Left>
                            <TouchableOpacity onPress={() => this.openDrawer()} >
                                <Image source={require("../../assets/whitelogo.png")} style={styles.ToucherbleIconStyle2} />
                            </TouchableOpacity>
                        </Left>
                        <View style={styles.header}>
                            {this.state.quick &&
                                <TouchableOpacity onPress={() => this.openQuickBillingModel()}>
                                    <View style={{ width: 80, height: 40, backgroundColor: '#168EB4', borderRadius: 20, justifyContent: 'center', alignContent: 'center', alignItems: 'center', right: 20 }}>
                                        <View style={{ top: 10, position: "absolute", }}>
                                            <Text style={{ fontWeight: 'bold', color: 'white', textAlign: "center" }}>Qiuck</Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            }
                        </View>
                        <Right>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('notification')}>
                                <Text style={{ justifyContent: 'center', alignItems: 'center', color: '#ffff', fontSize: this.state.tempPrice.toString().length >= 3 ? 18 : this.state.tempPrice.toString().length >= 5 ? 13 : 23 }}>Total : {this.financial(this.state.tempPrice)} </Text>
                            </TouchableOpacity>
                        </Right>
                    </Header>
                    <StatusBar barStyle="light-content" hidden={false} backgroundColor={Colors.PRIMARY_COLOR} />
                    <AlertPro
                        ref={ref => {
                            this.AlertPro = ref;
                        }}
                        onConfirm={() => this.AlertPro.close()}
                        title="Delete confirmation"
                        message="Are you sure to delete the entry?"
                        textCancel="Cancel"
                        textConfirm="Delete"
                        customStyles={{
                            mask: {
                                backgroundColor: "transparent"
                            },
                            container: {
                                borderWidth: 1,
                                borderColor: "#9900cc",
                                shadowColor: "#000000",
                                shadowOpacity: 0.1,
                                shadowRadius: 10
                            },
                            buttonCancel: {
                                backgroundColor: "#4da6ff"
                            },
                            buttonConfirm: {
                                backgroundColor: "#ffa31a"
                            }
                        }}
                    />
                    <Modal
                        style={{ backgroundColor: 'white' }}
                        ref={"sideMenu"}
                        swipeToClose={false}
                        onClosed={this.onClose}
                        onOpened={this.onOpen}
                        onClosingState={true}
                        position={"top"}
                        backdropPressToClose={() => {
                            this.closeDrawer();
                        }}
                        
                        >
                        <View style={{
                            width: this.props.width || screenWidth,
                            height: this.props.height || screenHeight,
                        }}>
                            <View style={{ height: hp('30%'), width: '100%', borderBottomLeftRadius: 50, borderBottomRightRadius: 50, backgroundColor: '#126F85', }}>
                                <View style={{ top: 30, justifyContent: "center", alignContent: "center", alignItems: "center", }}>
                                    <Image style={{ width: 170, height: 60, }} source={require('../../assets/whitelogo.png')}></Image>
                                </View>
                                <View style={{ marginTop: hp('10%') }}>
                                    <TouchableOpacity style={{ justifyContent: "center", alignContent: "center", alignSelf: "center", marginTop: hp('2%'), position: "absolute", left: 30 }} onPress={() => this.props.navigation.navigate('CompanyProfile')} activeOpacity={0.9}>
                                        <View style={{ width: wp('84%'), height: hp('20%'), borderRadius: 42, backgroundColor: '#fff' }}>
                                            <View style={{ top: hp('2%') }}>
                                                <Image source={this.state.uri == "" ? require('../../assets/nnn.jpg') : this.state.uri !== "" ? { uri: this.state.uri } : { uri: this.state.uri }} style={{ height: 72, width: 72, borderRadius: 50, bottom: 10, marginLeft: 4 }}></Image>
                                                <View style={{ position: "absolute", bottom: 40, marginLeft: hp('11%') }}>
                                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('CompanyProfile')} >
                                                        <Text numberOfLines={1} style={{ fontWeight: 'bold', fontSize: 18, color: '#34495E', top: 12, left: '22%' }}>{this.state.companyName}</Text>
                                                        <Text numberOfLines={1} style={{ color: '#34495E', textAlign: 'left', top: 12, left: '22%' }}>{this.state.userName}</Text>
                                                        <View style={{ borderRadius: 10, left: 190, position: "absolute", top: 18 }}>
                                                            <Image source={require('../../assets/set.png')} style={{ width: 30, height: 30, justifyContent: "center" }}></Image>
                                                        </View>

                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableOpacity>

                                </View>

                            </View>

                            {/* /logout button and close button */}

                            <View style={{ position: "absolute" }}>
                                <View style={{ left: 10, top: 2 }}>
                                    <TouchableOpacity onPress={() => this.closeDrawer()}>
                                        <View style={{ width: 35, height: 35, borderRadius: 30, backgroundColor: '#126F85', elevation: 10 }}>
                                            <Image style={{ width: 25, height: 25, top: 6, left: 5 }} source={require('../../assets/delete1.png')}></Image>
                                        </View>
                                    </TouchableOpacity>

                                </View>

                                {/* <View style={{ position: "absolute", top: 2 }}>

                                <TouchableOpacity onPress={() => this.logOut()} >
                                    <View style={{ width: 35, height: 35, borderRadius: 30, backgroundColor: '#126F85', elevation: 10 }}>
                                        <Image style={{ width: 25, height: 25, top: 5, left: 5, rotation: 180, elevation: 10 }} source={require('../../assets/logout3.png')}></Image>
                                    </View>
                                </TouchableOpacity>
                            </View> */}

                            </View>

                            <View style={{ position: "absolute", right: 10 }}>
                                <View style={{ top: 2 }}>
                                    <TouchableOpacity onPress={() => this.showAlert()}>
                                        <View style={{ width: 35, height: 35, borderRadius: 30, backgroundColor: '#126F85', elevation: 10 }}>
                                            <Image style={{ width: 25, height: 25, top: 6, left: 5 }} source={require('../../assets/logout3.png')}></Image>
                                        </View>
                                    </TouchableOpacity>

                                </View>



                                {/* <View style={{ position: "absolute", top: 2 }}>

                                <TouchableOpacity onPress={() => this.logOut()} >
                                    <View style={{ width: 35, height: 35, borderRadius: 30, backgroundColor: '#126F85', elevation: 10 }}>
                                        <Image style={{ width: 25, height: 25, top: 5, left: 5, rotation: 180, elevation: 10 }} source={require('../../assets/logout3.png')}></Image>
                                    </View>
                                </TouchableOpacity>
                            </View> */}

                            </View>



                            {/* <View style={{justifyContent:"center",alignContent:"center",alignItems:"center"}}> */}
                            <FlatGrid
                                itemDimension={130}
                                style={styles.gridViewModal}
                                refreshing={this.state.isFetching}
                                items={this.state.items}

                                renderItem={({ item, index }) => (

                                    <TouchableOpacity activeOpacity={0.8} onPress={() => this.navigateToScreen(item.Label)} nPress={() => {
                                        this.Alert();
                                    }}>
                                        <View style={[styles.itemContent, { backgroundColor: 'white', borderColor: '#E5E4FD', borderWidth: 1.1 }]}>
                                            <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
                                                <Image source={item.url} style={{ width: 70, height: 70 }} ></Image>
                                                <Text style={{ fontSize: hp('1.7%'), top: 5, fontWeight: 'bold' }}>  {item.name}</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                )}
                            />
                            {/* </View> */}
                        </View >
                        <AwesomeAlert
                            show={showAlert}
                            showProgress={false}
                            title="Confirm"
                            message="       Are you sure you want to do this ?      "
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={true}
                            showConfirmButton={true}
                            cancelText="    No    "
                            confirmText="   Yes   "
                            confirmButtonColor="#126F85"
                            onCancelPressed={() => {
                                this.hideAlert();
                            }}
                            onConfirmPressed={() => {
                                this.logOut();
                            }}
                        />



                        <AwesomeAlert
                            show={showAlert1}
                            showProgress={false}
                            title="Confirm"
                            message="       Are you sure you want to exit  ?      "
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={true}
                            showConfirmButton={true}
                            cancelText="    No    "
                            confirmText="   Yes   "
                            confirmButtonColor="#126F85"
                            onCancelPressed={() => {
                                this.hideAlert1();
                            }}
                            onConfirmPressed={() => {
                             BackHandler.exitApp()
                            }}
                        />


                    </Modal>

                    <Modal style={[styles.longpressmodal, styles.longpressmodal3]} position={"center"} ref={"modal3"} isDisabled={this.state.isDisabled}>

                        <Text style={{ color: '#126F85', fontSize: 18, fontWeight: 'bold', top: 10, right: 5 }}>Select Batch Number</Text>
                        <Text style={{ color: '#126F85', fontSize: hp('2.0%'), top: 13, fontWeight: 'bold', right: 5 }}>{"BatchNo."}                {"Dis.Pre"}               {"Price"}</Text>

                        <FlatList
                            itemDimension={130}
                            data={this.state.batchDetails}
                            style={{
                                flex: 1,

                                marginBottom: 15,

                                marginTop: 25,
                            }}
                            renderItem={({ item, index }) => (
                                <ScrollView>
                                    <TouchableOpacity activeOpacity={0.8} onPress={() => this.batchPriceAddToCart(item)}>
                                        <View style={[styles.itemContainermodal, { backgroundColor: 'white', borderColor: '#E5E4FD', borderWidth: 0.9, marginLeft:10,marginRight:10,}]}>
                                            <View style={{ flexDirection:'row' }}>
                                                <Text style={{ color: '#74b5c4', fontSize: hp('2.0%'), top: 2, fontWeight: 'bold',position:"absolute",left:0}}>{item.batchNumber}</Text>
                                                <Text style={{ color: '#74b5c4', fontSize: hp('2.0%'), top: 2, fontWeight: 'bold',left:140}}>{item.saleDiscountPercentage}</Text>
                                                <Text style={{ color: '#74b5c4', fontSize: hp('2.0%'), top: 2, fontWeight: 'bold',position:"absolute",right:0,}}>{item.salePrice}</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                </ScrollView>
                            )}

                        />


                    </Modal>

                    <Modal style={[styles.modal4]} position={"bottom"} ref={"modal4"} onClosed={this.onClose} backdropPressToClose={false}>
                        <TouchableOpacity style={{ top: -20, justifyContent: "center", alignContent: "center", alignItems: "center" }} onPress={() => this.closeOneItemModel6()}>
                            <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: '#ffff', borderRadius: 100, }}>
                                <Image style={{ width: 40, height: 40 }} source={require('../../assets/closemodl.png')} />
                            </View>
                        </TouchableOpacity>
                        <View style={{
                            flex: 1,
                            justifyContent: 'flex-end',
                            padding: PixelRatio.getPixelSizeForLayoutSize(12),
                        }}>
                            <Text style={{ color: Colors.PRIMARY_COLOR, fontSize: 20, textAlign: 'left', fontWeight: 'bold', top: -10 }}>Add Item</Text>
                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../assets/description.png')}
                                />
                                <Input
                                    style={styles.input}
                                    placeholder="Name"
                                    placeholderTextColor="#ffff"
                                    autoCapitalize="none"
                                    onChangeText={description => this.setState({ description })}
                                />
                            </Item>

                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../assets/price.png')}
                                />
                                <Input
                                    keyboardType={"numeric"}
                                    style={styles.input}
                                    placeholder="Price"
                                    placeholderTextColor="#ffff"
                                    autoCapitalize="none"
                                    onChangeText={priceT => this.setState({ priceT })}
                                />
                            </Item>
                            <TouchableOpacity
                                style={[styles.buttonContainerProcess1, styles.ProcessButton]}
                                onPress={() => this.validation()}
                            >
                                <Text style={styles.ProcessText}>Save</Text>
                            </TouchableOpacity>

                        </View>
                    </Modal>

                    <Modal style={[styles.quickBilling]} position={"bottom"} ref={"quickBilling"} onClosed={this.onClose} backdropPressToClose={false}>
                        <TouchableOpacity style={{ top: -20, justifyContent: "center", alignContent: "center", alignItems: "center" }} onPress={() => this.closequickmodal()}>
                            <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: '#ffff', borderRadius: 100, }}>
                                <Image style={{ width: 40, height: 40 }} source={require('../../assets/closemodl.png')} />
                            </View>
                        </TouchableOpacity>
                        <View style={{
                            flex: 1,
                            // justifyContent: 'flex-end',
                            padding: PixelRatio.getPixelSizeForLayoutSize(12),
                        }}>
                            <View style={{ bottom: 30 }}>

                                {/* <Text style={{ fontWeight: 'bold', fontSize: 20 }}>Printer</Text> */}


                                <View style={{ flexDirection: 'row', bottom: 20 }}>
                                    <Text style={{ color: Colors.PRIMARY_COLOR, fontSize: 20, textAlign: 'left', fontWeight: 'bold', top: -10 }}>Quick Billing</Text>
                                    {/* <View style={{ height: 20, width: 20, borderRadius: 100, backgroundColor: 'red', left: 10,top:-5}}></View> */}
                                </View>
                                <Form style={styles.form}>
                                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>

                                        {/* <Text style={{ fontSize: 20 }}>Received:</Text> */}
                                        <Label></Label>
                                    </View>
                                    <Item style={styles.TextItemStyle}>
                                        <Input style={styles.InputStyle}
                                            placeholder={"Rs:" + this.financial(this.state.tempPrice)}
                                            textAlign={'center'}
                                            keyboardType={"numeric"}
                                            onChangeText={text => this.calculateTotal(text)}
                                        />
                                    </Item>
                                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 20 }}>
                                        <Label></Label>
                                        <Text style={{ fontSize: 20, color: this.state.textColor }}>Balance : Rs.{this.state.balance}</Text>
                                    </View>
                                </Form>
                            </View>
                            <View style={{ top: 90 }}>
                                <TouchableOpacity onPress={() => this.QuickprintRecept()} >
                                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>

                                        <View style={{ borderColor: Colors.PRIMARY_COLOR, borderWidth: 2, width: wp('70%'), height: hp('9%'), borderRadius: 30 }}>
                                            <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 10 }}>
                                                <Text style={styles.NotSelectedText2}>Print Receipt & Charge</Text>
                                                <Text style={{ fontSize: 10, fontWeight: 'bold', color: this.state.bluetoothConnected == 0 ? '#fc7b03' : this.state.bluetoothConnected == 1 ? Colors.PRIMARY_COLOR : Colors.PRIMARY_COLOR }}>{this.state.bluetoothConnected == 0 ? 'Printer Disconnected' : this.state.bluetoothConnected == 1 ? 'Connected' : 'Connected'}</Text>
                                            </View>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>

                        </View>
                        <TouchableOpacity
                            activeOpacity={0.9}
                            style={{
                                height: 70,
                                backgroundColor: this.state.backgroundColor2
                            }}
                            onPress={() => this.navigateToDone()}

                        >
                            <View style={{ justifyContent: 'center', alignItems: 'center', top: 20 }}>
                                <Text style={styles.loginText}>Charge  Rs.{this.state.tempPrice}</Text>
                            </View>
                        </TouchableOpacity>
                    </Modal>

                    <View ref="view3">
                        <View style={styles.inputTextContainer2}>
                            <TouchableOpacity
                                style={[styles.buttonContainerProcess, styles.ProcessButton]}
                                onPress={() => this.navigateToCart()}
                                disabled={false}
                            >
                                <View style={styles.notificationViewStyle}><Text style={styles.notificationTextStyle}>{this.state.count}</Text></View>
                                <Text style={styles.ProcessText}>Cart</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.viewStyle}>
                        <View style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            flexDirection: 'row',
                        }}>
                            {this.state.search &&

                                <Item
                                    style={styles.item}
                                    rounded
                                    last
                                >
                                    <Image
                                        style={styles.place}
                                        source={require('../../assets/search.png')}
                                    />

                                    <Input
                                        style={styles.input}
                                        placeholder="Search"
                                        placeholderTextColor="#ffffff80"
                                        autoCapitalize="none"
                                        onChangeText={text => this.search(text)}
                                    />

                                    <TouchableOpacity onPress={() => this.visibleDropDown()}>
                                        <View style={{ backgroundColor: Colors.FLAT_TEXT_COLOR, width: 50, height: 50, borderBottomRightRadius: 20, borderTopRightRadius: 20 }}>
                                            <View style={{ justifyContent: 'center', alignItems: 'center', top: 10 }}>
                                                <Image
                                                    style={styles.place4}
                                                    source={require('../../assets/down3.png')}
                                                />
                                            </View>
                                        </View>
                                    </TouchableOpacity>

                                </Item>

                            }

                            {this.state.categoryDrop &&
                                <Item
                                    style={styles.item}
                                    rounded
                                    last
                                >
                                    <Picker
                                        style={{ width: 200, color: '#ffff', left: 10, }}
                                        selectedValue={this.state.catname}

                                        onValueChange={this.onPickerValueChange.bind(this)}
                                    >
                                        <Picker.Item label="Category" value="" />
                                        {this.loadCategoryTypes()}

                                    </Picker>
                                    <TouchableOpacity onPress={() => this.InvisibleDropDown()}>
                                        <View style={{ backgroundColor: Colors.FLAT_TEXT_COLOR, width: 50, height: 50, borderBottomRightRadius: 20, borderTopRightRadius: 20 }}>
                                            <View style={{ justifyContent: 'center', alignItems: 'center', top: 10 }}>
                                                <Image
                                                    style={styles.place4}
                                                    source={require('../../assets/search.png')}
                                                />
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                </Item>
                            }

                        </View>

                    </View>


                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Spinner
                            isVisible={this.state.spinner}
                            type={'ThreeBounce'}
                            textStyle={{ color: 'white' }}
                            size={70}
                            color={Colors.FLAT_TEXT_COLOR}
                        />

                    </View>
                    <Spinner2
                        visible={this.state.spinner2}
                        textContent={'Loading...'}
                        textStyle={{ color: 'white' }}
                        overlayColor={"#0f0e0e90"}
                        animation={"fade"}
                        color={"#00CEFD"}
                    />
                    {this.nodataImage()}
                    <FlatGrid
                        itemDimension={130}
                        items={this.state.arrayholder}
                        onEndReached={this.onScrollHandler}
                        style={styles.gridView}
                        renderItem={({ item, index }) => (
                            <TouchableOpacity activeOpacity={0.8} style={styles.touchableOpacityStyle} onPress={() => this.countQty(item)} onLongPress={() => this.showBatchListModel(item)}>
                                <View style={[styles.itemContainer, { backgroundColor: Colors.BACKGROUD_WHITE }]}>

                                    <Text style={styles.itemName3}>LKR.<Text style={styles.itemName2}>{item.price}</Text></Text>

                                    <Image
                                        name={item.name}
                                        style={styles.iconlImage}
                                        color={item.color}
                                        source={{ uri: item.imageUrl }}
                                    />

                                    <Text numberOfLines={1} style={styles.itemName}>{item.name}</Text>

                                </View>
                            </TouchableOpacity>

                        )}

                    />
                    <AwesomeAlert
                        show={showAlert1}
                        showProgress={false}
                        title="Confirm"
                        message="  Are you sure you want to exit ?      "
                        closeOnTouchOutside={false}
                        closeOnHardwareBackPress={false}
                        showCancelButton={true}
                        showConfirmButton={true}
                        cancelText="    No    "
                        confirmText="   Yes   "
                        confirmButtonColor="#126F85"
                        onCancelPressed={() => {
                            this.hideAlert1();
                        }}
                        onConfirmPressed={() => {
                            BackHandler.exitApp()
                        }}
                    />

                    {this.state.showTheThing &&
                        <View>
                            {this.state.temporaryItem &&
                                <Fab
                                    active={this.state.active}
                                    direction="up"
                                    containerStyle={{}}
                                    style={styles.temporaryFab}
                                    position="bottomRight"
                                    onPress={() => this.addTemporaryItem()}>
                                    <Image source={require("../../assets/circle2.png")} style={styles.ToucherbleIconStyle3} />
                                </Fab>
                            }
                            <Fab
                                active={this.state.active}
                                direction="up"
                                containerStyle={{}}
                                style={styles.backGround}
                                position="bottomRight"
                                onPress={() => this.showDefault()}
                                onLongPress={() => this.navigateToAddItemScreen()}>
                                <Image source={require("../../assets/plus.png")} style={styles.ToucherbleIconStyle3} />
                            </Fab>
                        </View>
                    }
                </ImageBackground>
            </View>

        );
    }
}
