import React, { Component } from 'react';
import { View, TextInput, Image, TouchableOpacity, ImageBackground, StatusBar, TouchableHighlight, AsyncStorage, PixelRatio, Alert,Dimensions } from 'react-native';
import {
    Item,
    Input,
    Text,
    Header,
    Left,
    Right,
    Fab,
    Label
} from 'native-base';
import styles from './CustomerScreenStyle';
import Colors from '../../resources/Colors';
import Strings from '../../resources/Strings';
import { FlatGrid } from 'react-native-super-grid';
import Spinner from "react-native-spinkit";
import Modal from 'react-native-modalbox';
import NetInfo from "@react-native-community/netinfo";
import Messages from '../../resources/Message';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

export default class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {

            customBackgroundDialog: false,
            defaultAnimationDialog: false,
            scaleAnimationDialog: false,
            scaleAnimationDialog2: false,
            slideAnimationDialog: false,
            name: "",
            data: [],
            spinner: false,
            searchText: "",
            nodata: false,
            email: '',
            nic: '',
            contact: '',
            address: '',
            showTheThing: true,
            removeCustomer: false,
            customerId: "",
            moreFields: false,
            moreButton: true
        };

    }


    componentDidMount() {
        this.state.customerId = this.props.navigation.getParam('customerId', '')
        if (this.state.customerId !== "") {
            this.setState({
                removeCustomer: true
            })
        }

        this.showData()
    }

    removeCustomer() {
        const { navigate } = this.props.navigation;

        navigate('PaymentScreen', {
            customerName: "",
            customerId: ""

        })
    }

    //getting auth code from AsyncStorage
    showData = async () => {
        let myArray = await AsyncStorage.getItem('myArray');
        let d = JSON.parse(myArray);


        this.setState({
            token: d

        })
        console.log(this.state.token);
        this.getAllCustomers()

    }

    //asign search word
    search(text) {
        this.state.searchText = text;
        this.getAllCustomers()
    }

    //when close model plus button visible
    onClose = () => {
        this.setState({
            showTheThing: true
        })
    }

    //fetching customers from api
    getAllCustomers() {
        this.setState({
            spinner: true,
        });
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + "/api/customer?page=0&limit=100&text=" + this.state.searchText,
                    {
                        method: "GET",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },

                    }
                )
                    .then(resp => resp.json())
                    .then(responseJson => {
                        console.log("customers" + JSON.stringify(responseJson.content));
                        this.setState({
                            spinner: false,
                        });
                        this.setState({
                            data: responseJson.content
                        })

                        if (responseJson.content.length == 0) {
                            this.setState({
                                nodata: true
                            })
                        } else {
                            this.setState({
                                nodata: false
                            })
                        }

                    })

                    .catch(error => {
                        console.log(error);

                        this.setState({
                            spinner: false,
                        });

                    });

            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.getAllCustomers()

                            }
                        },
                    ],
                    { cancelable: true }
                );


            }
        });

    }



    //customer add form show & plus buton invisible
    anima() {
        this.setState({
            showTheThing: false
        })
        this.refs.modal2.open()
    }



    static navigationOptions = {
        title: 'CustomerScreen',
    };


    //navigate to payment screen (with customer name & id)
    navigateToPaymentScreen(item) {
        const { navigate } = this.props.navigation;

        navigate('PaymentScreen', {
            customerName: item.name,
            customerId: item.id

        })

    }

    //no data image
    nodataImage() {
        if (this.state.nodata) {
            return (
                <View style={{ top: 60, justifyContent: 'center', alignItems: 'center', }}>
                    <Image style={{ width: 200, height: 200, borderRadius: 100 }}
                        source={require("../../assets/no_data_found.png")}
                    />

                </View>
            )
        }
    }

    //Email Validator
    emailvalidate(text) {
        const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(text) === false) {
            return false;
        } else {
            return true;
        }
    }

    //Mobile Validate
    mobilevalidate(text) {
        const reg = /^(?:0|94|\+94)?(?:(11|21|23|24|25|26|27|31|32|33|34|35|36|37|38|41|45|47|51|52|54|55|57|63|65|66|67|81|912)(0|2|3|4|5|7|9)|7(0|1|2|5|6|7|8)\d)\d{6}$/;
        if (reg.test(text) === false) {
            return false;
        } else {
            return true;
        }
    }

    //NIC validator
    nicvalidatate(text) {
        const reg = /^([0-9]{9}[x|X|v|V]|[0-9]{12})$/
        if (reg.test(text) === false) {
            return false;
        } else {
            return true;
        }
    }

    namevalidate(text) {
        const reg = /^[a-zA-Z ]{2,30}$/;
        if (reg.test(text) === false) {
            return false;
        } else {
            return true;
        }
    }



    Validation() {

        if (this.state.moreFields == false) {
            if (this.namevalidate(this.state.name) === false) {
                Messages.messageName(Strings.WARNING_FAILED, Strings.CUSTOMER_NAME, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);

            } else if (this.mobilevalidate(this.state.contact) === false) {
                Messages.messageName(Strings.WARNING_FAILED, Strings.CUSTOMER_CONTACT, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
            } else {
                this.saveCustomer()
            }
        } else {
            if (this.state.moreFields == true) {
                if (this.namevalidate(this.state.name) === false) {
                    Messages.messageName(Strings.WARNING_FAILED, Strings.CUSTOMER_NAME, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);

                } else if (this.mobilevalidate(this.state.contact) === false) {
                    Messages.messageName(Strings.WARNING_FAILED, Strings.CUSTOMER_CONTACT, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);

                } else if (this.nicvalidatate(this.state.nic) === false) {
                    Messages.messageName(Strings.WARNING_FAILED, Strings.CUSTOMER_NIC, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);

                } else if (this.emailvalidate(this.state.email) === false) {
                    Messages.messageName(Strings.WARNING_FAILED, Strings.CUSTOMER_EMAIL, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);

                } else if (this.state.address == "") {
                    Messages.messageName(Strings.WARNING_FAILED, Strings.CUSTOMER_ADDRESS, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);

                } else {
                    this.saveCustomer()
                }
            }

        }

    }
    //add new customer
    saveCustomer() {
        this.setState({
            spinner: true,

        });
        var data = JSON.stringify({

            "address": this.state.address,
            "birthDate": {
                "basic": "1990-5-5"

            },
            "contactNumber": this.state.contact,
            "creditAmountLimit": 0,
            "creditDayLimit": 0,
            "description": "string",
            "email": this.state.email,
            "name": this.state.name,
            "nicNumber": this.state.nic,
            "totalCreditAmount": 0

        });

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + '/api/customer',
                    {
                        method: "POST",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },
                        body: data
                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        this.getAllCustomers()
                        this.refs.modal2.close()
                        Messages.messageName(Strings.WARNING_SUCESS, Strings.CUSTOMER_ADDED_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                        console.log(JSON.stringify(responseJson));

                        this.setState({
                            scaleAnimationDialog: false,
                            spinner: false,
                            name: "",
                            contact: "",
                            address: "",
                            email: "",
                            nic: ""
                        });
                    })


                    .catch(error => {
                        console.log(error);
                        this.setState({
                            spinner: false,
                        });
                        Messages.messageName(Strings.WARNING_SUCESS, Strings.CUSTOMER_ADDED_Fail, Strings.ICON[2], Strings.TYPE[2], Colors.WARNING_ALERT);


                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.saveCustomer()

                            }
                        },
                    ],
                    { cancelable: true }
                );


            }
        });

    }


    visibleField() {
        this.setState({
            moreFields: true,
            moreButton: false
        })
    }

    visibleFalseField() {
        this.setState({
            moreFields: false,
            moreButton: true
        })
    }

    closeModel2() {
        this.refs.modal2.close()
    }

    render() {
        return (
            <ImageBackground
                source={require("../../assets/newBack2.png")}
                style={{ width: "100%", height: "100%" }}>

                <View style={{ backgroundColor: '#126F85', width: '100%', height: 60, borderBottomLeftRadius: 50 }}>
                    <View style={{ backgroundColor: '#126F85', width: 50, top: 13, borderRadius: 60, left: 25, position: "absolute", top: 8 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('PaymentScreen')}>
                            <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                    <Image source={require("../../assets/newBack3.png")} style={{ width: 20, height: 20 }}></Image>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Text style={{ fontFamily: 'Ubuntu-Medium', color: '#ffff', justifyContent: "center", textAlign: "center", fontSize: 20, position: "absolute", top: 10, left: 80, fontWeight: "bold" }}>Select Customer</Text>
                    {
                        this.state.removeCustomer &&

                        <View style={{ backgroundColor: '#126F85', width: 70, top: 15, borderRadius: 6, right: 30, position: "absolute", top: 8 }}>
                            <TouchableOpacity onPress={() => this.removeCustomer()} activeOpacity={0.5}>
                                <View style={{ width: 87, height: 34, borderRadius: 6, backgroundColor: '#126F85', elevation: 10, borderColor: '#ffff', borderWidth: 1 }}>
                                    <View style={{ justifyContent: 'flex-end', top: 5 }} numberOfLines={1}>
                                        <Image source={require("../../assets/user.png")} style={{ width: 20, height: 20, left: 2 }}></Image>

                                        <View style={{ width: 57, position: "absolute", bottom: 5, left: 22 }}>
                                            <Text numberOfLines={1} style={{ fontSize: 9, color: '#ffff', textAlign: "center", }}>Remove</Text>
                                        </View>

                                        <StatusBar barStyle="light-content" hidden={false} backgroundColor={'#126F85'} />

                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                    }
                </View>
                <StatusBar barStyle="light-content" hidden={false} backgroundColor={'#126F85'} />


                <View style={styles.viewStyle}>
                    {/* <TextInput
                        style={styles.textInputStyle}
                        onChangeText={text => this.search(text)}
                        value={this.state.text}
                        underlineColorAndroid="transparent"
                        placeholder="Search Here"
                        placeholderTextColor='white'
                    /> */}
                    <Item
                        style={{
                            marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
                            backgroundColor: Colors.PRIMARY_COLOR,
                            borderColor: Colors.PRIMARY_COLOR,
                        }}
                        rounded
                        last
                    >
                        <Image
                            style={{
                                width: 25,
                                height: 25,
                                left: 10
                            }}
                            source={require('../../assets/search.png')}
                        />

                        <Input
                            style={{
                                color: 'white',
                                left: 10,
                                maxWidth: '82%',
                            }}
                            placeholder="Search"
                            placeholderTextColor="#ffffff80"
                            autoCapitalize="none"
                            onChangeText={text => this.search(text)}
                        />


                    </Item>

                </View>


                {/* Add Customer Modal */}




                <Modal style={[styles.modal4]} position={"bottom"} ref={"modal2"} onClosed={this.onClose} backdropPressToClose={false}>
                    <TouchableOpacity style={{ top: -20, justifyContent: "center", alignContent: "center", alignItems: "center" }} onPress={() => this.closeModel2()}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: '#ffff', borderRadius: 100, }}>
                            <Image style={{ width: 40, height: 40 }} source={require('../../assets/closemodl.png')} />
                        </View>
                    </TouchableOpacity>
                    <View style={{
                        flex: 1,
                        // justifyContent: 'flex-end',
                        padding: PixelRatio.getPixelSizeForLayoutSize(12),
                    }}>
                        {/* <View style={{ position: "absolute", width: 50, right: 150, }}>
                            <TouchableOpacity onPress={() => this.closeModel2()}>
                                <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: '#ffff', borderRadius: 100, }}>
                                    <Image style={{ width: 40, height: 40 }} source={require('../../assets/closemodl.png')} />
                                </View>
                            </TouchableOpacity>
                        </View> */}




                        <View style={{ bottom: 50 }}>
                            <Text style={{ color: Colors.PRIMARY_COLOR, fontSize: 20, textAlign: 'left', fontWeight: 'bold', top: -10 }}>Add Customer</Text>
                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../assets/description.png')}
                                />
                                <Input
                                    style={styles.input}
                                    placeholder="Name"
                                    placeholderTextColor="#ffffff80"
                                    autoCapitalize="none"
                                    value={this.state.name}
                                    onChangeText={name => this.setState({ name })}
                                />
                            </Item>
                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../assets/price.png')}
                                />
                                <Input
                                    style={styles.input}
                                    keyboardType="numeric"
                                    maxLength={10}
                                    placeholder="Contact"
                                    placeholderTextColor="#ffffff80"
                                    autoCapitalize="none"
                                    value={this.state.contact}
                                    onChangeText={contact => this.setState({ contact })}
                                />
                            </Item>

                            {this.state.moreButton &&
                                <View>
                                    <Label></Label>
                                    <TouchableOpacity onPress={() => this.visibleField()}>
                                        <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
                                            <Text style={{ fontWeight: 'bold', color: Colors.PRIMARY_COLOR }}>____________More Details____________</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <Label></Label>
                                </View>
                            }

                            {this.state.moreFields &&
                                <View>
                                    <Item
                                        style={styles.item}
                                        rounded
                                        last
                                    >
                                        <Image
                                            style={styles.place}
                                            source={require('../../assets/price.png')}
                                        />
                                        <Input
                                            style={styles.input} 
                                            placeholder="Nic"
                                            placeholderTextColor="#ffffff80"
                                            autoCapitalize="none"
                                            value={this.state.nic}
                                            onChangeText={nic => this.setState({ nic })}
                                        />
                                    </Item>
                                    <Item
                                        style={styles.item}
                                        rounded
                                        last
                                    >
                                        <Image
                                            style={styles.place}
                                            source={require('../../assets/price.png')}
                                        />
                                        <Input
                                            style={styles.input}
                                            placeholder="Email"
                                            placeholderTextColor="#ffffff80"
                                            autoCapitalize="none"
                                            value={this.state.email}
                                            onChangeText={email => this.setState({ email })}
                                        />
                                    </Item>
                                    <Item
                                        style={styles.item}
                                        rounded
                                        last
                                    >
                                        <Image
                                            style={styles.place}
                                            source={require('../../assets/address.png')}
                                        />
                                        <Input
                                            style={styles.input}
                                            placeholder="Address"
                                            multiline={true}
                                            placeholderTextColor="#ffffff80"
                                            autoCapitalize="none"
                                            value={this.state.address}
                                            onChangeText={address => this.setState({ address })}
                                        />
                                    </Item>
                                    <View>
                                        {/* <Label></Label> */}
                                        <TouchableOpacity onPress={() => this.visibleFalseField()}>
                                            <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
                                                <Text style={{ fontWeight: 'bold', color: Colors.PRIMARY_COLOR }}>____________Close____________</Text>
                                            </View>
                                        </TouchableOpacity>
                                        {/* <Label></Label> */}
                                    </View>
                                </View>

                            }

                        </View>
                        <View style={styles.inputTextContainer2}>
                            <TouchableOpacity
                                style={[styles.buttonContainerProcess, styles.ProcessButton]}
                                onPress={() => this.Validation()}
                            >
                                <Text style={styles.ProcessText}>Save</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>












                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Spinner
                        isVisible={this.state.spinner}
                        type={'ThreeBounce'}
                        textStyle={{ color: 'white' }}
                        size={70}
                        color={Colors.FLAT_TEXT_COLOR}
                    />

                </View>
                {this.nodataImage()}
                <FlatGrid
                    itemDimension={130}
                    items={this.state.data}
                    style={styles.gridView}
                    renderItem={({ item, index }) => (
                        <TouchableOpacity activeOpacity={0.8} onPress={() => this.navigateToPaymentScreen(item)}>
                            <View style={[styles.itemContainer, { backgroundColor: Colors.BACKGROUD_WHITE }]}>
                                <Text numberOfLines={1} style={styles.itemName}>{item.name}</Text>
                                <Text style={styles.itemName2}>{item.contactNumber}</Text>

                            </View>
                        </TouchableOpacity>
                    )}
                />

                {this.state.showTheThing &&
                    <Fab
                        active={this.state.active}
                        direction="up"
                        containerStyle={{}}
                        style={styles.backGround}
                        position="bottomRight"
                        onPress={() => this.anima()}>
                        <Image source={require("../../assets/plus.png")} style={styles.ToucherbleIconStyle2} />
                    </Fab>
                }
            </ImageBackground>

        );
    }
}



