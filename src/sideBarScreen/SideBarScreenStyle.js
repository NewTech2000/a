import React, { StyleSheet } from 'react-native';
import Colors from '../../resources/Colors';

export default StyleSheet.create({

    ImageStyle2: {

        height: '80%',
        width: '70%',
        top: 30,
        left: 35
    },
    separaterStyle: {
        height: 1
    },
    txtContact: {

        justifyContent: 'center',

    },

    MainContainer: {
 
        flex: 1,
        paddingTop: (Platform.OS) === 'ios' ? 20 : 0,
        alignItems: 'center',
        justifyContent: 'center',
     
      },
     
      sideMenuContainer: {
     
        width: '100%',
        height: '100%',
        backgroundColor: '#fff',
        alignItems: 'center',
        paddingTop: 20
      },
     
      sideMenuProfileIcon:
      {
        resizeMode: 'center',
        width: 150, 
        height: 150, 
        borderRadius: 150/2
      },
     
      sideMenuIcon:
      {
        resizeMode: 'center',
        width: 28, 
        height: 28, 
        marginRight: 10,
        marginLeft: 20
        
      },
     
      menuText:{
     
        fontSize: 15,
        color: '#222222',
        
      },

    txtCallCenter: {
        justifyContent: 'center',
    },
    txtHotline: {
        justifyContent: 'center',
    },
    buttonTextStyle2: {
        right: 80,
        fontWeight: 'bold',

    },
    buttonTextStyle23: {
        right: 90,
        fontWeight: 'bold',

    },
    buttonTextStyle3: {
        right: 120,
        fontWeight: 'bold',
    },
    buttonTextStyle1: {
        right: 135,
        fontWeight: 'bold',
    },
    backgroundImage: {
        width: '100%',
        height: '100%',


    },
    cardImageRight:{
        width:310,
        height:210
    },
    viewStyle: {
        height: 100,
        width: '100%'
    },
    ButtonStyle: {
        height: '50%',
        marginBottom: 10,
        width: '100%',
        borderRadius: 10,
        left: 20,
        backgroundColor: Colors.PRIMARY_COLOR,
    },
    ButtonStyle5: {
        height: '30%',
        marginBottom: 10,
        width: '100%',
        borderRadius: 10,
        left: 20,
        top: 150,
        backgroundColor: Colors.PRIMARY_COLOR,
    },
    secondView: {
        height: 100,
        width: '100%',
        top: 70
    },
    container:{
        top:-150
    },
    container1: {
       height:900
      },
    ThirdView: {
        height: 100,
        width: '100%'
    },
    forthView: {
        height: 100,
        width: '100%',
        justifyContent: 'center', alignItems: 'center'

    },
    View1: { justifyContent: 'center', alignItems: 'center', top: 100 },

    ContentStyle: {

        backgroundColor: 'white'
    },
    logoText: {
        color: Colors.PRIMARY_COLOR,
        justifyContent: 'center',
        fontWeight: 'bold',
        top: 30,
        textShadowColor: Colors.PRIMARY_COLOR,
        textShadowOffset: { width: -1, height: 1 },
        textShadowRadius: 10,
        fontSize:20

    },

    BackgroundImage: { width: "100%", height: "100%" }
});