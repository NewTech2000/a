import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  Alert,
  AsyncStorage
} from 'react-native';
import Colors from '../../resources/Colors';
import Messages from '../../resources/Message';
import Strings from '../../resources/Strings';
import { ScrollView } from 'react-native-gesture-handler';
import { Container, Header, Content, Button, Icon, Card, CardItem, Body, } from 'native-base';
import PropTypes from 'prop-types';
import { NavigationActions, StackActions } from 'react-navigation'


class SideBar extends Component {

  constructor(props) {
    super(props);
  }

  navigateToScreen(route) {
    if (route === 'customer') {
      this.props.navigation.navigate("ManageCustomerScreen");
    } else if (route === 'item') {
      this.props.navigation.navigate("ManageItemScreen");
    } else if (route === 'category') {
      this.props.navigation.navigate("CategoryScreen");

    } else if (route === 'invoice') {
      this.props.navigation.navigate("InvoiceScreen");

    } else if (route === 'home') {
      this.props.navigation.navigate("HomeScreen");
    }
    else if (route === 'Credit') {
      this.props.navigation.navigate("CreditBookScreen");
    }
    else if (route === 'settings') {
      this.props.navigation.navigate("SettingScreen");
    } else if (route === 'reports') {
      this.props.navigation.navigate("ReportsScreen");
    } else if (route === 'logout') {
      // Linking.openURL('https:///')

    }
  }

  logout() {

    Alert.alert(
      'Confirm',
      'Are you sure you want to do this ?',
      [
        { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        {
          text: 'Yes', onPress: () => {
            this.setState({
              slideAnimationDialog: false,
            });
            AsyncStorage.removeItem('myArray');
            const resetAction = StackActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({ routeName: 'LoginScreen' })],
            });

            this.props.navigation.dispatch(resetAction);

            Messages.messageName(Strings.WARNING_CONNECTION, Strings.LOG_OUT, Strings.ICON[1], Strings.TYPE[1]);


          }
        },
      ],
      { cancelable: true }
    );


  }

  navigatetoCompanyProfile() {
    this.props.navigation.navigate("CompanyProfile");
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={{ marginTop: 20 }}>
            <TouchableOpacity onPress={() => this.navigateToScreen('home')}>
              <View style={{ justifyContent: 'center', alignItems: 'center', }}>
                <Image source={require('../../assets/whitelogo.png')} style={styles.HeaderIconlImage2} />
              </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.navigatetoCompanyProfile()}>
              <View style={{ top: 30, backgroundColor: 'rgba(0,128,128,0.4)', height: 90 }}>
                <View style={{ width: 60, height: 60, borderRadius: 100, backgroundColor: '#CCCCCC', justifyContent: "center", alignContent: "center", alignItems: "center", borderColor: '#ffff', borderWidth: 3, left: 10, position: "absolute", top: 15 }}>
                </View>
                <Text style={{ color: '#ffff', textAlign: "center", justifyContent: "center", fontSize: 17, position: "absolute", left: 80, top: 25, fontWeight: "bold" }}>Company Name</Text>
                <Text style={{ color: '#ffff', textAlign: "center", justifyContent: "center", fontSize: 15, position: "absolute", left: 80, top: 45 }}>User Name</Text>
              </View>
              <View style={{ justifyContent: 'center', alignItems: 'center', position: "absolute", right: 15, top: 65 }}>
                <Image source={require('../../assets/setting.png')} style={{ width: 25, height: 25, }} />
              </View>
            </TouchableOpacity>


            <View style={{ marginTop: 50 }}>
              <Button iconLeft full bordered style={{ borderRadius: 100, borderColor: Colors.PRIMARY_COLOR, backgroundColor: Colors.PRIMARY_COLOR, justifyContent: 'flex-start' }} onPress={() => this.navigateToScreen('customer')}>
                <Image source={require("../../assets/dCustomer.png")} style={{ height: 20, width: 20, left: 5 }} />
                <Text style={{ color: 'white', marginRight: 18, fontSize: 16, fontWeight: '500' }} >&nbsp;&nbsp;&nbsp;Manage Customer</Text>
              </Button>
              <Button iconLeft full bordered style={{ borderRadius: 100, borderColor: Colors.PRIMARY_COLOR, backgroundColor: Colors.PRIMARY_COLOR, justifyContent: 'flex-start' }} onPress={() => this.navigateToScreen('category')}>
                <Image source={require("../../assets/category.png")} style={{ height: 20, width: 20, left: 5 }} />
                <Text style={{ color: 'white', marginRight: 10, fontSize: 16, fontWeight: '500' }}>&nbsp;&nbsp;&nbsp; Manage Category</Text>
              </Button>
              <Button iconLeft full bordered style={{ borderRadius: 100, borderColor: Colors.PRIMARY_COLOR, backgroundColor: Colors.PRIMARY_COLOR, justifyContent: 'flex-start' }} onPress={() => this.navigateToScreen('item')}>
                <Image source={require("../../assets/dItem.png")} style={{ height: 20, width: 20, left: 5 }} />
                <Text style={{ color: 'white', marginRight: 10, fontSize: 16, fontWeight: '500' }}>&nbsp;&nbsp;&nbsp; Manage Item</Text>
              </Button>
              <Button iconLeft full bordered style={{ borderRadius: 100, borderColor: Colors.PRIMARY_COLOR, backgroundColor: Colors.PRIMARY_COLOR, justifyContent: 'flex-start' }} onPress={() => this.navigateToScreen('invoice')}>
                <Image source={require("../../assets/invoice.png")} style={{ height: 20, width: 20, left: 5 }} />
                <Text style={{ color: 'white', marginRight: 10, fontSize: 16, fontWeight: '500' }}>&nbsp;&nbsp;&nbsp; Invoice details</Text>
              </Button>
              <Button iconLeft full bordered style={{ borderRadius: 100, borderColor: Colors.PRIMARY_COLOR, backgroundColor: Colors.PRIMARY_COLOR, justifyContent: 'flex-start' }} onPress={() => this.navigateToScreen('Credit')}>
                <Image source={require("../../assets/credit.png")} style={{ height: 20, width: 20, left: 5 }} />
                <Text style={{ color: 'white', marginRight: 10, fontSize: 16, fontWeight: '500' }}>&nbsp;&nbsp;&nbsp; Credit Book</Text>
              </Button>
              <Button iconLeft full bordered style={{ borderRadius: 100, borderColor: Colors.PRIMARY_COLOR, backgroundColor: Colors.PRIMARY_COLOR, justifyContent: 'flex-start' }} onPress={() => this.navigateToScreen('reports')}>
                <Image source={require("../../assets/reports.png")} style={{ height: 20, width: 20, left: 5 }} />
                <Text style={{ color: 'white', marginRight: 10, fontSize: 16, fontWeight: '500' }}>&nbsp;&nbsp;&nbsp;Reports</Text>
              </Button>
              <Button iconLeft full bordered style={{ borderRadius: 100, borderColor: Colors.PRIMARY_COLOR, backgroundColor: Colors.PRIMARY_COLOR, justifyContent: 'flex-start' }} onPress={() => this.navigateToScreen('settings')}>
                <Image source={require("../../assets/setting.png")} style={{ height: 20, width: 20, left: 5 }} />
                <Text style={{ color: 'white', marginRight: 10, fontSize: 16, fontWeight: '500' }}>&nbsp;&nbsp;&nbsp;Settings</Text>
              </Button>
              <Button iconLeft full bordered style={{ borderRadius: 100, borderColor: Colors.PRIMARY_COLOR, backgroundColor: Colors.PRIMARY_COLOR, justifyContent: 'flex-start' }} onPress={() => this.navigateToScreen('about')}>
                <Image source={require("../../assets/about.png")} style={{ height: 20, width: 20, left: 5 }} />
                <Text style={{ color: 'white', marginRight: 10, fontSize: 16, fontWeight: '500' }}>&nbsp;&nbsp;&nbsp;About US</Text>
              </Button>
              <Button iconLeft full bordered style={{ borderRadius: 100, borderColor: Colors.PRIMARY_COLOR, backgroundColor: Colors.PRIMARY_COLOR, justifyContent: 'flex-start' }} onPress={() => this.logout()}>
                <Image source={require("../../assets/logout.png")} style={{ height: 20, width: 20, left: 5 }} />
                <Text style={{ color: 'white', marginRight: 10, fontSize: 16, fontWeight: '500' }}>&nbsp;&nbsp;&nbsp;Log-out</Text>
              </Button>
              {/* <Card transparent style={styles.cardStyle} >
                <CardItem style={{ backgroundColor: Colors.PRIMARY_COLOR }}>
                  <Body>
                    <Text style={styles.slideHeader}>
                      Contact Us
                  </Text>
                  </Body>
                </CardItem>

                <CardItem style={{ backgroundColor: Colors.PRIMARY_COLOR }}>
                  <Body style={styles.dataStyle} >
                    <Text style={styles.textStyle}>
                      Our Call Center :  +940774653543
                  </Text>

                    <Text style={styles.textStyle}>
                      Hotline : +940774653543
                  </Text>

                  </Body>
                </CardItem> */}

              {/* <CardItem style={{ backgroundColor: Colors.PRIMARY_COLOR }}>
                  <Body>
                    <Text style={styles.slideHeader2}>
                      Email
                  </Text>
                  </Body>
                </CardItem> */}

              {/* <CardItem style={{ backgroundColor: Colors.PRIMARY_COLOR }}>
                  <Body style={styles.dataStyle} >
                    <Text style={styles.textStyle}>
                      info@commercialtp.com
                  </Text>
                  </Body>
                </CardItem>

              </Card> */}



            </View>




          </View>
        </ScrollView>

        <View style={{ marginBottom: 10 }}>
          <Button iconLeft full bordered warning style={{ margin: 10, borderRadius: 100, borderColor: Colors.PRIMARY_COLOR, backgroundColor: Colors.PRIMARY_COLOR, justifyContent: 'flex-start' }} onPress={() => this.navigateToScreen('logout')}>
            <Image source={require("../../assets/cloud.png")} style={{ height: 20, width: 20, left: 10 }} />
            <Text style={{ color: 'white', left: 20, fontSize: 16, fontWeight: '500' }}>&nbsp;Visit BillaCloud.com</Text>
          </Button>


        </View>


      </View>
    );
  }
}

const styles = StyleSheet.create({

  container: {
    paddingTop: 6,
    paddingRight: 1,
    flex: 1,
    backgroundColor: Colors.PRIMARY_COLOR
  },

  cardStyle: {
    borderRadius: 20,
    backgroundColor: Colors.PRIMARY_COLOR


  },

  textStyle: {
    fontWeight: 'normal',
    fontSize: 15,
    color: Colors.BACKGROUD_WHITE,
    fontWeight: '200'


  },

  slideHeader: {

    flex: 1,
    flexDirection: 'column',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',

    marginLeft: 95,
    color: Colors.BACKGROUD_WHITE,
    fontWeight: 'bold',
    fontSize: 16

  },



  slideHeader2: {
    flex: 1,
    flexDirection: 'column',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',

    marginLeft: 110,
    color: Colors.BACKGROUD_WHITE,
    fontWeight: 'bold',
    fontSize: 16

  },

  dataStyle: {
    flex: 1,
    flexDirection: 'column',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    // marginLeft: 10
  },

  navItemStyle: {
    padding: 10
  },
  navSectionStyle: {
    backgroundColor: 'lightgrey'
  },
  sectionHeadingStyle: {
    paddingVertical: 10,
    paddingHorizontal: 5
  },
  footerContainer: {
    padding: 20,
    backgroundColor: 'lightgrey'
  },
  HeaderIconlImage2: {
    width: 120,
    height: 50,
    marginTop: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
})
SideBar.propTypes = {
  navigation: PropTypes.object
};


export default SideBar;