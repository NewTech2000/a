import React, { Component } from 'react';
import { View, Image, ImageBackground, StatusBar, Alert, TouchableOpacity } from 'react-native';
import {
    Button,
    Content,
    Form,
    Item,
    Input,
    Text,
    Footer
} from 'native-base';
import styles from './RegisterScreenStyle';
import Messages from '../../resources/Message';
import Strings from '../../resources/Strings';
import NetInfo from "@react-native-community/netinfo";
import CompressImage from "react-native-compress-image";
import ImagePicker from "react-native-image-picker";
import Spinner from 'react-native-loading-spinner-overlay';
import { ScrollView, TouchableHighlight } from "react-native-gesture-handler";
import Colors from '../../resources/Colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            address: "",
            domain: "",
            contact: "",
            description: "",
            username: "",
            spinner: false,
            password: "",
            businessTyp: '',
            background: '',
            passwordSecure: true,
            eyeOne: true,
            eyeTwo: false,
            namePlaceHolder: "Name",
            passwordPlaceHolder: "Password",
            userNamePlaceHolder: "Username",
            image: null,
            photo: null,
            url: "",
            photoUri: '',
            uri: "",
            companyDetails: true,
            branchDetails: false,
            userDetails: false,
            userDetailsButton: true,
            userDetailsCloseButton: false,
            branchDetailsButton: true,
            branchDetailsCloseButton: false
        };
        // this.state.businessTyp = this.props.navigation.state.params.name,
        //     this.state.background = this.props.navigation.state.params.background
    }

    //validate text fields
    validation() {
        if (this.state.name == "") {
            Messages.messageName(Strings.WARNING_SUCESS, Strings.NAME_EMPTY, Strings.ICON[0], Strings.TYPE[0]);
        } else if (this.state.address == "") {

        } else if (this.state.domain == "") {
            Messages.messageName(Strings.WARNING_SUCESS, Strings.DOMAIN_EMPTY, Strings.ICON[0], Strings.TYPE[0]);
        } else if (this.state.contact == "" && this.state.contact >= 10) {
            Messages.messageName(Strings.WARNING_SUCESS, Strings.MOBILE_EMPTY, Strings.ICON[0], Strings.TYPE[0]);
        } else if (this.state.description == "") {

        } else if (this.state.username == "") {
            Messages.messageName(Strings.WARNING_SUCESS, Strings.USER_NAME_EMPTY, Strings.ICON[0], Strings.TYPE[0]);
        } else if (this.state.password == "") {
            Messages.messageName(Strings.WARNING_SUCESS, Strings.place, Strings.ICON[0], Strings.TYPE[0]);
        } else {
            this.registerCustomer()
        }

    }
    //upload Image
    handleUploadPhoto = () => {

        if (this.state.photo == null) {
            this.registerCustomer()
        } else {

            this.setState({
                spinner: true,
            });

            fetch(Strings.Image_URL + "/file-server", {
                method: "POST",
                body: this.createFormData(this.state.photo, { userId: "123" })
            })
                .then(response => response.json())
                .then(response => {



                    this.setState({
                        photo: null,
                        uri: response.url
                    });
                    this.setState({
                        spinner: false,
                    });
                    this.registerCustomer()

                    console.log("upload succes", this.state.uri);

                })
                .catch(error => {
                    this.setState({
                        spinner: false,
                    });

                    console.log("upload error", error);
                    // alert("Upload failed!");
                });


        }

    };


    //chooseing image
    handleChoosePhoto = () => {
        const options = {
            noData: true,
        }
        ImagePicker.launchImageLibrary(options, response => {
            console.log("upload succes", response);
            if (response.uri) {
                this.setState({
                    photo: response

                })
                this.compress()
            }
        })
    }


    //split image url
    createFormData = (photo, body) => {
        const data = new FormData();

        data.append("document", {
            name: photo.fileName,
            type: photo.type,
            uri:
                Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "")
        });

        Object.keys(body).forEach(key => {
            data.append(key, body[key]);
        });
        return data;
    };

    //To compress a image
    compress() {
        CompressImage.createCompressedImage(this.state.photo.uri, "Compress/Images")

            .then(response => {
                this.state.photo.uri = response.uri;
                this.state.photo.path = response.path;
                this.state.photo.mime = response.mime;
            })

            .catch(err => {
                return Alert.alert(
                    Strings.COMPRESS_ERR
                );
            });

    }

    passwordSecure(value) {
        if (value == true) {

            this.setState({
                passwordSecure: false,
                eyeOne: false,
                eyeTwo: true,
            })
        } else {
            this.setState({
                passwordSecure: true,
                eyeOne: true,
                eyeTwo: false,
            })
        }

    }

    registerCustomer() {
        this.setState({
            spinner: true,
        });


        var data = JSON.stringify({

            "branch": {
                "address": "demo",
                "contactNumber": "077",
                "description": "demo",
                "name": "demo"
            },
            "company": {
                "address": this.state.address,
                "description": this.state.businessTyp,
                "domain": this.state.domain,
                "name": this.state.domain,
            },
            "user": {
                "address": "colombo",
                "contact": this.state.contact,
                "description": "good",
                "imageUrl": "../../",
                "name": this.state.name,
                "nicNumber": "981451791v",
                "password": this.state.password,
                "username": this.state.username,
            }
        });

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + "/api/register",
                    {
                        method: "POST",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                        },
                        body: data
                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        this.setState({
                            spinner: false,
                        })
                        if (responseJson.id > 0) {
                            this.props.navigation.navigate("LoginScreen")
                            Messages.messageName(Strings.WARNING_SUCESS, Strings.REGISTER_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                        } else if (responseJson.errorMessage == "Username already exists") {
                            Messages.messageName(Strings.WARNING_SUCESS, Strings.USER_NAME_ALRDY_TEAKE, Strings.ICON[0], Strings.TYPE[0], Colors.SUCCESS_ALERT);
                        } else if (responseJson.errorMessage == "Invalid domain in username")
                            Messages.messageName(Strings.WARNING_SUCESS, Strings.INVALID_DOMAIN, Strings.ICON[0], Strings.TYPE[0], Colors.SUCCESS_ALERT);
                        console.log(JSON.stringify(responseJson));
                    })

                    .catch(error => {
                        console.log(error);
                        Messages.messageName(Strings.WARNING_SUCESS, Strings.REGISTER_FAILED, Strings.ICON[2], Strings.TYPE[2], Colors.SUCCESS_ALERT);


                    });


            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.registerCustomer()

                            }
                        },
                    ],
                    { cancelable: true }
                );
            }
        });

    }


    visibleBranchDetails() {
        this.setState({
            branchDetails: true,
            branchDetailsCloseButton: true,
            branchDetailsButton: false
        })
    }

    visibleFalseBranchDetails() {
        this.setState({
            branchDetails: false,
            branchDetailsCloseButton: false,
            branchDetailsButton: true
        })
    }

    visibleUserDetails() {
        this.setState({
            userDetails: true,
            userDetailsCloseButton: true,
            userDetailsButton: false
        })
    }

    visibleFalseUserDetails() {
        this.setState({
            userDetails: false,
            userDetailsCloseButton: false,
            userDetailsButton: true
        })
    }


    render() {
        return (

            <ImageBackground
                source={require("../../assets/newBack2.png")}
                style={{ width: "100%", height: "100%" }}>
                <StatusBar barStyle="dark-content" hidden={false} backgroundColor={"#85CCD9"} />
                <View style={{ backgroundColor: '#85CCD9', width: '100%', height: hp('18%'), paddingLeft: 23, borderBottomLeftRadius: 90 }}>
                    <View style={{ backgroundColor: '#85CCD9', width: 50, top: 13, left: 25, position: "absolute", top: 8 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('LoginScreen')}>
                            <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                    <Image source={require("../../assets/newBack3.png")} style={{ width: 20, height: 20 }}></Image>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{ top: 30, left: 15, justifyContent: 'center', }}>
                        {/* <Text style={{ fontSize: hp('4%'), fontFamily: 'Ubuntu-Medium',fontWeight:"bold",color: '#ffff' ,marginRight:100,paddingRight:40,position:"absolute",top:10}}>Please  <Text style={{color: Colors.DARK_BLACK_TEXT_COLOR, fontSize: hp('4%'),fontWeight:"bold",position:"absolute",marginLeft:100,top:10}}>select</Text></Text>                      
                        <Text style={{ fontSize: hp('4%'),color: '#ffff' ,position:"absolute",top:50}}>business</Text>
                        <Text style={{ color: Colors.PRIMARY_COLOR, fontSize: hp('4%'),fontWeight:"bold",position:"absolute" ,top:85,left:2 }}>category</Text> */}
                    </View>
                </View>
                <Spinner
                    visible={this.state.spinner}
                    textContent={'Loading...'}
                    textStyle={{ color: 'white' }}
                    overlayColor={"#0f0e0e90"}
                    animation={"fade"}
                    color={"#00CEFD"}
                />
                <Content contentContainerStyle={styles.content}>
                    {/* <View style={styles.logoContainer}>

                            <Text style={styles.logoText}>{this.state.businessTyp}</Text>

                        </View> */}
                    <ScrollView>
                        <Form style={styles.form}>
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <TouchableOpacity style={{ borderRadius: 100, height: 100, width: 100, marginBottom: 10, marginTop: 20 }}
                                    onPress={() => this.handleChoosePhoto()}
                                >

                                    <Image
                                        source={this.state.photo == null ? require('../../assets/nnn.jpg') : this.state.photo !== null ? this.state.photo : this.state.photo}
                                        style={{ width: 100, height: 100, borderRadius: 100, borderWidth: 2, }}
                                    />
                                </TouchableOpacity>

                            </View>
                            {this.state.companyDetails &&
                                <View>
                                    <Item
                                        style={styles.item}
                                        rounded
                                        last
                                    >
                                        <Image
                                            style={styles.place}
                                            source={require('../../assets/owner.png')}
                                        />
                                        <Input
                                            style={styles.input}
                                            placeholder="Company"
                                            placeholderTextColor="#ffffff80"
                                            autoCapitalize="none"
                                            onChangeText={name => this.setState({ name })}
                                        />
                                    </Item>
                                    <Item
                                        style={styles.item}
                                        rounded
                                        last
                                    >
                                        <Image
                                            style={styles.place}
                                            source={require('../../assets/business.png')}
                                        />
                                        <Input
                                            style={styles.input}
                                            placeholder="Domain"
                                            placeholderTextColor="#ffffff80"
                                            autoCapitalize="none"
                                            onChangeText={domain => this.setState({ domain })}
                                        />
                                    </Item>

                                    <Item
                                        style={styles.item}
                                        rounded
                                        last
                                    >
                                        <Image
                                            style={styles.place}
                                            source={require('../../assets/contact.png')}
                                        />
                                        <Input
                                            style={styles.input}
                                            keyboardType="numeric"
                                            // maxLength={10}
                                            placeholder="Contact"
                                            placeholderTextColor="#ffffff80"
                                            autoCapitalize="none"
                                            onChangeText={contact => this.setState({ contact })}

                                        />
                                    </Item>
                                    <Item
                                        style={styles.item}
                                        rounded
                                        last
                                    >
                                        <Image
                                            style={styles.place}
                                            source={require('../../assets/address.png')}
                                        />
                                        <Input
                                            style={styles.input}
                                            placeholder="Address"
                                            placeholderTextColor="#ffffff80"
                                            autoCapitalize="none"
                                            onChangeText={address => this.setState({ address })}
                                        />
                                    </Item>
                                </View>
                            }
                            {this.state.branchDetailsButton &&
                                <TouchableOpacity onPress={() => this.visibleBranchDetails()}>
                                    <View style={{ width: wp('80%'), height: hp('5%'), top: 5, borderColor: Colors.DARK_BLACK_TEXT_COLOR, borderWidth: 1, borderRadius: 30 }}>
                                        <Text style={{ textAlign: 'center', color: Colors.PRIMARY_COLOR, fontWeight: 'bold' }}>Branch</Text>
                                    </View>
                                </TouchableOpacity>
                            }
                            {this.state.branchDetailsCloseButton &&
                                <TouchableOpacity onPress={() => this.visibleFalseBranchDetails()}>
                                    <View style={{ width: wp('80%'), height: hp('5%'), top: 5, borderColor: Colors.DARK_BLACK_TEXT_COLOR, borderWidth: 1, borderRadius: 30 }}>
                                        <Text style={{ textAlign: 'center', color: Colors.PRIMARY_COLOR, fontWeight: 'bold' }}>Branch close</Text>
                                    </View>
                                </TouchableOpacity>
                            }
                            {/* ////////////////////////////////////////// end of company details */}
                            {this.state.branchDetails &&
                                <View style={{ top: 5 }}>
                                    <Item
                                        style={styles.item}
                                        rounded
                                        last
                                    >
                                        <Image
                                            style={styles.place}
                                            source={require('../../assets/owner.png')}
                                        />
                                        <Input
                                            style={styles.input}
                                            placeholder="Branch"
                                            placeholderTextColor="#ffffff80"
                                            autoCapitalize="none"
                                            onChangeText={name => this.setState({ name })}
                                        />
                                    </Item>
                                    <Item
                                        style={styles.item}
                                        rounded
                                        last
                                    >
                                        <Image
                                            style={styles.place}
                                            source={require('../../assets/contact.png')}
                                        />
                                        <Input
                                            style={styles.input}
                                            keyboardType="numeric"
                                            // maxLength={10}
                                            placeholder="Contact"
                                            placeholderTextColor="#ffffff80"
                                            autoCapitalize="none"
                                            onChangeText={contact => this.setState({ contact })}
                                        />
                                    </Item>

                                    <Item
                                        style={styles.item}
                                        rounded
                                        last
                                    >
                                        <Image
                                            style={styles.place}
                                            source={require('../../assets/address.png')}
                                        />
                                        <Input
                                            style={styles.input}
                                            placeholder="Address"
                                            placeholderTextColor="#ffffff80"
                                            autoCapitalize="none"
                                            onChangeText={address => this.setState({ address })}
                                        />
                                    </Item>
                                </View>
                            }
                            {this.state.userDetailsButton &&
                                <TouchableOpacity onPress={() => this.visibleUserDetails()}>
                                    <View style={{ width: wp('80%'), height: hp('5%'), top: 15, borderColor: Colors.DARK_BLACK_TEXT_COLOR, borderWidth: 1, borderRadius: 30 }}>
                                        <Text style={{ textAlign: 'center', color: Colors.PRIMARY_COLOR, fontWeight: 'bold' }}>User</Text>
                                    </View>
                                </TouchableOpacity>
                            }
                            {this.state.userDetailsCloseButton &&
                                <TouchableOpacity onPress={() => this.visibleFalseUserDetails()}>
                                    <View style={{ width: wp('80%'), top: 15, height: hp('5%'), borderColor: Colors.DARK_BLACK_TEXT_COLOR, borderWidth: 1, borderRadius: 30 }}>
                                        <Text style={{ textAlign: 'center', color: Colors.PRIMARY_COLOR, fontWeight: 'bold' }}>User close</Text>
                                    </View>
                                </TouchableOpacity>
                            }
                            {/* //////////////////////////////////// end of branch details */}

                            {this.state.userDetails &&
                                <View style={{ top: 15 }}>
                                    <Item
                                        style={styles.item}
                                        rounded
                                        last
                                    >
                                        <Image
                                            style={styles.place}
                                            source={require('../../assets/user.png')}
                                        />
                                        <Input
                                            style={styles.input}
                                            placeholder={this.state.namePlaceHolder}
                                            placeholderTextColor="#ffffff80"
                                            autoCapitalize="none"
                                            // onFocus={()=>this.setState({namePlaceHolder:''})}
                                            onChangeText={name => this.setState({ name })}
                                        />
                                    </Item>
                                    <Item
                                        style={styles.item}
                                        rounded
                                        last
                                    >

                                        <Image
                                            style={styles.place}
                                            source={require('../../assets/owner.png')}
                                        />
                                        <Input
                                            style={styles.input}
                                            placeholder={this.state.userNamePlaceHolder}
                                            placeholderTextColor="#ffffff80"
                                            onChangeText={username => this.setState({ username })}

                                        />
                                        <Item
                                            style={styles.item2}
                                            rounded
                                        // last
                                        >
                                            <Input
                                                style={styles.input}
                                                editable={false}
                                                value={"@" + this.state.CompanyDomain}
                                                placeholderTextColor="#ffffff80"
                                                onChangeText={username => this.setState({ username })}

                                            />
                                        </Item>
                                    </Item>

                                    <Item
                                        style={styles.item}
                                        rounded
                                        last
                                    >
                                        <Image
                                            style={styles.place}
                                            source={require('../../assets/confirm.png')}
                                        />
                                        <Input
                                            style={styles.input}
                                            placeholder={this.state.passwordPlaceHolder}
                                            placeholderTextColor="#ffffff80"
                                            onChangeText={password => this.setState({ password })}
                                            secureTextEntry={this.state.passwordSecure}
                                        />
                                        {this.state.eyeOne &&
                                            <TouchableOpacity onPress={() => this.passwordSecure(true)} style={{ left: -10 }}>
                                                <Image
                                                    style={styles.place}
                                                    source={require('../../assets/eye.png')}
                                                />
                                            </TouchableOpacity>
                                        }
                                        {this.state.eyeTwo &&
                                            <TouchableOpacity onPress={() => this.passwordSecure(false)} style={{ left: -10 }}>
                                                <Image
                                                    style={styles.place}
                                                    source={require('../../assets/eye.png')}
                                                />
                                            </TouchableOpacity>
                                        }
                                    </Item>
                                </View>
                            }
                        </Form>
                    </ScrollView>

                    {/* <View style={styles.buttonContainer}>
                        <Button
                            style={styles.button}
                            onPress={() => this.validation()}
                            hasText
                            block
                            large
                            dark
                            rounded
                        >
                            <Text style={styles.loginText}>SUBMIT</Text>
                        </Button>


                        <View style={styles.signupContainer}>

                        </View>
                    </View> */}
                </Content>
                <TouchableOpacity>
                    <Footer style={{ backgroundColor: Colors.PRIMARY_COLOR, borderTopLeftRadius: 20, borderTopRightRadius: 20,elevation:8 }}>


                        <Text style={styles.loginText}>SUBMIT</Text>


                    </Footer>
                </TouchableOpacity>
            </ImageBackground>

        );
    }
}



