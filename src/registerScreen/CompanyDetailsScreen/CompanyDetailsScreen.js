import React, { Component } from 'react';
import { View, Image, ImageBackground, StatusBar, Alert, TouchableOpacity } from 'react-native';
import {
    Button,
    Content,
    Form,
    Item,
    Input,
    Text,
    Label,
} from 'native-base';
import styles from './CompanyDetailsScreenStyles';
import Messages from '../../../resources/Message';
import Strings from '../../../resources/Strings';
import Spinner from 'react-native-loading-spinner-overlay';
import { ScrollView } from "react-native-gesture-handler";
import Colors from '../../../resources/Colors';
export default class companyDetailsScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            contact: '',
            domain: "",
            address:"",
            businessType: '',
            background: ''
        };
        this.state.businessType = this.props.navigation.state.params.name
    }

    mobilevalidate(text) {
        const reg = /^(?:0|94|\+94)?(?:(11|21|23|24|25|26|27|31|32|33|34|35|36|37|38|41|45|47|51|52|54|55|57|63|65|66|67|81|912)(0|2|3|4|5|7|9)|7(0|1|2|5|6|7|8)\d)\d{6}$/;
        if (reg.test(text) === false) {
            return false;
        } else {
            return true;
        }
    }

    //validate text fields
    validation() {
        
        if (this.state.name == "") {
            Messages.messageName(Strings.WARNING, Strings.COMPANY_NAME_EMPTY, Strings.ICON[0], Strings.TYPE[0],Colors.WARNING_ALERT);
        } else if (this.state.domain == "") {
            Messages.messageName(Strings.DOMAIN_WARNING, Strings.DOMAIN_EMPTY, Strings.ICON[0], Strings.TYPE[0],Colors.WARNING_ALERT);
        } else if (this.mobilevalidate(this.state.contact)== false) {
            Messages.messageName(Strings.WARNING, Strings.MOBILE_EMPTY, Strings.ICON[0], Strings.TYPE[0],Colors.WARNING_ALERT);
          }  else if (this.state.address == "") {
                Messages.messageName(Strings.WARNING, Strings.ADDRESS_EMPTY, Strings.ICON[0], Strings.TYPE[0],Colors.WARNING_ALERT);
            } else {
                this.props.navigation.navigate('BaranchDetailsScreen', {
                    businessType: this.state.businessType,
                    companyName: this.state.name,
                    companyDomain: this.state.domain.trim(),
                    companyAddress: this.state.address,
                    companyContact:this.state.contact

                });
            }

        }

        render() {
            return (
               
                    <ImageBackground

                        source={require("../../../assets/newBack2.png")}
                        style={{ width: "100%", height: "100%" }}>
                        <StatusBar barStyle="dark-content" hidden={false} backgroundColor={"#85CCD9"} />
                        <Spinner
                            visible={this.state.spinner}
                            textContent={'Loading...'}
                            textStyle={{ color: 'white' }}
                            overlayColor={"#0f0e0e90"}
                            animation={"fade"}
                            color={"#00CEFD"}
                        />
                            <ScrollView>
                        <View style={{ backgroundColor: '#85CCD9' ,width:50}}>
                            <ImageBackground source={require("../../../assets/company2.jpg")} style={styles.companyImage}>
                                <TouchableOpacity onPress={()=>this.props.navigation.navigate('BusinessCategoryScreen')}>
                                    <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: 'white', left: 10, elevation: 8 }}>
                                        <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                            <Image source={require("../../../assets/back2.png")} style={{ width: 20, height: 20 }}></Image>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </ImageBackground>
                        </View>
                        
                        <View style={{ width: '100%', height: 90, backgroundColor: '#85CCD9', borderBottomLeftRadius: 20, borderBottomRightRadius: 20 }}>
                            <Text style={styles.logoText}>{this.state.businessType}</Text>
                            <Text style={{ fontSize: 23, fontWeight: 'bold', color: 'white', left: 20 }}>Company Profile</Text>
                        </View>
                    
                        <Content contentContainerStyle={styles.content}>

                            <Form style={styles.form}>
                                <Item
                                    style={styles.item}
                                    rounded
                                    last
                                >
                                    <Image
                                        style={styles.place}
                                        source={require('../../../assets/owner.png')}
                                    />
                                    <Input
                                        style={styles.input}
                                        placeholder="Company"
                                        placeholderTextColor="#ffffff80"
                                        autoCapitalize="none"
                                        onChangeText={name => this.setState({ name })}
                                    />
                                </Item>
                                <Item
                                    style={styles.item}
                                    rounded
                                    last
                                >
                                    <Image
                                        style={styles.place}
                                        source={require('../../../assets/business.png')}
                                    />
                                    <Input
                                        style={styles.input}
                                        placeholder="Domain"
                                        placeholderTextColor="#ffffff80"
                                        autoCapitalize="none"
                                        onChangeText={domain => this.setState({ domain })}
                                    />
                                </Item>

                                <Item
                                    style={styles.item}
                                    rounded
                                    last
                                >
                                    <Image
                                        style={styles.place}
                                        source={require('../../../assets/contact.png')}
                                    />
                                    <Input
                                        style={styles.input}
                                        keyboardType="numeric"
                                        // maxLength={10}
                                        placeholder="Contact"
                                        placeholderTextColor="#ffffff80"
                                        autoCapitalize="none"
                                        onChangeText={contact => this.setState({ contact })}
                                       
                                    />
                                </Item>
                                <Item
                                    style={styles.item}
                                    rounded
                                    last
                                >
                                    <Image
                                        style={styles.place}
                                        source={require('../../../assets/address.png')}
                                    />
                                    <Input
                                        style={styles.input}
                                        placeholder="Address"
                                        placeholderTextColor="#ffffff80"
                                        autoCapitalize="none"
                                        onChangeText={address => this.setState({ address })}
                                    />
                                </Item>

                            </Form>
                            <Label></Label>

                            <View style={styles.buttonContainer}>
                                <Button
                                    style={styles.button}
                                    onPress={() => this.validation()}
                                    hasText
                                    block
                                    large
                                    dark
                                    rounded
                                >
                                    <Text style={styles.loginText}>Next</Text>
                                </Button>
                            </View>
                        </Content>
                        </ScrollView>
                    </ImageBackground>
               
            );
        }
    }



