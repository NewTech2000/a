import React, { Component } from 'react';
import { View, Image, ImageBackground, StatusBar, Alert, TouchableOpacity } from 'react-native';
import {
    Button,
    Content,
    Form,
    Item,
    Input,
    Text,
    Label,
} from 'native-base';
import styles from './BranchDetailsScreenStyles';
import Messages from '../../../resources/Message';
import Strings from '../../../resources/Strings';
import Colors from '../../../resources/Colors';
import Spinner from 'react-native-loading-spinner-overlay';
import { ScrollView } from "react-native-gesture-handler";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
export default class companyDetailsScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            address: "",
            domain: "",
            businessType: '',
            background: '',
            contact: '',
            companyName:'',
            companyContact:'',
            CompanyDomain:'',
            companyAddress:''
        };
        this.state.businessType = this.props.navigation.state.params.businessType,
        this.state.companyName = this.props.navigation.state.params.companyName,
        this.state.companyContact = this.props.navigation.state.params.companyContact,
        this.state.CompanyDomain = this.props.navigation.state.params.companyDomain,
        this.state.companyAddress = this.props.navigation.state.params.companyAddress
    }


    mobilevalidate(text) {
        const reg = /^(?:0|94|\+94)?(?:(11|21|23|24|25|26|27|31|32|33|34|35|36|37|38|41|45|47|51|52|54|55|57|63|65|66|67|81|912)(0|2|3|4|5|7|9)|7(0|1|2|5|6|7|8)\d)\d{6}$/;
        if (reg.test(text) === false) {
            return false;
        } else {
            return true;
        }
    }
    //validate text fields
    validation() {
       
      
        if (this.state.name == "") {
            Messages.messageName(Strings.WARNING, Strings.BRANCH_NAME_EMPTY, Strings.ICON[0], Strings.TYPE[0],Colors.WARNING_ALERT);
        } else if (this.state.address == "") {
            Messages.messageName(Strings.WARNING, Strings.ADDRESS_EMPTY, Strings.ICON[0], Strings.TYPE[0],Colors.WARNING_ALERT);
        } else if (this.mobilevalidate(this.state.contact)== false) {
            Messages.messageName(Strings.WARNING, Strings.MOBILE_EMPTY, Strings.ICON[0], Strings.TYPE[0],Colors.WARNING_ALERT);
        } else {
            this.props.navigation.navigate('UserDetailsScreen', {
                businessType: this.state.businessType,
                branchName:this.state.name,
                branchContact:this.state.contact,
                branchAddress:this.state.address,
                companyName: this.state.companyName,
                companyDomain: this.state.CompanyDomain,
                companyContact:this.state.companyContact,
                companyAddress:this.state.companyAddress

            });
        }


    }


    skip() {
        this.props.navigation.navigate('UserDetailsScreen', {
            businessType: this.state.businessType,
            branchName:'unknown',
            branchContact:'unknown',
            branchAddress:'unknown',
            companyName: this.state.companyName,
            companyDomain: this.state.CompanyDomain,
            companyContact:this.state.companyContact,
            companyAddress:this.state.companyAddress

        });
    }


    render() {
        return (
            <ScrollView>
                <ImageBackground
                    source={require("../../../assets/newBack2.png")}
                    style={{ width: "100%", height: "100%" }}>
                    <StatusBar barStyle="dark-content" hidden={false} backgroundColor={"#85CCD9"} />
                    <Spinner
                        visible={this.state.spinner}
                        textContent={'Loading...'}
                        textStyle={{ color: 'white' }}
                        overlayColor={"#0f0e0e90"}
                        animation={"fade"}
                        color={"#00CEFD"}
                    />
                    <ImageBackground source={require("../../../assets/branch2.jpg")} style={styles.companyImage}>
                        <View style={{ backgroundColor: '#85CCD9' ,width:50}}>       
                        <TouchableOpacity  onPress={()=>this.props.navigation.navigate('CompanyDetailsScreen')}>
                            <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: 'white', left: 10, elevation: 8 }}>
                                <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                    <Image source={require("../../../assets/back2.png")} style={{ width: 20, height: 20 }}></Image>
                                </View>
                            </View>
                        </TouchableOpacity>
                        </View>
                        <TouchableOpacity onPress={() => this.skip()}>
                            <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', alignContent: 'flex-end', left: -10 }}>
                                <View style={{ borderWidth: 1, borderColor: 'white', width: 100 }}>
                                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ fontWeight: 'bold', color: 'white' }} >Skip for now</Text>
                                    </View>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </ImageBackground>
                    
                    <View style={{ width: '100%',height:100,backgroundColor: '#85CCD9', borderBottomLeftRadius: 20, borderBottomRightRadius: 20 }}>
                        <Text style={styles.logoText}>{this.state.businessType}</Text>
                        <Text style={{ fontSize: 23, fontWeight: 'bold', color: 'white', left: 20 }}>Branch Profile</Text>
                    </View>
                    <Content contentContainerStyle={styles.content}>

                        <Form style={styles.form}>
                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../../assets/owner.png')}
                                />
                                <Input
                                    style={styles.input}
                                    placeholder="Branch"
                                    placeholderTextColor="#ffffff80"
                                    autoCapitalize="none"
                                    onChangeText={name => this.setState({ name })}
                                />
                            </Item>
                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../../assets/contact.png')}
                                />
                                <Input
                                    style={styles.input}
                                    keyboardType="numeric"
                                    // maxLength={10}
                                    placeholder="Contact"
                                    placeholderTextColor="#ffffff80"
                                    autoCapitalize="none"
                                    onChangeText={contact => this.setState({ contact })}
                                />
                            </Item>

                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../../assets/address.png')}
                                />
                                <Input
                                    style={styles.input}
                                    placeholder="Address"
                                    placeholderTextColor="#ffffff80"
                                    autoCapitalize="none"
                                    onChangeText={address => this.setState({ address })}
                                />
                            </Item>

                        </Form>
                        <Label></Label>

                        <View style={styles.buttonContainer}>
                            <Button
                                style={styles.button}
                                onPress={() => this.validation()}
                                hasText
                                block
                                large
                                dark
                                rounded
                            >
                                <Text style={styles.loginText}>Next</Text>
                            </Button>

                        </View>

                    </Content>
                    
                </ImageBackground>
                </ScrollView>
            
        );
    }
}



