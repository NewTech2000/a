import React, { Component } from 'react';
import { View, Image, ImageBackground, StatusBar, Alert, TouchableOpacity, TouchableHighlight } from 'react-native';
import {
    Button,
    Content,
    Form,
    Item,
    Input,
    Text,
    Label,
} from 'native-base';
import styles from './UserDetailsScreenStyles';
import Messages from '../../../resources/Message';
import Colors from '../../../resources/Colors';
import NetInfo from "@react-native-community/netinfo";
import Strings from '../../../resources/Strings';
import CompressImage from "react-native-compress-image";
import ImagePicker from "react-native-image-picker";
import Spinner from 'react-native-loading-spinner-overlay';
import { ScrollView } from "react-native-gesture-handler";
export default class UserDetailsScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            address: "",
            domain: "",
            businessType: '',
            background: '',
            username: '',
            password: '',
            confirmpassword: '',
            companyName: '',
            companyContact: '',
            CompanyDomain: '',
            companyAddress: '',
            branchName: '',
            branchContact: '',
            branchAddress: '',
            contact: '',
            nicnumber: '',
            avatarSource: null,
            image: null,
            photo: null,
            url: "",
            photoUri: '',
            uri: "",
            spinner: false,
            passwordSecure: true,
            eyeOne: true,
            eyeTwo: false,
            namePlaceHolder: "Name",
            passwordPlaceHolder: "Password",
            userNamePlaceHolder: "Username"
        };
        this.state.businessType = this.props.navigation.state.params.businessType,
            this.state.companyName = this.props.navigation.state.params.companyName,
            this.state.companyContact = this.props.navigation.state.params.companyContact,
            this.state.CompanyDomain = this.props.navigation.state.params.companyDomain
            this.state.branchAddress = this.props.navigation.state.params.branchAddress,
            this.state.branchContact = this.props.navigation.state.params.branchContact,
            this.state.branchName = this.props.navigation.state.params.branchName,
            this.state.companyAddress = this.props.navigation.state.params.companyAddress

    }



    //validate text fields
    validation() {
        if (this.state.name == "") {
            Messages.messageName(Strings.WARNING, Strings.NAME_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        } else if (this.state.username == "") {
            Messages.messageName(Strings.WARNING, Strings.USER_NAME_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        } else if (this.state.password == "") {
            Messages.messageName(Strings.WARNING, Strings.PASSWORD_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
            // } else if (this.state.confirmpassword != this.state.password) {
            //     Messages.messageName(Strings.WARNING, Strings.CONFIRMPASSWORD_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
            //     // } else if (this.state.contact == "") {
            //     Messages.messageName(Strings.WARNING, Strings.MOBILE_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
            // } else if (this.state.nicnumber == "") {
            //     Messages.messageName(Strings.WARNING, Strings.NIC_WARNING, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
            // } else if (this.state.address == "") {
            //     Messages.messageName(Strings.WARNING, Strings.ADDRESS_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
            // }
            // else if (this.state.photo == null) {
            //     Messages.messageName(Strings.WARNING, Strings.IMAGE_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
            // }
        } else {
            this.handleUploadPhoto()
        }

    }


    registerCustomer() {
        var tempUserName = this.state.username.trim()
        var test = tempUserName+"@"+this.state.CompanyDomain
       
        this.setState({
            spinner: true,
        });


        var data = JSON.stringify({

            "branch": {
                "address": this.state.branchAddress,
                "contactNumber": this.state.branchContact,
                "description": "unknown",
                "name": this.state.branchName
            },
            "company": {
                "address": this.state.companyAddress,
                "customSettings": {
                    "contact": this.state.companyContact,
                    "type": this.state.businessType
                },
                "description": "unknown",
                "domain": this.state.CompanyDomain,
                "name": this.state.companyName,
            },
            "user": {
                "address": this.state.address,
                "contactNumber": this.state.contact,
                "description": "unknown",
                "imageUrl": this.state.uri,
                "name": this.state.name,
                "nicNumber": this.state.nicnumqber,
                "password": this.state.password,
                "username": test,
            }
        });

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + "/api/register",
                    {
                        method: "POST",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                        },
                        body: data
                    }
                )
                    .then(resp => resp.json())
                    .then(responseJson => {
                        this.setState({
                            spinner: false,
                        })
                        if (responseJson.id > 0) {
                            this.props.navigation.navigate("LoginScreen")
                            Messages.messageName(Strings.WARNING_SUCESS, Strings.REGISTER_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                        } else if (responseJson.errorMessage == "Username already exists") {
                            Messages.messageName(Strings.WARNING_SUCESS, Strings.USER_NAME_ALRDY_TEAKE, Strings.ICON[0], Strings.TYPE[0], Colors.SUCCESS_ALERT);
                        } else if (responseJson.errorMessage == "Invalid domain in username")
                            Messages.messageName(Strings.WARNING_SUCESS, Strings.INVALID_DOMAIN, Strings.ICON[0], Strings.TYPE[0], Colors.SUCCESS_ALERT);
                        console.log(JSON.stringify(responseJson));
                    })

                    .catch(error => {
                        console.log(error);
                        Messages.messageName(Strings.WARNING_FAILED, Strings.REGISTER_FAILED, Strings.ICON[2], Strings.TYPE[2], Colors.SUCCESS_ALERT);


                    });


            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.registerCustomer()

                            }
                        },
                    ],
                    { cancelable: true }
                );
            }
        });

    }

    //upload Image
    handleUploadPhoto = () => {

        if (this.state.photo == null) {
            this.registerCustomer()
        } else {

            this.setState({
                spinner: true,
            });

            fetch(Strings.Image_URL + "/api/files", {
                method: "POST",
                body: this.createFormData(this.state.photo, { userId: "123" })
            })
                .then(response => response.json())
                .then(response => {



                    this.setState({
                        photo: null,
                        uri: response.url
                    });
                    this.setState({
                        spinner: false,
                    });
                    this.registerCustomer()

                    console.log("upload succes", this.state.uri);

                })
                .catch(error => {
                    this.setState({
                        spinner: false,
                    });

                    console.log("upload error", error);
                    // alert("Upload failed!");
                });


        }

    };


    //chooseing image
    handleChoosePhoto = () => {
        const options = {
            noData: true,
        }
        ImagePicker.launchImageLibrary(options, response => {
            console.log("upload succes", response);
            if (response.uri) {
                this.setState({
                    photo: response

                })
                this.compress()
            }
        })
    }


    //split image url
    createFormData = (photo, body) => {
        const data = new FormData();

        data.append("document", {
            name: photo.fileName,
            type: photo.type,
            uri:
                Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "")
        });

        Object.keys(body).forEach(key => {
            data.append(key, body[key]);
        });
        return data;
    };

    //To compress a image
    compress() {
        CompressImage.createCompressedImage(this.state.photo.uri, "Compress/Images")

            .then(response => {
                this.state.photo.uri = response.uri;
                this.state.photo.path = response.path;
                this.state.photo.mime = response.mime;
            })

            .catch(err => {
                return Alert.alert(
                    Strings.COMPRESS_ERR
                );
            });

    }


    passwordSecure(value) {
        if (value == true) {

            this.setState({
                passwordSecure: false,
                eyeOne: false,
                eyeTwo: true,
            })
        } else {
            this.setState({
                passwordSecure: true,
                eyeOne: true,
                eyeTwo: false,
            })
        }

    }



    render() {
        return (
            <ScrollView>
                <ImageBackground
                    source={require("../../../assets/newBack2.png")}
                    style={{ width: "100%", height: "100%" }}>
                    <StatusBar barStyle="dark-content" hidden={false} backgroundColor={"#85CCD9"} />
                    <Spinner
                        visible={this.state.spinner}
                        textContent={'Loading...'}
                        textStyle={{ color: 'white' }}
                        overlayColor={"#0f0e0e90"}
                        animation={"fade"}
                        color={"#00CEFD"}
                    />



                    <ImageBackground source={require("../../../assets/UserBack2.jpg")} style={styles.companyImage}>
                        <View style={{ backgroundColor: '#85CCD9', width: 50 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('BaranchDetailsScreen')}>
                                <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: 'white', left: 10, elevation: 8 }}>
                                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                        <Image source={require("../../../assets/back2.png")} style={{ width: 20, height: 20 }}></Image>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </ImageBackground>
                    <View style={{ width: '100%', height: 90, backgroundColor: '#85CCD9', borderBottomLeftRadius: 20, borderBottomRightRadius: 20 }}>
                        <Text style={styles.logoText}>{this.state.businessType}</Text>
                        <Text style={{ fontSize: 23, fontWeight: 'bold', color: 'white', left: 20 }}>User Profile</Text>
                    </View>
                    <Content contentContainerStyle={styles.content}>

                        <Form style={styles.form}>
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <TouchableHighlight style={{ borderRadius: 100, height: 100, width: 100, marginBottom: 10, marginTop: 20 }}
                                    onPress={() => this.handleChoosePhoto()}
                                >

                                    <Image
                                        source={this.state.photo == null ? require('../../../assets/nnn.jpg') : this.state.photo !== null ? this.state.photo : this.state.photo}
                                        style={{ width: 100, height: 100, borderRadius: 100, borderWidth: 2, }}
                                    />
                                </TouchableHighlight>

                            </View>

                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../../assets/user.png')}
                                />
                                <Input
                                    style={styles.input}
                                    placeholder={this.state.namePlaceHolder}
                                    placeholderTextColor="#ffffff80"
                                    autoCapitalize="none"
                                    // onFocus={()=>this.setState({namePlaceHolder:''})}
                                    onChangeText={name => this.setState({ name })}
                                />
                            </Item>
                            <Item
                                style={styles.item}
                                rounded
                                last
                            >

                                <Image
                                    style={styles.place}
                                    source={require('../../../assets/owner.png')}
                                />
                                <Input
                                    style={styles.input}
                                    placeholder={this.state.userNamePlaceHolder}
                                    placeholderTextColor="#ffffff80"
                                    onChangeText={username => this.setState({ username })}

                                />
                                <Item
                                    style={styles.item2}
                                    rounded
                                // last
                                >
                                    <Input
                                        style={styles.input}
                                        editable={false}
                                        value={"@" + this.state.CompanyDomain}
                                        placeholderTextColor="#ffffff80"
                                        onChangeText={username => this.setState({ username })}

                                    />
                                </Item>
                            </Item>
                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../../assets/confirm.png')}
                                />
                                <Input
                                    style={styles.input}
                                    placeholder={this.state.passwordPlaceHolder}
                                    placeholderTextColor="#ffffff80"
                                    onChangeText={password => this.setState({ password })}
                                    secureTextEntry={this.state.passwordSecure}
                                />
                                {this.state.eyeOne &&
                                    <TouchableOpacity onPress={() => this.passwordSecure(true)} style={{ left: -10 }}>
                                        <Image
                                            style={styles.place}
                                            source={require('../../../assets/eye.png')}
                                        />
                                    </TouchableOpacity>
                                }
                                {this.state.eyeTwo &&
                                    <TouchableOpacity onPress={() => this.passwordSecure(false)} style={{ left: -10 }}>
                                        <Image
                                            style={styles.place}
                                            source={require('../../../assets/eye.png')}
                                        />
                                    </TouchableOpacity>
                                }
                            </Item>
                            {/* <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../../assets/confirm.png')}
                                />
                                <Input
                                    style={styles.input}
                                    placeholder="Confirm Password"
                                    placeholderTextColor="#ffff"
                                    onChangeText={confirmpassword => this.setState({ confirmpassword })}
                                    secureTextEntry
                                />
                            </Item> */}
                            {/* <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../../assets/contact.png')}
                                />
                                <Input
                                    style={styles.input}
                                    keyboardType="numeric"
                                    maxLength={10}
                                    placeholder="Contact"
                                    placeholderTextColor="#ffff"
                                    autoCapitalize="none"
                                    onChangeText={contact => this.setState({ contact })}
                                />
                            </Item>
                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../../assets/invoice.png')}
                                />
                                <Input
                                    style={styles.input}
                                    placeholder="NicNumber"
                                    placeholderTextColor="#ffff"
                                    onChangeText={nicnumber => this.setState({ nicnumber })}

                                />
                            </Item> */}
                            {/* <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../../assets/address.png')}
                                />
                                <Input
                                    style={styles.input}
                                    placeholder="Address"
                                    placeholderTextColor="#ffff"
                                    onChangeText={address => this.setState({ address })}

                                />
                            </Item> */}

                        </Form>
                        <Label></Label>

                        <View style={styles.buttonContainer}>
                            <Button
                                style={styles.button}
                                onPress={() => this.validation()}
                                hasText
                                block
                                large
                                dark
                                rounded
                            >
                                <Text style={styles.loginText}>Submit</Text>
                            </Button>

                            {/* 
                            <Label></Label>
                            <Label></Label> */}
                        </View>
                    </Content>
                </ImageBackground>
            </ScrollView>
        );
    }
}



