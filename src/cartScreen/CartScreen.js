import React, { Component } from 'react';
import { View, Image, TouchableOpacity, ImageBackground, StatusBar, FlatList, BackHandler, SafeAreaView, Alert, TextInput, Switch,Dimensions} from 'react-native';

import {
    Button,
    Header,
    Left,
    Right,
    Text,
    Input,
} from 'native-base';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Colors from '../../resources/Colors';
import Strings from '../../resources/Strings';
import styles from './CartScreenStyle';
import NetInfo from "@react-native-community/netinfo";
import Messages from '../../resources/Message';
import Modal from 'react-native-modalbox';
import VirtualKeyboard from 'react-native-virtual-keyboard';
import { NavigationActions, StackActions } from 'react-navigation'
import Dialog, {
    DialogTitle,
    DialogContent,
    DialogButton,
    ScaleAnimation,
} from 'react-native-popup-dialog';
var obj;
var obj2;
var obj3;

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

export default class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            addedDetails: [],
            tempAddedDetails: [],
            text: '',
            oneitemtext: '',
            customer: '',
            subTotal: 120,
            totalDiscount: 0,
            grandTotal: 0,
            tempryTOt: 0,
            textQty: 0,
            changeText3: 0,
            refresh: false,
            showTheThing: true,
            oneitemdis: '',
            itemqty: '',
            defaultAnimationDialog: false,
            scaleAnimationDialog: false,
            slideAnimationDialog: false,
            itemNote: "",
            switchValue: false,
            switchValue2: false,
            value: "%",
            precentageVisible: true,
            valueVisible: false,
            precentageVisible2: true,
            valueVisible2: false
        }
        // const arrayDetails = this.props.navigation.state.params.text
        this.state.addedDetails = this.props.navigation.state.params.text,
            console.log("KKKKKKKKKKKK" + JSON.stringify(this.state.addedDetails))

    }

    //for visible & invisible under two butons
    onClose = () => {
        this.setState({
            showTheThing: true
        })
    }

    //to show number keypad
    showTotalDiscountModel() {
        this.setState({ showTheThing: false })
        this.refs.modal4.open()
    }

    //to show number keypad for update Qty
    animaQty() {
        this.refs.modal6.open()
    }

    //to cleat qty textfield
    clearText() {
        this.setState({
            text: "",
            textQty: "",
            oneitemtext: "",

        })
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        this.state.totalDiscount = this.props.navigation.getParam('totalDiscount', '')
        this.state.switchValue = this.props.navigation.getParam('switchValue', '')
        if (this.state.switchValue == true) {
            this.setState({
                value: "Rs.",
                valueVisible: true,
                precentageVisible: false
            })
        } else {
            this.setState({
                value: "%",
                valueVisible: false,
                precentageVisible: true

            })

        }
        console.log("cart" + JSON.stringify(this.state.switchValue))
        this.calculateTotal();
    }


    /////////////////////////////////////////////////////////////// this is error code

    onButtonPress = () => {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        // then navigate
        // navigate('HomeScreen');
    }

    //when backhandler press,
    handleBackButton = () => {
        this.props.navigation.navigate('HomeScreen', {
            details: this.state.addedDetails,
            totalDiscount: this.state.totalDiscount,
            switchValue: this.state.switchValue,
            subTot: this.state.subTotal,

        });
        return true;
    }


    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////


    //when update qty or discount ,this function work for all calculation (Total)
    calculateTotal() {
        this.state.tempryTOt = 0;
        var subTotal = 0;
        var temp = 0;

        if (this.state.totalDiscount > 0) {
            if (this.state.switchValue == true) {
                for (let index = 0; index < this.state.addedDetails.length; index++) {
                    temp = temp + this.state.addedDetails[index].total
                    subTotal = subTotal + this.state.addedDetails[index].fullTotal

                }
                this.state.tempryTOt = temp - this.state.totalDiscount
                this.setState({
                    subTotal: subTotal,
                    grandTotal: this.state.tempryTOt

                })

            } else {
                var qty = 0
                var price = 0
                for (let index = 0; index < this.state.addedDetails.length; index++) {
                    var tot1 = this.state.addedDetails[index].qty * this.state.addedDetails[index].price
                    this.state.tempryTOt = this.state.tempryTOt + tot1 - (tot1 * this.state.totalDiscount / 100)
                    subTotal = subTotal + this.state.addedDetails[index].fullTotal


                }
                console.log(price)
                this.setState({
                    subTotal: subTotal,
                    grandTotal: this.state.tempryTOt

                })
            }

        } else {
            for (let index = 0; index < this.state.addedDetails.length; index++) {
                this.state.tempryTOt = this.state.tempryTOt + this.state.addedDetails[index].total
                subTotal = subTotal + this.state.addedDetails[index].fullTotal
            }
            this.setState({
                subTotal: subTotal,
                grandTotal: this.state.tempryTOt

            })
        }


    }
    //when calulate discount for one item
    calculatediscountTotal() {
        this.state.tempryTOt = 0;
        for (let index = 0; index < this.state.addedDetails.length; index++) {
            this.state.tempryTOt = this.state.tempryTOt + this.state.addedDetails[index].total
        }
        this.setState({
            grandTotal: this.state.tempryTOt

        })

        if (this.state.totalDiscount < 0) {
            this.calculateDiscount()
        }

    }

    //when update qty or discount ,this function work for all calculation (Discount)
    calculateDiscount() {

        if (this.state.switchValue == true) {
            if (this.state.text >= this.state.grandTotal || this.state.text == "") {
                Messages.messageName(Strings.WARNING, Strings.DISCOUNT_FAILD, Strings.ICON[0], Strings.TYPE[0]);
            } else {

                for (let index = 0; index < this.state.addedDetails.length; index++) {
                    this.state.addedDetails[index].total = this.state.addedDetails[index].qty * this.state.addedDetails[index].price,
                        this.state.addedDetails[index].discountPercentage = 0
                    this.state.addedDetails[index].discountAmount = 0

                }
                this.refs.modal4.close();
                this.clearText()
                this.setState({
                    totalDiscount: this.state.text
                })
                let finalAmount = this.state.subTotal - this.state.text
                this.setState({
                    grandTotal: finalAmount,

                })
            }
        } else {
            if (this.state.text >= 100 || this.state.text == "") {
                Messages.messageName(Strings.WARNING, Strings.DISCOUNT_FAILD, Strings.ICON[0], Strings.TYPE[0]);
            } else {
                for (let index = 0; index < this.state.addedDetails.length; index++) {

                    this.state.addedDetails[index].total = this.state.addedDetails[index].qty * this.state.addedDetails[index].price,
                        this.state.addedDetails[index].discountPercentage = 0
                    this.state.addedDetails[index].discountAmount = 0

                }
                this.refs.modal4.close();
                this.clearText()
                this.setState({
                    totalDiscount: this.state.text
                })
                let finalAmount = this.state.subTotal - (this.state.subTotal * this.state.text / 100)
                this.setState({
                    grandTotal: finalAmount,

                })
            }
        }
    }

    //remove item from cart 
    deleteRow(index, discountAmount) {
        let addedDetails = this.state.addedDetails.filter((e, i) => i !== index);
        var tot = 0;

        var temp = 0;
        var subTotal = 0;
        if (addedDetails.length == 0) {
            const resetAction = StackActions.reset({
                index: 0,
                actions: [
                    NavigationActions.navigate({ routeName: 'HomeScreen' })],

            });
            this.props.navigation.dispatch(resetAction);
        }
        if (this.state.totalDiscount > 0) {
            if (this.state.switchValue == true) {
                for (let index = 0; index < addedDetails.length; index++) {
                    temp = temp + addedDetails[index].price
                    subTotal = subTotal + this.state.addedDetails[index].fullTotal
                }
                tot = temp - this.state.totalDiscount
                this.setState({
                    subTotal: subTotal,
                    addedDetails: addedDetails,
                    grandTotal: tot,
                });
            } else {
                for (let index = 0; index < addedDetails.length; index++) {
                    tot = tot + addedDetails[index].price - (addedDetails[index].price * this.state.totalDiscount / 100)
                    subTotal = subTotal + this.state.addedDetails[index].fullTotal
                }
                this.setState({
                    subTotal: subTotal,
                    addedDetails: addedDetails,
                    grandTotal: tot,
                });
            }

        } else {
            for (let index = 0; index < addedDetails.length; index++) {
                if (addedDetails[index].discountPercentage > 1) {
                    tot = tot + addedDetails[index].total
                    subTotal = subTotal + this.state.addedDetails[index].fullTotal
                } else {
                    tot = tot + addedDetails[index].total
                    subTotal = subTotal + this.state.addedDetails[index].fullTotal
                }

            }
        }
        this.setState({
            subTotal: subTotal,
            addedDetails: addedDetails,
            grandTotal: tot,
        });

    }

    //when update qty,this function work for (qty*total)
    updateQty() {
        this.refs.modal6.close();
        for (let index = 0; index < this.state.addedDetails.length; index++) {
            if (this.state.addedDetails[index].itemId == obj.itemId && this.state.addedDetails[index].batchId == obj.batchId) {
                if (this.state.addedDetails[index].discountPercentage > 1 || this.state.addedDetails[index].discountAmount > 1) {

                    if (this.state.switchValue2 == true) {
                        this.state.addedDetails[index].qty = parseInt(this.state.textQty)
                        let tot1 = this.state.addedDetails[index].qty * this.state.addedDetails[index].price
                        this.state.addedDetails[index].fullTotal = tot1
                        let finalAmount1 = tot1 - this.state.addedDetails[index].discountAmount
                        this.state.addedDetails[index].total = finalAmount1
                        this.state.addedDetails[index].salePrice = this.state.addedDetails[index].price - this.state.addedDetails[index].discountAmount
                        var tot = 0;
                        var sub = 0;
                        for (let index = 0; index < this.state.addedDetails.length; index++) {
                            tot = tot + this.state.addedDetails[index].total;
                            sub = sub + this.state.addedDetails[index].fullTotal
                        }
                        this.state.grandTotal = tot
                        this.state.subTotal = sub

                    } else {

                        this.state.addedDetails[index].qty = parseInt(this.state.textQty)
                        let tot2 = this.state.addedDetails[index].qty * this.state.addedDetails[index].price
                        this.state.addedDetails[index].fullTotal = tot2
                        let finalAmount2 = tot2 - (tot * this.state.addedDetails[index].discountPercentage / 100)
                        this.state.addedDetails[index].total = finalAmount2
                        this.state.addedDetails[index].salePrice = this.state.addedDetails[index].price - (this.state.addedDetails[index].price * this.state.addedDetails[index].discountPercentage / 100)
                        var tot = 0;
                        var sub = 0;
                        for (let index = 0; index < this.state.addedDetails.length; index++) {
                            tot = tot + this.state.addedDetails[index].total;
                            sub = sub + this.state.addedDetails[index].fullTotal
                        }
                        this.state.grandTotal = tot
                        this.state.subTotal = sub
                    }

                } else {
                    this.state.addedDetails[index].qty = parseInt(this.state.textQty)
                    this.state.addedDetails[index].total = obj.price * this.state.addedDetails[index].qty
                    this.state.addedDetails[index].fullTotal = obj.price * this.state.addedDetails[index].qty
                    // this.state.addedDetails[index].salePrice = this.state.addedDetails[index].price
                    var tempTot = 0;
                    var tempSub = 0;
                    if (this.state.totalDiscount > 0) {

                        if (this.state.switchValue == true) {
                            for (let index = 0; index < this.state.addedDetails.length; index++) {
                                tempSub = tempSub + this.state.addedDetails[index].fullTotal
                                tempTot = tempSub - this.state.totalDiscount

                            }
                            this.setState({
                                grandTotal: tempTot,
                                subTotal: tempSub
                            })
                        } else {

                            for (let index = 0; index < this.state.addedDetails.length; index++) {
                                tempTot = tempTot + this.state.addedDetails[index].total - (this.state.addedDetails[index].total * this.state.totalDiscount / 100)
                                tempSub = tempSub + this.state.addedDetails[index].fullTotal
                            }
                            this.setState({
                                grandTotal: tempTot,
                                subTotal: tempSub
                            })
                        }


                    } else {
                        for (let index = 0; index < this.state.addedDetails.length; index++) {
                            tempTot = tempTot + this.state.addedDetails[index].total
                            tempSub = tempSub + this.state.addedDetails[index].fullTotal
                        }
                        this.setState({
                            grandTotal: tempTot,
                            subTotal: tempSub
                        })

                    }

                }
            }
        }

        this.setState({
            slideAnimationDialog2: false,
        });
    }

    //invidual discount calculation
    setDiscountForOneItem() {
        this.setState({
            valueVisible: false,
            precentageVisible: true,
            switchValue: false
        })
        this.refs.modal5.close();
        if (this.state.totalDiscount > 0) {
            if (this.state.switchValue2 == true) {
                for (let index = 0; index < this.state.addedDetails.length; index++) {
                    if (this.state.addedDetails[index].itemId == obj2.itemId && this.state.addedDetails[index].batchId == obj2.batchId) {
                        if (this.state.text >= this.state.addedDetails[index].price) {
                            Messages.messageName(Strings.WARNING, Strings.DISCOUNT_FAILD, Strings.ICON[0], Strings.TYPE[0]);
                        } else {

                            this.state.addedDetails[index].customFields = { "type": "value" }

                            this.state.addedDetails[index].discountPercentage = this.state.text
                            var tot1 = this.state.addedDetails[index].qty * this.state.addedDetails[index].total
                            this.state.addedDetails[index].total = tot1 - this.state.text
                            var temp1 = this.state.text * 100 / tot1
                            this.state.addedDetails[index].salePrice = this.state.addedDetails[index].price - (this.state.addedDetails[index].price * temp1 / 100)
                            // this.state.addedDetails[index].salePrice = tot1 - this.state.text
                        }

                    }
                    console.log("ssss" + JSON.stringify(this.state.addedDetails))
                    let subTot = 0;
                    let tot = 0;
                    for (let index = 0; index < this.state.addedDetails.length; index++) {
                        subTot = this.state.addedDetails[index].qty * this.state.addedDetails[index].price
                        tot = tot + this.state.addedDetails[index].total

                    }
                    this.setState({
                        grandTotal: tot,
                        subTotal: subTot,
                        totalDiscount: 0
                    })

                }

            } else {
                if (this.state.text >= 100) {
                    Messages.messageName(Strings.WARNING, Strings.DISCOUNT_FAILD, Strings.ICON[0], Strings.TYPE[0]);
                } else {
                    for (let index = 0; index < this.state.addedDetails.length; index++) {

                        if (this.state.addedDetails[index].itemId == obj2.itemId && this.state.addedDetails[index].batchId == obj2.batchId) {
                            this.state.addedDetails[index].discountPercentage = parseInt(this.state.text)
                            this.state.addedDetails[index].customFields = { "type": "precentage" }

                            var tot3 = this.state.addedDetails[index].qty * this.state.addedDetails[index].total
                            let finalAmount = tot3 - (tot3 * this.state.text / 100)
                            this.state.addedDetails[index].total = finalAmount
                            this.state.addedDetails[index].salePrice = this.state.addedDetails[index].price - (this.state.addedDetails[index].price * this.state.text / 100)

                        }
                        let subTot = 0;
                        let tot = 0;
                        for (let index = 0; index < this.state.addedDetails.length; index++) {
                            subTot = this.state.addedDetails[index].qty * this.state.addedDetails[index].price
                            tot = tot + this.state.addedDetails[index].total

                        }
                        this.setState({
                            grandTotal: tot,
                            subTotal: subTot,
                            totalDiscount: 0
                        })
                    }
                }

            }


        } else {

            if (this.state.switchValue2 == true) {
                for (let index = 0; index < this.state.addedDetails.length; index++) {
                    if (this.state.addedDetails[index].itemId == obj2.itemId && this.state.addedDetails[index].batchId == obj2.batchId) {
                        if (this.state.text >= this.state.addedDetails[index].price) {
                            Messages.messageName(Strings.WARNING, Strings.DISCOUNT_FAILD, Strings.ICON[0], Strings.TYPE[0]);
                        } else {
                            var temp;
                            var TPrice;
                            this.state.addedDetails[index].customFields = { "type": "value" }

                            var tot4 = this.state.addedDetails[index].qty * this.state.addedDetails[index].price
                            temp = this.state.text * 100 / this.state.addedDetails[index].total
                            TPrice = this.state.addedDetails[index].total * temp / 100

                            this.state.addedDetails[index].discountPercentage = 0
                            this.state.addedDetails[index].discountAmount = TPrice
                            this.state.addedDetails[index].total = tot4 - this.state.text
                            this.state.addedDetails[index].salePrice = this.state.addedDetails[index].price - (this.state.addedDetails[index].price * temp / 100)
                            console.log(this.state.addedDetails[index].salePrice)
                        }
                    }
                    let subTot = 0;
                    let tot = 0;
                    for (let index = 0; index < this.state.addedDetails.length; index++) {
                        subTot = this.state.addedDetails[index].qty * this.state.addedDetails[index].price
                        tot = tot + this.state.addedDetails[index].total


                    }
                    this.setState({
                        grandTotal: tot,
                        subTotal: subTot,
                        totalDiscount: 0
                    })

                }

            } else {
                if (this.state.text >= 100) {
                    Messages.messageName(Strings.WARNING, Strings.DISCOUNT_FAILD, Strings.ICON[0], Strings.TYPE[0]);
                } else {
                    for (let index = 0; index < this.state.addedDetails.length; index++) {
                        if (this.state.addedDetails[index].itemId == obj2.itemId && this.state.addedDetails[index].batchId == obj2.batchId) {
                            this.state.addedDetails[index].discountPercentage = parseInt(this.state.text)
                            this.state.addedDetails[index].customFields = { "type": "precentage" }
                            var tot5 = this.state.addedDetails[index].qty * this.state.addedDetails[index].price
                            let finalAmount = tot5 - (tot5 * this.state.text / 100)
                            this.state.addedDetails[index].salePrice = this.state.addedDetails[index].price - (this.state.addedDetails[index].price * this.state.text / 100)
                            this.state.addedDetails[index].total = finalAmount

                        }
                    }
                    let subTot1 = 0;
                    let tot1 = 0;
                    for (let index = 0; index < this.state.addedDetails.length; index++) {
                        subTot1 = subTot1 + this.state.addedDetails[index].fullTotal
                        tot1 = tot1 + this.state.addedDetails[index].total
                    }
                    this.setState({
                        grandTotal: tot1,
                        subTotal: subTot1,
                    })

                }
            }

        }
    }

    //number pad show (invidual item discount)
    oneItemDiscount(item) {


        if (item.customFields.type == "precentage") {
            this.refs.modal5.open()
            this.setState({
                value: "%",
                valueVisible2: false,
                precentageVisible2: true,
                switchValue2: false,
                text: ""

            })

        } else if (item.customFields.type == "value") {
            this.refs.modal5.open()
            this.setState({
                value: "Rs.",
                oneitemtext: this.financial(item.discountAmount),
                valueVisible2: true,
                precentageVisible2: false,
                switchValue2: true,
                text: ""
            })

        } else {
            this.refs.modal5.open()
            this.setState({
                value: "%",
                oneitemtext: item.discountPercentage,
                valueVisible2: false,
                precentageVisible2: true,
                switchValue2: false,
                text: ""
            })

        }
        console.log("item" + JSON.stringify(item.customFields.type))
        this.setState({ showTheThing: false, Text: "" })
        obj2 = item
        // this.state.oneitemtext = item.discountPercentage
        // this.refs.modal5.open()

    }

    //number pad show (Qty)
    changeQty(item) {
        this.setState({ showTheThing: false })
        console.log("item" + JSON.stringify(item))
        this.state.itemqty = item.qty
        this.animaQty()
        obj = item;

    }

    financial(x) {
        return Number.parseFloat(x).toFixed(2);
    }

    openAddNoteModel(item) {
        obj3 = item
        console.log(JSON.stringify(obj3.customFields.itemNote))
        this.setState({
            itemNote: obj3.customFields.itemNote,
            scaleAnimationDialog: true,
        })
    }

    addNoteToItem() {

        for (let index = 0; index < this.state.addedDetails.length; index++) {
            if (this.state.addedDetails[index].itemId == obj3.itemId && this.state.addedDetails[index].batchId == obj3.batchId) {
                this.state.addedDetails[index].customFields = { itemNote: this.state.itemNote }
            }

        } console.log(JSON.stringify(this.state.addedDetails))
        console.log(JSON.stringify(obj3.customFields.itemNote))
        this.setState({ scaleAnimationDialog: false })

    }

    //navigate to home screen 
    navigateToHome() {
        this.props.navigation.navigate('HomeScreen', {
            details: this.state.addedDetails,
            totalDiscount: this.state.totalDiscount,
            subTot: this.state.subTotal,
            switchValue: this.state.switchValue
        });
    }

    //navigate to payment screen
    navigateToPayment() {
        if (this.state.addedDetails.length == 0) {
            Messages.messageName(Strings.WARNING, Strings.EMPTY_CURT, Strings.ICON[0], Strings.TYPE[0], Colors.DARK_BLACK_TEXT_COLOR);
        } else {
            console.log("cartttttttttttttttttt" + JSON.stringify(this.state.addedDetails))
            this.props.navigation.navigate('PaymentScreen', {
                orderDetails: this.state.addedDetails,
                subTot: this.state.subTotal,
                grandTot: this.state.grandTotal,
                discount: this.state.totalDiscount,
                switchValue: this.state.switchValue,
                value: this.state.value

            });
        }


    }

    clearDiscountItemWise() {
        this.setState({
            oneitemtext: 0
        })
        for (let index = 0; index < this.state.addedDetails.length; index++) {
            if
                (this.state.addedDetails[index].itemId == obj2.itemId) {
                included = true;
                this.state.addedDetails[index].discountPercentage = 0
                let finalAmount = this.state.addedDetails[index].price - (this.state.addedDetails[index].price * 0 / 100)
                this.state.addedDetails[index].total = finalAmount

            }
        }
        this.calculatediscountTotal()
    }

    _storeData(value) {

        if (value == true) {
            this.setState({
                value: "Rs.",
                valueVisible: true,
                precentageVisible: false
            })
        } else {
            this.setState({
                value: "%",
                valueVisible: false,
                precentageVisible: true

            })

        }
        this.setState({
            switchValue: value,
        })

    }
    _storeData2(value) {
        if (value == true) {
            this.setState({
                value: "Rs.",
                valueVisible2: true,
                precentageVisible2: false
            })
        } else {
            this.setState({
                value: "%",
                valueVisible2: false,
                precentageVisible2: true

            })

        }
        this.setState({
            switchValue2: value,
        })
    }

    clearInvoiceDiscount() {
        var tot = 0;
        for (let index = 0; index < this.state.addedDetails.length; index++) {
            this.state.addedDetails[index].discountPercentage = 0
            this.state.addedDetails[index].total = this.state.addedDetails[index].qty * this.state.addedDetails[index].price
            tot = tot + this.state.addedDetails[index].fullTotal

        }

        this.setState({
            grandTotal: tot,
            text: "",
            subTotal: tot,
            totalDiscount: 0
        })

    }

    clearOneItemDiscount() {
        for (let index = 0; index < this.state.addedDetails.length; index++) {
            if (this.state.addedDetails[index].itemId == obj2.itemId) {
                this.state.addedDetails[index].discountPercentage = 0
                this.state.addedDetails[index].discountAmount = 0
                this.state.addedDetails[index].customFields = {}
                this.state.addedDetails[index].total = this.state.addedDetails[index].qty * this.state.addedDetails[index].price
                this.state.addedDetails[index].salePrice = this.state.addedDetails[index].total
            }
        }

        let subTot1 = 0;
        let tot1 = 0;
        for (let index = 0; index < this.state.addedDetails.length; index++) {
            subTot1 = subTot1 + this.state.addedDetails[index].fullTotal
            tot1 = tot1 + this.state.addedDetails[index].fullTotal

        }
        this.setState({
            grandTotal: tot1,
            subTotal: subTot1,
        })
        this.clearText()

    }

    closeOneItemModel() {
        this.refs.modal5.close()
    }
    closeOneItemModel2() {
        this.refs.modal4.close()
    }
    closeOneItemModel6() {
        this.refs.modal6.close()
    }
    render() {
        return (

            <ImageBackground
                source={require("../../assets/newBack2.png")}
                style={{ width: "100%", height: "100%" }}>

                <View style={{ backgroundColor: '#126F85', width: '100%', height: 60, borderBottomLeftRadius: 50 }}>

                    <View numberOfLines={1} style={styles.header}>
                        <SafeAreaView style={{ left: -90, top: 8 }}>
                            <View style={{ backgroundColor: '#126F85', width: 50, borderRadius: 60, position: "absolute", }}>
                                <TouchableOpacity onPress={() => this.navigateToHome()}>
                                    <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', elevation: 10, }}>
                                        <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                            <Image source={require("../../assets/newBack3.png")} style={{ width: 20, height: 20 }}></Image>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <Text style={styles.totalTextStyle2}>Sub Total   : {this.financial(this.state.subTotal)}</Text>
                            {this.state.precentageVisible &&
                                <Text style={styles.totalTextStyle}>Total Discount (%) : {this.state.totalDiscount}</Text>
                            }
                            {this.state.valueVisible &&
                                <Text style={styles.totalTextStyle}>Total Discount (Rs) : {this.state.totalDiscount}</Text>
                            }
                        </SafeAreaView>
                    </View>
                    <View style={{ backgroundColor: '#126F85', top: 13, right: 25, position: "absolute", }}>
                        <Text style={styles.totalTextStyle1}>Total :  {this.financial(this.state.grandTotal)}</Text>
                    </View>

                </View>

                <StatusBar barStyle="light-content" hidden={false} backgroundColor={Colors.PRIMARY_COLOR} />
                <Modal style={[styles.modal4]} position={"bottom"} ref={"modal4"} backdropPressToClose={false}
                    onClosed={this.onClose}
                >

                    <View style={styles.container2}>
                        <View style={{
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}>
                            <View style={{ position: "absolute", width: 50, bottom: 30 }}>
                                <TouchableOpacity style={{ justifyContent: 'flex-end', alignItems: 'flex-end', top: 10 }} onPress={() => this.closeOneItemModel2()}>
                                    <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: '#ffff', borderRadius: 100, }}>
                                        <Image style={{ width: 40, height: 40 }} source={require('../../assets/closemodl.png')} />
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={{ position: "absolute", width: 70, bottom: 10, right: 65 }}>
                                <Switch
                                    //  tintColor="transparent"
                                    thumbTintColor={this.state.switchValue
                                        ? Colors.PRIMARY_COLOR : "#cccccc"}
                                    style={{ left: 10, transform: [{ scaleX: 1.5 }, { scaleY: 1.5 }] }}
                                    value={this.state.switchValue}
                                    onValueChange={(switchValue) => this._storeData(switchValue)} />
                            </View>
                            <Text style={{ color: Colors.PRIMARY_COLOR, fontSize: 20, textAlign: 'left', fontWeight: 'bold', top: 25 }}>Discount</Text>
                            <View style={{ position: "absolute", left: 30, top: 20, width: 80, }}>
                                {this.state.precentageVisible &&
                                    <Text style={{ color: Colors.PRIMARY_COLOR, fontSize: 20, position: "absolute", textAlign: "center" }}>{this.state.totalDiscount}<Text style={{ color: Colors.PRIMARY_COLOR, fontSize: 20 }} >{this.state.value}</Text></Text>
                                }
                                {this.state.valueVisible &&
                                    <Text style={{ color: Colors.PRIMARY_COLOR, fontSize: 20, position: "absolute", textAlign: "center" }}>{this.state.value}<Text style={{ color: Colors.PRIMARY_COLOR, fontSize: 20 }} >{this.state.totalDiscount}</Text></Text>
                                }
                                <View style={{ position: "absolute", right: -20 }}>
                                    <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', }} onPress={() => this.clearInvoiceDiscount()}>
                                        <View style={{ justifyContent: 'center', alignItems: 'center', width: 25, height: 25, backgroundColor: '#ffff', borderRadius: 50, elevation: 10, }}>
                                            <Image style={{ width: 20, height: 20 }} source={require('../../assets/cleardiscount.png')} />
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <Text numberOfLines={1} style={styles.textInput}>{this.state.text}</Text>
                        </View>
                        <VirtualKeyboard pressMode='string' cellStyle={{ width: 50, height: 50 }} onPress={(val) => this.changeText(val)} />
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <TouchableOpacity style={styles.calButton} onPress={() => this.calculateDiscount()}>
                                <View style={{
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                    {/* <Image source={require("../../assets/right.png")} style={styles.ToucherbleIconStyle4} /> */}
                                    <Text style={{ fontSize: 20, color: '#ffff', textAlign: "center", top: 10, fontWeight: "bold" }}>SUBMIT</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                <Modal style={[styles.modal4]} position={"bottom"} ref={"modal5"}
                    onClosed={this.onClose} backdropPressToClose={false}
                >
                    <View style={styles.container2}>
                        <View style={{
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}>

                            <View style={{ position: "absolute", width: 50, bottom: 30 }}>
                                <TouchableOpacity style={{ justifyContent: 'flex-end', alignItems: 'flex-end', top: 10 }} onPress={() => this.closeOneItemModel()}>
                                    <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: '#ffff', borderRadius: 100, }}>
                                        <Image style={{ width: 40, height: 40 }} source={require('../../assets/closemodl.png')} />
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={{ position: "absolute", width: 70, bottom: 10, right: 65 }}>
                                <Switch
                                    //  tintColor="transparent"
                                    thumbTintColor={this.state.switchValue2
                                        ? Colors.PRIMARY_COLOR : "#cccccc"}
                                    style={{ left: 10, transform: [{ scaleX: 1.5 }, { scaleY: 1.5 }] }}
                                    value={this.state.switchValue2}
                                    onValueChange={(switchValue2) => this._storeData2(switchValue2)} />
                            </View>
                            <Text style={{ color: Colors.PRIMARY_COLOR, fontSize: 20, textAlign: 'left', fontWeight: 'bold', top: 25 }}>Discount</Text>
                            <View style={{ position: "absolute", left: 30, top: 20, width: 80, }}>
                                {this.state.precentageVisible2 &&
                                    <Text style={{ color: Colors.PRIMARY_COLOR, fontSize: 20, position: "absolute", textAlign: "center" }}>{this.state.oneitemtext}<Text style={{ color: Colors.PRIMARY_COLOR, fontSize: 20 }} >{this.state.value}</Text></Text>
                                }
                                {this.state.valueVisible2 &&
                                    <Text style={{ color: Colors.PRIMARY_COLOR, fontSize: 20, position: "absolute", textAlign: "center" }}>{this.state.value}<Text style={{ color: Colors.PRIMARY_COLOR, fontSize: 20 }} >{this.state.oneitemtext}</Text></Text>
                                }
                                <View style={{ position: "absolute", right: -20 }}>
                                    <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', }} onPress={() => this.clearOneItemDiscount()}>
                                        <View style={{ justifyContent: 'center', alignItems: 'center', width: 25, height: 25, backgroundColor: '#ffff', borderRadius: 50, elevation: 10, }}>
                                            <Image style={{ width: 20, height: 20 }} source={require('../../assets/cleardiscount.png')} />
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <Text numberOfLines={1} style={styles.textInput}>{this.state.text}</Text>
                        </View>
                        <VirtualKeyboard pressMode='string' cellStyle={{ width: 50, height: 50 }} onPress={(val) => this.changeText3(val)} />
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <TouchableOpacity style={styles.calButton} onPress={() => this.setDiscountForOneItem()}>
                                <View style={{
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                    {/* <Image source={require("../../assets/right.png")} style={styles.ToucherbleIconStyle4} /> */}
                                    <Text style={{ fontSize: 20, color: '#ffff', textAlign: "center", top: 10, fontWeight: "bold" }}>SUBMIT</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>

                </Modal>
                <Modal style={[styles.modal4]} position={"bottom"} ref={"modal6"}
                    onClosed={this.onClose}
                >

                    <View style={styles.container2}>
                        <View style={{
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}>
                            <View style={{ position: "absolute", width: 50, bottom: 30 }}>
                                <TouchableOpacity style={{ justifyContent: 'flex-end', alignItems: 'flex-end', top: 10 }} onPress={() => this.closeOneItemModel6()}>
                                    <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: '#ffff', borderRadius: 100, }}>
                                        <Image style={{ width: 40, height: 40 }} source={require('../../assets/closemodl.png')} />
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <Text style={{ color: Colors.PRIMARY_COLOR, fontSize: 20, textAlign: 'left', fontWeight: 'bold', top: 25 }}>Quantity</Text>
                            <Text style={{ color: Colors.PRIMARY_COLOR, fontSize: 20, position: "absolute", top: 25, left: 20 }}>{this.state.itemqty}</Text>
                            <View style={{ position: "absolute", left: 62 }}>
                                {/* <TouchableOpacity style={{ justifyContent: 'flex-end', alignItems: 'flex-end', top: 10 }} onPress={() => Alert.alert("working..!")}>
                                    <View style={{ justifyContent: 'center', alignItems: 'center', width: 25, height: 25, backgroundColor: '#ffff', borderRadius: 50, elevation: 10, }}>
                                        <Image style={{ width: 20, height: 20 }} source={require('../../assets/cleardiscount.png')} />
                                    </View>
                                </TouchableOpacity> */}
                            </View>
                            <Text numberOfLines={1} style={styles.textInput}>{this.state.textQty}</Text>
                        </View>
                        <VirtualKeyboard pressMode='string' cellStyle={{ width: 50, height: 50 }} onPress={(val) => this.changeTextQty(val)} />
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <TouchableOpacity style={styles.calButton} onPress={() => this.updateQty()}>
                                <View style={{
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                    {/* <Image source={require("../../assets/right.png")} style={styles.ToucherbleIconStyle4} /> */}
                                    <Text style={{ fontSize: 20, color: '#ffff', textAlign: "center", top: 10, fontWeight: "bold" }}>SUBMIT</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>

                </Modal>
                <View style={styles.container1}>

                    <View style={{ justifyContent: 'center', alignItems: 'center', }}>


                    </View>

                    <View style={{ height: 50 }}>
                        <View style={{
                            marginLeft: 7, marginRight: 7, backgroundColor: Colors.PRIMARY_COLOR,
                            height: 50, flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderTopEndRadius: 13, borderTopLeftRadius: 13
                        }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 17, color: '#FFFFFF', textAlign: 'left', left: -50 }}>Name</Text>
                            <Text style={{ fontWeight: 'bold', fontSize: 17, color: '#FFFFFF', left: -10 }}>Qty</Text>
                            <Text style={{ fontWeight: 'bold', fontSize: 17, color: '#FFFFFF', left: 25 }}>Price</Text>
                            <Text style={{ fontWeight: 'bold', fontSize: 17, color: '#FFFFFF', left: 40 }}>Total</Text>

                        </View>
                    </View>
                    <FlatList
                        itemDimension={300}
                        data={this.state.addedDetails}
                        extraData={this.state}
                        style={styles.gridView}
                        renderItem={({ item, index }) => (
                            <TouchableOpacity activeOpacity={0.8} onPress={() => this.openAddNoteModel(item)}>
                                <View style={{ justifyContent: 'center', alignItems: 'center', alignContent: "center" }}>
                                    <View style={[styles.itemContainer, { backgroundColor: Colors.BACKGROUD_WHITE, }]}>
                                        <Text numberOfLines={1} style={styles.itemName}>{item.name}</Text>
                                        <TouchableOpacity style={{ width: 50, top: -15, left: 100, backgroundColor: Colors.PRIMARY_COLOR }} onPress={() => this.changeQty(item)}>
                                            <View style={{ backgroundColor: Colors.PRIMARY_COLOR, width: 50, height: 50, }}>
                                                <Text style={styles.itemName3}>{item.qty} </Text>

                                            </View>
                                        </TouchableOpacity>
                                        <View style={{}}>
                                            <SafeAreaView>
                                                <Text style={styles.itemName2}>{item.price}  <Text numberOfLines={1} style={styles.itemprice}>{this.financial(item.total)}</Text></Text>
                                            </SafeAreaView>
                                        </View>

                                        <View style={{ top: 4 }}>
                                            <View style={{ alignItems: 'flex-end', left: 10, bottom: 84 }}>
                                                <TouchableOpacity onPress={() => this.deleteRow(index, item.total)} style={{}}>
                                                    <View style={{ backgroundColor: Colors.PRIMARY_COLOR, width: 50, height: hp('11%'), borderBottomRightRadius: 20, borderTopRightRadius: 20 }}>
                                                        <Image
                                                            style={styles.place1}
                                                            source={require('../../assets/re.png')}
                                                        />
                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                        <TouchableOpacity style={{ top: -140, left: 180, width: 60, height: 50, borderBottomColor: Colors.DARK_BLACK_TEXT_COLOR, borderBottomWidth: 2, }} onPress={() => this.oneItemDiscount(item)}>

                                            <Text style={styles.itemName6}>{(item.discountPercentage > 0) ? item.discountPercentage + "%" : (item.discountAmount > 0) ? this.financial(item.discountAmount) : 'Discount'}</Text>

                                        </TouchableOpacity>

                                    </View>
                                </View>
                            </TouchableOpacity>
                        )}
                    />
                    <Dialog
                        onTouchOutside={() => {
                            this.setState({ scaleAnimationDialog: false });
                        }}
                        width={0.9}
                        visible={this.state.scaleAnimationDialog}
                        dialogAnimation={new ScaleAnimation()}
                        onHardwareBackPress={() => {
                            this.setState({ scaleAnimationDialog: false });
                            return true;
                        }}
                        dialogTitle={
                            <DialogTitle
                                title="Add Note"
                                hasTitleBar={false}

                            />
                        }
                        actions={[
                            <DialogButton
                                text="DISMISS"
                                onPress={() => {
                                    this.setState({ scaleAnimationDialog: false });
                                }}
                                key="button-1"
                            />,
                        ]}>
                        <DialogContent>
                            <View>
                                <View style={styles.textAreaContainer}>
                                    <TextInput
                                        style={styles.textArea}
                                        underlineColorAndroid="transparent"
                                        placeholder="Type something"
                                        placeholderTextColor="#ffffff80"
                                        numberOfLines={5}
                                        multiline={true}
                                        value={this.state.itemNote}
                                        onChangeText={itemNote => this.setState({ itemNote })} />
                                </View>
                                <TouchableOpacity onPress={() => this.addNoteToItem()}>
                                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                        <View style={{ height: 50, width: 320, backgroundColor: Colors.PRIMARY_COLOR, borderRadius: 20 }}>
                                            <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 15 }}>
                                                <Text style={{ fontWeight: 'bold', color: 'white' }}>SUBMIT</Text>
                                            </View>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </DialogContent>
                    </Dialog>

                    {this.state.showTheThing &&
                        <View style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            flexDirection: 'row',
                        }}>
                            <Button
                                style={styles.button2}
                                onPress={() => this.showTotalDiscountModel()}
                                hasText
                                block
                                large
                                dark
                            >
                                <Text style={styles.loginText}>     Discount      </Text>
                            </Button>
                            <Button
                                style={styles.button3}
                                onPress={() => this.navigateToPayment()}
                                hasText
                                block
                                large
                                dark
                            >
                                <Text style={styles.loginText}>     Payment       </Text>
                            </Button>

                        </View>
                    }

                </View>

            </ImageBackground>

        );
    }
    changeText(newText) {
        this.setState({ text: newText });
    }

    changeText3(newText) {
        this.setState({ text: newText });
    }

    changeTextQty(newText) {
        this.setState({ textQty: newText });
    }
}
