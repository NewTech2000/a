import {
  PixelRatio,
  StyleSheet,
} from 'react-native';
import Colors from '../../resources/Colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Right } from 'native-base';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#ffff',

  },
  textAreaContainer: {
    borderColor: 'gray',
    borderWidth: 1,
    padding: 5,
    borderRadius:20,
    bottom:20,
  },
  textArea: {
    height: 100,
    justifyContent: "flex-start",
  },
  content: {
    flex: 1,
    justifyContent: 'flex-end',
    padding: PixelRatio.getPixelSizeForLayoutSize(12),
  },
  logoBack: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 150,
    width: 150,
    top: -30,
    borderRadius: 100,
    backgroundColor: '#ffffff90'
  },
  modal4: {
    height: 420,
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    backgroundColor: 'white'
  },


  gridView: {
    // marginTop: 1,
    flex: 1,
  },
  itemContainer: {
    borderRadius: 20,
    padding: 10,
    height: hp('11%'),
    width: wp('90%'),
    // left: 5,
    // right: 5,
    marginTop: 8,
    marginBottom:8,
    // marginLeft: -5,
    shadowColor: Colors.DARK_BLACK_TEXT_COLOR,
    shadowOpacity: 0.8,
    elevation: 7,
    shadowRadius: 30,
    shadowOffset: { width: 1, height: 13 }

  },
  button: {
    position: 'absolute',
    top: 20,
    backgroundColor: Colors.PRIMARY_COLOR,
    padding: 10,
  },
  button2: {
    // position: 'absolute',
    // top: 20,
    borderBottomLeftRadius: 15,
    borderTopLeftRadius: 15,
    backgroundColor: Colors.PRIMARY_COLOR,
    // padding: 10,
  },
  button3: {
    // position: 'absolute',
    // top: 20,
    borderBottomRightRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: Colors.PRIMARY_COLOR,
    // padding: 10,
  },
  buttonContainer1: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    // top: 0


  },
  button1: {
    position: 'absolute',
    top: 95,
    padding: 10,
  },
  loginText: {
    color: '#ffff',
    fontSize: 18,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 20,
    fontWeight: 'bold',
    alignItems: 'center',
  },

  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  itemName: {
    fontSize: 15,
    width: 100,
    textAlign:"center",
    color: Colors.DARK_BLACK_TEXT_COLOR,
    fontWeight: 'bold',
    // backgroundColor:'red'
  },
  TextItemStyle: {
    width: 70,
    height: 55,
    top: -120,
    left: -110
  },
  InputStyle: {
    fontSize: 15,
  },
  itemName2: {
    fontSize: 14,
    top: -70,
    left: 180,
    color: Colors.DARK_BLACK_TEXT_COLOR,
    fontWeight: 'bold',
    position:"absolute"
  },
  itemprice: {
    fontSize: 14,
    top: -70,
    left: 180,
    color: Colors.DARK_BLACK_TEXT_COLOR,
    fontWeight: 'bold',
    position:"absolute",
    
  },
  itemName4: {
    fontSize: 15,
    top: -70,
    left:210,
    color: Colors.DARK_BLACK_TEXT_COLOR,
    fontWeight: 'bold',
    position:"absolute"
  },
  itemName6: {
    fontSize: 12,
    top: 17,
    color: Colors.DARK_BLACK_TEXT_COLOR,
    // fontWeight: 'bold',
  },
  itemName3: {

    fontSize: 20,
    color: Colors.BACKGROUD_WHITE,
    fontWeight: 'bold',
    justifyContent: 'center',
    textAlign: 'center',
    top: 10
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
  MainContainer: {
    justifyContent: 'center',
    flex: 1,
    paddingTop: 30,
  },
  imageThumbnail: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 100,
  },
  textInputStyle: {
    height: 40,
    borderWidth: 1,
    paddingLeft: 10,
    borderColor: Colors.PRIMARY_COLOR,
    backgroundColor: 'white',
  },
  viewStyle: {
    justifyContent: 'center',
    // flex: 1,
    //  marginTop: 40,
    padding: 16,
    //top:-120
  },
  ProcessText: {
    fontSize: 18,
    fontWeight: 'bold',
    justifyContent: 'center',
    alignItems: 'center',
    color: 'white'
  },
  ProcessButton: {
    backgroundColor: Colors.PRIMARY_COLOR
  },
  inputTextContainer: {
    left: 15
  },
  buttonContainerProcess: {
    height: 40,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 30,
    marginTop: 20,
    width: 180,
    borderRadius: 10,
    shadowColor: Colors.DARK_BLACK_TEXT_COLOR,
    shadowOpacity: 0.8,
    elevation: 6,
    shadowRadius: 15,
    shadowOffset: { width: 1, height: 13 }
    // marginLeft:40,

  },
  input: {
    color: 'white',

    //   justifyContent:'center',
    //     alignItems:'center'
    left: 20
  },
  item: {
    marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
    backgroundColor: Colors.PRIMARY_COLOR,
    borderColor: Colors.PRIMARY_COLOR,
  },
  place: {
    width: 25,
    height: 25,
    left: 10
  },
  place1: {
    width: 20,
    height: 20,
    top: 30,
    left: 15

  },
  totalTextStyle: {
   
    color: Colors.BACKGROUD_WHITE,
    fontSize: 13,
    fontWeight: 'bold',
   left:33
    // left:-20,
    
  
     
     
  },
  totalTextStyle2: {
    left:33,
    color: Colors.BACKGROUD_WHITE,
    fontSize: 15,
    fontWeight: 'bold',
    
  },
  totalTextStyle1: {
    color: Colors.BACKGROUD_WHITE,
    fontSize: 18,
    fontWeight: 'bold',
    // left:-50,
    // top: 10
  },
  form: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  TextInputStyle2: {
    height: 40,
    borderBottomColor: Colors.DARK_BLACK_TEXT_COLOR,
    // marginLeft: 30,
    width: 200,
    // marginRight: 30,
    // top: -35,
    marginBottom: 10,
    borderBottomWidth: 1
  },
  backGround: {
    backgroundColor: Colors.PRIMARY_COLOR,

  },

  headerTextStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 25,
    left: 70,
    // fontWeight:'bold',
    color: 'white',
  },
  header: {
    justifyContent: 'center',
    alignItems: 'center',
    alignContent:"center",
    
  },
  headerStyle: {
    backgroundColor: Colors.PRIMARY_COLOR,
    shadowOpacity: 0.75,
    shadowRadius: 5,
    shadowOffset: { height: 0, width: 0 },
    alignContent:"center",
  },
  ToucherbleIconStyle: { color: Colors.PRIMARY_COLOR },
  ToucherbleIconStyle2: { height: 30, width: 30 },
  ToucherbleIconStyle4: { height: 40, width: 40, top: 5 },
  ToucherbleIconStyle3: { height: 20, width: 20 },

  container1: { flex: 1, padding: 16, },
  head: { height: 40, backgroundColor: Colors.PRIMARY_COLOR },
  text: { margin: 6, color: 'white' },
  row: { flexDirection: 'row', backgroundColor: Colors.FLAT_TEXT_COLOR },
  btn: { width: 58, height: 18, },
  btnText: { textAlign: 'center', color: '#fff' },
  buttonContainer: {
    // flex: ,
    // marginTop: PixelRatio.getPixelSizeForLayoutSize(60),
  },
  container2: {
    // flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    // backgroundColor: '#F5FCFF',
  },
  calButton: {
    backgroundColor: Colors.PRIMARY_COLOR,
    top: 10,
    width: 300,
    height: 50,
    borderRadius: 10,
  },
  textInput: {
    top:20,
    height: 34,
    fontSize: 22,
    fontWeight: 'bold'

  }

});


export default styles;