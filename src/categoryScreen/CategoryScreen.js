import React, { Component } from 'react';
import { View, TextInput, Image, TouchableOpacity, ImageBackground, StatusBar, AsyncStorage, BackHandler, PixelRatio, FlatList, Vibration, Alert, ScrollView,Dimensions } from 'react-native';
import {
    Item,
    Input,
    Label,
    Text,
    Header,
    Left,
    Right,
    Fab,
    Form
} from 'native-base';
import styles from './CategoryScreenStyle';
import Colors from '../../resources/Colors';
import NetInfo from "@react-native-community/netinfo";
import Modal from 'react-native-modalbox';
import Strings from '../../resources/Strings';
import Messages from '../../resources/Message';
import Spinner from "react-native-spinkit";
import Spinner2 from 'react-native-loading-spinner-overlay';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
 
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
 
const DURATION = 40;
 
export default class Category extends Component {
 
    constructor(props) {
        super(props);
        this.state = {
            searchText: '',
            showTheThing: true,
            categoryName: '',
            subCategoryName: '',
            description: '',
            subcategory: '',
            token: '',
            id: '',
            delid: '',
            data: [],
            spinner: false,
            Spinner2: false,
            nodata: false,
            nodata2: false,
            categoryId: 0,
            text: '',
            subcategoryName: "",
            addSubcategory: true,
            catValue: "",
            subCategory: [],
            subid: '',
 
        };
 
    }
 
    //getting auth code from AsyncStorage
    showData = async () => {
        let myArray = await AsyncStorage.getItem('myArray');
        let d = JSON.parse(myArray);
 
        this.setState({
            token: d
 
        })
        console.log(this.state.token);
        this.getAllCategories()
 
    }
    //when close the model(category  form)
    onClose = () => {
        this.setState({
            showTheThing: true
        })
    }
 
    //add category form show
    anima() {
        this.setState({ showTheThing: false
        })
        this.refs.modal2.open()
    }
 
    onButtonPress = () => {
 
    }
 
    handleBackButton = () => {
        this.props.navigation.goBack();
        return true;
    };


 
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }


 
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        this.showData()
 
    }
 
    modalclose() {
        this.refs.modal2.close()
    }
    //search word asign 
    search(text) {
        this.setState({
            searchText: text
        })
        // this.state.searchText = text;
        this.getAllCategories()
 
    }
 
    //fetching categories from api
    getAllCategories() {
        this.setState({
            spinner: true
        })
 
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + "/api/product/category?page=0&limit=100&text=" + this.state.searchText,
                    {
                        method: "GET",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },
                    }
                )
 
                    .then(resp => resp.json())
                    .then(responseJson => {
                        console.log("categories" + JSON.stringify(responseJson));
                        this.setState({
                            spinner: false
                        })
                        this.setState({
                            data: responseJson.content
                        })
                        if (responseJson.content.length == 0) {
                            this.setState({
                                nodata: true
                            })
                        } else {
                            this.setState({
                                nodata: false
                            })
                        }
                    })
 
                    .catch(error => {
                        console.log(error);
 
                        this.setState({
                            spinner: false
                        })
 
                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })
 
                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.getAllCategories()
 
                            }
                        },
                    ],
                    { cancelable: true }
                );
 
            }
        });
 
    }
 
    //save category (First)
    saveCategory() {
        this.setState({
            Spinner2: true,
        });
        var data = JSON.stringify({
            "description": this.state.description,
            "name": this.state.categoryName.trim()
        });
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + '/api/product/category',
                    {
                        method: "POST",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },
                        body: data
                    }
                )
 
                    .then(resp => resp.json())
                    .then(responseJson => {
                        console.log(JSON.stringify(responseJson));
                        this.setState({
                            id: responseJson.id,
                            Spinner2: false,
                            categoryName: ""
                        });
                        // this.saveSubCategory()
                        this.getAllCategories()
                        this.refs.modal2.close()
                    })
 
                    .catch(error => {
                        console.log(error);
                        this.setState({
                            Spinner2: false,
                        });
                        this.getAllCategories()
                        this.refs.modal2.close()
                        Messages.messageName(Strings.WARNING_FAILED, Strings.CATEGORY_ADDED_Fail, Strings.ICON[2], Strings.TYPE[2], Colors.SUCCESS_ALERT);
                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })
 
                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.saveCategory()
 
                            }
                        },
                    ],
                    { cancelable: true }
                );
            }
        });
    }
 
    //save Sub category (Second)
    saveSubCategory() {
        this.setState({
            Spinner2: true,
        });
        var data = JSON.stringify({
            "description": "",
            "name": this.state.subcategoryName.trim()
        });
 
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(Strings.BASE_URL + '/api/product/category/' + this.state.catValue + '/sub-category',
                    {
                        method: "POST",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },
                        body: data
                    }
                )
 
                    .then(resp => resp.json())
                    .then(responseJson => {
                        Messages.messageName(Strings.WARNING_SUCESS, Strings.CATEGORY_ADDED_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                        this.getAllCategories()
                        this.refs.modal5.close()
                        console.log(JSON.stringify(responseJson));
                        this.setState({
                            Spinner2: false,
                            subcategoryName:""
                        });
                    })
 
                    .catch(error => {
                        console.log(error);
                        this.setState({
                            Spinner2: false,
                            
                        });
                        
                        Messages.messageName(Strings.WARNING_FAILED, Strings.CATEGORY_ADDED_Fail, Strings.ICON[2], Strings.TYPE[2], Colors.WARNING_ALERT);
                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })
 
                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.saveSubCategory()
 
                            }
                        },
                    ],
                    { cancelable: true }
                );
 
            }
        });
 
    }
    // update Category
 
    updateCategory() {
        this.setState({
            Spinner2: true,
        });
        var data = JSON.stringify({
            "description": "",
 
            "name": this.state.categoryName
        });
 
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(Strings.BASE_URL + '/api/product/category/' + this.state.delid,
                    {
                        method: "PUT",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },
                        body: data
                    }
                )
 
                    .then(resp => resp.json())
                    .then(responseJson => {
                        Messages.messageName(Strings.WARNING_SUCESS, Strings.CATEGORY_ADDED_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                        this.getAllCategories()
                        // this.refs.modal7.close()
                        console.log(JSON.stringify(responseJson));
                        this.setState({
                            Spinner2: false
                        });
                    })
 
                    .catch(error => {
                        console.log(error);
                        this.setState({
                            Spinner2: false,
                        });
                        this.getAllCategories()
                        this.refs.modal3.close()
                        
                        Messages.messageName(Strings.WARNING_SUCESS, Strings.CATEGORY_UPDATE_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })
 
                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.updateCategory()
                                
                            }
                        },
                    ],
                    { cancelable: true }
                );
 
            }
        });
 
    }
    // update sub category
 
    updateSubCategory() {
        this.setState({
            Spinner2: true,
        });
        var data = JSON.stringify({
            "description": "",
            "id": this.state.catValue,
            "name": this.state.subcategoryName
        });
 
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(Strings.BASE_URL + '/api/product/category/sub-category/' + this.state.subid,
                    {
                        method: "PUT",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },
                        body: data
                    }
                )
 
                    .then(resp => resp.json())
                    .then(responseJson => {
                        Messages.messageName(Strings.WARNING_SUCESS, Strings.CATEGORY_ADDED_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
 
                        // this.refs.modal7.close()
                        console.log(JSON.stringify(responseJson));
                        this.setState({
                            Spinner2: false
                        });
                    })
 
                    .catch(error => {
                        console.log(error);
                        this.setState({
                            Spinner2: false,
                        });
                        this.refs.modal7.close()
                        Messages.messageName(Strings.WARNING_SUCESS, Strings.SUB_CATEGORY_UPDATE_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })
 
                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.updateSubCategory()
 
                            }
                        },
                    ],
                    { cancelable: true }
                );
 
            }
        });
 
    }
    // delete subcategory
    deleteSubcategory() {
        Alert.alert(
            'Confirm',
            'Are you sure you want to do this ?',
            [
                { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                {
                    text: 'Yes', onPress: () => {
 
                        this.setState({
                            spinner: true,
                        });
 
                        NetInfo.isConnected.fetch().then(isConnected => {
                            if (isConnected === true) {
                                fetch(
                                    Strings.BASE_URL + '/api/product/category/sub-category/' + this.state.subid,
                                    {
                                        method: "DELETE",
                                        headers: {
                                            Accept: "application/json",
                                            "Content-Type": "application/json",
                                            'Authorization': this.state.token
                                        },
                                    }
                                )
 
                                    .then(resp => resp.json())
                                    .then(responseJson => {
                                        this.setState({
                                            spinner: false,
 
                                        });
                                        console.log(JSON.stringify(responseJson));
                                        Messages.messageName(Strings.WARNING_SUCESS, Strings.SUBCATEGORY_DELETE_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                                        console.log(JSON.stringify(responseJson));
                                        this.gettingSubCategories()
                                        this.refs.modal7.close()
                                    })
 
                                    .catch(error => {
                                        this.setState({
                                            spinner: false,
                                        });
                                        console.log(error);
                                        this.refs.modal7.close()
                                        this.gettingSubCategories()
                                        // Messages.messageName(Strings.WARNING_SUCESS, Strings.ITEM_DELETE_FAIL, Strings.ICON[2], Strings.TYPE[2]);
                                        Messages.messageName(Strings.WARNING_SUCESS, Strings.SUBCATEGORY_DELETE_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                                        console.log(JSON.stringify(responseJson));
 
                                    });
 
                            } else {
                                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                                this.setState({
                                    spinner: false
                                })
 
                                Alert.alert(
                                    'Connection Error!',
                                    'Please check your connection and try again',
                                    [
                                        {
                                            text: 'Retry', onPress: () => {
                                                this.deleteSubcategory()
 
                                            }
                                        },
                                    ],
                                    { cancelable: true }
                                );
                            }
                        });
 
                    }
 
                },
            ],
            { cancelable: true }
        );
 
    }
    // delete category
 
    deletecategory() {
        Alert.alert(
            'Confirm',
            'Are you sure you want to do this ?',
            [
                { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                {
                    text: 'Yes', onPress: () => {
 
                        this.setState({
                            spinner: true,
                        });
 
                        NetInfo.isConnected.fetch().then(isConnected => {
                            if (isConnected === true) {
                                fetch(
                                    Strings.BASE_URL + '/api/product/category/' + this.state.delid,
                                    {
                                        method: "DELETE",
                                        headers: {
                                            Accept: "application/json",
                                            "Content-Type": "application/json",
                                            'Authorization': this.state.token
                                        },
                                    }
                                )
 
                                    .then(resp => resp.json())
                                    .then(responseJson => {
                                        this.setState({
                                            spinner: false,
 
                                        });
                                        console.log(JSON.stringify(responseJson));
 
                                        Messages.messageName(Strings.WARNING_SUCESS, Strings.SUBCATEGORY_DELETE_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                                        console.log(JSON.stringify(responseJson));
                                        this.getAllCategories()
                                        this.refs.modal3.close()
                                    })
 
                                    .catch(error => {
                                        this.setState({
                                            spinner: false,
                                        });
                                        console.log(error);
                                        this.refs.modal3.close()
                                        this.getAllCategories()
                                        // Messages.messageName(Strings.WARNING_SUCESS, Strings.ITEM_DELETE_FAIL, Strings.ICON[2], Strings.TYPE[2]);
                                        Messages.messageName(Strings.WARNING_SUCESS, Strings.CATEGORY_DELETE_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                                        console.log(JSON.stringify(responseJson));
 
                                    });
 
                            } else {
                                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                                this.setState({
                                    spinner: false
                                })
 
                                Alert.alert(
                                    'Connection Error!',
                                    'Please check your connection and try again',
                                    [
                                        {
                                            text: 'Retry', onPress: () => {
                                                this.deleteSubcategory()
 
                                            }
                                        },
                                    ],
                                    { cancelable: true }
                                );
                            }
                        });
 
                    }
 
                },
            ],
            { cancelable: true }
        );
 
    }
 
    //fetching Subcategory types from api
    gettingSubCategories(value) {
        this.setState({
            spinner: true,
        });
 
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + '/api/product/category/' + value + '/sub-category?page=0&limit=100',
                    {
                        method: "GET",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },
 
                    }
                )
                    .then(resp => resp.json())
                    .then(responseJson => {
                        console.log("aaaaaaaaaaaaaaaaaa" + JSON.stringify(responseJson.content))
                        this.setState({
                            spinner: false,
                        });
 
                        this.setState({
                            subCategory: responseJson.content
                        })
                        if (responseJson.content.length == 0) {
                            this.setState({
                                nodata2: true
                            })
                        } else {
                            this.setState({
                                nodata2: false
                            })
                        }
 
                    })
                    .catch(error => {
                        console.log(error);
                        this.setState({
                            spinner: false,
                        });
 
                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })
 
                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.getAllCategories()
 
                            }
                        },
                    ],
                    { cancelable: true }
                );
 
            }
        });
    }


 
    //delete category
 
    // deleteItem() {
    //     Alert.alert(
    //         'Confirm',
    //         'Are you sure you want to do this ?',
    //         [
    //             { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
    //             {
    //                 text: 'Yes', onPress: () => {
 
    //                     this.setState({
    //                         spinner: true,
    //                     });
 
    //                     fetch(
    //                         Strings.BASE_URL + "/api/product/category/" + this.state.delid,
    //                         {
    //                             method: "DELETE",
    //                             headers: {
    //                                 Accept: "application/json",
    //                                 "Content-Type": "application/json",
    //                                 'Authorization': this.state.token
    //                             },
    //                         }
    //                     )
 
    //                         .then(resp => resp.json())
    //                         .then(responseJson => {
    //                             this.setState({
    //                                 spinner: false,
    //                             });
    //                             this.refs.modal3.close()
    //                             this.state.delid = responseJson.id;
 
    //                             // Messages.messageName(Strings.WARNING_SUCESS, Strings.ITEM_DELETE_SUCCESS, Strings.ICON[1], Strings.TYPE[1]);
    //                             console.log(JSON.stringify(responseJson.id));
    //                            this.getAllCategories()
 
    //                         })
 
    //                         .catch(error => {
    //                             this.setState({
    //                                 spinner: false,
    //                             });
    //                             console.log(error);
 
    //                             // Messages.messageName(Strings.WARNING_SUCESS, Strings.ITEM_DELETE_FAIL, Strings.ICON[2], Strings.TYPE[2]);
    //                             // Messages.messageName(Strings.WARNING_SUCESS, Strings.ITEM_DELETE_SUCCESS, Strings.ICON[1], Strings.TYPE[1]);
    //                             console.log(JSON.stringify(responseJson));



 
    //                         });
 
    //                 }
    //             },
    //         ],
    //         { cancelable: true }
    //     );
 
    // }
 
    //////////////////////////
    //update categories
 
    // updateCategory() {
 
    //     this.setState({
    //         spinner: true,
    //     });
    //     var data = JSON.stringify({
 
    //         "description": "",
    //         "name": this.state.categoryName
    //     });
 
    //     fetch(
    //         Strings.BASE_URL + '/api/product/category/' + this.state.delid,
    //         {
    //             method: "PUT",
    //             headers: {
    //                 Accept: "application/json",
    //                 "Content-Type": "application/json",
    //                 'Authorization': this.state.token
    //             },
    //             body: data
    //         }
    //     )
 
    //         .then(resp => resp.json())
    //         .then(responseJson => {
    //             console.log(JSON.stringify(responseJson));
    //             this.setState({
    //                 spinner: false
    //             });
    //             this.refs.modal2.close()
    //             console.log("updateeeeeeeeeee" + JSON.stringify(responseJson));
    //         })
 
    //         .catch(error => {
    //             this.refs.modal4.close()
    //             console.log(error);
    //             this.setState({
    //                 spinner: false,
    //             });


 
    //         });
 
    // }




 
    ////////////////////////////
    editSelectedItem = (item) => {
        this.state.delid = item.id
        console.log("hjbhjhjg" + JSON.stringify(item))
 
        this.setState({
            categoryName: item.name,
 
        })
        this.setState({
            showTheThing: false
        })
        this.refs.modal3.open()
        Vibration.vibrate(DURATION);
 
    }
    onPressAlert() {
        Messages.messageName("Need", Strings.LONG_TAP, Strings.ICON[2], Strings.TYPE[2], Colors.DARK_BLACK_TEXT_COLOR);
    }
 
    // //long tap alert
    // onPressAlert() {
    //     Messages.messageName("Need", Strings.LONG_TAP, Strings.ICON[2], Strings.TYPE[2], Colors.DARK_BLACK_TEXT_COLOR);
    // }
 
    //no data iamge
    nodataImage() {
        if (this.state.nodata) {
            return (
                <View style={{ top: 60, justifyContent: 'center', alignItems: 'center', }}>
                    <Image style={{ width: 200, height: 200, borderRadius: 100 }}
                        source={require("../../assets/no_data_found.png")}
                    />
 
                </View>
            )
        }
    }
 
    nodataImage2() {
        if (this.state.nodata2) {
            return (
                <View style={{ bottom: 40, justifyContent: 'center', alignItems: 'center', }}>
                    <Image style={{ width: 100, height: 100, borderRadius: 100 }}
                        source={require("../../assets/no_data_found.png")}
                    />
 
                </View>
            )
        }
    }
 
    // validation
    validationSave() {
 
        if (this.categorynamevalidate(this.state.categoryName) == false  || this.state.categoryName == "" ) {
            Messages.messageName(Strings.CATEGORY_ADDED_Fail, Strings.CATEGORY_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        } else {
            this.saveCategory()
        }
    }
    validationSaveSubCategory() {
 
        if (this.subCategorynamevalidate(this.state.subcategoryName) == false || this.state.subcategoryName == "") {
            Messages.messageName(Strings.CATEGORY_ADDED_Fail, Strings.SUB_CATEGORY_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        } else {
            this.saveSubCategory()
        }
    }
 
    validationUpdateSubCategory() {
 
        if (this.subCategorynamevalidate(this.state.subcategoryName) == false || this.state.subcategoryName == "" ) {
            Messages.messageName(Strings.CATEGORY_ADDED_Fail, Strings.SUB_CATEGORY_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        } else {
            this.updateSubCategory()
        }
    }
 
    validationupdate() {
        if (this.categorynamevalidate(this.state.categoryName) == false || this.state.categoryName == "")  {
            Messages.messageName(Strings.CATEGORY_ADDED_Fail, Strings.CATEGORY_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        } else {
            this.updateCategory()
        }
    }
    modal3(item) {
        console.log("aaaaaaaaaaaaaaaaaa" + JSON.stringify(item))
        this.refs.modal6.open()
        this.gettingSubCategories(item.id)
 
        this.setState({
            showTheThing: false,
            catValue: item.id
        })
 
    }
    modal5() {
        this.setState({
            showTheThing: false,
 
        })
        this.refs.modal6.close()
        this.refs.modal5.open()
 
    }
    modal7(item) {
        this.setState({
            showTheThing: false,
            subid: item.id,
            subcategoryName: item.name
 
        })
        this.refs.modal6.close()
        this.refs.modal7.open()
 
    }
    closeModel5() {
        this.refs.modal5.close()
    }
    closeOneItemModel3() {
        this.refs.modal3.close()
    }
    closeOneItemModel7() {
        this.refs.modal7.close()
    }
    categorynamevalidate(text) {
        const reg = /^[a-zA-Z -]*$/;
        if (reg.test(text) === false) {
            return false;
        } else {
            return true;
        }
    }
    subCategorynamevalidate(text) {
        const reg = /^[a-zA-Z -]*$/;
        if (reg.test(text) === false) {
            return false;
        } else {
            return true;
        }
    }
    update(e){
        if(this.state.subcategoryName.trim()) this.props.dispatch(subcategoryName(this.state.subcategoryName)); 
        this.setState({subcategoryName: "" }); 
    }
    render() {
 
        return (
            <ImageBackground
                source={require("../../assets/newBack2.png")}
                style={{ width: "100%", height: "100%" }}>
                <StatusBar barStyle="light-content" hidden={false} backgroundColor={'#126F85'} />
                <View style={{ backgroundColor: '#126F85', width: '100%', height: 60, borderBottomLeftRadius: 50 }}>
                    <View style={{ backgroundColor: '#126F85', width: 50, top: 13, borderRadius: 60, left: 25, position: "absolute", top: 8 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('HomeScreen')}>
                            <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                    <Image source={require("../../assets/newBack3.png")} style={{ width: 20, height: 20 }}></Image>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Text style={{ fontFamily: 'Ubuntu-Medium', color: '#ffff', justifyContent: "center", textAlign: "center", fontSize: 20, position: "absolute", top: 10, left: 100, fontWeight: "bold" }}>Manage Category</Text>
 
                </View>
                <StatusBar barStyle="light-content" hidden={false} backgroundColor={'#126F85'} />
 
                <Modal style={[styles.modal4]} position={"bottom"} ref={"modal2"} onClosed={this.onClose} backdropPressToClose={false}>
                    <View style={{ position: "absolute", width: 50, right: 160 }}>
                        <TouchableOpacity style={{ justifyContent: 'flex-end', alignItems: 'flex-end', top: -20 }} onPress={() => this.modalclose()}>
                            <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: '#ffff', borderRadius: 100, }}>
                                <Image style={{ width: 40, height: 40 }} source={require('../../assets/closemodl.png')} />
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{
                        flex: 1,
                        justifyContent: 'flex-end',
                        padding: PixelRatio.getPixelSizeForLayoutSize(12),
                    }}>
                        {/* <View style={{ top: -10, }}>
                            <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', top: 30 }} onPress={() => this.modalclose()} >
                                <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: '#ffff', borderRadius: 100, }}>
                                    <Image style={{ width: 40, height: 40 }} source={require('../../assets/closemodl.png')} />
                                </View>
                            </TouchableOpacity>
 
                        </View> */}
 
                        <View style={{ top: 20 }}>
                            <Text style={{ color: Colors.PRIMARY_COLOR, fontSize: 20, textAlign: 'left', fontWeight: 'bold', top: -10 }}>Add Category</Text>
 
                        </View>
                        <View style={{}}>
                            <Item
                                style={styles.item2}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../assets/description.png')}
                                />
                                <Input
                                    style={styles.input}
                                    placeholder="Category"
                                    placeholderTextColor="#ffffff80"
                                    autoCapitalize="none"
                                    value={this.state.categoryName}
                                    onChangeText={categoryName => this.setState({ categoryName })}
                                />
                            </Item>
 
                        </View>
                    </View>
                    <View style={styles.inputTextContainer3}>
                        <TouchableOpacity
                            style={[styles.buttonContainerProcess, styles.ProcessButton]}
                            onPress={() => this.validationSave()}
                        >
                            <Text style={styles.ProcessText}>Save</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
 
                <Modal style={[styles.modal3]} position={"bottom"} ref={"modal3"} onClosed={this.onClose} backdropPressToClose={false} >
                    <View style={{ position: "absolute", width: 50, right: 150, }}>
                        <TouchableOpacity style={{ justifyContent: 'flex-end', alignItems: 'flex-end', top: -20 }} onPress={() => this.closeOneItemModel3()}>
                            <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: '#ffff', borderRadius: 100, }}>
                                <Image style={{ width: 40, height: 40 }} source={require('../../assets/closemodl.png')} />
                            </View>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity style={{ justifyContent: 'flex-end', alignItems: 'flex-end', position: "absolute", right: 10, top: 5 }} onPress={() => this.deletecategory()}>
                        {/* <View style={{ width: 50, }}> */}
 
                        <View style={{ justifyContent: 'center', alignItems: 'center', width: 50, height: 50, backgroundColor: Colors.PRIMARY_COLOR, borderRadius: 100 }}>
                            <Image style={{ width: 20, height: 20 }} source={require('../../assets/re.png')} />
                        </View>
 
                        {/* </View> */}
                    </TouchableOpacity>
 
                    <View style={{
                        flex: 1,
                        justifyContent: 'flex-end',
                        
                        padding: PixelRatio.getPixelSizeForLayoutSize(12),
                    }}>
 
                           
                            <Text style={{ color: Colors.PRIMARY_COLOR, fontSize: 20, textAlign: 'left', fontWeight: 'bold',top:15 }}>Update Category</Text>
 
                            
                        <Form>
                           
                           
                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../assets/category2.png')}
                                />
                                <Input
                                    style={styles.input}
                                    placeholder="Category"
                                    placeholderTextColor="#ffffff80"
                                    autoCapitalize="none"
                                    value={this.state.categoryName}
                                    onChangeText={categoryName => this.setState({ categoryName })}
 
                                />
                            </Item>
 
                            <View style={styles.inputTextContainer2}>
                                <TouchableOpacity
                                    style={[styles.buttonContainerProcess, styles.ProcessButton]}
                                    onPress={() => this.validationupdate()}
                                >
                                    <Text style={styles.ProcessText}>Update</Text>
                                </TouchableOpacity>
                            </View>
                        </Form>
                    </View>
 
                </Modal>
                {/* sub category add modal open */}
 
                <Modal style={[styles.modal3]} position={"bottom"} ref={"modal5"} onClosed={this.onClose} backdropPressToClose={false}>
                    <View style={{ position: "absolute", width: 50, right: 150, }}>
                        <TouchableOpacity style={{ justifyContent: 'flex-end', alignItems: 'flex-end', top: -20 }} onPress={() => this.closeModel5()}>
                            <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: '#ffff', borderRadius: 100, }}>
                                <Image style={{ width: 40, height: 40 }} source={require('../../assets/closemodl.png')} />
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{
                        flex: 1,
                        justifyContent: 'flex-end',
                        padding: PixelRatio.getPixelSizeForLayoutSize(12),
                    }}>
                        {/* <TouchableOpacity style={{ top: -20, justifyContent: "center", alignContent: "center", alignItems: "center" }} onPress={() => this.closeModel5()}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: '#ffff', borderRadius: 100, }}>
                            <Image style={{ width: 40, height: 40 }} source={require('../../assets/closemodl.png')} />
                        </View>
                    </TouchableOpacity>
                    <View style={{
                        flex: 1,
                        justifyContent: 'flex-end',
                       
                        padding: PixelRatio.getPixelSizeForLayoutSize(12),
                    }}> */}
                        {/* <View style={{ width: 50, justifyContent: 'flex-end', alignItems: 'flex-end', position: "absolute", right: 10, top: -30 }}>
                            <TouchableOpacity style={{}} onPress={() => this.deleteCustomer()}>
 
                                <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: Colors.PRIMARY_COLOR, borderRadius: 100 }}>
                                    <Image style={{ width: 20, height: 20 }} source={require('../../assets/re.png')} />
                                </View>
                            </TouchableOpacity>
                        </View> */}
                       
 
                            <Text style={{ color: Colors.PRIMARY_COLOR, fontSize: 20, textAlign: 'left', fontWeight: 'bold',top:15 }}>Add SubCategory</Text>
                       <Form>
 
                       
                        <Item
                            style={styles.item2}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/category2.png')}
                            />
                            <Input
                                style={styles.input}
                                placeholder="Sub Category"
                                placeholderTextColor="#ffffff80"
                                autoCapitalize="none"
                               
                                value={this.state.subcategoryName}
                                onChangeText={subcategoryName => this.setState({ subcategoryName })}
 
                            />
                        </Item>
                        </Form>
                    </View>
                    <View style={styles.inputTextContainer3}>
                        <TouchableOpacity
                            style={[styles.buttonContainerProcess, styles.ProcessButton]}
                            onPress={() => this.validationSaveSubCategory()}
                        >
                            <Text style={styles.ProcessText}>Submit</Text>
                        </TouchableOpacity>
                    </View>
                   
                </Modal>
 
                {/* add sub category modal close */}
 
                {/* SubCategory update modal */}
                <Modal style={[styles.modal3]} position={"bottom"} ref={"modal7"} onClosed={this.onClose} backdropPressToClose={false}>
                    <View style={{ position: "absolute", width: 50, right: 150, }}>
                        <TouchableOpacity style={{ justifyContent: 'flex-end', alignItems: 'flex-end', top: -20 }} onPress={() => this.closeOneItemModel7()}>
                            <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: '#ffff', borderRadius: 100, }}>
                                <Image style={{ width: 40, height: 40 }} source={require('../../assets/closemodl.png')} />
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{
                        flex: 1,
                        justifyContent: 'flex-end',
                        padding: PixelRatio.getPixelSizeForLayoutSize(12),
                    }}>
                        {/* <View style={{ width: 50, justifyContent: 'flex-end', alignItems: 'flex-end', position: "absolute", right: 10, top: -30 }}>
                            <TouchableOpacity style={{}} onPress={() => this.deleteSubcategory()}>
 
                                <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: Colors.PRIMARY_COLOR, borderRadius: 100 }}>
                                    <Image style={{ width: 20, height: 20 }} source={require('../../assets/re.png')} />
                                </View>
                            </TouchableOpacity>
                        </View> */}
 
                        <TouchableOpacity style={{ justifyContent: 'flex-end', alignItems: 'flex-end', position: "absolute", right: 10, top: 5 }} onPress={() => this.deleteSubcategory()}>
                            {/* <View style={{ width: 50, }}> */}
 
                            <View style={{ justifyContent: 'center', alignItems: 'center', width: 50, height: 50, backgroundColor: Colors.PRIMARY_COLOR, borderRadius: 100 }}>
                                <Image style={{ width: 20, height: 20 }} source={require('../../assets/re.png')} />
                            </View>
 
                            {/* </View> */}
                        </TouchableOpacity>
 
                        <Text style={{ color: Colors.PRIMARY_COLOR, fontSize: 20, textAlign: 'left', fontWeight: 'bold', top: 20 }}>Update SubCategory</Text>
 
                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/category2.png')}
                            />
                            <Input
                                style={styles.input}
                                placeholder="Sub Category"
                                placeholderTextColor="#ffffff80"
                                autoCapitalize="none"
                                value={this.state.subcategoryName}
                                onChangeText={subcategoryName => this.setState({ subcategoryName })}
 
                            />
                        </Item>
 
                    </View>
                    <View style={styles.inputTextContainer3}>
                        <TouchableOpacity
                            style={[styles.buttonContainerProcess, styles.ProcessButton]}
                            onPress={() => this.validationUpdateSubCategory()}
                        >
                            <Text style={styles.ProcessText}>Submit</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
 
                {/* close subctegory update modal */}
                <View style={styles.viewStyle}>
                    {/* <TextInput
                        style={styles.textInputStyle}
                        onChangeText={text => this.search(text)}
                        // value={this.state.text}
                        underlineColorAndroid="transparent"
                        placeholder="Search Here"
                        placeholderTextColor="#ffff"
                    /> */}
                    <Item
                        style={{
                            marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
                            backgroundColor: Colors.PRIMARY_COLOR,
                            borderColor: Colors.PRIMARY_COLOR,
                        }}
                        rounded
                        last
                    >
                        <Image
                            style={{
                                width: 25,
                                height: 25,
                                left: 10
                            }}
                            source={require('../../assets/search.png')}
                        />
 
                        <Input
                            style={{
                                color: 'white',
                                left: 10,
                                maxWidth: '82%',
                            }}
                            placeholder="Search"
                            placeholderTextColor="#ffffff80"
                            autoCapitalize="none"
                            onChangeText={text => this.search(text)}
                        />
 
                    </Item>
                </View>
                {/* <Label></Label>
                <Label></Label> */}
                {/* <View style={styles.viewStyle}>
                        <Image
                            style={styles.place}
                            source={require('../../assets/search.png')}
                        />
                        <TextInput
                            style={styles.textInputStyle}
                            onChangeText={text => this.search(text)}
                            value={this.state.text}
                            underlineColorAndroid="transparent"
                            placeholder="Search Here"
                            placeholderTextColor='white'
                        />
                    </View> */}
 
                <Modal style={[styles.longpressmodal, styles.longpressmodal3]} position={"center"} ref={"modal6"}>
 
                    <Text style={{ color: '#126F85', fontSize: 18, fontWeight: 'bold', top: 10, right: 5 }}>Sub Categories</Text>
                    {/* <Text style={{ color: '#126F85', fontSize: hp('2.0%'), top: 13, fontWeight: 'bold', right: 5 }}>{"BatchNo."}                {"Dis.Pre"}               {"Price"}</Text> */}
                    <ScrollView>
 
                        <FlatList
                            itemDimension={300}
                            data={this.state.subCategory}
                            style={styles.gridView3}
                            renderItem={({ item, index }) => (
 
                                <TouchableOpacity activeOpacity={0.8} onPress={() => this.modal7(item)}>
                                    <View style={[styles.itemContainermodal, { backgroundColor: 'white', borderColor: '#E5E4FD', borderWidth: 0.9, }]}>
                                        <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
                                            <Text style={{ color: '#74b5c4', fontSize: hp('2.0%'), top: 2, fontWeight: 'bold', right: 10 }}>{item.name}</Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
 
                            )}
 
                        />
 
                    </ScrollView>
                    {this.nodataImage2()}
                    <View style={{ width: '100%', backgroundColor: Colors.PRIMARY_COLOR, height: hp('8%') }}>
                        <TouchableOpacity onPress={() => this.modal5()}>
                            <View style={{ justifyContent: "center", alignContent: "center", alignItems: "center", top: 10 }}>
                                <Image source={require("../../assets/plus.png")} style={styles.ToucherbleIconStyle3} />
 
                            </View>
                        </TouchableOpacity>
 
                    </View>
 
                </Modal>
 
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Spinner
                        isVisible={this.state.spinner}
                        type={'ThreeBounce'}
                        textStyle={{ color: 'white' }}
                        size={70}
                        color={Colors.FLAT_TEXT_COLOR}
                    />
                </View>
                <Spinner2
                    visible={this.state.Spinner2}
                    textContent={'Loading...'}
                    textStyle={{ color: 'white' }}
                    overlayColor={"#0f0e0e90"}
                    animation={"fade"}
                    color={"#00CEFD"}
                />
                {this.nodataImage()}
                <FlatList
                    itemDimension={300}
                    data={this.state.data}
 
                    style={styles.gridView}
                    renderItem={({ item, index }) => (
                        <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', elevation: 6, }} activeOpacity={5} onPress={() => this.onPressAlert()} onLongPress={() => this.editSelectedItem(item)}>
                            <View style={[styles.itemContainer, { backgroundColor: Colors.BACKGROUD_WHITE }]}>
                                <Image style={styles.cardImage}
                                    source={require("../../assets/cat.png")}
                                />
                                <Text style={styles.itemName}>{item.name}</Text>
                                <View style={{ position: "absolute", right: 0 }}>
                                    <TouchableOpacity onPress={() => this.modal3(item)} activeOpacity={5}>
                                        <View style={{ width: 80, height: hp('7%'), backgroundColor: Colors.PRIMARY_COLOR, borderBottomRightRadius: 30, borderTopRightRadius: 30 }}>
                                            <Text style={{ color: '#ffff', fontSize: 12, fontWeight: "bold", textAlign: "center", justifyContent: "center", top: 15 }}>Section</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
 
                            </View>
 
                        </TouchableOpacity>
 
                    )}
                />
                {this.state.showTheThing &&
                    <Fab
                        active={this.state.active}
                        direction="up"
                        containerStyle={{}}
                        style={styles.backGround}
                        position="bottomRight"
                        onPress={() => this.anima()}>
                        <Image source={require("../../assets/plus.png")} style={styles.ToucherbleIconStyle2} />
                    </Fab>
                }
            </ImageBackground>
 
        );
    }
}
