import {
    PixelRatio,
    StyleSheet,
} from 'react-native';
import Colors from '../../resources/Colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#0a1142',
    },
    content: {
        flex: 1,
        justifyContent: 'flex-end',
        padding: PixelRatio.getPixelSizeForLayoutSize(12),
    },
    viewStyle2: {
        justifyContent: 'center',
        // marginTop: 10,
        top:-10
        // padding: ,
    },
    logoBack: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 150,
        width: 150,
        top: -30,
        borderRadius: 100,
        backgroundColor: '#ffffff90'
    },
    modal4: {
        height: hp('30%'),
        borderTopRightRadius: 30,
        borderTopLeftRadius: 30,
        backgroundColor: 'white'
    },

    modal3: {
        height: hp('28%'),
        borderTopRightRadius: 30,
        borderTopLeftRadius: 30,
        backgroundColor: 'white'
    },
    gridView: {
        // marginTop: 1,
        flex: 1,
        marginBottom: 30,


    },
    gridView3: {
        top: 10,
        flex: 1,
        marginBottom: 30,


    },
    itemContainer: {
        // justifyContent:'center',
        // alignItems:'center',
        // // justifyContent: 'flex-end',
        // borderRadius: 30,
        // borderColor:Colors.PRIMARY_COLOR,
        // borderWidth:2,
        // padding: 10,
        // height: 50,
        // shadowColor: Colors.DARK_BLACK_TEXT_COLOR,
        // shadowOpacity: 0.8,
        // elevation: 7,
        // shadowRadius: 30,
        // shadowOffset: { width: 1, height: 13 }
        flex: 1,
        padding: 10,
        height: hp('7%'),
        width: wp('90%'),
        // left: 5,
        borderRadius: 30,
        // borderColor: Colors.PRIMARY_COLOR,
        // borderWidth: 2,
        // right: 5,
        marginTop: 6,
        // marginLef;t: -5,
        marginBottom: 5,
        shadowColor: Colors.DARK_BLACK_TEXT_COLOR,
        shadowOpacity: 0.8,
        elevation: 7,
        shadowRadius: 30,
        shadowOffset: { width: 1, height: 13 }

    },

    cardImage: {
        width: 25,
        height: 25
    },
    itemName: {
        fontSize: hp('2.0%'),
        left: 40,
        top: -20,
        color: 'black',
        fontWeight: 'bold',
    },
    itemName2: {
        fontSize: hp('2.0%'),
        color: 'black',
        fontWeight: 'bold',
    },
    itemCode: {
        fontWeight: '600',
        fontSize: hp('1.9%'),
        color: '#fff',
    },
    MainContainer: {
        justifyContent: 'center',
        flex: 1,
        paddingTop: 30,
    },
    imageThumbnail: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 100,
    },
    textInputStyle: {
        height: 50,
        borderWidth: 1,
        paddingLeft: 15,
        borderRadius: 30,
        borderColor: Colors.PRIMARY_COLOR,
        backgroundColor: Colors.PRIMARY_COLOR,
    },
    viewStyle: {
        justifyContent: 'center',
        // flex: 1,
        //  marginTop: 40,
        padding: 16,
        //top:-120
    },
    ProcessText: {
        fontSize: 18,
        fontWeight: 'bold',
        justifyContent: 'center',
        alignItems: 'center',
        color: 'white'
    },
    ProcessButton: {
        backgroundColor: Colors.PRIMARY_COLOR
    },
    inputTextContainer: {
        left: 19
    },
    inputTextContainer2: {
        justifyContent: 'center',
        alignItems: 'center',
         top:40
    },
    inputTextContainer3: {
        justifyContent: 'center',
        alignItems: 'center',
         top:10
    },

    buttonContainerProcess: {
        height: 50,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 20,
        width: wp('80%'),
        left: -1,
        borderRadius: 30,
        shadowColor: Colors.DARK_BLACK_TEXT_COLOR,
        shadowOpacity: 0.8,
        elevation: 6,
        shadowRadius: 15,
        shadowOffset: { width: 1, height: 13 }
        // marginLeft:40,
    },

    input: {
        color: 'white',
        left: 20,
        maxWidth: '82%',
    },

    item: {
        marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
        backgroundColor: Colors.PRIMARY_COLOR,
        borderColor: Colors.PRIMARY_COLOR,
        top: 30
    },
    item2: {
        marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
        backgroundColor: Colors.PRIMARY_COLOR,
        borderColor: Colors.PRIMARY_COLOR,
        top:30,
       
        
    },
    form: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    TextInputStyle2: {
        height: 40,
        borderBottomColor: Colors.DARK_BLACK_TEXT_COLOR,
        width: 200,
        marginBottom: 10,
        borderBottomWidth: 1
    },
    backGround: {
        backgroundColor: Colors.PRIMARY_COLOR,

    },

    headerTextStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: hp('2.5%'),
        color: 'white',
    },
    header: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerStyle: {
        backgroundColor: Colors.PRIMARY_COLOR,
        shadowOpacity: 0.75,
        shadowRadius: 5,
        shadowOffset: { height: 0, width: 0 },

    },
    ToucherbleIconStyle: { color: Colors.PRIMARY_COLOR },
    ToucherbleIconStyle2: { height: 30, width: 30 },
    ToucherbleIconStyle3: { height: 30, width: 30, },

    logoContainer: {

        justifyContent: 'center',
        alignItems: 'center',
        marginTop: PixelRatio.getPixelSizeForLayoutSize(40),
        marginBottom: PixelRatio.getPixelSizeForLayoutSize(40),
    },
    logoText: {
        color: Colors.DARK_BLACK_TEXT_COLOR,
        fontSize: 24,
        fontWeight: '700',
        marginTop: 10
    },
    logoText1: {
        color: Colors.DARK_BLACK_TEXT_COLOR,
        fontSize: 15,
        fontWeight: '700',

    },
    backgroundImage: { width: "100%", height: "100%" },

    logo: {
        left: -3,
        height: PixelRatio.getPixelSizeForLayoutSize(150),
        width: PixelRatio.getPixelSizeForLayoutSize(150),
        resizeMode: 'contain',
    },
    place: {
        width: 25,
        height: 25,
        left: 10
    },
    buttonContainer: {
        flex: 1,
        marginTop: PixelRatio.getPixelSizeForLayoutSize(10),
    },
    forgotPasswordContainer: {
        alignItems: 'center',
        bottom: -150
    },
    forgotPasswordText: {
        color: 'white',
        fontSize: 16,
    },
    button: {
        marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
        backgroundColor: Colors.PRIMARY_COLOR,
    },
    button2: {
        marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
        backgroundColor: "white"
    },
    loginText: {
        color: '#fff',
        fontSize: 20,
        fontWeight: 'bold',
    },
    signupContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: PixelRatio.getPixelSizeForLayoutSize(3),
    },
    dontHaveAccountText: {
        color: '#bec0ce',
        fontSize: 16,
    },
    signupText: {
        color: '#000',
        fontSize: 10,
    },
    longpressmodal: {
        justifyContent: 'center',
        alignItems: 'center'
    },

    longpressmodal3: {
        height: hp('40%'),
        width: wp('82%'),
        borderRadius: 20,
    },
    itemContainermodal: {
        borderRadius: 15,
        padding: 10,
        height: hp('6.5%'),
        width: wp('75%'),

        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        marginBottom: 5,
        // shadowColor: Colc ors.DARK_BLACK_TEXT_COLOR,
        shadowOpacity: 0.8,
        elevation: 3,
        shadowRadius: 10,
        shadowOffset: { width: 1, height: 1 }

    },

});


export default styles;