import React, { StyleSheet } from 'react-native';
import Colors from '../../resources/Colors'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 30
    },
    logoText1: {
        color: Colors.DARK_BLACK_TEXT_COLOR,
        fontSize: hp('1.6%'),
        fontWeight: '700',
      },
    HeadericonlImage2: {
        width: 300,
        height: 100,
        marginBottom: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    IndicatorStyle:{ justifyContent: 'center', alignItems: 'center', top: 230 }

});