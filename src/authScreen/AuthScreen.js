
import React, { Component } from 'react';
import {
    View,
    AsyncStorage,
    StatusBar,
    ActivityIndicator,
    Image,
    Text

} from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation'
import BackgroundTimer from 'react-native-background-timer';
import Spinner from "react-native-spinkit";
import styles from './AuthScreenStyle';
import Colors from '../../resources/Colors';
export default class Authenticator extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fullName: '',
            username: '',
            email: '',
            password: '',
            validated: false,
            clientData: null,
            spinner: false,
            indicator: true,
        }
    }


    //To retrieve data in astync storage
    componentDidMount() {
        this.setState({
            spinner: true,
        })
        this._retrieveData();
    }

    // RetrieveUsername and password.....
    _retrieveData = async () => {
        this.setState({
            spinner: true
        })

        try {
            const value = await AsyncStorage.getItem('myArray');
            const value2 = await AsyncStorage.getItem('loginDetails');
            console.log("Value:" + value2);
            if (value2 !== null) {
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'PendingScreen' })],
                });
                this.setState({
                    spinner: false
                })
                this.props.navigation.dispatch(resetAction);
            } else {
                if (value !== null) {
                    const resetAction = StackActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({ routeName: 'HomeScreen' })],
                    });
                    this.setState({
                        spinner: false
                    })
                    this.props.navigation.dispatch(resetAction);
                } else {
                    const resetAction = StackActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({ routeName: 'LoginScreen' })],
                    });
                    this.setState({
                        spinner: false
                    })
                    this.props.navigation.dispatch(resetAction);

                }
            }

        } catch (error) {
            console.log(error);
            this.setState({
                spinner: false
            })

        }
    }
    //To indicate
    indicator() {
        if (this.state.indicator) {
            return (
                <View style={styles.IndicatorStyle}>
                    <ActivityIndicator size="large" color={Colors.PRIMARY_COLOR} />
                </View>

            )
        }
    }

    //To show the view
    render() {
        return (

            <View style={{ backgroundColor: 'white' }}>
                <View style={{ justifyContent: 'center', alignItems: 'center',top:50 }}>
                    <Image
                        source={require("../../assets/LOGO-FINAL-05.png")}
                        style={{ width: 200, height: 120, top: 200 }}
                    />
                </View>

                <StatusBar barStyle="dark-content" hidden={false} backgroundColor={Colors.BACKGROUD_WHITE} />

                <View style={{ justifyContent: 'center', alignItems: 'center',top:100 }}>
                    <Spinner
                        isVisible={this.state.spinner}
                        type={'WanderingCubes'}
                        textStyle={{ color: 'white' }}
                        size={200}
                        color={Colors.FLAT_TEXT_COLOR}
                    />

                </View>
                <View style={{justifyContent: 'center', alignItems: 'center',top:300}}>
                <Text style={styles.logoText1}>#F E E L   T H E   G R O W T H</Text>
                </View>
                {/* {this.indicator()} */}
            </View>


        );
    }
}
