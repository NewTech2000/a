import React, { Component } from 'react';
import { View, Image, TouchableOpacity, ImageBackground, StatusBar, BackHandler, TouchableHighlight, Alert, AsyncStorage, PixelRatio, Switch,Dimensions } from 'react-native';
import { Item,Input,Text,Header,Left,Right,Picker,Label,
} from 'native-base';
import styles from './addItemDetailsFormStyle';
import Colors from '../../resources/Colors';
import Strings from '../../resources/Strings';
import { NavigationActions, StackActions } from 'react-navigation'
import NumericInput from '@wwdrew/react-native-numeric-textinput';
import Spinner from 'react-native-loading-spinner-overlay';
import NetInfo from "@react-native-community/netinfo";
import Messages from '../../resources/Message';
import CompressImage from "react-native-compress-image";
import ImagePicker from "react-native-image-picker";
import { ScrollView } from 'react-native-gesture-handler';
import TextInputMask from 'react-native-text-input-mask';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

import Dialog, {
    DialogTitle,
    DialogContent,
    DialogButton,
    ScaleAnimation,
} from 'react-native-popup-dialog';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
export default class addItemForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            selectedItem: 'About',
            arrayholder: [],
            count: 0,
            temp: 0,
            price: 0,
            tempPrice: 0,
            text: '',
            trancerdate: [],
            tempArray: [],
            itemType: '',
            avatarSource: "",
            image: null,
            photo: null,
            url: "",
            description: '',
            priceT: "",
            ItemPrice: 100,
            cost: "",
            token: '',
            apiData: [],
            spinner: false,
            searchText: "",
            productId: "",
            uri: "",
            showTheThing: true,
            pKey: 0,
            qty: 0,
            ItemCost: '',
            ItemName: '',
            ItemPrice: '',
            ItemQty: '',
            photoUri: '',
            category: [],
            subCategory: [],
            catName: "",
            id: "",
            cat: "",
            switchValue: false,
            qtyvisible: false,
            inventory: false,
            batchNumber: "0001",
            categoryModel: false,
            SubCategoryModel: false,
            newCategoryName: "",
            newSubCategoryName: "",
            catValue: "",
            SubCatName: "",
            SubCatValue: "",
            productImageHide: false,



        };

    }

    onButtonPress = () => {

    }



    handleBackButton = () => {
        this.props.navigation.goBack(null);
        return true;
    };



    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }



    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        this.showData()


    }

    //getting auth code from AsyncStorage
    showData = async () => {
        let myArray = await AsyncStorage.getItem('myArray');
        let d = JSON.parse(myArray);
        let myArray1 = await AsyncStorage.getItem('productImage');
        let d1 = JSON.parse(myArray1);
        this.setState({
            token: d,
            productImageHide: d1

        })
        this.gettingCategories()
        console.log(this.state.token);

    }

    //dropdown value item type
    onValueChange(value) {
        this.setState({
            itemType: value
        });
    }


    //dropdown value category name
    onValueChangeCategory = (value) => {

        if (value == "") {
            this.setState({
                catName: "",
                catValue: "",
                SubCatName: "",
                SubCatValue: "",
            });

        } else {
            let categoryName = this.state.category.find(category => category.name === value);
            this.state.catValue = categoryName.id
            this.gettingSubCategories(this.state.catValue)
            this.setState({
                catName: value
            });
        }

    }


    //dropdown value Subcategory name
    onValueChangeSubCategory = (value) => {
        if (value == "") {
            this.setState({
                SubCatName: "",
                SubCatValue: ""
            });
        } else {
            const { subCategory } = this.state;
            let categoryName = subCategory.find(subCategory => subCategory.name === value);
            this.state.SubCatValue = categoryName.id
            this.setState({
                SubCatName: value
            });
        }

    }



    //validation
    validation3() {

        if (this.state.inventory == true) {
            console.log(this.state.priceT)
            if (this.state.description == "") {
                Messages.messageName(Strings.ITEM_ADDED_FAIL, Strings.ITEM_NAME_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
            } else if (this.state.catName == "") {
                Messages.messageName(Strings.ITEM_ADDED_FAIL, Strings.ITEM_CATEGORY_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
            } else if (this.state.itemType == "") {
                Messages.messageName(Strings.ITEM_ADDED_FAIL, Strings.ITEM_TYPE_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
            } else if (this.state.qty == "") {
                Messages.messageName(Strings.ITEM_ADDED_FAIL, Strings.ITEM_QTY_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
            } else if (this.state.priceT == "") {
                Messages.messageName(Strings.ITEM_ADDED_FAIL, Strings.ITEM_PRICE_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
            } else if (this.state.cost == "") {
                Messages.messageName(Strings.ITEM_ADDED_FAIL, Strings.ITEM_COST_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
            } else {
                if (this.state.productImageHide == true) {
                    if (this.state.photo == null) {
                        Messages.messageName(Strings.ITEM_ADDED_FAIL, Strings.ITEM_IMAGE_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
                    } else {
                        this.handleUploadPhoto('save')
                    }

                } else {
                    this.saveProduct()
                }

            }
        } else {
            console.log(this.state.priceT)
            if (this.state.description == "") {
                Messages.messageName(Strings.ITEM_ADDED_FAIL, Strings.ITEM_NAME_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
            } else if (this.state.catName == "") {
                Messages.messageName(Strings.ITEM_ADDED_FAIL, Strings.ITEM_CATEGORY_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
            } else if (this.state.itemType == "") {
                Messages.messageName(Strings.ITEM_ADDED_FAIL, Strings.ITEM_TYPE_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
            } else if (this.state.priceT == "") {
                Messages.messageName(Strings.ITEM_ADDED_FAIL, Strings.ITEM_PRICE_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
            } else if (this.state.cost == "") {
                Messages.messageName(Strings.ITEM_ADDED_FAIL, Strings.ITEM_COST_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
            } else {
                if (this.state.productImageHide == true) {
                    if (this.state.photo == null) {
                        Messages.messageName(Strings.ITEM_ADDED_FAIL, Strings.ITEM_IMAGE_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
                    } else {
                        this.handleUploadPhoto('save')
                    }

                } else {
                    this.saveProduct()
                }
            }
        }
    }

    //load category types for dropdown
    loadCategoryTypes() {
        var data = [];
        return this.state.category.map(data => (
            <Picker.Item label={data.name} value={data.name} key={data.id} />
        ));
    }

    //load Subcategory types for dropdown
    loadSubCategoryTypes() {
        var data = [];
        return this.state.subCategory.map(data => (
            <Picker.Item label={data.name} value={data.name} key={data.id} />
        ));
    }


    //fetching category types from api
    gettingCategories() {
        this.setState({
            spinner: true,
        });

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + '/api/product/category?page=0&limit=200&text=' + this.state.cat,
                    {
                        method: "GET",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },

                    }
                )
                    .then(resp => resp.json())
                    .then(responseJson => {
                        console.log("aaaaaaaaaaaaaaaaaa" + JSON.stringify(responseJson))
                        this.setState({
                            spinner: false,
                        });

                        this.setState({
                            category: responseJson.content
                        })

                    })
                    .catch(error => {
                        console.log(error);
                        this.setState({
                            spinner: false,
                        });

                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.gettingCategories()

                            }
                        },
                    ],
                    { cancelable: true }
                );

            }
        });
    }


    //fetching Subcategory types from api
    gettingSubCategories(value) {
        this.setState({
            spinner: true,
        });

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + '/api/product/category/' + this.state.catValue + '/sub-category?page=0&limit=100',
                    {
                        method: "GET",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },

                    }
                )
                    .then(resp => resp.json())
                    .then(responseJson => {
                        console.log("aaaaaaaaaaaaaaaaaa" + JSON.stringify(responseJson.content))
                        this.setState({
                            spinner: false,
                        });

                        this.setState({
                            subCategory: responseJson.content
                        })

                    })
                    .catch(error => {
                        console.log(error);
                        this.setState({
                            spinner: false,
                        });

                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.gettingCategories()

                            }
                        },
                    ],
                    { cancelable: true }
                );

            }
        });
    }


    //chooseing image
    handleChoosePhoto = () => {
        const options = {
            noData: true,
        }
        ImagePicker.launchImageLibrary(options, response => {
            console.log("upload succes", response);
            if (response.uri) {
                this.setState({
                    photo: response

                })
                this.compress()
            }
        })



    }

    openAddCategoryModel() {
        this.setState({
            categoryModel: true
        })

    }

    openAddSubCategoryModel() {
        if (this.state.catName == "") {
            Messages.messageName(Strings.WARNING, Strings.SELECT_CATEGORY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        } else {
            this.setState({
                SubCategoryModel: true
            })
        }

    }

    addNewCategory() {
        if (this.state.newCategoryName == "") {
            Messages.messageName(Strings.CATEGORY_ADDED_Fail, Strings.CATEGORY_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        } else {
            this.saveCategory()
        }
    }

    addNewSubCategory() {
        if (this.state.newSubCategoryName == "") {
            Messages.messageName(Strings.SUB_CATEGORY_ADDED_Fail, Strings.CATEGORY_EMPTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        } else if (this.state.catValue == "") {
            Messages.messageName(Strings.WARNING, Strings.SELECT_CATEGORY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);
        } else {
            this.saveSubCategory()
        }
    }


    //save category (First)
    saveCategory() {
        this.setState({
            spinner: true,
        });
        var data = JSON.stringify({
            "description": this.state.description,
            "name": this.state.newCategoryName.trim()
        });
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + '/api/product/category',
                    {
                        method: "POST",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },
                        body: data
                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        console.log("ccccccccccccccc" + JSON.stringify(responseJson));
                        Messages.messageName(Strings.WARNING_FAILED, Strings.CATEGORY_ADDED_SUCCESS, Strings.ICON[2], Strings.TYPE[2], Colors.SUCCESS_ALERT);
                        this.setState({
                            id: responseJson.id,
                            spinner: false,
                            categoryModel: false,
                            newCategoryName: ""
                        });
                        this.gettingCategories()
                    })

                    .catch(error => {
                        console.log(error);
                        this.setState({
                            spinner: false,
                            categoryModel: false,
                        });
                        Messages.messageName(Strings.WARNING_FAILED, Strings.CATEGORY_ADDED_Fail, Strings.ICON[2], Strings.TYPE[2], Colors.SUCCESS_ALERT);
                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false,
                    categoryModel: false,
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.saveCategory()

                            }
                        },
                    ],
                    { cancelable: true }
                );
            }
        });
    }

    //save Sub category (Second)
    saveSubCategory() {
        this.setState({
            spinner: true,
        });
        var data = JSON.stringify({
            "description": "",
            "name": this.state.newSubCategoryName
        });

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(Strings.BASE_URL + '/api/product/category/' + this.state.catValue + '/sub-category',
                    {
                        method: "POST",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },
                        body: data
                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        Messages.messageName(Strings.WARNING_SUCESS, Strings.SUB_CATEGORY_ADDED_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                        this.gettingSubCategories()
                        console.log(JSON.stringify(responseJson));
                        this.setState({
                            SubCategoryModel: false,
                            spinner: false
                        });
                    })

                    .catch(error => {
                        console.log(error);
                        this.setState({
                            spinner: false,
                            scaleAnimationDialog: false,
                        });
                        Messages.messageName(Strings.WARNING_FAILED, Strings.SUB_CATEGORY_ADDED_Fail, Strings.ICON[2], Strings.TYPE[2], Colors.WARNING_ALERT);
                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false,
                    scaleAnimationDialog: false,
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.saveSubCategory()

                            }
                        },
                    ],
                    { cancelable: true }
                );


            }
        });

    }


    //To compress a image
    compress() {
        CompressImage.createCompressedImage(this.state.photo.uri, "Compress/Images")

            .then(response => {
                this.state.photo.uri = response.uri;
                this.state.photo.path = response.path;
                this.state.photo.mime = response.mime;
                this.state.photo.size = response.size;

            })

            .catch(err => {
                return Alert.alert(
                    Strings.COMPRESS_ERR
                );
            });

    }


    //upload Image
    handleUploadPhoto = (string) => {
        this.setState({
            spinner: true,
        });

        fetch(Strings.Image_URL + "/api/files", {
            method: "POST",
            body: this.createFormData(this.state.photo, { userId: "123" })
        })
            .then(response => response.json())
            .then(response => {
                this.setState({
                    photo: null,
                    uri: response.url
                });
                this.setState({
                    spinner: false,
                });
                console.log("upload succes", this.state.uri);
                if (string == 'save') {
                    this.saveProduct()
                } else {
                    this.updateItem()
                }

            })
            .catch(error => {
                Messages.messageName(Strings.WARNING, Strings.IMAGE_FAILED, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false,
                });
                console.log("upload error", error);
            });
    };

    //split image url
    createFormData = (photo, body) => {
        const data = new FormData();

        data.append("document", {
            name: photo.fileName,
            type: photo.type,
            uri:
                Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "")
        });

        Object.keys(body).forEach(key => {
            data.append(key, body[key]);
        });
        return data;
    };

    //save product first method (1)
    saveProduct() {
        this.setState({
            spinner: true,
        });
        var data = JSON.stringify({
            "category": this.state.catName,
            "changeAvailabilityOnEmpty": true,
            "description": "unknown",
            "imageUrl": this.state.uri,
            "manageInventory": this.state.inventory,
            "minusInventory": true,
            "name": this.state.description,
            "productCode": "c001",
            "qtyMeasurement": this.state.itemType,
            "subCategory": this.state.SubCatName
        });

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + "/api/product",
                    {
                        method: "POST",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },
                        body: data
                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        this.setState({
                            spinner: false,

                        });
                        this.state.productId = responseJson.id;
                        console.log(JSON.stringify(responseJson));
                        this.saveItemPrice()
                    })

                    .catch(error => {
                        this.setState({
                            spinner: false,
                        });
                        console.log(error);

                    });

            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.saveProduct()

                            }
                        },
                    ],
                    { cancelable: true }
                );

            }
        });
    }
    qtyvisible(value) {

        if (value == false) {
            this.setState({
                inventory: false,
                qtyvisible: false,
            })
        } else {
            this.setState({
                inventory: true,
                qtyvisible: true,

            })
        }
        this.setState({
            switchValue: value,
        })
    }

    //save product second method (2)
    saveItemPrice() {
        this.setState({
            spinner: true,
        });

        var data = JSON.stringify({

            "assumedCost": this.state.cost.toString(),
            "batchNumber": this.state.batchNumber,
            "cost": this.state.cost.toString(),
            "price": this.state.priceT.toString(),
            "productId": this.state.productId,
            "qty": this.state.qty,
            "salePrice": this.state.priceT.toString(),
            "serialNumbers": [
                "string"
            ]
        });

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + "/api/product/batch",
                    {
                        method: "POST",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },
                        body: data
                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        this.setState({
                            spinner: false,
                            slideAnimationDialog: false,

                        });

                        Messages.messageName(Strings.WARNING_SUCESS, Strings.ITEM_ADDED_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                        console.log(JSON.stringify(responseJson));
                        const resetAction = StackActions.reset({
                            index: 0,
                            actions: [NavigationActions.navigate({ routeName: 'ManageItemScreen' })],
                        });
                        this.setState({
                            indicator: false
                        })
                        this.props.navigation.dispatch(resetAction);
                    })

                    .catch(error => {
                        this.setState({
                            spinner: false,
                        });
                        console.log(error);
                        Messages.messageName(Strings.WARNING_SUCESS, Strings.ITEM_ADDED_FAIL, Strings.ICON[2], Strings.TYPE[2]);

                    });

            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.saveItemPrice()

                            }
                        },
                    ],
                    { cancelable: true }
                );

            }
        });

    }


    onPickerValueChange = (value, index) => {
        this.setState(
            {
                "throttlemode": value
            },
            () => {
                // here is our callback that will be fired after state change.
                Alert.alert("Throttlemode", this.state.throttlemode);
            }
        );
    }


    // toFixed(price) {
    //     let value = parseFloat(price).toFixed(2)
    //     console.log(value)
    //     // this.setState({
    //     //     priceT:value
    //     // })
    // }
    // formatValue(value) {
    //     return accounting.formatMoney(parseFloat(value) / 100, "$ ");
    // }



    // financial(x) {
    //     return Number.parseFloat(x).toFixed(2);
    // }

    render() {
        return (

            <ImageBackground
                source={require("../../assets/newBack2.png")}
                style={{ width: "100%", height: "100%" }}>

                <StatusBar barStyle="light-content" hidden={false} backgroundColor='#126F85' />
                <Spinner
                    visible={this.state.spinner}
                    textContent={'Loading...'}
                    textStyle={{ color: 'white' }}
                    overlayColor={"#0f0e0e90"}
                    animation={"fade"}
                    color={"#00CEFD"}
                />
                <View style={{ backgroundColor: '#126F85', width: '100%', height: 60, borderBottomLeftRadius: 50 }}>
                    <View style={{ backgroundColor: '#126F85', width: 50, top: 13, borderRadius: 60, left: 25, position: "absolute", top: 8 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('ManageItemScreen')}>
                            <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                    <Image source={require("../../assets/newBack3.png")} style={{ width: 20, height: 20 }}></Image>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Text style={{ fontFamily: 'Ubuntu-Medium', color: '#ffff', justifyContent: "center", textAlign: "center", fontSize: 20, position: "absolute", top: 10, left: 80, fontWeight: "bold" }}>New Item</Text>
                    <View style={{ position: "absolute", width: 70, bottom: 10, right: 65 }}>
                        <Text numberOfLines={1} style={{ fontSize: 12, color: '#ffff', textAlign: "center", top: 8, }}>Inventory</Text>
                        <Switch
                            //  tintColor="transparent"
                            thumbTintColor={this.state.switchValue
                                ? Colors.BTN_COLOR : "#cccccc"}
                            style={{ left: 40, bottom: 10, transform: [{ scaleX: 1.5 }, { scaleY: 1.5 }] }}
                            value={this.state.switchValue}
                            onValueChange={(switchValue) => this.qtyvisible(switchValue)}
                        />
                    </View>
                </View>
                <Dialog
                    onTouchOutside={() => {
                        this.setState({ categoryModel: false });
                    }}
                    width={0.9}
                    visible={this.state.categoryModel}
                    dialogAnimation={new ScaleAnimation()}
                    onHardwareBackPress={() => {
                        this.setState({ categoryModel: false });
                        return true;
                    }}
                    dialogTitle={
                        <DialogTitle
                            title="New Category"
                            hasTitleBar={false}

                        />
                    }
                    actions={[
                        <DialogButton
                            text="DISMISS"
                            onPress={() => {
                                this.setState({ categoryModel: false });
                            }}
                            key="button-1"
                        />,
                    ]}>
                    <DialogContent>
                        <View>
                            <View style={styles.textAreaContainer}>
                                <Item
                                    style={styles.item}
                                    rounded
                                    last
                                >
                                    <Image
                                        style={styles.place}
                                        source={require('../../assets/description.png')}
                                    />

                                    <Input
                                        style={styles.input}
                                        placeholder="Name"
                                        placeholderTextColor="#ffff"
                                        autoCapitalize="none"
                                        value={this.state.newCategoryName}
                                        onChangeText={newCategoryName => this.setState({ newCategoryName })}
                                    />
                                </Item>
                            </View>
                            <TouchableOpacity onPress={() => this.addNewCategory()}>
                                <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                    <View style={{ height: 50, width: 320, backgroundColor: Colors.PRIMARY_COLOR, borderRadius: 30 }}>
                                        <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 15 }}>
                                            <Text style={{ fontWeight: 'bold', color: 'white' }}>SUBMIT</Text>
                                        </View>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </DialogContent>
                </Dialog>
                <Dialog
                    onTouchOutside={() => {
                        this.setState({ SubCategoryModel: false });
                    }}
                    width={0.9}
                    visible={this.state.SubCategoryModel}
                    dialogAnimation={new ScaleAnimation()}
                    onHardwareBackPress={() => {
                        this.setState({ SubCategoryModel: false });
                        return true;
                    }}
                    dialogTitle={
                        <DialogTitle
                            title="New Sub category"
                            hasTitleBar={false}

                        />
                    }
                    actions={[
                        <DialogButton
                            text="DISMISS"
                            onPress={() => {
                                this.setState({ SubCategoryModel: false });
                            }}
                            key="button-1"
                        />,
                    ]}>
                    <DialogContent>
                        <View>
                            <View style={styles.textAreaContainer}>
                                <Item
                                    style={styles.item}
                                    rounded
                                    last
                                >
                                    <Image
                                        style={styles.place}
                                        source={require('../../assets/description.png')}
                                    />

                                    <Input
                                        style={styles.input}
                                        placeholder="Name"
                                        placeholderTextColor="#ffff"
                                        autoCapitalize="none"
                                        value={this.state.newSubCategoryName}
                                        onChangeText={newSubCategoryName => this.setState({ newSubCategoryName })}
                                    />
                                </Item>
                            </View>
                            <TouchableOpacity onPress={() => this.addNewSubCategory()}>
                                <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                    <View style={{ height: 50, width: 320, backgroundColor: Colors.PRIMARY_COLOR, borderRadius: 30 }}>
                                        <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 15 }}>
                                            <Text style={{ fontWeight: 'bold', color: 'white' }}>SUBMIT</Text>
                                        </View>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </DialogContent>
                </Dialog>
                <ScrollView>
                    <View style={{
                        flex: 1,
                        justifyContent: 'flex-end',
                        padding: PixelRatio.getPixelSizeForLayoutSize(12),
                    }}>
                        {this.state.productImageHide &&
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <TouchableHighlight style={{ borderRadius: 100, height: 100, width: 100, marginBottom: 10, marginTop: 20 }}
                                    onPress={() => this.handleChoosePhoto()}
                                >

                                    <Image
                                        source={this.state.photo == null ? require('../../assets/nnn.jpg') : this.state.photo !== null ? this.state.photo : this.state.photo}
                                        style={{ width: 100, height: 100, borderRadius: 100, borderWidth: 2, }}
                                    />

                                </TouchableHighlight>
                            </View>
                        }

                        <Label></Label>

                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/description.png')}
                            />
                            <Input
                                style={styles.input}
                                placeholder="*Name"
                                placeholderTextColor='#ffffff80'
                                autoCapitalize="none"
                                value={this.state.description}
                                onChangeText={description => this.setState({ description })}
                            />
                        </Item>

                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/category.png')}
                            />
                            <Picker
                                style={{ width: 200, color: '#ffffff80', left: 10 }}
                                selectedValue={this.state.catName}
                                onValueChange={this.onValueChangeCategory.bind(this)}
                            >
                                <Picker.Item label="*Category" value="" />
                                {this.loadCategoryTypes()}

                            </Picker>
                            <TouchableOpacity onPress={() => this.openAddCategoryModel()}>
                                <View style={{ width: 50, height: 50, backgroundColor: Colors.FLAT_TEXT_COLOR, borderBottomRightRadius: 20, borderTopRightRadius: 20 }}>
                                    <Image
                                        style={styles.place2}
                                        source={require('../../assets/plus.png')}
                                    />
                                </View>
                            </TouchableOpacity>
                        </Item>

                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/category.png')}
                            />
                            <Picker
                                style={{ width: 200, color: '#ffffff80', left: 10 }}
                                selectedValue={this.state.SubCatName}
                                onValueChange={this.onValueChangeSubCategory.bind(this)}
                            >
                                <Picker.Item label="Sub category" value="" />
                                {this.loadSubCategoryTypes()}

                            </Picker>
                            <TouchableOpacity onPress={() => this.openAddSubCategoryModel()}>
                                <View style={{ width: 50, height: 50, backgroundColor: Colors.FLAT_TEXT_COLOR, borderBottomRightRadius: 20, borderTopRightRadius: 20 }}>
                                    <Image
                                        style={styles.place2}
                                        source={require('../../assets/plus.png')}
                                    />
                                </View>
                            </TouchableOpacity>
                        </Item>

                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/type.png')}
                            />
                            <Picker
                                style={{ width: 200, color: '#ffffff80', left: 10 }}
                                selectedValue={this.state.itemType}
                                onValueChange={this.onValueChange.bind(this)}
                            >
                                <Picker.Item label="*Type" value="" />
                                <Picker.Item label="Pcs" value="Pcs" />
                                <Picker.Item label="Kg/g" value="Kg/g" />
                                <Picker.Item label="L/ml" value="L/ml" />
                                <Picker.Item label="Cm/mm" value="Cm/mm" />

                            </Picker>
                        </Item>

                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/money.png')}
                            />
                            {/* <NumericInput

                                type='decimal'
                                style={styles.input}
                                decimalPlaces={2}
                                precision="3"
                               
                                placeholder="Price                                      "
                                placeholderTextColor="#ffff"
                                value={this.state.priceT}
                                onUpdate={priceT => this.setState({ priceT })}
                            /> */}

                            <TextInputMask
                             refInput={ref => { this.input = ref }}
                                // type={'money'}
                                style={styles.input3}
                                
                                // options={{
                                //     // precision: 3,
                                //     separator: '.',
                                //     delimiter: ',',
                                //     unit: '',
                                    
                                //     // suffixUnit: '.00'
                                // }}
                                mask={"[99990].[99]"}
                                placeholder='Price'
                                keyboardType={"numeric"}
                                placeholderTextColor="#ffffff80"
                                value={this.state.priceT}
                                onChangeText={priceT => this.setState({ priceT })}
                                
                            />

                            {/* <Input
                                keyboardType={"numeric"}
                                style={styles.input}
                                placeholder="*Regular unit Price"
                                placeholderTextColor="#ffff"
                                autoCapitalize="none"
                                value={this.state.priceT}
                                onChangeText={priceT => this.setState({priceT})}
                            /> */}
                        </Item>
                        <Item
                            style={styles.item}
                            rounded
                            last
                        >
                            <Image
                                style={styles.place}
                                source={require('../../assets/cost.png')}
                            />
                            {/* <Input
                                style={styles.input}
                                keyboardType={"numeric"}
                                placeholder="*Cost"

                                placeholderTextColor="#ffffff80"
                                autoCapitalize="none"
                                value={this.state.cost}
                                onChangeText={cost => this.setState({ cost })}
                            /> */}
                            <TextInputMask
                                refInput={ref => { this.input = ref }}
                                style={styles.input3}
                                keyboardType={"numeric"}
                                mask={"[99990].[99]"}
                                placeholderTextColor="#ffffff80"
                                placeholder="cost"
                                value={this.state.cost}
                                onChangeText={cost => this.setState({ cost })}
                            />
                        </Item>
                        {this.state.qtyvisible &&
                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../assets/description.png')}
                                />

                                <Input
                                    style={styles.input}
                                    placeholder="Batch Number"
                                    placeholderTextColor="#ffffff80"
                                    autoCapitalize="none"
                                    value={this.state.batchNumber}
                                    onChangeText={batchNumber => this.setState({ batchNumber })}
                                />
                            </Item>
                        }
                        {this.state.qtyvisible &&
                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../assets/price.png')}
                                />
                                <Input
                                    keyboardType={"numeric"}
                                    style={styles.input}
                                    placeholder="Qty"
                                    placeholderTextColor="#ffff"
                                    autoCapitalize="none"
                                    value={this.state.qty}
                                    onChangeText={qty => this.setState({ qty })}
                                />
                            </Item>
                        }
                        <View style={{ justifyContent: "center", alignContent: "center", alignItems: "center" }}>
                            <TouchableOpacity
                                style={[styles.buttonContainerProcess, styles.ProcessButton]}
                                onPress={() => this.validation3()}
                            >
                                <Text style={styles.ProcessText}>Save</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                </ScrollView>
            </ImageBackground>

        );
    }
}