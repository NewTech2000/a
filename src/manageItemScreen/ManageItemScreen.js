import React, { Component } from 'react';
import { Image, TouchableOpacity, ImageBackground, StatusBar, TouchableHighlight, BackHandler, Alert, AsyncStorage, Vibration, PixelRatio } from 'react-native';
import {
    Item,
    Input,
    Text,
    Header,
    Left,
    Right,
    Fab,
    Picker,
    Switch,
    Toast,
    Label
} from 'native-base';
import styles from './ManageItemScreenStyle';
import Modal from 'react-native-modalbox';
import Colors from '../../resources/Colors';
import NetInfo from "@react-native-community/netinfo";
import Strings from '../../resources/Strings';
import Messages from '../../resources/Message';
import { FlatGrid } from 'react-native-super-grid';
import { View } from 'react-native-animatable';
import CompressImage from "react-native-compress-image";
import AwesomeAlert from 'react-native-awesome-alerts';
import ImagePicker from "react-native-image-picker";
import TextInputMask from 'react-native-text-input-mask';
import Spinner from "react-native-spinkit";
import Dialog, {
    DialogTitle,
    DialogContent,
    DialogButton,
    ScaleAnimation,
} from 'react-native-popup-dialog';
import Spinner2 from 'react-native-loading-spinner-overlay';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import ImageResizer from 'react-native-image-resizer';
import { ScrollView } from 'react-native-gesture-handler';

const DURATION = 40;
export default class ManageItem extends Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false,
            selectedItem: 'About',
            arrayholder: [],
            customBackgroundDialog: false,
            defaultAnimationDialog: false,
            scaleAnimationDialog: false,
            scaleAnimationDialog2: false,
            slideAnimationDialog: false,
            count: 0,
            temp: 0,
            price: 0,
            tempPrice: 0,
            text: "",
            trancerdate: [],
            tempArray: [],
            itemType: '',
            avatarSource: "",
            image: null,
            photo: null,
            url: "",
            description: '',
            priceT: 0,
            ItemPrice: 100,
            cost: 0,
            token: '',
            apiData: [],
            spinner: false,
            spinner2: false,
            searchText: "",
            productId: "",
            uri: "",
            showTheThing: true,
            pKey: 0,
            qty: 0,
            ItemCost: '',
            ItemName: '',
            ItemPrice: '',
            ItemQty: '',
            photoUri: '',
            category: [],
            subCategory: [],
            catName: "",
            SubCatName: "",
            SubCatValue: "",
            cat: "",
            nodata: false,
            resizedImageUri: '',
            batchNumber: "",
            showAlert: false
        };
    }

    //getting auth code from AsyncStorage
    showData = async () => {
        let myArray = await AsyncStorage.getItem('myArray');
        let d = JSON.parse(myArray);
        this.setState({
            token: d
        })
        this.getAllProduct()
        this.gettingCategories()
        this.gettingSubCategories()
        console.log(this.state.token);

    }

    onValueChange(value) {
        this.setState({
            itemType: value
        });
    }

    onValueChange2(value) {
        this.setState({
            catName: value
        });
    }

    //dropdown value category name
    onValueChangeCategory = (value) => {

        if (value == "") {
            this.setState({
                catName: "",
                catValue: "",
                SubCatName: "",
                SubCatValue: "",
            });

        } else {
            let categoryName = this.state.category.find(category => category.name === value);
            this.state.catValue = categoryName.id
            this.gettingSubCategories(this.state.catValue)
            this.setState({
                catName: value
            });
        }

    }




    //dropdown value Subcategory name
    onValueChangeSubCategory = (value) => {
        if (value == "") {
            this.setState({
                SubCatName: "",
                SubCatValue: ""
            });
        } else {
            const { subCategory } = this.state;
            let categoryName = subCategory.find(subCategory => subCategory.name === value);
            this.state.SubCatValue = categoryName.id
            this.setState({
                SubCatName: value
            });
        }

    }


    toggle() {
        this.props.navigation.navigate('HomeScreen')
    }
    editSelectedItem = (item) => {
        this.state.pKey = item.productId
        var price = item.price
        console.log(item)
        this.setState({
            catName: item.category,
            scaleAnimationDialog: true,
            ItemName: item.name,
            itemType: item.qtyMeasurement,
            ItemPrice: item.price.toString(),
            ItemQty: item.qty.toString(),
            photo: { uri: item.imageUrl },
            photoUri: item.imageUrl
        })
        this.setState({
            showTheThing: false
        })
        Vibration.vibrate(DURATION);
    }

    navigateTOProcess() {
        if (this.state.photo.uri == this.state.photoUri) {
            this.state.uri = this.state.photo.uri
            console.log("vvvvvvvvvvvvvvvvv" + this.state.photoUri)
            console.log("aaaaaaaaaaaaaaaaaaa" + this.state.photo.uri)
            this.updateItem()
        } else {
            this.handleUploadPhoto()
        }
    }

    updateItem() {
        this.setState({
            spinner2: true,
        });
        console.log(this.state.ItemName,this.state.SubCatName,this.state.itemType)
        var data = JSON.stringify({

            "category": this.state.catName,
            "changeAvailabilityOnEmpty": true,
            "description": "string",
            "imageUrl": this.state.uri,
            "manageInventory": true,
            "minusInventory": true,
            "name": this.state.ItemName,
            "productCode": "c001",
            "qtyMeasurement": this.state.itemType,
            "subCategory": this.state.SubCatName
        });

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + "/api/product/" + this.state.pKey,
                    {
                        method: "PUT",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },
                        body: data
                    }
                )
                    .then(resp => resp.json())
                    .then(responseJson => {
                        console.log("updateeeeeeeeeee" + JSON.stringify(responseJson));
                        this.setState({
                            spinner2: false,
                        });
                        this.setState({
                            spinner2: false,
                        });
                        this.refs.modal2.close()


                    })
                    .catch(error => {
                        this.refs.modal4.close()
                        // this.saveItemPrice()
                        this.getAllProduct()
                        this.setState({
                            uri: ""
                        })
                        Messages.messageName(Strings.WARNING_SUCESS, Strings.ITEM_UPDATE_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                        this.setState({
                            spinner2: false,
                        });
                        console.log("upate error" + error);
                    });

            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })
                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.updateItem()
                            }
                        },
                    ],
                    { cancelable: true }
                );
            }
        });
    }


    onButtonPress = () => {
    }

    //show alert hide and show method
    showAlert = () => {
        this.setState({
            showAlert: true
        });
        // this.refs.sideMenu.close()
    };

    hideAlert = () => {
        this.setState({
            showAlert: false
        });
    };

    handleBackButton = () => {
        this.props.navigation.goBack(null);
        return true;
    };

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }


    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        this.showData()
    }

    renderNext3() {
        this.refs.view3.bounceIn(1000);
    }

    anima() {
        this.props.navigation.navigate('AddItemDetailsScreen')
    }



    animaNavigate() {
        this.setState({
            scaleAnimationDialog2: false,
        });
        this.props.navigation.navigate('CustomerScreen')
    }

    //Integer Validator
    intValidator(text) {
        const reg = /^[-+]?[0-9]\d*$/;
        if (reg.test(text) === false) {
            return false;
        } else {
            return true;
        }
    }


    Validation() {
        // if (this.state.price.match(/\.\d{2}$/)) {
        //     this.state.price = this.state.price.replace(',', '');
        // }
        // if (this.state.cost.match(/\.\d{2}$/)) {
        //     this.state.cost = this.state.cost.replace(',', '');
        // }
        if (this.intValidator(this.state.batchNumber) === false) {
            Messages.messageName(Strings.WARNING_FAILED, Strings.BATCH_NUMBER, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);

        } else if (this.state.cost === 0.00) {
            Messages.messageName(Strings.WARNING_FAILED, Strings.BATCH_COST, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);

        } else if (this.state.price == 0.00) {
            Messages.messageName(Strings.WARNING_FAILED, Strings.BATCH_SALES_PRICE, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);

        } else if (this.intValidator(this.state.qty) === false) {
            Messages.messageName(Strings.WARNING_FAILED, Strings.BATCH_QTY, Strings.ICON[0], Strings.TYPE[0], Colors.WARNING_ALERT);

        } else {
            this.addNewBatch()
        }
    }

    addNewBatch() {
        this.setState({
            spinner2: true,
        });
        var data = JSON.stringify({
            "assumedCost": this.state.cost,
            "batchNumber": this.state.batchNumber,
            "cost": this.state.cost,
            "price": this.state.price,
            "productId": this.state.pKey,
            "qty": this.state.qty,
            "salePrice": this.state.price,
            "serialNumbers": [
                "string"
            ]
        });

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + "/api/product/batch",
                    {
                        method: "POST",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },
                        body: data
                    }
                )

                    .then(resp => resp.json())
                    .then(responseJson => {
                        this.setState({
                            spinner2: false,
                            slideAnimationDialog: false,
                            batchNumber: "",
                            cost: "",
                            price: "",
                            qty: ""

                        });
                        this.refs.modal2.close()
                        Messages.messageName(Strings.WARNING_SUCESS, Strings.ADDED_NEW_BATCH_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                        console.log(JSON.stringify(responseJson));
                        this.getAllProduct()
                    })

                    .catch(error => {
                        this.setState({
                            spinner2: false,
                        });
                        console.log(error);
                        Messages.messageName(Strings.WARNING_SUCESS, Strings.ADDED_NEW_BATCH_FAIL, Strings.ICON[2], Strings.TYPE[2], Colors.SUCCESS_ALERT);


                    });

            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner2: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.saveItemPrice()

                            }
                        },
                    ],
                    { cancelable: true }
                );
            }
        });

    }

    search(text) {
        this.state.searchText = text;
        this.getAllProduct()
    }

    //get All Product 
    getAllProduct() {
        this.setState({
            spinner: true,
        });

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + "/api/inventory?page=0&limit=100&text=" + this.state.searchText,
                    {
                        method: "GET",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },

                    }
                )
                    .then(resp => resp.json())
                    .then(responseJson => {
                        console.log("gettAll" + JSON.stringify(responseJson));
                        this.setState({
                            spinner: false,
                        });

                        this.setState({
                            arrayholder: responseJson.content
                        })

                        if (responseJson.content.length == 0) {
                            this.setState({
                                nodata: true
                            })
                        } else {
                            this.setState({
                                nodata: false
                            })
                        }

                    })
                    .catch(error => {
                        console.log(error);
                        this.setState({
                            spinner: false,
                        });

                    });

            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })
                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.getAllProduct()
                            }
                        },
                    ],
                    { cancelable: true }
                );
            }
        });
    }


    onClose = () => {
        this.setState({
            showTheThing: true,
            showAlert: false,
            photo: "",
            catName: "",
            ItemName: "",
            itemType: "",
        })
    }

    //load category types for dropdown
    loadCategoryTypes() {
        var data = [];
        return this.state.category.map(data => (
            <Picker.Item label={data.name} value={data.name} key={data.id} />
        ));
    }

    //load Subcategory types for dropdown
    loadSubCategoryTypes() {
        var data = [];
        return this.state.subCategory.map(data => (
            <Picker.Item label={data.name} value={data.name} key={data.id} />
        ));
    }



    deleteItem() {
        // Alert.alert(
        //     'Confirm',
        //     'Are you sure you want to do this ?',
        //     [
        //         { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        //         {
        //             text: 'Yes', onPress: () => {


        this.setState({
            spinner: true,
        });

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + '/api/product/' + this.state.pKey + '?deleteFromAllBranches=false',
                    {
                        method: "DELETE",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },
                    }
                )
                    .then(resp => resp.json())
                    .then(responseJson => {
                        this.setState({
                            spinner: false,
                            showAlert: false


                        });
                        console.log(JSON.stringify(responseJson));
                        Messages.messageName(Strings.WARNING_SUCESS, Strings.ITEM_DELETE_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                        console.log(JSON.stringify(responseJson));
                        this.getAllProduct()
                        this.refs.modal4.close()
                    })

                    .catch(error => {
                        this.setState({
                            spinner: false,
                            showAlert: false
                        });
                        console.log(error);
                        this.refs.modal4.close()
                        this.getAllProduct()
                        // Messages.messageName(Strings.WARNING_SUCESS, Strings.ITEM_DELETE_FAIL, Strings.ICON[2], Strings.TYPE[2]);
                        Messages.messageName(Strings.WARNING_SUCESS, Strings.ITEM_DELETE_SUCCESS, Strings.ICON[1], Strings.TYPE[1], Colors.SUCCESS_ALERT);
                        console.log(JSON.stringify(responseJson));

                    });

            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.deleteItem()

                            }
                        },
                    ],
                    { cancelable: true }
                );
            }
        });




    }

    //no data iamge
    nodataImage() {
        if (this.state.nodata) {
            return (

                <View style={{ top: 60, justifyContent: 'center', alignItems: 'center', }}>
                    <Image style={{ width: 200, height: 200, borderRadius: 100 }}
                        source={require("../../assets/no_data_found.png")}
                    />
                </View>
            )
        }
    }

    //Save Item
    saveItem() {

        this.saveProduct()


    }

    //chooseing image
    handleChoosePhoto = () => {
        const options = {
            noData: true,
        }
        ImagePicker.launchImageLibrary(options, response => {
            console.log("upload succes", response);
            if (response.uri) {
                this.setState({
                    photo: response

                })
                this.compress()
            }
        })
    }


    closeOneItemModel6() {
        this.refs.modal2.close()
    }

    //To compress a image
    compress() {
        CompressImage.createCompressedImage(this.state.photo.uri, "Compress/Images")

            .then(response => {
                this.state.photo.uri = response.uri;
                this.state.photo.path = response.path;
                this.state.photo.mime = response.mime;
            })

            .catch(err => {
                // return Alert.alert(
                //     Strings.COMPRESS_ERR
                // );

            });

    }

    //upload Image
    handleUploadPhoto = (string) => {
        this.setState({
            spinner2: true,
        });

        fetch(Strings.Image_URL + "/api/files", {
            method: "POST",
            body: this.createFormData(this.state.photo, { userId: "123" })
        })
            .then(response => response.json())
            .then(response => {


                // alert("Upload success!");
                this.setState({
                    photo: null,
                    uri: response.url
                });
                this.setState({
                    spinner2: false,
                });


                console.log("upload succes", this.state.uri);
                if (string == 'save') {
                    this.saveProduct()
                } else {
                    this.updateItem()
                }


            })
            .catch(error => {
                this.setState({
                    spinner: false,
                    spinner2: false,
                });
                Messages.messageName(Strings.WARNING, Strings.IMAGE_FAILED, Strings.ICON[2], Strings.TYPE[2]);
                console.log("upload error", error);
                // alert("Upload failed!");
            });
    };

    gettingCategories() {
        this.setState({
            spinner: true,
        });

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + '/api/product/category?page=0&limit=200&text=' + this.state.cat,
                    {
                        method: "GET",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },

                    }
                )
                    .then(resp => resp.json())
                    .then(responseJson => {
                        console.log("fffffffffffffffffffff" + JSON.stringify(responseJson));
                        this.setState({
                            spinner: false,
                        });

                        this.setState({
                            category: responseJson.content
                        })

                    })
                    .catch(error => {
                        console.log(error);
                        this.setState({
                            spinner: false,
                        });

                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.gettingCategories()

                            }
                        },
                    ],
                    { cancelable: true }
                );
            }
        }); 0

    }

    gettingSubCategories(value) {
        this.setState({
            spinner: true,
        });

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected === true) {
                fetch(
                    Strings.BASE_URL + '/api/product/category/' + this.state.catValue + '/sub-category?page=0&limit=100',
                    {
                        method: "GET",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            'Authorization': this.state.token
                        },

                    }
                )
                    .then(resp => resp.json())
                    .then(responseJson => {
                        console.log("aaaaaaaaaaaaaaaaaa" + JSON.stringify(responseJson.content))
                        this.setState({
                            spinner: false,
                        });

                        this.setState({
                            subCategory: responseJson.content
                        })

                    })
                    .catch(error => {
                        console.log(error);
                        this.setState({
                            spinner: false,
                        });

                    });
            } else {
                Messages.messageName(Strings.WARNING_CONNECTION, Strings.CHECK_NETWORK_CONNECTION, Strings.ICON[2], Strings.TYPE[2]);
                this.setState({
                    spinner: false
                })

                Alert.alert(
                    'Connection Error!',
                    'Please check your connection and try again',
                    [
                        {
                            text: 'Retry', onPress: () => {
                                this.gettingCategories()

                            }
                        },
                    ],
                    { cancelable: true }
                );

            }
        });
    }
    //load category types for dropdown
    loadCategoryTypes() {
        var data = [];
        return this.state.category.map(data => (
            <Picker.Item label={data.name} value={data.name} key={data.id} />
        ));
    }

    //split image url
    createFormData = (photo, body) => {
        const data = new FormData();

        data.append("document", {
            name: photo.fileName,
            type: photo.type,
            uri:
                Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "")
        });

        Object.keys(body).forEach(key => {
            data.append(key, body[key]);
        });
        console.log("qqqqqqqqqqqqqqqqqqqqqqqqqqq", data);
        return data;
    };

    onPressAlert() {
        Messages.messageName("Need", Strings.LONG_TAP, Strings.ICON[2], Strings.TYPE[2], Colors.WARNING_ALERT);
    }

    showUpdateItemModel() {

        this.setState({
            scaleAnimationDialog: false
        })
        this.refs.modal4.open()
    }

    showaddNewBatchModel() {
        this.setState({
            scaleAnimationDialog: false
        })
        this.refs.modal2.open()
    }

    closeOneItemModel4() {
        this.refs.modal4.close()
    }

    render() {
        const { showAlert } = this.state;
        return (
            <View style={styles.container}>
                <ImageBackground
                    source={require("../../assets/newBack2.png")}
                    style={{ width: "100%", height: "100%" }}>
                    <View style={{ backgroundColor: '#126F85', width: '100%', height: 60, borderBottomLeftRadius: 50 }}>
                        <View style={{ backgroundColor: '#126F85', width: 50, top: 13, left: 25, position: "absolute", top: 8 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('HomeScreen')}>
                                <View style={{ width: 30, height: 30, borderRadius: 100, backgroundColor: '#126F85', left: 10, elevation: 10, }}>
                                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                        <Image source={require("../../assets/newBack3.png")} style={{ width: 20, height: 20 }}></Image>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <Text style={{ fontFamily: 'Ubuntu-Medium', color: '#ffff', justifyContent: "center", textAlign: "center", fontSize: 20, position: "absolute", top: 10, left: 100, fontWeight: "bold" }}>Manage Item</Text>
                    </View>

                    <StatusBar barStyle="light-content" hidden={false} backgroundColor={'#126F85'} />
                    <Spinner2
                        visible={this.state.spinner2}
                        textContent={'Loading...'}
                        textStyle={{ color: 'white' }}
                        overlayColor={"#0f0e0e90"}
                        animation={"fade"}
                        color={"#00CEFD"}
                    />
                    <Dialog
                        onTouchOutside={() => {
                            this.setState({ scaleAnimationDialog: false, showTheThing: true });
                        }}
                        width={0.9}
                        visible={this.state.scaleAnimationDialog}
                        dialogAnimation={new ScaleAnimation()}
                        onHardwareBackPress={() => {
                            this.setState({ scaleAnimationDialog: false });
                            return true;
                        }}
                        dialogTitle={
                            <DialogTitle
                                title="Select one of action"
                                hasTitleBar={false}

                            />
                        }
                        actions={[
                            <DialogButton
                                text="DISMISS"
                                onPress={() => {
                                    this.setState({ scaleAnimationDialog: false });
                                }}
                                key="button-1"
                            />,
                        ]}>
                        <DialogContent>
                            <View>
                                <View style={styles.textAreaContainer}>

                                </View>
                                <TouchableOpacity onPress={() => this.showUpdateItemModel()}>
                                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 5 }}>
                                        <View style={{ height: 50, width: wp('70%'), backgroundColor: Colors.PRIMARY_COLOR, borderRadius: 20 }}>
                                            <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 15 }}>
                                                <Text style={{ fontWeight: 'bold', color: 'white' }}>Update & Remove</Text>
                                            </View>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.showaddNewBatchModel()}>
                                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 12 }}>
                                        <View style={{ height: 50, width: wp('70%'), backgroundColor: Colors.PRIMARY_COLOR, borderRadius: 20 }}>
                                            <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', top: 15 }}>
                                                <Text style={{ fontWeight: 'bold', color: 'white' }}>New Batch Number</Text>
                                            </View>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </DialogContent>
                    </Dialog>

                    <Modal style={[styles.modal42]} position={"bottom"} ref={"modal4"} onClosed={this.onClose} backdropPressToClose={false} >
                    
                        <View style={{ position: "absolute", width: 50, right: 150, }}>
                            <TouchableOpacity style={{ justifyContent: 'flex-end', alignItems: 'flex-end', top: -20 }} onPress={() => this.closeOneItemModel4()}>
                                <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: '#ffff', borderRadius: 100, }}>
                                    <Image style={{ width: 40, height: 40 }} source={require('../../assets/closemodl.png')} />
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{
                            flex: 1,
                            justifyContent: 'flex-end',
                            padding: PixelRatio.getPixelSizeForLayoutSize(12),
                        }}>
                            <TouchableOpacity style={{ justifyContent: 'flex-end', alignItems: 'flex-end', position: "absolute", right: 10, top: 5 }} onPress={() => this.showAlert()}>
                                {/* <View style={{ width: 50, }}> */}


                                <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: Colors.PRIMARY_COLOR, borderRadius: 100 }}>
                                    <Image style={{ width: 20, height: 20 }} source={require('../../assets/re.png')} />
                                </View>

                                {/* </View> */}
                            </TouchableOpacity>

                           
                                <View style={{ top: 25 }}>
                                    <Text style={{ color: Colors.PRIMARY_COLOR, fontSize: 20, textAlign: 'left', fontWeight: 'bold', top: 15 }}>Edit Item</Text>
                                    <View style={{ justifyContent: 'center', alignItems: 'center', }}>
                                        <TouchableOpacity style={{ borderRadius: 100, height: 100, width: 100, marginBottom: 10, marginTop: 20 }}
                                            onPress={() => this.handleChoosePhoto()}
                                        >

                                            <Image
                                                source={this.state.photo}
                                                style={{ width: 100, height: 100, borderRadius: 100, borderColor: Colors.PRIMARY_COLOR, borderWidth: 2, }}
                                            />

                                        </TouchableOpacity>

                                    </View>
                                    <ScrollView>
                                    <Item
                                        style={styles.item}
                                        rounded
                                        last
                                    >
                                        <Image
                                            style={styles.place}
                                            source={require('../../assets/description.png')}
                                        />
                                        <Input
                                            style={styles.input}
                                            placeholder="Item Name"
                                            placeholderTextColor="#ffffff80"
                                            autoCapitalize="none"
                                            value={this.state.ItemName}
                                            onChangeText={ItemName => this.setState({ ItemName })}
                                        />
                                    </Item>
                                    <Item
                                        style={styles.item}
                                        rounded
                                        last
                                    >
                                        <Image
                                            style={styles.place}
                                            source={require('../../assets/type.png')}
                                        />
                                        <Picker
                                            style={{ width: 200, color: '#ffffff80', left: 10 }}
                                            selectedValue={this.state.itemType}
                                            onValueChange={this.onValueChange.bind(this)}
                                        >
                                            <Picker.Item label="Item Type" value="" />
                                            <Picker.Item label="Kg/g" value="Kg/g" />
                                            <Picker.Item label="L/ml" value="L/ml" />
                                            <Picker.Item label="Cm/mm" value="Cm/mm" />
                                            <Picker.Item label="Pcs" value="Pcs" />

                                        </Picker>
                                    </Item>
                                    <Item
                                        style={styles.item}
                                        rounded
                                        last
                                    >
                                        <Image
                                            style={styles.place}
                                            source={require('../../assets/category.png')}
                                        />
                                        <Picker
                                            style={{ width: 200, color: '#ffffff80', left: 10 }}
                                            selectedValue={this.state.catName}
                                            onValueChange={this.onValueChangeCategory.bind(this)}
                                        >
                                            <Picker.Item label="*Category" value="" />
                                            {this.loadCategoryTypes()}

                                        </Picker>

                                    </Item>

                                    <Item
                                        style={styles.item}
                                        rounded
                                        last
                                    >
                                        <Image
                                            style={styles.place}
                                            source={require('../../assets/category.png')}
                                        />
                                        <Picker
                                            style={{ width: 200, color: '#ffffff80', left: 10 }}
                                            selectedValue={this.state.SubCatName}
                                            onValueChange={this.onValueChangeSubCategory.bind(this)}
                                        >
                                            <Picker.Item label="Sub category" value="" />
                                            {this.loadSubCategoryTypes()}

                                        </Picker>


                                    </Item>

                                    <View style={{
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        marginBottom: 10
                                    }}>
                                        <TouchableOpacity
                                            style={[styles.buttonContainerProcess, styles.ProcessButton]}
                                            onPress={() => this.navigateTOProcess()}
                                        >
                                            <Text style={styles.ProcessText}>Update</Text>
                                        </TouchableOpacity>
                                    </View>
                                    </ScrollView>
                                </View>
                                
                            </View>
                            <AwesomeAlert
                                show={showAlert}
                                showProgress={false}
                                title="Confirm"
                                message="       Are you sure you want to do this ?      "
                                closeOnTouchOutside={false}
                                closeOnHardwareBackPress={false}
                                showCancelButton={true}
                                showConfirmButton={true}
                                cancelText="    No    "
                                confirmText="   Yes   "
                                confirmButtonColor="#126F85"
                                onCancelPressed={() => {
                                    this.hideAlert();
                                }}
                                onConfirmPressed={() => {
                                    this.deleteItem();
                                }}
                            />
                           
                    </Modal>

                        <Modal style={[styles.modal4]} position={"bottom"} ref={"modal2"} onClosed={this.onClose} backdropPressToClose={false}>
                            <TouchableOpacity style={{ top: -20, justifyContent: "center", alignContent: "center", alignItems: "center" }} onPress={() => this.closeOneItemModel6()}>
                                <View style={{ justifyContent: 'center', alignItems: 'center', width: 60, height: 60, backgroundColor: '#ffff', borderRadius: 100, }}>
                                    <Image style={{ width: 40, height: 40 }} source={require('../../assets/closemodl.png')} />
                                </View>
                            </TouchableOpacity>
                            <View style={{
                                flex: 1,
                                justifyContent: 'flex-end',
                                padding: PixelRatio.getPixelSizeForLayoutSize(12),
                            }}>
                                <Text style={{ color: Colors.PRIMARY_COLOR, fontSize: 20, textAlign: 'left', fontWeight: 'bold', top: -10 }}>New Batch</Text>
                                <Item
                                    style={styles.item}
                                    rounded
                                    last
                                >
                                    <Image
                                        style={styles.place}
                                        source={require('../../assets/description.png')}
                                    />
                                    <Input
                                        keyboardType={"numeric"}
                                        style={styles.input}
                                        placeholder="*Batch Number"
                                        placeholderTextColor="#ffffff80"
                                        autoCapitalize="none"
                                        value={this.state.batchNumber}
                                        onChangeText={batchNumber => this.setState({ batchNumber })}
                                    />
                                </Item>

                                <Item
                                    style={styles.item}
                                    rounded
                                    last
                                >
                                    <Image
                                        style={styles.place}
                                        source={require('../../assets/price.png')}
                                    />
                                  
                                    <TextInputMask
                                        type={'money'}
                                        style={styles.input3}
                                        options={{
                                            separator: '.',
                                            delimiter: ',',
                                            unit: '',
                                        }}
                                        placeholderTextColor="#ffffff80"
                                        value={this.state.cost}
                                        onChangeText={cost => this.setState({ cost })}
                                    />
                                </Item>

                                <Item
                                    style={styles.item}
                                    rounded
                                    last
                                >
                                    <Image
                                        style={styles.place}
                                        source={require('../../assets/money.png')}
                                    />
                                    <TextInputMask
                                        type={'money'}
                                        style={styles.input3}
                                        options={{
                                            separator: '.',
                                            delimiter: ',',
                                            unit: '',
                                        }}
                                        placeholderTextColor="#ffffff80"
                                        value={this.state.price}
                                        onChangeText={price => this.setState({ price })}
                                    />


                                </Item>
                                <Item
                                    style={styles.item}
                                    rounded
                                    last
                                >
                                    <Image
                                        style={styles.place}
                                        source={require('../../assets/cost.png')}
                                    />
                                    <Input
                                        style={styles.input}
                                        keyboardType={"numeric"}
                                        placeholder="*Qty"
                                        placeholderTextColor="#ffffff80"
                                        autoCapitalize="none"
                                        value={this.state.qty}
                                        onChangeText={qty => this.setState({ qty })}
                                    />


                                </Item>

                        
                                <View style={{
                                    justifyContent: 'center',
                                    alignItems: 'center',

                                }}>
                                    <TouchableOpacity
                                        style={[styles.buttonContainerProcess, styles.ProcessButton]}
                                        onPress={() => this.Validation()}
                                    >
                                        <Text style={styles.ProcessText}>Submit</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>


                        </Modal>

                        <View style={styles.viewStyle}>
                           
                            <Item
                                style={styles.item}
                                rounded
                                last
                            >
                                <Image
                                    style={styles.place}
                                    source={require('../../assets/search.png')}
                                />

                                <Input
                                    style={styles.input}
                                    placeholder="Search"
                                    placeholderTextColor="#ffffff80"
                                    autoCapitalize="none"
                                    onChangeText={text => this.search(text)}
                                />


                            </Item>
                        </View>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Spinner
                                isVisible={this.state.spinner}
                                type={'ThreeBounce'}
                                textStyle={{ color: 'white' }}
                                size={70}
                                color={Colors.FLAT_TEXT_COLOR}
                            />

                        </View>
                        {this.nodataImage()}
                        <FlatGrid
                            itemDimension={130}
                            items={this.state.arrayholder}
                            style={styles.gridView}
                            renderItem={({ item, index }) => (
                                // onPress={() => this.onPressAlert()}
                                <TouchableOpacity activeOpacity={0.8} style={styles.touchableOpacityStyle} onPress={() => this.editSelectedItem(item)} >
                                    <View style={[styles.itemContainer, { backgroundColor: Colors.BACKGROUD_WHITE }]}>

                                        <Text style={styles.itemName3}>LKR.<Text style={styles.itemName2}>{item.price}</Text></Text>

                                        <Image
                                            name={item.name}
                                            style={styles.iconlImage}
                                            color={item.color}
                                            source={{ uri: item.imageUrl }}
                                        />

                                        <Text style={styles.itemName}>{item.name}</Text>



                                    </View>
                                </TouchableOpacity>
                            )}
                        />
                        {this.state.showTheThing &&
                            <Fab
                                active={this.state.active}
                                direction="up"
                                containerStyle={{}}
                                style={styles.backGround}
                                position="bottomRight"
                                onPress={() => this.anima()}>
                                <Image source={require("../../assets/plus.png")} style={styles.ToucherbleIconStyle3} />
                            </Fab>
                        }
                </ImageBackground>
            </View >
                );
    }
}
