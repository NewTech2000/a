import {
    PixelRatio,
    StyleSheet,
  } from 'react-native';
  import Colors from '../../resources/Colors';
  import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
  
    },
    content: {
      flex: 1,
      justifyContent: 'flex-end',
      padding: PixelRatio.getPixelSizeForLayoutSize(12),
      top:280
   
    },
    logoBack: {
      justifyContent: 'center',
      alignItems: 'center',
      height: 150,
      width: 150,
      top: -30,
      borderRadius: 100,
      backgroundColor: '#ffffff90'
    },
  
  
    gridView: {
      flex: 1,
    },
    itemContainer: {
      borderRadius: 20,
      padding: 10,
      height: hp('10%'),
      width: wp('90%'),
      marginTop: 6,
      shadowColor: Colors.DARK_BLACK_TEXT_COLOR,
      shadowOpacity: 0.8,
      elevation: 7,
      shadowRadius: 30,
      shadowOffset: { width: 1, height: 13 }
  
    },
    button: {
      marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
      backgroundColor: Colors.PRIMARY_COLOR,
    },
    button2: {
      borderBottomLeftRadius: 15,
      borderTopLeftRadius: 15,
      backgroundColor: Colors.PRIMARY_COLOR,
    },
    button3: {
      borderBottomRightRadius: 15,
      borderTopRightRadius: 15,
      backgroundColor: Colors.PRIMARY_COLOR,
    },
    buttonContainer1: {
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'row',
      top: 20
  
  
    },
    button1: {
      position: 'absolute',
      top: 95,
      padding: 10,
    },
    loginText: {
      color: '#ffff',
      fontSize: 18,
      fontWeight: 'bold',
    },
    caption: {
      fontSize: 20,
      fontWeight: 'bold',
      alignItems: 'center',
    },
  
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
    itemName: {
      fontSize: 15,
      color: '#fff',
      fontWeight: 'bold',
    },
    itemName2: {
      fontSize: 20,
      color: '#fff',
      fontWeight: 'bold',
    },
    itemName3: {
    
      backgroundColor:'gray',
      fontSize: 20,
      color: '#fff',
      fontWeight: 'bold',
    },
    itemCode: {
      fontWeight: '600',
      fontSize: 12,
      color: '#fff',
    },
    MainContainer: {
      justifyContent: 'center',
      flex: 1,
      paddingTop: 30,
    },
    imageThumbnail: {
      justifyContent: 'center',
      alignItems: 'center',
      height: 100,
    },
    textInputStyle: {
      height: 40,
      borderWidth: 1,
      paddingLeft: 10,
      borderColor: Colors.PRIMARY_COLOR,
      backgroundColor: 'white',
    },
    viewStyle: {
      justifyContent: 'center',
      padding: 16,
    },
    ProcessText: {
      fontSize: 18,
      fontWeight: 'bold',
      justifyContent: 'center',
      alignItems: 'center',
      color: 'white'
    },
    ProcessButton: {
      backgroundColor: Colors.PRIMARY_COLOR
    },
    inputTextContainer: {
      left: 15
    },
    buttonContainerProcess: {
      height: 40,
      flexDirection: "row",
      justifyContent: "center",
      alignItems: "center",
      marginBottom: 30,
      marginTop: 20,
      width: 180,
      borderRadius: 10,
      shadowColor: Colors.DARK_BLACK_TEXT_COLOR,
      shadowOpacity: 0.8,
      elevation: 6,
      shadowRadius: 15,
      shadowOffset: { width: 1, height: 13 }
  
    },
    input: {
      color: 'white',
      left: 20
    },
    item: {
      marginVertical: PixelRatio.getPixelSizeForLayoutSize(2),
      backgroundColor: Colors.PRIMARY_COLOR,
      borderColor: Colors.PRIMARY_COLOR,
    },
    place: {
      width: 25,
      height: 25,
      left: 10
    },
    place1: {
      width: 30,
      height: 30,
  
    },
    totalTextStyle: {
      color: Colors.DARK_BLACK_TEXT_COLOR,
      fontSize: 20,
      fontWeight: 'bold',
      top: 10
    },
    form: {
      flex: 1,
    },
    txtColor:{
        fontSize:20,
        fontWeight:'bold',
        color:Colors.DARK_BLACK_TEXT_COLOR
    },
    TextInputStyle2: {
      height: 40,
      borderBottomColor: Colors.DARK_BLACK_TEXT_COLOR,
      width: 200,
      marginBottom: 10,
      borderBottomWidth: 1
    },
    backGround: {
      backgroundColor: Colors.PRIMARY_COLOR,
  
    },
  
    headerTextStyle: {
      justifyContent: 'center',
      alignItems: 'center',
      fontSize: 25,
      left: 70,
      color: 'white',
    },
    header: {
      justifyContent: 'center',
      alignItems: 'center'
    },
    headerStyle: {
      backgroundColor: Colors.PRIMARY_COLOR,
      shadowOpacity: 0.75,
      shadowRadius: 5,
      shadowOffset: { height: 0, width: 0 },
  
    },
    ToucherbleIconStyle: { color: Colors.PRIMARY_COLOR },
    ToucherbleIconStyle2: { height: 30, width: 30 },
    ToucherbleIconStyle4: { height: 40, width: 40, left: 135 },
    ToucherbleIconStyle3: { height: 20, width: 20 },
  
    container1: { flex: 1, padding: 16, paddingTop: 30, },
    head: { height: 40, backgroundColor: Colors.PRIMARY_COLOR },
    text: { margin: 6, color: 'white' },
    row: { flexDirection: 'row', backgroundColor: Colors.FLAT_TEXT_COLOR },
    btn: { width: 58, height: 18, },
    btnText: { textAlign: 'center', color: '#fff' },
    buttonContainer: {
      flex: 1,
      marginTop: PixelRatio.getPixelSizeForLayoutSize(10),
    },
    container2: {
      backgroundColor: '#F5FCFF',
    },
    calButton: {
      backgroundColor: Colors.PRIMARY_COLOR
    },
    textInput: {
      height: 34,
      fontSize: 22,
      fontWeight: 'bold'
  
    }
  
  });
  
  
  export default styles;