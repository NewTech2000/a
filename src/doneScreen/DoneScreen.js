import React, { Component } from 'react';
import { View, ImageBackground, StatusBar, Animated,Dimensions } from 'react-native';
import {
    Button,
    Content,
    Text,
} from 'native-base';
import LottieView from 'lottie-react-native';
import Colors from '../../resources/Colors';
import styles from './DoneScreenStyle';
import BackgroundTimer from 'react-native-background-timer';
import { NavigationActions, StackActions } from 'react-navigation'

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

export default class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            text: '',
            total: '',
            balance: '',
            subTotal: '',
            discount: '',
            sub: '',
            grand: '',
            dis: '',
            progress: new Animated.Value(0),
            amount: ''

        }
    }



    financial(x) {
        return Number.parseFloat(x).toFixed(2);
    }


    componentDidMount() {
        this.state.total = this.props.navigation.getParam('total', 'nothing sent')
        this.state.balance = this.props.navigation.getParam('balance', 'nothing sent')


        this.setState({
            total: this.state.total,
            balance: this.state.balance
        })

        Animated.timing(this.state.progress, {
            toValue: 1,
            duration: 3000,
            //   easing: Easing.linear,
        }).start();

        this.test()
    }

    navigatetoHome() {
        const resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName: 'HomeScreen' })],

        });
        this.props.navigation.dispatch(resetAction);
    }



    test() {

        BackgroundTimer.setTimeout(() => {
            const resetAction = StackActions.reset({
                index: 0,
                actions: [
                    NavigationActions.navigate({ routeName: 'HomeScreen' })],

            });

            this.props.navigation.dispatch(resetAction);


        }, 3000);

    }
 
    render() {

        return (

            <ImageBackground
                source={require("../../assets/newBack2.png")}
                style={{ width: "100%", height: "100%" }}>
                <StatusBar barStyle="light-content" hidden={false} backgroundColor={Colors.PRIMARY_COLOR} />
                <LottieView source={require('./lf30_editor_JGnTJP.json')} progress={this.state.progress} style={{ top: -200 }} />

                <View style={{ justifyContent: 'center', alignItems: 'center', top: 300 }}>
                    <Text style={styles.txtColor}>Total</Text>

                    <Text style={styles.txtColor}>LKR    {this.financial(this.state.total)}</Text>

                    <Text> ──────────────── </Text>

                    <Text style={styles.txtColor}>Balance</Text>

                    <Text style={styles.txtColor}>LKR    {this.financial(this.state.balance)}</Text>

                </View>
                <Content contentContainerStyle={styles.content}>
                    <View style={styles.buttonContainer}>
                        <Button
                            style={styles.button}
                            onPress={() => this.navigatetoHome()}
                            hasText
                            block
                            large
                            dark
                            rounded
                        >
                            <Text style={styles.loginText}>New Order</Text>
                        </Button>
                    </View>
                </Content>
            </ImageBackground>

        );
    }


}



