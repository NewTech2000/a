export default {

    LOG_OUT: "Logged out succesfully",

    // BASE_URL:"https://billa.commercialtp.com",

    // BASE_URL:"http://128.199.198.129:8804",

    // BASE_URL: "http://167.86.79.17:8804",

    BASE_URL: "http://18.191.174.212:8804",//AWS

    // Image_URL:"https://billafiles.commercialtp.com",

    // Image_URL:"http://128.199.198.129:8805",

    // Image_URL: "http://167.86.79.17:8805",

    Image_URL: "http://18.191.174.212:8804", //AWS

    LOGIN_SUCCESS: "Logged in succesfully",

    PRINTER_ERROE: "There is something wrong with printer connection",

    USER_NAME_EMPTY: "Username field is empty!",

    PASSWORD_EMPTY: "Password field is empty!",

    CONFIRMPASSWORD_EMPTY: "You must enter correct password!",

    ITEM_ADDED_SUCCESS: "Item Added Succesfully..!",

    ADDED_NEW_BATCH_SUCCESS: "Batch added succesfully",

    ADDED_NEW_BATCH_FAIL: "Batch added fail",

    PROFILE_UPDATE_SUCCESS: "Profile update success",

    PROFILE_UPDATE_FAIL: "Profile update fail",

    CHECK_NETWORK_CONNECTION: "Please Check your Network Connection !",

    IMAGE_FAILED: "Incompatible image type!",

    INTERNAL_ERROR: "Internal server error!",

    WARNING_CONNECTION: "Connection Failed",

    ITEM_UPDATE_SUCCESS: "Item Update Succesfully..!",

    ITEM_Update_FAIL: "Item Added Fail..!",

    INVALID_VALUE: "Invalid Value!",

    HIGH_VALUE: "value can't be higher than total!",

    WARNING_SUCESS: "Success",

    WARNING: 'Warning',

    WARNING_FAILED: "Fail",

    EMPTY_CURT: "Your cart is empty!",

    DISCOUNT_FAILD: "invalid disount try again ..!",

    CUSTOMER_ADDED_SUCCESS: "Customer Added Succesfully..!",
    
    INVOICE_NUMBER_SUCCESS:"Invoice number create succesfully",

    INVOICE_NUMBER_FAIL:"Invoice number create fail!",

    CUSTOMER_UPDATE_SUCCESS: "Customer Update Succesfully..!",

    CUSTOMER_UPDATE_FAIL: "Customer Update Fail..!",

    CUSTOMER_ADDED_Fail: "Customer Added Fail..!",

    BATCH_NUMBER_ADDED_FAIL: "Batch Number Added Fail..!",

    BATCH_COST_ADDED_FAIL: "Batch Cost Added Fail..!",

    BATCH_SALES_PRICE_ADDED_FAIL: "Batch Sales Price Added Fail..!",

    BATCH_QTY_ADDED_FAIL: "Batch Qty Added Fail..!",

    BATCH_NUMBER: "Please check the Batch Number",

    BATCH_COST: "Please check the Cost",

    BATCH_SALES_PRICE: "Please check the Sales price",

    BATCH_QTY: "Please check the qty",

    CATEGORY_ADDED_Fail: "Category added fail !",

    SUB_CATEGORY_ADDED_Fail: "Sub category added fail !",

    SELECT_CATEGORY: "Please select category!",

    CATEGORY_ADDED_SUCCESS: "Category added succesfully",

    SUB_CATEGORY_ADDED_SUCCESS: "Sub category added succesfully",

    SUB_CATEGORY_UPDATE_SUCCESS: "Sub category update succesfully",

    CATEGORY_UPDATE_SUCCESS: "Category update succesfully",

    SUBCATEGORY_DELETE_SUCCESS: "Subcategory Delete Succesfully..!",

    CATEGORY_DELETE_SUCCESS: "Category Delete Succesfully..!",

    USER_ADDED_SUCCESS: "User added succesfully",

    SUPPLIER_ADDED_SUCCESS:"Supplier added succesfully.",

    SUPPLIER_ADDED_FAIL:"Supplier added fail!.",

    SUPPLIER_NAME_EMPTY: "Supplier name field is empty!",

    SUPPLIER_ADDRESS_EMPTY: "Supplier address field is empty!",

    SUPPLIER_CONTACT_EMPTY: "Supplier contact number field is empty!",

    SUPPLIER_CONTACT_PERSON_EMPTY: "Supplier contactperson field is empty!",

    SUPPLIER_EMAIL_EMPTY: "Supplier email field is empty!",

    SUPPLIER_DESCRIPTION_EMPTY: "Supplier description field is empty!",

    USER_ADDED_FAIL: "User added fail!",

    ITEM_DELETE_SUCCESS: "Item Delete Succesfully..!",

    CUSTOMER_DELETE_SUCCESS: "Customer Delete Succesfully..!",

    ITEM_DELETE_FAIL: "Item Delete Fail..!",

    PLACE_ORDER_SUCCESS: "Succesfully Placed Order",

    SUPPLIER_INVOICE_SUCCESS: "Supplier invoice placed",

    CREDIT_BOOK_ADDED_SUCCESS:"Succesfully added to credit book",

    CREDIT_BOOK_ADDED_FAIL:"Fail to add credit book!",
    
    PLACE_ORDER_FAILED: "Failed Place Order..!",

    CUSTOMER_PAYMENT_SUCCESS: "Payment Success!",

    CUSTOMER_PAYMENT: "Payment fail!",

    CUSTOMER_NAME: "Customer name added fail!",

    CUSTOMER_CONTACT: "Customer contact added fail!",

    CUSTOMER_NIC: "Customer NIC added fail!",

    CUSTOMER_EMAIL: "Customer email added fail!",

    CUSTOMER_ADDRESS: "Customer address added fail!",

    LOWER_AMOUNT: "The Amount Received can't be lower than the amount sold!",

    ITEM_ADDED_FAIL: "Item Added Failed..!",

    SUPPLIER_ADDED_FAIL: " Supplier Added Failed..!",

    SUPPLIER_ITEM_ADDED_FAIL: "Warning",
    
    SUPPLIER_ITEM_NAME: " Supplier product Name field is empty!",

    SUPPLIER_ITEM_BATCHNUM: " Supplier product BatchNo field is empty!",

    SUPPLIER_ITEM_PRICE: " Supplier product Price field is empty!",

    SUPPLIER_ITEM_SALEPRICE: " Supplier product Saleprice field is empty!",

    SUPPLIER_ITEM_COST: " Supplier product Cost field is empty!",

    SUPPLIER_ITEM_QTY: " Supplier product Qty field is empty!",

    BANK_ADD_FAIL: "Bank name field is wrong ",

    CHECKNUM_ADD_FAIL: "CheckNumber field is wrong ",

    CARDNUM_ADD_FAIL: "Invalid card number!",

    INVALID_CARD_NUMBER: "Invalid CArd Number!",

    CHECKAMOUNT_ADD_FAIL: "CheckAmount field is wrong ",

    CARDAMOUNT_ADD_FAIL: "CardAmount field is wrong ",

    CHECKEXPIREDATE_ADD_FAIL: "Check expire date field is wrong ",

    CHECKDESCRIP_ADD_FAIL: "Check description field is wrong ",

    CARDDESCRIP_ADD_FAIL: "Card description field is wrong ",

    LONG_TAP: "Please long tap to edit and remove item",

    LONG_TAP2: "Please long tap to show more details!",

    REGISTER_SUCCESS: "Registration reauest sent Succesfully",

    DOMAIN_EMPTY: "Ex :- username@companyname.lk",

    DOMAIN_WARNING: "Invalid domain",

    NIC_WARNING: "Invalid NIC Number or empty folder",

    ADDRESS_EMPTY: "Address Field is empty!",

    IMAGE_EMPTY: "Please Select Profile Image!",

    NAME_EMPTY: "You must enter your name",

    INVOICE_NUMBER: "Check Next Invoice Number",

    PREFIX: "Check the Prefix Number",

    DESCRIPTION_EMPTY: "Name field is empty!",

    CATEGORY_EMPTY: "Category name is empty",

    SUB_CATEGORY_EMPTY: "Subcategory name is empty",

    SELECT_CATEGORY: "Please select the category first",

    PRICE_EMPTY: "Price is empty!",

    COMPANY_NAME_EMPTY: "Company name field is empty!",

    ITEM_NAME_EMPTY: "item name field is empty!",

    ITEM_BATCH_NUMBER: "item batch number field is empty!",

    ITEM_CATEGORY_EMPTY: "item category field is empty!",

    ITEM_QTY_EMPTY: "item Qty field is empty!",

    ITEM_PRICE_EMPTY: "item price field is empty!",

    ITEM_SALEPRICE_EMPTY: "item saleprice field is empty!",

    ITEM_COST_EMPTY: "item cost field is empty!",

    ITEM_IMAGE_EMPTY: "item photo is empty!",

    ITEM_TYPE_EMPTY: "item type field is empty!",

    BRANCH_NAME_EMPTY: "Branch name field is empty!",

    MOBILE_EMPTY: "You must enter your mobile number correctly!",

    USER_DID_NOT_MATCH: "Username or password dit not match !",

    USER_NOT_ACTIVE: "User not activated !",

    TEMPERY_USER_DEACTIVE: "Temporarily User Deactiveted ..!",

    USER_NOT_FOUNd: "User not found!",

    USER_NAME_ALRDY_TEAKE: "Opss.. User Name Already Taken",

    INVALID_DOMAIN: "Opss.. Invalid Domain In Username!",

    REGISTER_FAILED: "Register Failed..!",



    ICON: ["warning", "success", "danger", "default", "none"],

    TYPE: ["warning", "success", "danger", "default", "none"],

    color: ["red"]

};